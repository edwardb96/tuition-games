declare module 'react-dice-complete' {
  import { Component } from "react";

  export interface ReactDiceProps {
    // Callback called with total & individual values from dice roll
    rollDone?: (num: number, a: any) => void;
    // The number of dice you wish to have, defaults to 4
    numDice?: number;
    // The number you want displayed before a roll, defaults to 6
    defaultRoll?: number;
    // Show a 1px outline for each face of the die, defaults to false
    outline?: boolean;
    // Hex color code for outline color if outline is true, defaults to black
    outlineColor?: string;
    // Margin between each die, defaults to 15
    margin?: number;
    // Hex color code for the face of the die, defaults to #ff00ac
    faceColor?: string;
    // Hex color code for the dots on the die, defaults to #1eff00
    dotColor?: string;
    // Width/Height of each dice face, defaults to 60px
    dieSize?: number;
    // Time in seconds for the roll animation, defaults to 2
    rollTime?: number;
    // Disable clicks on die to roll each individually, defaults to false
    disableIndividual?: boolean;
  }

  class ReactDice extends Component<ReactDiceProps, any> {}

  export default ReactDice;
}
declare module 'sorted-array-functions' {
  type ComparisonFunction<T> = (a: T, b: T) => number;

  function add<T>(list: T[], value: T, compare?: ComparisonFunction<T>): void;
  function has<T>(list: T[], value: T, compare?: ComparisonFunction<T>): boolean;
  function remove<T>(list: T[], value: T, compare?: ComparisonFunction<T>): boolean;
  function eq<T>(list: T[], value: T, compare?: ComparisonFunction<T>): number;
  function gte<T>(list: T[], value: T, compare?: ComparisonFunction<T>): number;
  function gt<T>(list: T[], value: T, compare?: ComparisonFunction<T>): number;
  function lte<T>(list: T[], value: T, compare?: ComparisonFunction<T>): number;
  function lt<T>(list: T[], value: T, compare?: ComparisonFunction<T>): number;
}
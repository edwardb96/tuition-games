import { add as insort, remove as remsort } from "sorted-array-functions";
export { insort, remsort }

type Args = readonly unknown[];

export const removeAt = <T>(array: T[], index: number, itemsToRemove: number = 1) =>
  { array.splice(index, itemsToRemove); };
export const replaceAt = <T>(array: T[], index: number, value: T) =>
  { array[index] = value };
export const updateAt = <T>(array: T[], index: number, update: (previous: T) => T) =>
  { array[index] = update(array[index]) };
export const insertAt = <T>(array: T[], index: number, value: T) =>
  { array.splice(index, 0, value) }
export const push = <T>(array: T[], value: T) =>
  { array.push(value) };

export const last = <T>(array: T[]): T | null => {
  if (array.length > 0) {
    return array[array.length - 1];
  } else {
    return null;
  }
}

type ArrayMutator<T, A extends Args, R = void> = (array: T[], ...args: A) => R;
type ArrayTransform<T, A extends Args> = (array: T[], ...args: A) => T[];
type ArrayTransformWithResult<T, A extends Args, R = void> = (array: T[], ...args: A) => [R, T[]];

export function purify<T, A extends Args>(mutateArray: ArrayMutator<T, A>): ArrayTransform<T, A> {
  return (array, ...args) => {
    const newArray = array.slice();
    mutateArray(newArray, ...args);
    return newArray;
  }
}

export function purifyWithResult<T, A extends Args, R>(mutateArray: ArrayMutator<T, A, R>): ArrayTransformWithResult<T, A, R> {
  return (array, ...args) => {
    const newArray = array.slice();
    const result = mutateArray(newArray, ...args);
    return [result, newArray];
  }
}

export const removedAt = purify(removeAt);
export const replacedAt = purify(replaceAt);
export const updatedAt = purify(updateAt);
export const insertedAt = purify(insertAt);
export const insorted = purify(insort);
export const remsorted = purifyWithResult(remsort);
export const pushed = purify(push);
import React, { ChangeEvent } from "react";
import { FieldProps } from "formik";

export interface CheckboxItemProps {
  value: any;
  index?: any;
  label: string;
}

export const CheckboxItem = ({ field, form, label, index, value } : FieldProps & CheckboxItemProps) => {
  const { name, value: formikValue } = field;
  const { setFieldValue, setFieldTouched } = form;

  const valuesIsList = index == null;
  const values = valuesIsList ? formikValue || [] : formikValue || {};
  const indexIfPresent = (() => {
    if (valuesIsList) {
      const listIndex = values.indexOf(value);
      return listIndex === -1 ? null : listIndex;
    } else {
      return values.hasOwnProperty(index) ? index : null;
    }
  })();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = (() => {
      if (valuesIsList) {
        if (indexIfPresent != null) {
          values.splice(indexIfPresent, 1);
        } else {
          values.push(value);
        }
      } else {
        if (indexIfPresent != null) {
          delete values[index]
        } else {
          values[index] = value
        }
      }
      return values;
    })();

    setFieldTouched(name, true, false);
    setFieldValue(name, newValue);
  };

  return (
    <label className="checkbox">
      <input
        type="checkbox"
        onChange={handleChange}
        checked={indexIfPresent != null}/>
      <span>{label}</span>
    </label>
  );
};
"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.test = exports.SortedArray = void 0;
var compare_1 = require("@cat-tuition/protocol/compare");
var prelude_ts_1 = require("prelude-ts");
var array_functions_1 = require("./array-functions");
var sorted_array_functions_1 = require("sorted-array-functions");
var SortedArray = /** @class */ (function () {
    function SortedArray(sortedArray, compare) {
        this.storage = sortedArray;
        this.compare = compare;
    }
    SortedArray.prototype[Symbol.iterator] = function () {
        return this.storage[Symbol.iterator]();
    };
    SortedArray.prototype.getUnchecked = function (index) {
        return this.storage[index];
    };
    SortedArray.prototype.get = function (index) {
        if (0 <= index && index < this.storage.length) {
            return prelude_ts_1.Option.of(this.getUnchecked(index));
        }
        else {
            return prelude_ts_1.Option.none();
        }
    };
    SortedArray.prototype.last = function () {
        if (this.storage.length > 0) {
            return prelude_ts_1.Option.of(this.getUnchecked(0));
        }
        else {
            return prelude_ts_1.Option.none();
        }
    };
    SortedArray.prototype.contains = function (value) {
        return sorted_array_functions_1.has(this.storage, value, this.compare);
    };
    SortedArray.prototype.add = function (value) {
        return new SortedArray(array_functions_1.insorted(this.storage, value, this.compare), this.compare);
    };
    SortedArray.prototype.indexOf = function (value) {
        return sorted_array_functions_1.eq(this.storage, value, this.compare);
    };
    SortedArray.prototype.removeAt = function (index) {
        return new SortedArray(array_functions_1.removedAt(this.storage, index), this.compare);
    };
    SortedArray.prototype.updateAt = function (index, update) {
        return new SortedArray(array_functions_1.updatedAt(this.storage, index, update), this.compare);
    };
    SortedArray.prototype.replaceReinsert = function (index, value) {
        var _this = this;
        return new SortedArray(array_functions_1.purify(function (array) {
            array_functions_1.removeAt(array, index);
            array_functions_1.insort(array, value, _this.compare);
        })(this.storage), this.compare);
    };
    SortedArray.prototype.replaceAt = function (index, value) {
        if ((index == 0 || this.compare(this.getUnchecked(index - 1), value) <= 0)
            && (index == this.storage.length - 1 || this.compare(value, this.getUnchecked(index + 1)) >= 0)) {
            return new SortedArray(array_functions_1.replacedAt(this.storage, index, value), this.compare);
        }
        else {
            throw new Error("Tried to replace element " + this.storage[index] + " at index " + index + " element " + value + " this would result in an out of order list.");
        }
    };
    SortedArray.prototype.removeOnce = function (value) {
        var _a = array_functions_1.remsorted(this.storage, value, this.compare), contained = _a[0], withoutValue = _a[1];
        return [contained, new SortedArray(withoutValue, this.compare)];
    };
    SortedArray.prototype.removeAll = function (value) {
        var beforeIndex = sorted_array_functions_1.lt(this.storage, value, this.compare);
        if (beforeIndex >= 0) {
            var itemsToRemove = 1;
            while (itemsToRemove < this.storage.length &&
                this.compare(value, this.storage[beforeIndex + itemsToRemove]) == 0) {
                ++itemsToRemove;
            }
            return [true, new SortedArray(array_functions_1.removedAt(this.storage, beforeIndex + 1, itemsToRemove), this.compare)];
        }
        else {
            return [false, this];
        }
    };
    SortedArray.empty = function (compare) {
        if (compare === void 0) { compare = compare_1.compareValues; }
        return new SortedArray([], compare);
    };
    SortedArray.fromSorted = function (sortedIterable, compare) {
        if (compare === void 0) { compare = compare_1.compareValues; }
        return new SortedArray(__spreadArrays(sortedIterable), compare);
    };
    SortedArray.fromUnsorted = function (iterable, compare) {
        if (compare === void 0) { compare = compare_1.compareValues; }
        var items = __spreadArrays(iterable);
        items.sort(compare);
        return new SortedArray(items, compare);
    };
    return SortedArray;
}());
exports.SortedArray = SortedArray;
function test() {
    var array = SortedArray.fromSorted(['A', 'B', 'C']);
    array.replaceReinsert(2, 'D');
    console.log(__spreadArrays(array));
}
exports.test = test;

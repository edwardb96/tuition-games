import React from "react";
import { Field } from "formik";
import map from "iter-tools/methods/map";
import objectKeys from "iter-tools/methods/object-keys";
import "./colour-palette-picker.scss";

export type Palette = Record<string, string>;

export interface ColourPalettePickerProps {
  id: string;
  name: string;
  palette?: Palette;
}

export const simpleColourPalette : Palette = {
  white: '#FFFFFF',
  red: '#DB2828',
  orange: '#F2711C',
  yellow: '#FBBD08',
  green: '#21BA45',
  blue: '#2185D0',
  violet: '#6435C9',
  pink: '#E03997'
}

export const ColourPalettePicker = ({ id, name, palette = simpleColourPalette } : ColourPalettePickerProps) => {
  return (
    <div className="control colour-picker" role="group">
      {map(colour =>
        <React.Fragment key={colour}>
          <Field id={`${id}-${colour}`} className="colour-radio-input" type="radio" name={name} value={colour} />
          <label htmlFor={`${id}-${colour}`} className="colour-radio-label"><span className={`is-${colour}-coloured`}></span></label>
        </React.Fragment>, objectKeys(palette))}
    </div>
  );
};
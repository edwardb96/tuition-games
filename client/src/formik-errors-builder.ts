import { FormikErrors } from "formik";

export class FormikErrorsBuilder<T> {
  errors?: FormikErrors<T>;

  constructor(errors?: FormikErrors<T>) {
    this.errors = errors;
  }

  static build<T>(): FormikErrorsBuilder<T> {
    return new FormikErrorsBuilder();
  }

  add(id: keyof T, value: unknown): FormikErrorsBuilder<T> {
    if (value != null) {
      return new FormikErrorsBuilder({ ...this.errors, [id]: value });
    } else {
      return new FormikErrorsBuilder(this.errors);
    }
  }

  get(): FormikErrors<T> | undefined {
    return this.errors;
  }
}
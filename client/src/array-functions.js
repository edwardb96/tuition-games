"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.pushed = exports.remsorted = exports.insorted = exports.insertedAt = exports.updatedAt = exports.replacedAt = exports.removedAt = exports.purifyWithResult = exports.purify = exports.last = exports.push = exports.insertAt = exports.updateAt = exports.replaceAt = exports.removeAt = exports.remsort = exports.insort = void 0;
var sorted_array_functions_1 = require("sorted-array-functions");
exports.insort = sorted_array_functions_1.add;
exports.remsort = sorted_array_functions_1.remove;
var removeAt = function (array, index, itemsToRemove) {
    if (itemsToRemove === void 0) { itemsToRemove = 1; }
    array.slice(index, itemsToRemove);
};
exports.removeAt = removeAt;
var replaceAt = function (array, index, value) { array[index] = value; };
exports.replaceAt = replaceAt;
var updateAt = function (array, index, update) { array[index] = update(array[index]); };
exports.updateAt = updateAt;
var insertAt = function (array, index, value) { array.splice(index, 0, value); };
exports.insertAt = insertAt;
var push = function (array, value) { array.push(value); };
exports.push = push;
var last = function (array) {
    if (array.length > 0) {
        return array[array.length - 1];
    }
    else {
        return null;
    }
};
exports.last = last;
function purify(mutateArray) {
    return function (array) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var newArray = array.slice();
        mutateArray.apply(void 0, __spreadArrays([newArray], args));
        return newArray;
    };
}
exports.purify = purify;
function purifyWithResult(mutateArray) {
    return function (array) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var newArray = array.slice();
        var result = mutateArray.apply(void 0, __spreadArrays([newArray], args));
        return [result, newArray];
    };
}
exports.purifyWithResult = purifyWithResult;
exports.removedAt = purify(exports.removeAt);
exports.replacedAt = purify(exports.replaceAt);
exports.updatedAt = purify(exports.updateAt);
exports.insertedAt = purify(exports.insertAt);
exports.insorted = purify(sorted_array_functions_1.add);
exports.remsorted = purifyWithResult(sorted_array_functions_1.remove);
exports.pushed = purify(exports.push);

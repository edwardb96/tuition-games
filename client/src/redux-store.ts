import { ClassroomState } from "./classroom/reducer";
import { LobbyState } from "./lobby/lobby";
import { ProfileState } from "./navbar/reducer";
import { UsersState } from "./users/state";
import { ImagesState } from "./image-picker/state";
import { BoardsState } from "./boards/state";
import { State as NoughtsAndCrossesState } from "./classroom/activities/noughts-and-crosses/state";
import { State as PairsState } from "./classroom/activities/pairs/state";
import { State as BoardGameState } from "./classroom/activities/board-game/state";
import { CardPairsState } from "./card-pairs/state";

export interface AppState {
  noughtsAndCrosses: NoughtsAndCrossesState;
  pairs: PairsState;
  boards: BoardsState;
  profile: ProfileState;
  lobby: LobbyState;
  classroom: ClassroomState;
  users: UsersState;
  images: ImagesState;
  cardPairs: CardPairsState;
  boardGame: BoardGameState;
}
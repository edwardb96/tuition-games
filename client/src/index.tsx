import "./theme.scss";
import "@fortawesome/fontawesome-free/css/all.css";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux"
import { createStore, applyMiddleware, combineReducers, Store } from "redux"

import ClassroomPage from "./classroom/views/classroom-page";
import LobbyPage from "./lobby/views/lobby";
import { NotFoundPage } from "./error-pages/views/not-found";
import { UnverifiedEmailPage } from "./error-pages/views/unverified-email";
import { LoggedOutPage } from "./error-pages/views/logged-out";
import UsersPage from "./users/views/users-page";
import CardsPage from "./card-pairs/views/card-page";
import BoardsPage from "./boards/views/board-page";

import { reducer as pairs } from "./classroom/activities/pairs/reducer";
import { reducer as noughtsAndCrosses } from "./classroom/activities/noughts-and-crosses/reducer";
import { reducer as boardGame } from "./classroom/activities/board-game/reducer";

import { reducer as profile } from "./navbar/reducer";
import { reducer as lobby } from "./lobby/reducer";
import { reducer as classroom } from "./classroom/reducer";
import { reducer as users } from "./users/reducer";
import { reducer as images } from "./image-picker/reducer";
import { reducer as cardPairs } from "./card-pairs/reducer";
import { reducer as boards } from "./boards/reducer";

import { NoughtsAndCrossesChannel } from "./classroom/activities/noughts-and-crosses/channel";
import { PairsChannel } from "./classroom/activities/pairs/channel";
import { LobbyChannel } from "./lobby/channel";
import { ClassroomChannel } from "./classroom/channel";
import { LOBBY_CHANNEL }               from "@cat-tuition/protocol/channels/lobby";
import { CLASSROOM_CHANNEL }           from "@cat-tuition/protocol/channels/classroom";
import { NOUGHTS_AND_CROSSES_CHANNEL } from "@cat-tuition/protocol/channels/noughts-and-crosses";
import { PAIRS_CHANNEL }               from "@cat-tuition/protocol/channels/pairs";

import reduxWebsocket from "@giantmachines/redux-websocket";
import thunk from "redux-thunk";
import { Redirect, Route, Switch } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { Channel, multiplexChannels } from "./redux-utils";
import { AppState } from "./redux-store";
import { PING_CHANNEL } from "@cat-tuition/protocol/channels/ping";
import { PingChannel } from "./navbar/ping/channel";
import { BOARD_GAME_CHANNEL } from "@cat-tuition/protocol/channels/board-game";
import { BoardGameChannel } from "./classroom/activities/board-game/channel";

let channels = new Map<string, Channel>();
channels.set(PING_CHANNEL, new PingChannel());
channels.set(LOBBY_CHANNEL, new LobbyChannel());
channels.set(CLASSROOM_CHANNEL, new ClassroomChannel());
channels.set(NOUGHTS_AND_CROSSES_CHANNEL, new NoughtsAndCrossesChannel());
channels.set(PAIRS_CHANNEL, new PairsChannel());
channels.set(BOARD_GAME_CHANNEL, new BoardGameChannel());
const websocketMiddleware = reduxWebsocket();

const channelsMiddleware = multiplexChannels(channels);
const reducer = combineReducers({
  profile,
  classroom,
  noughtsAndCrosses,
  pairs,
  boardGame,
  lobby,
  users,
  cardPairs,
  boards,
  images });

const middlewares = (() => {
  const productionMiddlewares = [websocketMiddleware, thunk, channelsMiddleware];
  if (process.env.NODE_ENV === 'development') {
    const { logger: reduxLogger } = require('redux-logger');
    console.log("Redux logger is enabled");
    return [...productionMiddlewares, reduxLogger]
  } else {
    console.log("Redux logger is enabled");
    return productionMiddlewares;
  }
})();

const middleware = applyMiddleware(...middlewares);
const store: Store<AppState> = createStore(reducer, middleware);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={LobbyPage}/>
        <Route exact path="/classroom/:classroomId" component={ClassroomPage}/>
        <Route exact path="/users" component={UsersPage}/>
        <Route exact path="/cards" component={CardsPage}/>
        <Route exact path="/boards" component={BoardsPage}/>
        <Route path="/404" component={NotFoundPage} />
        <Route path="/unverified-email" component={UnverifiedEmailPage} />
        <Route path="/splash" component={LoggedOutPage} />
        <Redirect from="*" to="/404" />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
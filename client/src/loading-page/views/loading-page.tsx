import React from "react";
import Navbar from "../../navbar/views/navbar";

import { ConnectionState, ConnectionStateTag } from "../../navbar/reducer";
import { AppState } from "../../redux-store";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { connect } from "react-redux";
import "./loading-page.scss";

export interface LoadingPageProps {
  connection: ConnectionState;
  message: string;
  shouldConnect: boolean;
}

export const LoadingPage = (props: LoadingPageProps) => {
  const { connection, message, shouldConnect } = props;

  const connectionState = (() => {
    if (shouldConnect && connection.tag == ConnectionStateTag.Disconnected) {
      return (
        <>
          <p className="title is-1">Disconnected :(</p>
          <p className="subtitle">You were disconnected with code {connection.code}{mapNullable(connection.reason, r => ` because: ${r}`)}</p>
        </>
      );
    } else {
      return (
        <>
          <p className="title is-1">{message}</p>
          <div className="loader-wrapper">
            <div className="loader is-loading" />
          </div>
        </>
      );
    }
  })();

  return (
    <>
      <Navbar shouldConnect={shouldConnect}/>
      <div className="loading-page hero-body container">
        {connectionState}
      </div>
    </>
  );
};

const mapState = ({ profile }: AppState) => ({ connection: profile.connection });
export default connect(mapState, {})(LoadingPage);
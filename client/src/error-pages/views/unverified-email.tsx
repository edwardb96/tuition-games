import { connect } from "react-redux";
import React from "react";
import Navbar from "../../navbar/views/navbar";
import { AppState } from "../../redux-store";
import { User } from "@cat-tuition/protocol/user";
import { useLocation } from "react-router";

export interface UnverifiedEmailProps {
}

export const UnverifiedEmailPage = (props: UnverifiedEmailProps) => {
  const email = new URLSearchParams(useLocation().search).get('email');
  return (
    <>
      <Navbar shouldConnect={false}/>
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              You have not yet verified your email address {email || ''}
            </h1>
            <h2 className="subtitle">
              Please click the <em>Verify your account</em> button in the email from Auth0
              and log out and back in again to continue.
            </h2>
            <a href="/logout" className="button is-large is-primary is-light">Click Here to Logout</a>
          </div>
        </div>
      </section>
    </>
  );
}
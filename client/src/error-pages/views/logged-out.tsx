import { connect } from "react-redux";
import React from "react";
import Navbar from "../../navbar/views/navbar";

export interface LoggedOutProps {
}

export const LoggedOutPage = (props: LoggedOutProps) => {
  return (
    <>
      <Navbar shouldConnect={false} shouldFetchUser={false}/>
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              You have just logged out
            </h1>
            <h2 className="subtitle">
              Click the link below if you would like to re-login
            </h2>
            <a href="/login" className="button is-large is-primary is-light">Click Here to Login</a>
          </div>
        </div>
      </section>
    </>
  );
}
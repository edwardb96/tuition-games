import React from "react";
import Navbar from "../../navbar/views/navbar";

export interface NotFoundProps { }

export const NotFoundPage = (props: NotFoundProps) => {
  return (
    <>
      <Navbar shouldConnect={false}/>
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              This Page Does Not Exist
            </h1>
            <h2 className="subtitle">
              404 Not Found
            </h2>
          </div>
        </div>
      </section>
    </>
  );
}
import React, { MouseEvent, useEffect, useState } from "react";
import LoadingPage from "../../loading-page/views/loading-page";
import Navbar from "../../navbar/views/navbar";
import BoardEditWidget from "./board-edit-widget";

import { BoardCategory, Board, NewBoard, boardCategorySchema } from "@cat-tuition/protocol/board-game";
import { User } from "@cat-tuition/protocol/user";
import { HashSet, Option } from "prelude-ts";
import { Redirect } from "react-router";
import { AppState } from "../../redux-store";
import { connect } from "react-redux";
import './board-page.scss';
import { asOption, shouldReretrieve } from "../../retrieval";
import { BoardsState } from "../state";
import { retrieveBoards, jumpedToBoard } from "../actions";
import map from "iter-tools/methods/map";
import filter from "iter-tools/methods/filter";
import drop from "iter-tools/methods/drop";
import take from "iter-tools/methods/take";
import enumerate from "iter-tools/methods/enumerate";
import includesAny from "iter-tools/methods/includes-any";
import { eq } from "sorted-array-functions";
import { compareValues } from "@cat-tuition/protocol/compare";
import { TagsInput } from "../../tags-input/tags-input";

export interface BoardsPageProps {
  user?: User;
  boards: BoardsState;
  retrieveBoards(): Promise<boolean>;
  jumpedToBoard(): void;
}

const pageSize = 3 * 3;

export interface BoardsPageState {
  selectedIndex: number | null;
  newBoardText: string | null;
  newBoardCategories: BoardCategory[] | null;
  query: string;
  categoriesFilter: BoardCategory[];
  pageStartIndex: number;
}

export interface BoardPreviewProps {
  board: Board;
  canClick?: boolean;
  onClick?: () => void;
  onDelete?: () => void;
  isSelected?: boolean;
}

export const BoardPreview = (props: BoardPreviewProps) => {
  const { board, isSelected, onDelete, onClick, canClick } = props;
  const { image, title, categories } = board;

  return (
    <a onClick={onClick} className="board column is-4">
      <article className="card">
        <figure className="card-image">
          <img src={`/api/images/${image}.png`}/>
        </figure>
        <div className="card-content">
          <p className="title is-4">{title}</p>
        </div>
      </article>
    </a>
  );
};

export const BoardsPage = (props: BoardsPageProps) => {
  const [state, setState] = useState<BoardsPageState>({
    query: '',
    newBoardText: null,
    newBoardCategories: null,
    selectedIndex: null,
    categoriesFilter: [],
    pageStartIndex: 0
  });

  const { user } = props;
  const { selectedIndex: selectedId, categoriesFilter, query, pageStartIndex } = state;

  const loadBoards = async () => {
    const { retrieveBoards, boards } = props;
    if (shouldReretrieve(boards, 5)) {
      await retrieveBoards();
    }
  };

  const { boards: boardsRetrieval, jumpedToBoard } = props;
  useEffect(() => {
    if (user != null && user.canManageBoards()) {
      loadBoards();
    }
  }, [user]);

  const maybeBoards = asOption(boardsRetrieval);

  type PageJump = { pageIndex: number, clearFilter: boolean }

  const retrievalInfo: Option<{
    filteredPairs: [number, Board][],
    jumpTo: Option<PageJump>
    categories: HashSet<string>
  }> = maybeBoards.map(({ boards, categories, jumpToPairIndex: indexInUnfiltered }) => {
    const meetsCategoryFilter = (board: NewBoard): boolean =>
      categoriesFilter.length == 0 || includesAny(categoriesFilter, board.categories)

    const meetsQueryFilter = (board: NewBoard): boolean =>
      board.title.includes(query);

    const meetsFilter = (pair: NewBoard): boolean =>
      meetsCategoryFilter(pair) && meetsQueryFilter(pair)

    const filteredBoards: [number, Board][] = [...filter(([_, board]) => meetsFilter(board), enumerate(boards))];
    const jumpTo: Option<PageJump> = (() => {
      if (indexInUnfiltered != null) {
        const compareFirst = ([a, _a]: [number, Board], [b, _b]: [number, Board]) => compareValues(a, b);
        if (filteredBoards.length > 0) {
          const indexInFiltered = eq(filteredBoards, [indexInUnfiltered, filteredBoards[0][1]], compareFirst);
          if (indexInFiltered != -1) {
            return Option.of<PageJump>({ pageIndex: Math.floor(indexInFiltered / pageSize), clearFilter: false });
          } else {
            return Option.of<PageJump>({ pageIndex: Math.floor(indexInUnfiltered / pageSize), clearFilter: true });
          }
        }
      }
      return Option.none<PageJump>()
    })();

    return { filteredPairs: filteredBoards, jumpTo, categories };
  });

  const hasJump = retrievalInfo.map(({ jumpTo }) => jumpTo.isSome()).getOrElse(false);

  useEffect(() => {
    retrievalInfo.ifSome(({ jumpTo }) =>
      jumpTo.ifSome(({ pageIndex, clearFilter }: PageJump) => {
        const pageStartIndex = pageIndex * pageSize;
        if (clearFilter) {
          setState({ ...state, pageStartIndex, query: '', categoriesFilter: [] });
        } else {
          setState({ ...state, pageStartIndex });
        }
        jumpedToBoard();
      }));
  }, [user, hasJump]);

  if (user != null) {
    if (user.canManageBoards()) {
      return retrievalInfo.match({
        Some: ({ filteredPairs, categories }) => {
          const pairsOnCurrentPageAndImediatelyAfter = [
            ...take(pageSize + 1, drop(pageStartIndex, filteredPairs))];

          const boardsOnCurrentPage = take(pageSize, pairsOnCurrentPageAndImediatelyAfter);
          const isFirstPage = pageStartIndex == 0;
          const isLastPage = pairsOnCurrentPageAndImediatelyAfter.length <= pageSize;

          const onEditReset = () =>
            setState((s: BoardsPageState) => ({ ...s, selectedIndex: null, newBoardText: null, newBoardCategories: null }));

          const onQueryAdd = (e: MouseEvent<HTMLButtonElement>) =>
            setState({ ...state, newBoardText: state.query, newBoardCategories: categoriesFilter })

          const onSelectedBoard = (index: number) =>
            setState({ ...state, selectedIndex: index });

          const onChangeCategories = (categoriesFilter: BoardCategory[]) =>
            setState({ ...state, categoriesFilter, pageStartIndex: 0 });

          const onChangeQuery = (query: string) =>
            setState({ ...state, query, pageStartIndex: 0 });

          const onNextPage = () =>
            setState({ ...state, pageStartIndex: state.pageStartIndex + pageSize });

          const onPreviousPage = () =>
            setState({ ...state, pageStartIndex: state.pageStartIndex - pageSize });


          const editWidget = (() => {
            if (state.selectedIndex != null) {
              return (
                <BoardEditWidget
                  initialIndex={state.selectedIndex}
                  boardCategories={categories.toArray()}
                  onReset={onEditReset} />
              );
            } else {
              return (
                <BoardEditWidget
                  initialTitle={state.newBoardText}
                  initialCategories={state.newBoardCategories}
                  boardCategories={categories.toArray()}
                  onReset={onEditReset} />
              );
            }
          })();

          return (
            <>
              <Navbar shouldConnect={false}/>

              <div className="board-page management hero-body container">
                <p className="title is-1">Boards</p>

                <div className="tile is-ancestor">
                  <div className="tile is-half is-parent">
                    <div className="tile is-child board-view">
                      <div className="page">
                        <section className="board-search-options">
                          <div className="field has-addons">
                            <div className="control is-expanded has-icons-left">
                              <input onChange={e => onChangeQuery(e.target.value)}
                                    className="input"
                                    type="text"
                                    placeholder="Search"
                                    value={state.query}/>
                              <span className="icon is-left">
                                <i className="fas fa-search" aria-hidden="true"></i>
                              </span>
                            </div>
                            <div className="control">
                              <button
                                type="button"
                                onClick={onQueryAdd}
                                className="button is-link">
                                <i className="fas fa-plus" aria-hidden="true"></i>
                              </button>
                            </div>
                          </div>
                          <div className="field control has-icons-left">
                            <TagsInput
                              onChange={onChangeCategories}
                              placeholder="Filter by categories"
                              suggestions={categories.toArray()}
                              tagSchema={boardCategorySchema}
                              value={categoriesFilter}/>
                            <span className="icon is-left">
                              <i className="fas fa-tags" aria-hidden="true"></i>
                            </span>
                          </div>
                        </section>

                        <div className="columns is-multiline">
                          {map(([index, board]) =>
                            <BoardPreview
                              key={board.id}
                              board={board}
                              isSelected={index == selectedId}
                              canClick
                              onClick={() => onSelectedBoard(index)} />, boardsOnCurrentPage)}
                        </div>
                      </div>
                      <footer className="pagination-controls">
                        <nav className="pagination is-centered" role="navigation" aria-label="pagination">
                          <button onClick={onPreviousPage} className="pagination-previous" disabled={isFirstPage}>
                            Previous Page
                          </button>
                          <button onClick={onNextPage} className="pagination-next" disabled={isLastPage}>
                            Next Page
                          </button>
                        </nav>
                      </footer>
                    </div>
                  </div>

                  <div className="tile is-half is-parent">
                    <div className="tile is-child box">
                      {editWidget}
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        },
        None: () =>
          <LoadingPage shouldConnect={false} message="Loading the Board Management Dashboard..."/>
      })
    } else {
      return <Redirect push to='/404'/>
    }
  } else {
    return <LoadingPage shouldConnect={false} message="Loading the Board Management Dashboard..."/>
  }
};

const mapState = ({ profile, boards }: AppState) => ({ user: profile.user, boards });
export default connect(mapState, { retrieveBoards, jumpedToBoard })(BoardsPage);
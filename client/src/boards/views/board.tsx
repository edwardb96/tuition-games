import React, { ReactNode } from "react";
import useFitText from "use-fit-text";
import cx from "classnames";
import { Card, isTextCard } from "@cat-tuition/protocol/pairs";

export interface ClickableProps {
  canClick?: boolean;
  onClick?: () => void;
}

const handleClick = (props: ClickableProps) => {
  const { canClick, onClick } = props;
  if (canClick && onClick) {
    onClick();
  }
}

export interface CardFrontFaceBaseProps extends ClickableProps {
  iconImage?: ReactNode | null;
  isSelected?: boolean;
  onDelete?: () => void;
}

export interface TextCardFrontFaceProps extends CardFrontFaceBaseProps {
  text: string;
}

export const TextCardFrontFace = (props: TextCardFrontFaceProps) => {
  const { fontSize, ref } = useFitText({ maxFontSize: 396, minFontSize: 5 });
  const { text, iconImage, onDelete, isSelected, canClick } = props;

  const topRight = (() => {
    if (iconImage != null) {
      return (
        <div className="player-image">
          {iconImage}
        </div>
      );
    } else if (onDelete != null) {
      return <a onClick={e => onDelete()} className="delete is-large"/>;
    } else {
      return <></>;
    }
  })();

  return (
    <div
      onClick={() => handleClick(props)}
      className={
        cx("box game-card-face front", {
          'is-clickable': canClick && !isSelected,
          'is-selected': isSelected })}>
      {topRight}
      <div className="text-container" ref={ref} style={{ fontSize }}>{text}</div>
    </div>
  );
}

export interface ImageCardFrontFaceProps extends CardFrontFaceBaseProps {
  image: string | null;
}

export const ImageCardFrontFace = (props: ImageCardFrontFaceProps) => {
  const { image, iconImage, onDelete, canClick, isSelected } = props;

  const topRight = (() => {
    if (iconImage != null) {
      return (
        <div className="player-image">
          {iconImage}
        </div>
      );
    } else if (onDelete != null) {
      return <a onClick={e => onDelete()} className="delete is-large"/>;
    } else {
      return <></>;
    }
  })();

  return (
    <div
      onClick={() => handleClick(props)}
      className={
        cx("box game-card-face front", {
           'is-clickable': canClick && !isSelected,
           'is-selected': isSelected })}>
      {topRight}
      <img className="game-card-image" src={`/api/images/${image}.png`}/>
    </div>
  );
}

export interface CardFrontFaceProps extends CardFrontFaceBaseProps {
  card: Card;
}

export const CardFrontFace = (props: CardFrontFaceProps) => {
  const { card, ...rest } = props;
  if (isTextCard(card)) {
    return <TextCardFrontFace text={card.text} {...rest} />
  } else {
    return <ImageCardFrontFace image={card.image} {...rest} />
  }
}

export interface CardBackFaceProps extends ClickableProps {
  isSelected: boolean;
}

export const CardBackFace = (props: CardBackFaceProps) => {
  const { isSelected, canClick } = props;
  return (
    <div
      onClick={() => handleClick(props)}
      className={
        cx("box game-card-face back", {
          'is-clickable': canClick && !isSelected,
          'is-selected': isSelected })}/>
  );
}

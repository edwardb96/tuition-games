import React, { useState } from "react";
import { form as joiToForms } from "joi-errors-for-forms";
import { connect } from "react-redux";
import { ErrorMessage, Field, Form, Formik, FormikErrors, FormikHelpers, FormikProps } from "formik";

import { createBoard, updateBoard, removeBoard } from "../actions";

import { AppState } from "../../redux-store";

import cx from "classnames";
import { get, hasRetrievalSucceeded } from "../../retrieval";
import { ImagePicker, useImagePicker } from "../../image-picker/views/image-picker-field";
import { TagsField } from "../../tags-input/tags-field";
import ImagePickerModal, { ImagePickerTab } from "../../image-picker/views/image-picker-modal";
import { Board, BoardCategory, BoardId, CounterIcon, editedBoardSchema, NewBoard, Position, UpdatedBoard } from "@cat-tuition/protocol/board-game";
import "./board-edit-widget.scss";
import { boardCategorySchema } from "@cat-tuition/protocol/board-game";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { HashMap } from "prelude-ts";
import { BoardGameBoard } from "../../classroom/activities/board-game/views/board";
import { SimpleColour } from "@cat-tuition/protocol/colour";
import { Role } from "@cat-tuition/protocol/user";
import { setConstantValue } from "typescript";

const convertToForms = joiToForms();

export interface BoardEditWidgetProps {
  initialTitle?: string | null;
  initialCategories?: BoardCategory[] | null;
  initialIndex?: number;
  initialBoard?: Board;
  boardCategories: BoardCategory[];
  createBoard(pair: NewBoard): Promise<string | null>;
  updateBoard(id: BoardId, index: number, pair: UpdatedBoard): Promise<boolean>;
  removeBoard(id: Board): Promise<boolean>;
  onReset(): void;
}

interface BoardEdits {
  title: string;
  image: string | null;
  counterSize: number;
  categories: BoardCategory[];
}


const BoardEditWidget = (props: BoardEditWidgetProps) => {
  const { createBoard, updateBoard, removeBoard, onReset } = props;
  const { initialBoard, initialIndex, initialTitle, initialCategories, boardCategories } = props;

  const validateForm = (values: BoardEdits): FormikErrors<BoardEdits> | undefined => {
    const result = editedBoardSchema.validate(values)
    console.log(values);
    const es = mapNullable(result.error, e => convertToForms(e));
    console.log(es);
    return es;
  };

  const onSubmit = async (info: BoardEdits, { setSubmitting, resetForm, setFieldError } : FormikHelpers<BoardEdits>) => {
    if (initialIndex != null && initialBoard != null) {
      const ok = await updateBoard(initialBoard.id, initialIndex, info as UpdatedBoard);
      if (ok) {
        resetForm();
        onReset();
      } else {
        setFieldError('general', 'There was a problem updating the card pair, perhaps the server is down. Refresh and try again or contact an Admin')
      }
    } else {
      const newId = await createBoard(info as NewBoard);
      if (newId != null) {
        resetForm();
        onReset();
      } else {
        setFieldError('general', 'There was a problem creating the card pair, perhaps the server is down. Refresh and try again or contact an Admin')
      }
    }
    setSubmitting(false);
  };

  const boardEdits: BoardEdits = (() => {
    if (initialBoard != null) {
      const { title, image, counterSize, categories } = initialBoard;
      return { title, image, counterSize, categories };
    } else {
      return {
        title: initialTitle || '',
        image: null,
        counterSize: 0.1,
        categories: initialCategories || []
      };
    }
  })();

  const { title, isOpen, selectedTab, selectedImage,
    onPickImage, onPickedImage, onCancelPick } =
      useImagePicker({
        isOpen: false,
        selectedTab: ImagePickerTab.FromFile
      });

  return (
    <>
      <ImagePickerModal
        isOpen={isOpen}
        onSubmit={onPickedImage}
        onCancel={onCancelPick}
        title={title}
        selectedTab={selectedTab}
        selectedImage={selectedImage}/>

      <Formik initialValues={boardEdits}
              enableReinitialize
              validate={validateForm}
              onSubmit={onSubmit}>
        {
          ({ isSubmitting, errors, touched, values, setFieldValue, resetForm }: FormikProps<BoardEdits>) => {

            const onDelete = async () => {
              if (initialBoard != null) {
                onReset();
                const ok = await removeBoard(initialBoard);
                if (!ok) {
                  console.log(`Failed to remove board with id ${initialBoard.id}`);
                }
              }
            }

            const onCancel = () => {
              if (initialBoard == null) {
                resetForm();
              }
              onReset();
            }

            const [counterPosition, setCounterPosition] = useState<Position>({
              x: 0.35,
              y: 0.35
            })

            return (
              <>
              <Form className="form board-edit-widget">
                <p className="title">{initialBoard == null ? 'Add' : 'Edit'} {values.title} Board</p>

                <div className="fields">
                  <div className="field">
                    <label className="label">Categories</label>
                    <div className="control">
                      <Field component={TagsField}
                        name="categories"
                        placeholder="Add Categories"
                        tagSchema={boardCategorySchema}
                        suggestions={boardCategories} />
                    </div>
                    <ErrorMessage name="categories" render={msg => <p className="help is-danger">{msg}</p>}/>
                  </div>

                  <div className="field">
                    <label className="label">Title</label>
                    <div className="control">
                      <Field className={cx("input", { 'is-danger': errors.title && touched.title })}
                            type="text"
                            name="title"
                            placeholder="Title" />
                    </div>
                    <ErrorMessage name="title" render={msg => <p className="help is-danger">{msg}</p>}/>
                  </div>

                  <div className="field">
                    <label className="label">Image</label>
                    <Field component={ImagePicker}
                      name="image"
                      title="Pick an image for the first card"
                      initialTab={ImagePickerTab.FromFile}
                      onPickImage={onPickImage} />
                    <ErrorMessage name="image" render={msg => <p className="help is-danger">{msg}</p>} />
                  </div>

                  <div className="field">
                    <label className="label">Counter Size</label>
                    <Field name="counterSize" className="slider is-fullwidth" step="0.001" min="0" max="0.5" type="range"/>
                    <ErrorMessage name="counterSize" render={msg => <p className="help is-danger">{msg}</p>} />
                  </div>

                  <div className="field">
                    <p className="help is-danger">{(errors as any).general}</p>
                  </div>

                  <div className="field">
                    {values.image != null ?
                      <BoardGameBoard
                        boardImage={values.image}
                        counterSize={values.counterSize}
                        counters={HashMap.ofObjectDictionary({ 'sample': {
                          icon: CounterIcon.Puck,
                          colour: SimpleColour.Blue,
                          position: counterPosition
                        } })}
                        userInfo={id => ({
                          id,
                          displayName: 'John Smith',
                          role: Role.Tutee,
                          favouriteColour: SimpleColour.Blue
                        })}
                        onMoveCounter={(id, pos) => { setCounterPosition(pos) }}
                        canMoveCounter={u => true}/> : <></>}
                  </div>
                </div>

                <footer className="footer-controls">
                  <div className="field is-grouped">
                    <p className="control">
                      <button className="button is-link" type="submit" disabled={isSubmitting}>
                        Save Board
                      </button>
                    </p>
                    <p className="control">
                      <button onClick={() => onCancel()} className="button is-link is-light" type="button" disabled={isSubmitting}>
                        Cancel
                      </button>
                    </p>
                    <p className="control">
                      <button onClick={() => onDelete()} className="button is-link is-light" type="button" disabled={isSubmitting}>
                        Delete
                      </button>
                    </p>
                  </div>
                </footer>
              </Form>
              </>
            );
          }
        }
      </Formik>
    </>
  );
}
const mapState = (s: AppState, { initialIndex }: { initialIndex?: number }) => {
  const boardsState = s.boards;
  if (initialIndex != null && hasRetrievalSucceeded(boardsState)) {
    const { boards } = get(boardsState)
    const initialBoard = boards.get(initialIndex).getOrThrow('Provided invalid board index to edit');
    return { initialBoard }
  } else {
    return { }
  }
};
export default connect(mapState, { createBoard, updateBoard, removeBoard })(BoardEditWidget);
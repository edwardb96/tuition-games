import React, { useState, useEffect, ChangeEvent, MouseEvent } from "react";
import { connect } from "react-redux";

import filter from "iter-tools/methods/filter";
import map from "iter-tools/methods/map";
import includesAny from "iter-tools/methods/includes-any";

import { BoardCategory, Board, boardCategorySchema } from "@cat-tuition/protocol/board-game";
import { retrieveBoards, createBoard } from "../actions";
import { AppState } from "../../redux-store";
import { asOption, get, hasRetrievalFailed, hasRetrievalFinished, hasRetrievalSucceeded, shouldReretrieve } from "../../retrieval";
import { BoardsState, hasBoard } from "../state";
import cx from "classnames";
import { TagsInput } from "../../tags-input/tags-input";

import './board-selector.scss';

export interface BoardSelectorProps {
  retrieveBoards(): Promise<boolean>;
  onChange(board: Board): void;
  boards: BoardsState;
}

export interface BoardSelectorState {
  query: string;
  categoriesFilter: string[];
  selectedBoard: Board | null;
}

export const BoardSelector = (props: BoardSelectorProps) => {
  const [state, setState] = useState<BoardSelectorState>({
    query: '',
    categoriesFilter: [],
    selectedBoard: null
  });

  const { query, categoriesFilter, selectedBoard } = state;
  const { onChange } = props;

  const loadBoards = async () => {
    const { retrieveBoards, boards } = props;
    if (shouldReretrieve(boards, 5)) {
      await retrieveBoards();
    }
  };

  useEffect(() => {
    loadBoards();
  });

  const onSelected = (board: Board) => {
    setState({ ...state, query: '', selectedBoard: board });
    onChange(board);
  }

  const onChangeQuery = (e: ChangeEvent<HTMLInputElement>) =>
    setState({ ...state, query: e.target.value });

  const onChangeCategories = (categories: BoardCategory[]) => {
    setState({ ...state, categoriesFilter: categories });
  }

  const { boards } = props;
  //const canPut = query.length > 0 &&
  //  hasRetrievalSucceeded(boards) &&
  //  !hasBoard({
  //    ...queryBoard,
  //    /* id should be unused because the queryBoard is always a text card */
  //    id: (null as unknown as string) }, get(boards).boards);

  const renderBoardItem = (board: Board) => {
    const isSelected = selectedBoard && board.id == selectedBoard.id;
    return (
      <a key={board.id}
        className={cx("panel-block", { 'is-selected': isSelected })}
        onClick={() => onSelected(board)} >
        {board.title}
      </a>
    );
  };

  const renderFilteredBoards = (boards: Iterable<Board>) => {
    const items = filter(board => {
      const boardName = board.title.toLowerCase();
      const queryLowerCase = query.toLowerCase();
      const queryLowerCaseSlashAsDevide = query.toLowerCase().replaceAll('/', '÷');
      const meetsCategoryFilter = (categoriesFilter.length == 0 || includesAny(categoriesFilter, board.categories))
      return meetsCategoryFilter &&
        (boardName.includes(queryLowerCase) ||
         boardName.includes(queryLowerCaseSlashAsDevide))
    }, boards);
    return (
      <div className="items">
        {[...map(renderBoardItem, items)]}
      </div>
    );
  };

  const renderLoader = () => {
    return (
      <div className="loader-wrapper">
        <div className="loader is-loading" />
      </div>
    );
  };

  return (
    <>
      <nav className="board-selector panel">
        <div className="panel-block">
          <div className="field control">
            <div className="control is-expanded has-icons-left">
              <input onChange={e => onChangeQuery(e)} className="input" type="text" placeholder="Search" value={state.query} />
              <span className="icon is-left">
                <i className="fas fa-search" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
        <div className="panel-block">
          <div className="field categories-filter control has-icons-left">
            <TagsInput
              onChange={onChangeCategories}
              placeholder="Filter by categories"
              suggestions={asOption(boards).map(({ categories }) => categories.toArray())
                            .getOrElse([])}
              tagSchema={boardCategorySchema}
              value={categoriesFilter}/>
            <span className="icon is-left">
              <i className="fas fa-tags" aria-hidden="true"></i>
            </span>
          </div>
        </div>
        {hasRetrievalFinished(boards)
          ? hasRetrievalFailed(boards)
            /* TODO Write some ccs to display this error message more cleanly */
            ? <p className="is-danger">Failed to retrieve card pairs</p>
           : renderFilteredBoards(get(boards).boards)
          : renderLoader() }
      </nav>
    </>
  );
}
export default connect((s : AppState) => ({ boards: s.boards }), { retrieveBoards, createBoard })(BoardSelector);

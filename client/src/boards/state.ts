import { BoardCategory, Board, NewBoard } from "@cat-tuition/protocol/board-game";
import { compareWithKey } from "@cat-tuition/protocol/compare";
import { HashSet } from "prelude-ts";
import { Retrieve } from "../retrieval";
import { SortedArray } from "../sorted-array";

export type BoardsState = Retrieve<{
  boards: SortedArray<Board>,
  jumpToPairIndex: number | null;
  categories: HashSet<BoardCategory>
}>;

export const compareBoards = (a: NewBoard, b: NewBoard): number =>
  compareWithKey(a, b, x => x.title);

export const hasBoard = (board: Board, boards: SortedArray<Board>): boolean =>
  boards.contains(board);

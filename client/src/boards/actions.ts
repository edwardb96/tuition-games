import { Board, BoardId, NewBoard, UpdatedBoard } from "@cat-tuition/protocol/board-game";
import { Action, TagAction, ThunkAction } from "../redux-utils";
import { deleteBoard, fetchBoards, postBoard, putBoard } from "./api";
//import { fetchBoards, postBoard, putBoard, deleteBoard } from "./api";

export const ATTEMPTED_RETRIEVE_BOARDS = 'BOARDS::RETRIEVE_BOARDS';
export const RETRIEVED_BOARDS = 'BOARDS::RETRIEVED_BOARDS';
export const RETRIEVE_BOARDS_FAILURE = 'BOARDS::RETRIEVE_BOARDS_FAILURE';
export const CREATED_BOARD = 'BOARDS::CREATED_BOARD';
export const REMOVED_BOARD = 'BOARDS::REMOVED_BOARD';
export const UPDATED_BOARD = 'BOARDS::UPDATED_BOARD';
export const JUMPED_TO_BOARD = 'BOARDS::JUMPED_TO';

export interface BoardUpdateInfo {
  index: number;
  update: UpdatedBoard;
}

export interface RetrievedBoardsAction extends Action<typeof RETRIEVED_BOARDS, Board[]> { };
export interface AttemptedRetrieveBoardsAction extends Action<typeof ATTEMPTED_RETRIEVE_BOARDS, Date> { };
export interface RetrieveBoardsFailureAction extends TagAction<typeof RETRIEVE_BOARDS_FAILURE> { };
export interface CreatedBoardAction extends Action<typeof CREATED_BOARD, Board> { };
export interface UpdatedBoardAction extends Action<typeof UPDATED_BOARD, BoardUpdateInfo> { };
export interface RemovedBoardAction extends Action<typeof REMOVED_BOARD, Board> { };
export interface JumpedToBoardAction extends TagAction<typeof JUMPED_TO_BOARD> { };

export type BoardsAction =
  AttemptedRetrieveBoardsAction |
  RetrievedBoardsAction |
  RetrieveBoardsFailureAction |
  CreatedBoardAction |
  RemovedBoardAction |
  UpdatedBoardAction |
  JumpedToBoardAction;

export const retrievedBoards = (boards: Board[]): RetrievedBoardsAction => ({
  type: RETRIEVED_BOARDS,
  payload: boards
});

export const retrieveBoardsFailure = (): RetrieveBoardsFailureAction => ({
  type: RETRIEVE_BOARDS_FAILURE,
});

export const attemptedRetrieveBoards = (when: Date): AttemptedRetrieveBoardsAction => ({
  type: ATTEMPTED_RETRIEVE_BOARDS,
  payload: when
});

export const jumpedToBoard = (): JumpedToBoardAction => ({
  type: JUMPED_TO_BOARD,
});


export const retrieveBoards = (): ThunkAction<Promise<boolean>> => async dispatch => {
  dispatch(attemptedRetrieveBoards(new Date()))
  try {
    const boards = await fetchBoards();
    dispatch(retrievedBoards(boards));
    return true;
  } catch(err) {
    dispatch(retrieveBoardsFailure());
    return false;
  }
};

export const createdBoard = (pair: Board): CreatedBoardAction => ({
  type: CREATED_BOARD,
  payload: pair
})

export const createBoard = (pair: NewBoard): ThunkAction<Promise<string | null>> => async dispatch => {
  const response = await postBoard(pair);
  return response.map(response => {
    const { id } = response;
    dispatch(createdBoard({ id, ...pair }));
    return id;
  }).getOrNull();
};

export const updatedBoard = (index: number, update: UpdatedBoard): UpdatedBoardAction => ({
  type: UPDATED_BOARD,
  payload: { index, update }
})

export const updateBoard = (id: string, index: number, pair: UpdatedBoard): ThunkAction<Promise<boolean>> =>
  async dispatch => {
    const ok = await putBoard(id, pair);
    if (ok) {
      dispatch(updatedBoard(index, pair));
    }
    return ok;
  };

export const removedBoard = (board: Board): RemovedBoardAction => ({
  type: REMOVED_BOARD,
  payload: board
});

export const removeBoard = (board: Board): ThunkAction<Promise<boolean>> => async dispatch => {
  const ok = await deleteBoard(board.id);
  if (ok) {
    dispatch(removedBoard(board));
  }
  return ok;
};
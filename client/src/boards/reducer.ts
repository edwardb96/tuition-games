import { BoardsAction, RETRIEVED_BOARDS, CREATED_BOARD, REMOVED_BOARD, UPDATED_BOARD, ATTEMPTED_RETRIEVE_BOARDS, RETRIEVE_BOARDS_FAILURE, JUMPED_TO_BOARD } from "./actions";
import { attemptedRetrieval, failedRetrieval, isRetrieving, map as mapRetrieval, notYetRetrieved, successfulRetrieval } from "../retrieval";
import { BoardsState, compareBoards } from "./state";
import { SortedArray } from "../sorted-array";
import flatMap from "iter-tools/methods/flat-map";
import { HashSet } from "prelude-ts";

export const initialState: BoardsState = notYetRetrieved;

export const reducer = (state: BoardsState = initialState, action: BoardsAction) => {
  switch (action.type) {
    case ATTEMPTED_RETRIEVE_BOARDS: {
      return attemptedRetrieval(action.payload);
    }
    case RETRIEVED_BOARDS: {
      if (isRetrieving(state)) {
        const boards = SortedArray.fromUnsorted(action.payload, compareBoards);
        const categories = HashSet.ofIterable(flatMap(board => board.categories, boards));
        return successfulRetrieval(state, { boards, categories, jumpToPairIndex: null })
      } else {
        throw new Error('Got success before starting the retrieval when retrieving card pairs');
      }
    }
    case RETRIEVE_BOARDS_FAILURE: {
      if (isRetrieving(state)) {
        return failedRetrieval(state);
      } else {
        throw new Error('Got failure before starting the retrieval when retrieving card pairs');
      }
    }
    case UPDATED_BOARD: {
      return mapRetrieval(state, ({ boards, categories }) => {
        const { index, update } = action.payload;
        const beforeUpdate = boards.getUnchecked(index);
        const updated = { ...beforeUpdate, ...update };
        const newBoards = boards.replaceReinsert(index, updated);
        const jumpToPairIndex = newBoards.indexOf(updated)
        return {
          boards: newBoards,
          categories: categories.addAll(update.categories),
          jumpToPairIndex
        };
      });
    }
    case CREATED_BOARD: {
      return mapRetrieval(state, ({ boards, categories }) => {
        const newBoards = boards.add(action.payload);
        const jumpToPairIndex = newBoards.indexOf(action.payload);
        return {
          boards: newBoards,
          categories: categories.addAll(action.payload.categories),
          jumpToPairIndex
        };
      });
    }
    case JUMPED_TO_BOARD: {
      return mapRetrieval(state, retrieved => {
        return { ...retrieved, jumpToPairIndex: null }
      })
    }
    case REMOVED_BOARD: {
      return mapRetrieval(state, retrieved => {
        const { boards, categories } = retrieved;
        return {
          ...retrieved,
          boards: boards.removeOnce(action.payload)[1],
          categories
        };
      });
    }
    default: {
      return state;
    }
  }
};
import { Board, BoardId, boardIdSchema, NewBoard, UpdatedBoard } from "@cat-tuition/protocol/board-game";
import { validOrNone } from "@cat-tuition/protocol/joi-ext";
import { Option } from "prelude-ts";

import Joi from "joi";
import axios from "axios";
const { object } = Joi.types();

export const fetchBoards = async (): Promise<Board[]> => {
  return await (await axios.get('/api/boards')).data;
}

export const putBoard = async (id: BoardId, pair: UpdatedBoard): Promise<boolean> => {
  try {
    const response = await axios.put(`/api/boards/${id}`, pair);
    return (response.status == 200);
  } catch(err) {
    return false;
  }
}

export interface PostBoardResponse {
  id: BoardId;
}

export const postBoardResponseSchema =
  object.keys({
    id: boardIdSchema
  });

const validatePostBoardResponse = (value: unknown): Option<PostBoardResponse> => {
  return validOrNone(postBoardResponseSchema, value);
};

export const postBoard = async (newPair: NewBoard): Promise<Option<PostBoardResponse>> => {
  try {
    const response = await axios.post(`/api/boards`, newPair);
    if (response.status == 200) {
      return validatePostBoardResponse(response.data);
    } else {
      return Option.none();
    }
  } catch (err) {
    return Option.none();
  }
}

export const deleteBoard = async (id: string): Promise<boolean> => {
  try {
    const response = await axios.delete(`/api/boards/${id}`);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}
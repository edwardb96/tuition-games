import { Action as ReduxAction, Dispatch, Middleware } from "redux";
import { ThunkAction as ReduxThunkAction, ThunkDispatch as ReduxThunkDispatch } from "redux-thunk";
import { AppState } from "./redux-store";
import { ServerMessage, validateServerMessageShell } from "@cat-tuition/protocol/message";

export type ThunkAction<ReturnType = void> = ReduxThunkAction<
  ReturnType,
  AppState,
  unknown,
  ReduxAction
>;

export type ThunkDispatch<A extends ReduxAction> = ReduxThunkDispatch<AppState, unknown, A>;

export interface Action<Type, Payload> extends ReduxAction<Type> {
  type: Type;
  payload: Payload
}

export interface TagAction<Type> extends ReduxAction<Type> {
  type: Type;
}


export interface Channel {
  onReceive: (serverMessage: ServerMessage<string, any>, next: Dispatch<ReduxAction<any>>) => void
}

export const multiplexChannels =
  (channels: Map<string, Channel>): Middleware<{}, AppState> => store => next => (action: any) => {
    if (action.type == 'REDUX_WEBSOCKET::MESSAGE') {
      try {
        const rawParsedJSON = JSON.parse(action.payload.message);
        // TODO: origin checks?
        validateServerMessageShell(rawParsedJSON).match({
          Some: serverMessage => {
            const channel = channels.get(serverMessage.channel);
            if (channel != null) {
              channel.onReceive(serverMessage.payload, next)
            } else {
              console.log("Received server message for unregistered channel ${}.", serverMessage.channel);
            }
          },
          None: () => {
            console.log("Received invalid server message");
          }
        })
      } catch (e) {
        console.log(e);
      }
    } else {
      next(action);
    }
  };
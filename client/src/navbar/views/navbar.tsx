import './navbar.scss';
import { connect } from "react-redux";
import React, { useEffect, useState } from "react";
import logo from "url:../../assets/logo.png";
import cx from "classnames";
import { AppState } from "../../redux-store";
import { fetchUser, fetchToken, wsConnect, sendPong } from "../actions";
import { ConnectionStateTag, hasNotTriedConnectionYet, ProfileState } from "../reducer";
import { Link } from "react-router-dom";
import { ProfileImage } from "../../profile-image/profile-image";

interface NavbarProps extends ProfileState {
  shouldConnect: boolean;
  shouldFetchUser?: boolean;
  fetchUser(): void;
  fetchToken(): void;
  sendPong(): void;
  wsConnect(endpoint: string, token: string): void;
}

interface NavbarState {
  menuExpanded: boolean;
}

const toggleMenuExpanded = (state: NavbarState): NavbarState => {
  const { menuExpanded } = state;
  return { ...state, menuExpanded: !menuExpanded };
}

export const Navbar = (props: NavbarProps) => {
  const [state, setState] = useState({ menuExpanded: false });
  const onClickBurger = () => {
    setState(toggleMenuExpanded(state));
  }

  const { user, token, connection, shouldConnect, shouldFetchUser } = props;
  const { fetchUser, fetchToken, sendPong, wsConnect } = props;

  useEffect(() => {
    if (shouldConnect && token == null) {
      fetchToken();
    }
  }, [shouldConnect, token]);

  const fetchTheUser = user == null && (shouldFetchUser == null || shouldFetchUser);

  useEffect(() => {
    if (fetchTheUser) {
      fetchUser();
    }
  }, [fetchTheUser]);

  useEffect(() => {
    if (shouldConnect && token != null && hasNotTriedConnectionYet(connection)) {
      wsConnect(process.env.WEBSOCKETS_ENDPOINT || 'NOURL', token);
    }
  }, [shouldConnect, token, connection]);

  const shouldPong = connection.tag == ConnectionStateTag.Connected && connection.gotPing;

  useEffect(() => {
    if (shouldPong) {
      sendPong();
      console.log("Sent Pong! %s", new Date().toUTCString());
    }
  }, [shouldPong])

  const maybeProfileImage =
    user != null ?
      <a className="navbar-link user-profile-button">
        <ProfileImage displayName={user.displayName} size={32} colour={user.favouriteColour}/>
        <strong>{user.displayName}</strong>
      </a> : <></>

  return (
    <nav role="navigation" className="navbar is-primary">
      <div className="navbar-brand">
        <a className="navbar-item logo" href="/">
          <img src={logo} />
        </a>
        <div role="button"
          className={cx("navbar-burger", "burger", { 'is-active': state.menuExpanded })}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarItems"
          onClick={() => onClickBurger()}>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </div>
      </div>

      <div id="navbarItems" className={cx("navbar-menu", { 'is-active': state.menuExpanded })}>
        <div className="navbar-start">
          {user
            ? <Link to="/" className="navbar-item">Lobby</Link>
            : <></>}

          {user && user.canManageUsers()
            ? <Link to="/users" className="navbar-item">Users</Link>
            : <></>}

          {user && user.canManageCardPairs()
            ? <Link to="/cards" className="navbar-item">Cards</Link>
            : <></>}

          {user && user.canManageBoards()
            ? <Link to="/boards" className="navbar-item">Boards</Link>
            : <></>}
        </div>

        {user != null ?
          <div className="navbar-end">
            <div className="navbar-item has-dropdown is-hoverable">
              {maybeProfileImage}

              <div className="navbar-dropdown">
                <a className="navbar-item" href="/logout">
                  Sign Out
                </a>
              </div>
            </div>
          </div> : <></>}
      </div>
    </nav>
  );
}

const mapState = (x: AppState) => x.profile;
export default connect(mapState, { fetchUser, fetchToken, wsConnect, sendPong })(Navbar);
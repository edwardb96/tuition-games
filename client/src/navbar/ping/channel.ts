import { Channel, ThunkDispatch } from "../../redux-utils";
import { validatePingServerPayload } from "@cat-tuition/protocol/channels/ping";
import { Action as ReduxAction } from "redux";
import { gotPing } from "../actions";

export class PingChannel implements Channel {
  onReceive(payload: unknown, next: ThunkDispatch<ReduxAction<any>>) {
    const validPayload = validatePingServerPayload(payload);
    if (validPayload) {
      console.log("Got Ping! %s", new Date().toUTCString())
      next(gotPing());
    } else {
      console.log('Invalid ping payload from server', payload);
    }
  }
}
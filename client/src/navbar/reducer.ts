import { User } from "@cat-tuition/protocol/user";
import { Action } from "../redux-utils";
import { FetchedTokenAction, FetchedUserAction, FETCHED_TOKEN, FETCHED_USER, GOT_PING, SENT_PONG } from "./actions";

export enum ConnectionStateTag {
  Connecting,
  Connected,
  Disconnected
}

export interface Connected {
  tag: ConnectionStateTag.Connected;
  gotPing: boolean;
}

export interface Connecting {
  tag: ConnectionStateTag.Connecting;
}

export interface Disconnected {
  tag: ConnectionStateTag.Disconnected;
  reason: string;
  code: number;
}

export const hasNotTriedConnectionYet = (connection: ConnectionState) => {
  return connection.tag == ConnectionStateTag.Connecting;
};

export type ConnectionState = Connected | Connecting | Disconnected;

export interface ProfileState {
  user?: User;
  token?: string;
  connection: ConnectionState;
}

export const initialState: ProfileState = { connection: { tag: ConnectionStateTag.Connecting } };

export const reducer = (state: ProfileState = initialState,
                        action : Action<string, any> | FetchedUserAction | FetchedTokenAction): ProfileState => {
  switch (action.type) {
    case 'REDUX_WEBSOCKET::OPEN': {
      return { ...state, connection: { tag: ConnectionStateTag.Connected, gotPing: false } };
    }
    case GOT_PING: {
      console.log("Reducer GOT_PING %s", new Date().toUTCString());
      const { connection } = state;
      if (connection.tag == ConnectionStateTag.Connected) {
        return { ...state, connection: { ...connection, gotPing: true } };
      } else {
        return state;
      }
    }
    case SENT_PONG: {
      console.log("Reducer SENT_PONG %s", new Date().toUTCString());
      const { connection } = state;
      if (connection.tag == ConnectionStateTag.Connected) {
        return { ...state, connection: { ...connection, gotPing: false } };
      } else {
        return state;
      }
    }
    case 'REDUX_WEBSOCKET::CLOSED': {
      const { reason: rawReason, code } = action.payload;
      const reason = rawReason ? rawReason : undefined;
      return { ...state, connection: { tag: ConnectionStateTag.Disconnected, reason, code } };
    }
    case FETCHED_USER: {
      return { ...state, user: (action as FetchedUserAction).payload };
    }
    case FETCHED_TOKEN: {
      const token = (action as FetchedTokenAction).payload;
      try {
        return { ...state, token: token };
      } catch (err) {
        return state;
      }
    }
    default: {
      return state;
    }
  }
};
import { PrivateUserInfo, WithOptionalTokenInfo, User, UserInfo } from "@cat-tuition/protocol/user";
import { Action, TagAction, ThunkAction } from "../redux-utils";
import { Action as ReduxAction } from "redux";
import { connect, send } from "@giantmachines/redux-websocket";
import { PING_CHANNEL, ClientPayload } from "@cat-tuition/protocol/channels/ping";
import axios from "axios";

export const FETCHED_USER = 'PROFILE::FETCHED_USER';
export const FETCHED_TOKEN = 'PROFILE::FETCHED_TOKEN';
export interface FetchedUserAction extends Action<typeof FETCHED_USER, User> { };
export interface FetchedTokenAction extends Action<typeof FETCHED_TOKEN, string> { };

export const SENT_PONG = 'PROFILE::SENT_PONG';
export const GOT_PING = 'PROFILE::GOT_PING';
export interface SentPongAction extends TagAction<typeof SENT_PONG> { };
export interface GotPongAction extends TagAction<typeof GOT_PING> { };

export const fetchedUser = (user: UserInfo & PrivateUserInfo & WithOptionalTokenInfo): FetchedUserAction => {
  return {
    type: FETCHED_USER,
    payload: User.fromFullUserInfo(user)
  };
};

export const fetchedToken = (token: string): FetchedTokenAction => {
  return {
    type: FETCHED_TOKEN,
    payload: token
  };
};

export const wsConnect = (endpoint: string, token: string): ReduxAction<any> => {
  console.log(`Connecting to ${endpoint}`);
  return connect(`${endpoint}?token=${token}`);
};

export const sendPing = (payload: ClientPayload) => {
  return send({ channel: PING_CHANNEL, payload });
}

export const sendPongMessage = () => {
  return sendPing({ kind: 'pong' });
}

export const sendPong = (): ThunkAction<void> => async dispatch => {
  dispatch(sendPongMessage());
  dispatch(sentPong());
};

export const sentPong = () => ({
  type: SENT_PONG,
});

export const gotPing = () => ({
  type: GOT_PING,
});

export const fetchUser = (): ThunkAction<void> => async dispatch => {
  const response = await axios.get("/api/user");
  if (response.status == 200) {
    const userInfo = response.data;
    dispatch(fetchedUser(userInfo));
  }
};

export const fetchToken = (): ThunkAction<void> => async dispatch => {
  const response = await axios.get("/api/ws-token");
  if (response.status == 200) {
    const token = await response.data;
    dispatch(fetchedToken(token));
  }
}

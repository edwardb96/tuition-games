import { UserInfo } from "@cat-tuition/protocol/user";
import { ActivityId, RecievedChatMessageInfo, TuteeWelcomeToClassroomInfo, TutorWelcomeToClassroomInfo } from "@cat-tuition/protocol/channels/classroom";
import { HashMap, HashSet, Option, Vector } from "prelude-ts";
import { hashMapFromList } from "../prelude-ts-ext";
import { ReactChild } from "react";

export class Classroom {
  tutor: UserInfo;
  tutees: HashMap<string, UserInfo>;
  tuteesOnline: HashSet<string>;
  activityId: Option<ActivityId>;
  chatHistory: Vector<RecievedChatMessageInfo>;

  constructor(tutor: UserInfo,
              tutees: HashMap<string, UserInfo>,
              tuteesOnline: HashSet<string>,
              activity: Option<ActivityId>,
              chatHistory: Vector<RecievedChatMessageInfo>) {
    this.tutor = tutor;
    this.tutees = tutees;
    this.tuteesOnline = tuteesOnline;
    this.activityId = activity;
    this.chatHistory = chatHistory;
  }

  *occupants(): Iterable<UserInfo> {
    yield this.tutor;
    for (let tuteeId of this.tuteesOnline) {
      const tutee = this.tutees.get(tuteeId);
      if (tutee.isSome()) {
        yield tutee.get();
      }
    }
  }

  withNewChatMessage(info: RecievedChatMessageInfo): Classroom {
    return new Classroom(this.tutor,
                         this.tutees,
                         this.tuteesOnline,
                         this.activityId,
                         this.chatHistory.append(info));
  }

  withTutee(tutee: UserInfo): Classroom {
    const joinMessage = {
      senderId: tutee.id,
      recievedAt: new Date(),
      chatMessage: "Joined the class"
    };
    return new Classroom(this.tutor,
                         this.tutees.put(tutee.id, tutee),
                         this.tuteesOnline.add(tutee.id),
                         this.activityId,
                         this.chatHistory.append(joinMessage));
  }

  withoutTutee(tuteeId: string): Classroom {
    const leaveMessage = {
      senderId: tuteeId,
      recievedAt: new Date(),
      chatMessage: "Left the class"
    };
    return new Classroom(this.tutor,
                         this.tutees,
                         this.tuteesOnline.remove(tuteeId),
                         this.activityId,
                         this.chatHistory.append(leaveMessage));
  }

  onlineTutees(): UserInfo[] {
    return this.tuteesOnline.toArray()
                            .map(id =>
                              this.tutees.get(id)
                                         .getOrThrow('Missing info for online tutee'));
  }

  userInfo(userId: string) {
    if (this.isTutorOfClass(userId)) {
      return this.tutor;
    } else {
      return this.tutees.get(userId)
                        .getOrThrow(`Tried to get info for user ${userId} who was never present in the class`)
    }
  }

  isInClass(userId: string): boolean {
    return this.isTutorOfClass(userId) || this.tutees.get(userId).isSome();
  }

  isTutorOfClass(userId: string): boolean {
    return this.tutor.id == userId;
  }

  withActivity(newActivity: Option<ActivityId>): Classroom {
    return new Classroom(this.tutor,
                         this.tutees,
                         this.tuteesOnline,
                         newActivity,
                         this.chatHistory);
  }

  static fromTuteeInfo(info: TuteeWelcomeToClassroomInfo): Classroom {
    const { tutor, tutees: tuteesList, activityId } = info;
    const tutees = hashMapFromList(tuteesList, u => u.id);
    return new Classroom(tutor, tutees, tutees.keySet(), Option.ofNullable(activityId), Vector.empty());
  }

  static fromTutorInfo(info: TutorWelcomeToClassroomInfo): Classroom {
    const { tutor, tutees: tuteesList, activityId } = info;
    const tutees = hashMapFromList(tuteesList, u => u.id);
    return new Classroom(tutor, tutees, tutees.keySet(), Option.ofNullable(activityId), Vector.empty());
  }
}
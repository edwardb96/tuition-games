import { Classroom } from "./classroom";
import { Either } from "prelude-ts";
import { EnteredLobbyAction, ENTERED_LOBBY, NoSuchClassroomAction, NO_SUCH_CLASS } from "../lobby/actions";
import { ClassroomAction, ACTIVITY_CHANGED, CLASS_ENDED, RECEIVE_CHAT_MESSAGE, TUTEE_ADDED, TUTEE_REMOVED, TUTEE_ENTERED_CLASS, TUTOR_ENTERED_CLASS, ENTERING_CLASS } from "./actions";
import { Option } from "prelude-ts";

export enum ClassroomConnectionState {
  JoiningClass,
  ClassEnded,
  NoSuch
}

export type ClassroomState = Either<ClassroomConnectionState, Classroom>;

export const initialState: ClassroomState = Either.left(ClassroomConnectionState.JoiningClass);

export const reducer = (state: ClassroomState = initialState, action: EnteredLobbyAction | NoSuchClassroomAction | ClassroomAction): ClassroomState => {
  switch (action.type) {
    case TUTEE_ENTERED_CLASS:
      return Either.right(Classroom.fromTuteeInfo(action.payload));
    case TUTOR_ENTERED_CLASS:
      return Either.right(Classroom.fromTutorInfo(action.payload));
    case CLASS_ENDED:
      return Either.left(ClassroomConnectionState.ClassEnded);
    case ENTERED_LOBBY:
      return Either.left(ClassroomConnectionState.JoiningClass);
    case NO_SUCH_CLASS:
      return Either.left(ClassroomConnectionState.NoSuch)
    default:
      return state.map(classroom => {
        switch(action.type) {
          case ACTIVITY_CHANGED:
            return classroom.withActivity(Option.ofNullable(action.payload));
          case TUTEE_ADDED:
            return classroom.withTutee(action.payload);
          case TUTEE_REMOVED:
            return classroom.withoutTutee(action.payload);
          case RECEIVE_CHAT_MESSAGE:
            return classroom.withNewChatMessage(action.payload);
          default:
            return classroom;
        }
      });
  }
};
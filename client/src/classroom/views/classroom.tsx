import React, { Component, useRef } from "react";
import { Classroom } from "../classroom";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import "./classroom.scss";
import cx from "classnames";
import LobbyWidget from "../../lobby/views/lobby-widget";
import { ActivityWidget } from "./activity-widget";
import { ProfileImage } from "../../profile-image/profile-image";
import { HashMap } from "prelude-ts";
import { ChatWidget } from "./chat-widget";

interface ClassroomProps {
  classroom: Classroom;
  user: User;
  usersWaiting: HashMap<string, UserInfo>;
  onSendMessage(message: string): void;
  onSendInvitation(userId: string): void;
}


interface LobbyUserRow {
  user: UserInfo;
  onInvite(): void;
}

const LobbyUserRow = (props: LobbyUserRow) => {
  const { user, onInvite } = props;
  return (
    <tr className="tutee">
      <td>
        <ProfileImage className="media-left"
          displayName={user.displayName}
          colour={user.favouriteColour}
          size={24} />
      </td>
      <td>
        {user.displayName}
      </td>
      <td>
        <a onClick={onInvite} className="button is-small">Invite</a>
      </td>
    </tr>
  );
};

const TuteeIcon = (props: { tutee: UserInfo }) => {
  const { tutee } = props;
  return (
    <div className="column">
      <div className="tutee-icon">
        <div className="image-wrapper">
          <ProfileImage
            displayName={tutee.displayName}
            colour={tutee.favouriteColour}
            size={48} />
        </div>
        <p className="title is-6">{tutee.displayName}</p>
      </div>
    </div>
  );
}

const TuteeList = (props: { onlineTutees: UserInfo[], onAdd: () => void }) => {
  const { onlineTutees, onAdd } = props;
  if (onlineTutees.length > 0) {
    return (
      <section className="columns is-multiline is-centered has-text-centered">
        {onlineTutees.map(tutee => <TuteeIcon key={tutee.id} tutee={tutee}/>)}
      </section>
    );
  } else {
    return (
      <section className="columns is-multiline is-centered has-text-centered">
        <div className="column">
          <a onClick={onAdd} className="tutee-icon invite">
            <div className="image-wrapper">
              <div className="profile-image is-circular image is-48x48">
                <i className="fas fa-plus"/>
              </div>
            </div>
            <p className="title is-6">Add Tutees</p>
          </a>
        </div>
      </section>
    );
  }
};

interface ChatOrLobbyWidgetProps {
  user: User;
  classroom: Classroom;
  usersWaiting: HashMap<string, UserInfo>;
  onSendMessage(message: string): void;
  onSendAdmittance(userId: string): void;
}

enum ChatOrLobbyTab {
  AddToClass,
  Chat
}

interface ChatOrLobbyWidgetState {
  currentTab: ChatOrLobbyTab;
}


class ChatOrLobbyWidget extends Component<ChatOrLobbyWidgetProps, ChatOrLobbyWidgetState> {
  constructor(props: ChatOrLobbyWidgetProps) {
    super(props);
    this.state = { currentTab: ChatOrLobbyTab.Chat };
  }

  setCurrentTab(tab: ChatOrLobbyTab) {
    this.setState({ currentTab: tab });
  }

  renderCurrentTab() {
    const { currentTab } = this.state;
    const { user, classroom, usersWaiting, onSendMessage, onSendAdmittance } = this.props;

    switch (currentTab) {
      case ChatOrLobbyTab.AddToClass: {
        return (
          <LobbyWidget onSendAdmittance={onSendAdmittance} usersWaiting={usersWaiting}/>
        );
      }
      case ChatOrLobbyTab.Chat: {
        return (
          <ChatWidget user={user} classroom={classroom} onSendMessage={onSendMessage}/>
        );
      }
    }
  }

  render() {
    const { user, classroom } = this.props;
    const { currentTab } = this.state;
    return (
      <div className="card chat-or-lobby-widget is-scrollable">
        <header className="card-header">
          <div className="tabs is-fullwidth">
            <ul>
              <li className={cx({'is-active': currentTab == ChatOrLobbyTab.Chat })}>
                <a onClick={() => this.setState({ currentTab: ChatOrLobbyTab.Chat })}>Chat</a>
              </li>
              {user.id == classroom.tutor.id ?
                <li className={cx({'is-active': currentTab == ChatOrLobbyTab.AddToClass })}>
                  <a onClick={() => this.setState({ currentTab: ChatOrLobbyTab.AddToClass })}>Add</a>
                </li> : <></>}
            </ul>
          </div>
        </header>
        <div className="card-content">
          {this.renderCurrentTab()}
        </div>
      </div>
    );
  }
}

export const ClassroomDashboard = (props: ClassroomProps) => {
  const { user, classroom, usersWaiting, onSendMessage, onSendInvitation } = props;

  const chatOrLobbyWidget = useRef<ChatOrLobbyWidget>(null);

  const onAdd = () =>
    chatOrLobbyWidget.current ? chatOrLobbyWidget.current.setCurrentTab(ChatOrLobbyTab.AddToClass) : null;

  return (
    <>
      <div className="hero-body container">
        <h1 className="title is-1">{classroom.tutor.displayName}'s Classroom</h1>
        <div className="columns">
          <div className="column is-two-thirds is-parent">
            <ActivityWidget
              isInOwnClass={classroom.isTutorOfClass(user.id)}
              activityId={classroom.activityId} />
          </div>
          <div className="column">
            <div className="box">
              <p className="title">Tutees</p>
              <TuteeList onlineTutees={classroom.onlineTutees()} onAdd={onAdd}/>
            </div>
            <ChatOrLobbyWidget
              ref={chatOrLobbyWidget}
              user={user}
              classroom={classroom}
              usersWaiting={usersWaiting}
              onSendAdmittance={onSendInvitation}
              onSendMessage={onSendMessage}/>
          </div>
        </div>
      </div>
    </>
  );
}

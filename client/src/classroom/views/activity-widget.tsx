import React, { Component } from "react";
import cx from "classnames";
import { ActivityId } from "@cat-tuition/protocol/channels/classroom";
import { ActivityIdInfo } from "@cat-tuition/protocol/activity";
import { PAIRS_ACTIVITY_ID } from "@cat-tuition/protocol/pairs";
import { NOUGHTS_AND_CROSSES_ACTIVITY_ID } from "@cat-tuition/protocol/noughts-and-crosses";
import { BOARD_GAME_ACTIVITY_ID } from "@cat-tuition/protocol/board-game";
import { Option } from "prelude-ts";
import NoughtsAndCrossesGame from "../activities/noughts-and-crosses/views/game";
import NoughtsAndCrossesSetup from "../activities/noughts-and-crosses/views/setup";
import PairsGame from "../activities/pairs/views/game";
import PairsSetup from "../activities/pairs/views/setup";
import BoardGame from "../activities/board-game/views/game";
import BoardGameSetup from "../activities/board-game/views/setup";

export enum ActivityWidgetTab {
  Welcome,
  NoughtsAndCrosses,
  Pairs,
  BoardGame
}

const tabFromActivityId = (activity: Option<ActivityId>): ActivityWidgetTab => {
  return activity.map((id: ActivityId): ActivityWidgetTab => {
    switch (id) {
      case NOUGHTS_AND_CROSSES_ACTIVITY_ID:
        return ActivityWidgetTab.NoughtsAndCrosses;
      case PAIRS_ACTIVITY_ID:
        return ActivityWidgetTab.Pairs;
      case BOARD_GAME_ACTIVITY_ID:
        return ActivityWidgetTab.BoardGame;
    }
  }).getOrElse(ActivityWidgetTab.Welcome);
}

export interface ActivityWidgetProps extends ActivityIdInfo<Option<ActivityId>> {
  isInOwnClass: boolean;
}

export interface ActivityWidgetState {
  currentTab: Option<ActivityWidgetTab>;
}

export class ActivityWidget extends Component<ActivityWidgetProps, ActivityWidgetState> {
  constructor(props: ActivityWidgetProps) {
    super(props);
    const { isInOwnClass, activityId } = props;
    const currentTab : Option<ActivityWidgetTab> =
      isInOwnClass
        ? Option.of(tabFromActivityId(activityId))
        : Option.none();
    this.state = { currentTab }
  }

  setCurrentTab(tab: ActivityWidgetTab) {
    this.setState({ currentTab: Option.of(tab) });
  }

  render() {
    const { activityId, isInOwnClass } = this.props;

    const activityTab = (tab: ActivityWidgetTab) => {
      switch (tab) {
        case ActivityWidgetTab.Welcome:
          return (
            <div className="card-content">
              <p className="title">Welcome</p>
              <p className="subtitle">Waiting for an activity to begin...</p>
            </div>
          );
        case ActivityWidgetTab.NoughtsAndCrosses: {
          if (activityId.map(v => v == NOUGHTS_AND_CROSSES_ACTIVITY_ID).getOrElse(false)) {
            return <NoughtsAndCrossesGame/>;
          } else if (isInOwnClass) {
            return <NoughtsAndCrossesSetup/>;
          }
        }
        case ActivityWidgetTab.Pairs:
          if (activityId.map(v => v == PAIRS_ACTIVITY_ID).getOrElse(false)) {
            return <PairsGame/>;
          } else if (isInOwnClass) {
            return <PairsSetup/>;
          }
        case ActivityWidgetTab.BoardGame:
          if (activityId.map(v => v == BOARD_GAME_ACTIVITY_ID).getOrElse(false)) {
            return <BoardGame />
          } else {
            return <BoardGameSetup/>
          }
      }
    };

    const tabBar = (currentTab: ActivityWidgetTab) => {
      return (
        <header className="card-header">
          <div className="tabs is-fullwidth">
            <ul>
              <li className={cx({'is-active': currentTab == ActivityWidgetTab.Welcome })}>
                <a onClick={() => this.setCurrentTab(ActivityWidgetTab.Welcome)}>
                  Welcome
                </a>
              </li>
              <li className={cx({'is-active': currentTab == ActivityWidgetTab.NoughtsAndCrosses})}>
                <a onClick={() => this.setCurrentTab(ActivityWidgetTab.NoughtsAndCrosses)}>
                  Noughts and Crosses
                </a>
              </li>
              <li className={cx({'is-active': currentTab == ActivityWidgetTab.Pairs})}>
                <a onClick={() => this.setCurrentTab(ActivityWidgetTab.Pairs)}>
                  Pairs
                </a>
              </li>
              <li className={cx({'is-active': currentTab == ActivityWidgetTab.BoardGame})}>
                <a onClick={() => this.setCurrentTab(ActivityWidgetTab.BoardGame)}>
                  Board Game
                </a>
              </li>
            </ul>
          </div>
        </header>
      );
    };

    return (
      <div className="activity card">
        {isInOwnClass ? tabBar(this.state.currentTab.getOrThrow('Expected to have current tab when in own class')) : <></>}
        {activityTab(this.state.currentTab.getOrElse(tabFromActivityId(activityId)))}
      </div>
    );
  }
}
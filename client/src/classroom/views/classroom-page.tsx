import React, { useEffect } from "react";
import { Redirect, useParams } from "react-router";
import { AppState } from "../../redux-store";
import { ClassroomDashboard } from "./classroom";
import { receiveChatMessage, sendChatMessage } from "../actions";
import { sendAdmittance } from "../../lobby/actions";
import { sendEnterClassroom } from "../../lobby/actions";
import { ClassroomState, ClassroomConnectionState } from "../reducer";
import { ProfileState } from "../../navbar/reducer";
import { connect } from "react-redux";
import { UserInfo } from "@cat-tuition/protocol/user";
import { usersWaiting } from "../../lobby/lobby";
import Navbar from "../../navbar/views/navbar";
import { ConnectionStateTag } from "../../navbar/reducer";
import { HashMap, Option } from "prelude-ts";
import LoadingPage from "../../loading-page/views/loading-page";
import { RecievedChatMessageInfo } from "@cat-tuition/protocol/channels/classroom";

export interface ClassroomPageProps {
  classroom: ClassroomState;
  profile: ProfileState;
  usersWaiting: HashMap<string, UserInfo>;
  sendEnterClassroom(id: string): void;
  sendChatMessage(message: string): void;
  sendAdmittance(userId: string): void;
  receiveChatMessage(message: RecievedChatMessageInfo): void;
}

interface ClassroomParams {
  classroomId: string;
}

const ClassroomPage = (props: ClassroomPageProps) => {
  const { classroom,
          profile,
          usersWaiting,
          sendEnterClassroom,
          sendChatMessage,
          sendAdmittance,
          receiveChatMessage,
          } = props;
  const { connection, user } = profile;
  const { classroomId } = useParams<ClassroomParams>();

  const maybeRedirect : Option<string> =
    classroom.mapLeft(
      s => {
        switch (s) {
          case ClassroomConnectionState.JoiningClass:
            return Option.none<string>()
          case ClassroomConnectionState.NoSuch:
            return Option.of('/404');
          case ClassroomConnectionState.ClassEnded:
            return Option.of('/');
        }
      }).getLeftOrElse(Option.none<string>());

  useEffect(() => {
    if (user != null && connection.tag == ConnectionStateTag.Connected && !maybeRedirect.isSome()) {
      sendEnterClassroom(classroomId);
    }
  }, [user, connection.tag, maybeRedirect.isSome()]);

  if (user != null && connection.tag == ConnectionStateTag.Connected) {
    const onSendMessage = (chatMessage: string): void => {
      sendChatMessage(chatMessage);
      receiveChatMessage({ senderId: user.id, recievedAt: new Date(), chatMessage });
    }

    if (classroom.isRight()) {
      return (
        <>
          <Navbar shouldConnect/>
          <ClassroomDashboard
            onSendMessage={onSendMessage}
            onSendInvitation={sendAdmittance}
            classroom={classroom.get()}
            usersWaiting={usersWaiting}
            user={user} />
        </>
      );
    } else {
      if (maybeRedirect.isSome()) {
        return <Redirect push to={maybeRedirect.get()}/>
      } else {
        return <LoadingPage shouldConnect message="Connecting to classroom..."/>;
      }
    }
  } else {
    return <LoadingPage shouldConnect message="Connecting to classroom..."/>;
  }
}
const mapState = (x: AppState) => ({ classroom: x.classroom, profile: x.profile, usersWaiting: usersWaiting(x.lobby) });
const mapDispatch = { sendEnterClassroom, sendChatMessage, sendAdmittance, receiveChatMessage };
export default connect(mapState, mapDispatch)(ClassroomPage);
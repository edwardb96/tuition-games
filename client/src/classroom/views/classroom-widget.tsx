import { UserInfo } from "@cat-tuition/protocol/user";
import { ProfileImage } from "../../profile-image/profile-image";
import React from "react";
import { Link } from "react-router-dom";

const makeStatusString = (props: ClassroomWidgetProps) => {
  if (props.isActive) {
    switch (props.tutees.length) {
      case 0: return "No Tutees Present";
      case 1: return "1 Tutee Present";
      default: return `${props.tutees.length} Tutees Present`;
    }
  } else {
    return "Not Yet Started";
  }
}

const TuteeRow = ({ displayName, favouriteColour }: UserInfo) => {
  return (
    <tr className="tutee-row">
      <td>
        <ProfileImage size={24} displayName={displayName} colour={favouriteColour}/>
      </td>
      <td>
        {displayName}
      </td>
    </tr>
  );
};

export interface ClassroomWidgetProps {
  isActive: boolean;
  tutor: UserInfo;
  tutees: UserInfo[];
  isOwn: boolean;
  id: string;
}

export const ClassroomWidget = (props: ClassroomWidgetProps) => {
  const { isOwn, tutor, tutees } = props;
  const { favouriteColour, displayName } = tutor;
  return (
    <div className="column is-4">
      <div className="card classroom-widget equal-height">
        <div className="card-content">
          <div className="tutor media">
            <ProfileImage className="media-left" size={48} displayName={displayName} colour={favouriteColour}/>
            <div className="media-content">
              <p className="title is-4">{tutor.displayName}'s Class</p>
              <p className="subtitle is-6">{makeStatusString(props)}</p>
            </div>
          </div>
          <table className="table is-fullwidth">
            <colgroup>
              <col className="profile-image-col" />
            </colgroup>
            <tbody>
              {tutees.map(tutee => <TuteeRow key={tutee.id} {...tutee} />)}
            </tbody>
          </table>
        </div>
        <footer className="card-footer">
          {isOwn ?
            <Link to={`/classroom/${props.tutor.id}`} className="card-footer-item">Start Class</Link>
            : <Link to={`/classroom/${props.tutor.id}`} className="card-footer-item">Join Class</Link>}
        </footer>
      </div>
    </div>
  );
}
import React, { ChangeEvent, KeyboardEvent, useState } from "react";
import ScrollableFeed from "react-scrollable-feed";
import map from "iter-tools/methods/map";

import { RecievedChatMessageInfo } from "@cat-tuition/protocol/channels/classroom";
import { Role, UserInfo } from "@cat-tuition/protocol/user";
import { ProfileImage } from "../../profile-image/profile-image";
import { Classroom } from "../classroom";

interface ChatMessageProps {
  sender: UserInfo;
  message: string;
}

const ChatMessage = (props: ChatMessageProps) => {
  const { sender, message } = props;
  return (
    <article className="chat-message media">
      <ProfileImage className="media-left"
        displayName={sender.displayName}
        colour={sender.favouriteColour}
        size={48} />
      <div className="media-content">
        <div className="content">
          <p className="sender">
            <strong>{sender.displayName}</strong> <small>{Role[props.sender.role]}</small>
          </p>
          {message.split('\n\n')
            .map((p, i) => <p key={i}>{
              p.split('\n').map((s, i) =>
                <React.Fragment key={i}>
                  {s}<br/>
                </React.Fragment>)}</p>)}
        </div>
      </div>
    </article>
  );
}

interface MessageFeedProps {
  user: UserInfo;
  classroom: Classroom;
}

const MessageFeed = (props: MessageFeedProps) => {
  const { user, classroom } = props;
  return (
    <ScrollableFeed className="messages">
      <ChatMessage key={0} sender={user} message="Joined the class"/>
      {[...map(({ recievedAt, senderId, chatMessage}: RecievedChatMessageInfo) =>
        <ChatMessage
          key={recievedAt.getTime()}
          sender={classroom.userInfo(senderId)}
          message={chatMessage}/>, classroom.chatHistory)]}
    </ScrollableFeed>
  );
}

interface MessageComposerProps {
  user: UserInfo;
  onSendMessage(message: string): void
}

const MessageComposer = (props: MessageComposerProps) => {
  const [state, setState] = useState({ message: '' });
  const { user } = props;

  const onSendMessage = () => {
    if (state.message.trim().length > 0) {
      props.onSendMessage(state.message);
      setState({ message: '' });
    }
  };

  const onChangeMessage = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setState({ message: e.target.value });
  };

  const onKeyPressed = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === 'Enter') {
      if (e.ctrlKey) {
        onSendMessage();
        e.preventDefault();
      }
    }
  }

  return (
    <article className="compose media">
      <ProfileImage
        className="media-left"
        displayName={user.displayName}
        colour={user.favouriteColour}
        size={48} />
      <section className="media-content">
        <div className="field">
          <p className="control">
            <textarea
              onChange={onChangeMessage}
              onKeyPress={onKeyPressed}
              className="textarea"
              placeholder="Write your message here..."
              rows={2}
              value={state.message}/>
          </p>
        </div>
        <nav className="level">
          <div className="level-left">
            <div className="level-item">
              <button onClick={onSendMessage} className="button is-primary">Send</button>
            </div>
            <div className="level-item">
              <p>Press Ctrl + Enter to send</p>
            </div>
          </div>
        </nav>
      </section>
    </article>
  );
}

export interface ChatWidgetProps extends MessageComposerProps, MessageFeedProps { };

export const ChatWidget = (props: ChatWidgetProps) => {
  const { user, classroom, onSendMessage } = props;
  return (
    <div className="chat-widget">
      <MessageFeed user={user} classroom={classroom}/>
      <MessageComposer user={user} onSendMessage={onSendMessage}/>
    </div>
  );
}
import { valdiateClassroomServerPayload } from "@cat-tuition/protocol/channels/classroom";
import { ThunkDispatch, Channel } from "../redux-utils";
import { activityChanged,
         classEnded,
         tuteeAdded,
         tuteeRemoved,
         tuteeEnteredClass,
         tutorEnteredClass,
         ClassroomAction,
         receiveChatMessage } from "./actions";

export class ClassroomChannel implements Channel {
  onReceive(payload: any, next: ThunkDispatch<ClassroomAction>) {
    valdiateClassroomServerPayload(payload).match({
      Some: validPayload => {
        switch (validPayload.kind) {
          case 'activity-changed':
            next(activityChanged(validPayload.activityId));
            break;
          case 'tutee-added':
            next(tuteeAdded(validPayload));
            break;
          case 'tutee-removed':
            next(tuteeRemoved(validPayload.userId));
            break;
          case 'receive-chat':
            next(receiveChatMessage(validPayload));
            break;
          case 'class-ended':
            next(classEnded());
            break;
          case 'tutee-entered-classroom':
            next(tuteeEnteredClass(validPayload));
            break;
          case 'tutor-entered-classroom':
            next(tutorEnteredClass(validPayload));
            break;
        }
      },
      None: () => {
        console.log('Invalid classroom payload from server', payload);
      }
    });
  }
}
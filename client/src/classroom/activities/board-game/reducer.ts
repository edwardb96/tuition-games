import { HashMap, Option } from "prelude-ts";
import { TUTEE_ENTERED_CLASS, WelcomeTuteeAction, TUTOR_ENTERED_CLASS, WelcomeTutorAction } from "../../actions";
import { State, initialState, StateTag } from "./state";
import {
  BEGIN,
  BEGIN_SPECTATE,
  CONFIGURE,
  PIECE_MOVED,
  DICE_ROLLED,
  SURRENDERED,
  BoardGameAction,
  LOCKED_BOARD,
  UNLOCKED_BOARD
} from "./actions";

export const reducer = (state: State = initialState, action: WelcomeTuteeAction | WelcomeTutorAction | BoardGameAction): State => {
  switch (action.type) {
    case CONFIGURE:
      return { ...state, initialConfig: Option.of(action.payload) }
    case TUTEE_ENTERED_CLASS:
      return initialState;
    case TUTOR_ENTERED_CLASS:
      return initialState;
    case BEGIN: {
      const { title, board, counters, counterSize, diceColour, userPlayer } = action.payload;
      return {
        ...state,
        tag: StateTag.Player,
        counters: HashMap.ofObjectDictionary(counters),
        lastRoll: Option.none(),
        boardIsLocked: false,
        diceColour,
        title,
        board,
        counterSize,
        userPlayer
      }
    }
    case BEGIN_SPECTATE: {
      const { title, board, counters, counterSize, diceColour } = action.payload;
      console.log({ counters });
      return {
        ...state,
        tag: StateTag.Spectator,
        counters: HashMap.ofObjectDictionary(counters),
        lastRoll: Option.none(),
        boardIsLocked: false,
        diceColour,
        title,
        board,
        counterSize
      }
    }
    case PIECE_MOVED: {
      const { player, position } = action.payload;
      if (state.tag != StateTag.PreBegin) {
        const oldCounter = state.counters.get(player)
          .getOrThrow('Tried to move piece for player not currently in the game');
        const newCounter = { ...oldCounter, position };
        return {
          ...state,
          counters: state.counters.put(player, newCounter)
        }
      } else {
        return state;
      }
    }
    case DICE_ROLLED: {
      if (state.tag != StateTag.PreBegin) {
        return {
          ...state,
          lastRoll: Option.of(action.payload)
        }
      } else {
        return state;
      }
    }
    case LOCKED_BOARD: {
      if (state.tag != StateTag.PreBegin) {
        return { ...state, boardIsLocked: true }
      } else {
        return state;
      }
    }
    case UNLOCKED_BOARD: {
      if (state.tag != StateTag.PreBegin) {
        return { ...state, boardIsLocked: false }
      } else {
        return state;
      }
    }
    case SURRENDERED: {
      if (state.tag != StateTag.PreBegin) {
        const player = action.payload;
        return {
          ...state,
          counters: state.counters.remove(player)
        }
      } else {
        return state;
      }
    }
    default:
      return state;
  }
}

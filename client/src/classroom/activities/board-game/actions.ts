import { Action, TagAction } from "../../../redux-utils";
import { Action as ReduxAction } from "redux";
import { send as sendWs } from "@giantmachines/redux-websocket";
import { BOARD_GAME_CHANNEL, PlayerBeginInfo, SpectatorBeginInfo, ClientPayload } from "@cat-tuition/protocol/channels/board-game";
import { Player, Position, BeginInfo, PieceMovedInfo, DiceRoll, DiceRollMetadata } from "@cat-tuition/protocol/board-game";

export const CONFIGURE = 'GAMES::BOARD-GAME::CONFIGURE';
export const BEGIN = 'GAMES::BOARD-GAME::BEGIN';
export const BEGIN_SPECTATE = 'GAMES::BOARD-GAME::BEGIN_SPECTATE';
export const PIECE_MOVED = 'GAMES::BOARD-GAME::MOVE';
export const DICE_ROLLED = 'GAMES::BOARD-GAME::DICE_ROLLED';
export const SURRENDERED = 'GAMES::BOARD-GAME::SURRENDERED';
export const LOCKED_BOARD = 'GAMES::BOARD-GAME::LOCK_BOARD';
export const UNLOCKED_BOARD = 'GAMES::BOARD-GAME::UNLOCK_BOARD';

export interface BeginAction extends Action<typeof BEGIN, PlayerBeginInfo> { };
export interface BeginSpectateAction extends Action<typeof BEGIN_SPECTATE, SpectatorBeginInfo> { };

export interface PieceMovedAction extends Action<typeof PIECE_MOVED, PieceMovedInfo> { };
export interface DiceRolledAction extends Action<typeof DICE_ROLLED, DiceRoll & DiceRollMetadata> { };
export interface SurrenderedAction extends Action<typeof SURRENDERED, Player> { };
export interface ConfigureAction extends Action<typeof CONFIGURE, BeginInfo> { };

export interface LockedBoardAction extends TagAction<typeof LOCKED_BOARD> { };
export interface UnlockedBoardAction extends TagAction<typeof UNLOCKED_BOARD> { };

export type BoardGameAction =
  ConfigureAction
  | BeginAction
  | BeginSpectateAction
  | SurrenderedAction
  | PieceMovedAction
  | DiceRolledAction
  | LockedBoardAction
  | UnlockedBoardAction;

export const configure = (info: BeginInfo): ConfigureAction =>
  ({ type: CONFIGURE, payload: info })

export const begin = (info: PlayerBeginInfo): BeginAction =>
  ({ type: BEGIN, payload: info });

export const beginSpectate = (info: SpectatorBeginInfo): BeginSpectateAction =>
  ({ type: BEGIN_SPECTATE, payload: info });

export const surrendered = (player: Player) =>
  ({ type: SURRENDERED, payload: player })

export const pieceMoved = (player: Player, position: Position): PieceMovedAction =>
  ({ type: PIECE_MOVED, payload: { player, position } });

export const diceRolled = (player: Player, value: number): DiceRolledAction =>
  ({ type: DICE_ROLLED, payload: { player, value, when: new Date() } });

export const lockedBoard = (): LockedBoardAction =>
  ({ type: LOCKED_BOARD });

export const unlockedBoard = (): UnlockedBoardAction =>
  ({ type: UNLOCKED_BOARD });


export const sendDiceRoll = (): ReduxAction =>
  sendBoardGame({ kind: 'dice-roll' });

export const sendPieceMove = (position: Position, player?: Player): ReduxAction =>
  sendBoardGame({ kind: 'piece-move', position, player });

export const sendLockBoard = (): ReduxAction =>
  sendBoardGame({ kind: 'lock-board' });

export const sendUnlockBoard = (): ReduxAction =>
  sendBoardGame({ kind: 'unlock-board' });

export const sendBoardGame = (payload: ClientPayload): ReduxAction =>
  sendWs({ channel: BOARD_GAME_CHANNEL, payload: payload });

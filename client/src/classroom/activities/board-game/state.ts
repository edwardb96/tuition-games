import { HashMap, Option } from "prelude-ts";
import { Position, Player, BeginInfo, DiceRoll, Counter, DiceRollMetadata } from "@cat-tuition/protocol/board-game";
import { ImageId } from "@cat-tuition/protocol/image";
import { SimpleColour } from "@cat-tuition/protocol/colour";

export interface StateBase {
  initialConfig: Option<BeginInfo>;
  counters: HashMap<Player, Counter>;
}

export enum StateTag {
  PreBegin,
  Player,
  Spectator
}

export interface PlayerState extends StateBase {
  tag: StateTag.Player;
  board: ImageId;
  title: string;
  boardIsLocked: boolean;
  counterSize: number;
  diceColour: SimpleColour;
  lastRoll: Option<DiceRoll & DiceRollMetadata>;
  userPlayer: string;
}

export interface SpectatorState extends StateBase {
  tag: StateTag.Spectator;
  board: ImageId;
  title: string;
  boardIsLocked: boolean;
  counterSize: number;
  diceColour: SimpleColour;
  lastRoll: Option<DiceRoll & DiceRollMetadata>;
}

export interface PreBeginState extends StateBase {
  tag: StateTag.PreBegin;
}

export type State =
  PreBeginState |
  PlayerState |
  SpectatorState;

export const initialState: State = {
  tag: StateTag.PreBegin,
  counters: HashMap.empty<Player, Counter>(),
  initialConfig: Option.none(),
};
import { SimpleColour } from "@cat-tuition/protocol/colour";
import { ProfileImage } from "../../../../profile-image/profile-image";
import React from "react";
import Racecar from "../../../../assets/racecar.t.svg";
import Cat from "../../../../assets/cat.t.svg";
import Jet from "../../../../assets/jet.t.svg";
import Boot from "../../../../assets/boot.t.svg";
import Tank from "../../../../assets/tank.t.svg";
import Snail from "../../../../assets/snail.t.svg";
import Tortoise from "../../../../assets/tortoise.t.svg";
import './counter.scss';
import { CounterIcon } from "@cat-tuition/protocol/board-game";


export interface CounterProps {
  displayName: string;
  colour: SimpleColour;
  icon: CounterIcon;
  size: number;
}

export const Counter = (props: CounterProps) => {
  const { colour, icon, displayName, size } = props;
  switch (icon) {
    case CounterIcon.Puck:
      return (
        <ProfileImage
          displayName={displayName}
          colour={colour}
          size={size * 0.95}/>
      );
    case CounterIcon.Racecar:
      return <Racecar className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Cat:
      return <Cat className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Jet:
      return <Jet className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Boot:
      return <Boot className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Tank:
      return <Tank className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Snail:
      return <Snail className={`counter-image is-${colour}-coloured`} width={size}/>;
    case CounterIcon.Tortoise:
      return <Tortoise className={`counter-image is-${colour}-coloured`} width={size}/>;
    default:
      throw new Error(`Unrecognised counter icon ${icon}`);
  }
}
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { AppState } from "../../../../redux-store";
import { sendChangeActivity } from "../../../actions";
import { Classroom } from "../../../classroom";
import { ErrorMessage, Field, Form, Formik, FormikHelpers, FormikProps, FormikValues } from "formik";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import { UserInfo } from "@cat-tuition/protocol/user";
import { BeginInfo, Board, boardGameBeginInfoSchema, BOARD_GAME_ACTIVITY_ID, CounterIcon, Counters, Player, Position } from "@cat-tuition/protocol/board-game";
import { form as joiToForms } from "joi-errors-for-forms";
import { configure } from "../actions";
import { CheckboxItem } from "../../../../checkbox-list/checkbox-item";
import map from "iter-tools/methods/map";
import reduce from "iter-tools/methods/reduce";
import { ImagePicker, useImagePicker } from "../../../../image-picker/views/image-picker-field";
import ImagePickerModal, { ImagePickerTab } from "../../../../image-picker/views/image-picker-modal";
import { BoardGameBoard } from "./board";
import { HashMap } from "prelude-ts";
import { ColourPalettePicker } from "../../../../colour-palette-picker/colour-palette-picker";
import { CounterIconPicker } from "./counter-picker";
import './setup.scss';
import BoardSelector from "../../../../boards/views/board-selector";
import { ImageId } from "@cat-tuition/protocol/image";
import { SimpleColour } from "@cat-tuition/protocol/colour";
const convertToForms = joiToForms();

export interface BoardGameSetupProps {
  classroom: Classroom;
  sendChangeActivity(info: ActivityBeginInfo): void;
  configure(config: BeginInfo): void;
}

interface BeginInfoEdits {
  title: string | null;
  counters: Counters;
  counterSize: number;
  diceColour: SimpleColour;
  board: ImageId | null;
}

const validateForm = (values: BeginInfoEdits) => {
  console.log("Validating...", values);
  const result = boardGameBeginInfoSchema.validate(values);
  if (result.error != null) {
    const errors = convertToForms(result.error);
    console.log(errors);
    return errors;
  } else {
    console.log("no errors!");
    return null;
  }
};

interface BoardGameSetupState {
  counters: Counters;
}

const makeRandomCounterPositions = (users: Iterable<UserInfo>, lastValues: Counters) =>
  reduce(lastValues,
    (v : Counters, u: UserInfo) => {
      if (v[u.id] != null) {
        return v;
      } else {
        return { ...v, [u.id]: {
          // TODO fix coordinates based on the counter size
          position: { x: 0.25 + (Math.random() / 2), y: 0.25 + (Math.random() / 2) },
          icon: CounterIcon.Puck,
          colour: u.favouriteColour
        } };
      }
    }, users);


const BoardGameSetup = (props: BoardGameSetupProps) => {
  const { classroom, sendChangeActivity, configure } = props;

  const occupants = [...classroom.occupants()];

  const [state, setState] = useState<BoardGameSetupState>({
    counters: makeRandomCounterPositions(occupants, {})
  });

  const { counters: defaults } = state;

  useEffect(() => {
    setState({ counters: makeRandomCounterPositions(occupants, defaults) });
  }, [occupants.length])

  const onSubmit = (info: BeginInfoEdits, { setSubmitting }: FormikHelpers<BeginInfoEdits>) => {
    console.log(info);
    if (info.board != null) {
      sendChangeActivity({ activityId: BOARD_GAME_ACTIVITY_ID, info: info as BeginInfo })
      configure(info as BeginInfo);
      setSubmitting(false);
    }
  };

  const { title, isOpen, selectedTab, selectedImage,
    onPickImage, onPickedImage, onCancelPick } =
      useImagePicker({
        isOpen: false,
        selectedTab: ImagePickerTab.FromFile
      });

  const initialValues: BeginInfoEdits = {
    title: null,
    counters: {...defaults},
    counterSize: 0.1,
    diceColour: SimpleColour.White,
    board: selectedImage
  };

  return (
    <>
      <div className="card-content">
        <ImagePickerModal
          isOpen={isOpen}
          onSubmit={onPickedImage}
          onCancel={onCancelPick}
          title={title}
          selectedTab={selectedTab}
          selectedImage={selectedImage}/>
        <Formik
          initialValues={initialValues}
          validate={validateForm}
          onSubmit={onSubmit}>
          {
            ({ isSubmitting, errors, touched, values, setFieldValue }: FormikProps<BeginInfoEdits>) => {

              const renderUserOptions = (user: UserInfo) => {
                const currentCounter =
                  values.counters.hasOwnProperty(user.id)
                    ? values.counters[user.id]
                    : null;
                return (
                  <div key={user.id} className="field player-field is-horizontal has-addons">
                    <div className="control is-expanded">
                      <Field component={CheckboxItem}
                        name="counters"
                        index={user.id}
                        value={defaults[user.id]}
                        label={user.displayName} />
                    </div>
                    {currentCounter != null ?
                      <>
                        <div className="control">
                          <CounterIconPicker
                            id={user.id}
                            name={`counters["${user.id}"].icon`}
                            colour={currentCounter != null ? currentCounter.colour : user.favouriteColour}
                            displayName={user.displayName}/>
                        </div>
                        <ColourPalettePicker id={user.id} name={`counters["${user.id}"].colour`} />
                      </> : <></>}
                  </div>
                );
              }

              const onChangeBoard = (board: Board) => {
                setFieldValue("title", board.title);
                setFieldValue("counterSize", board.counterSize);
                setFieldValue("board", board.image);
              }

              return (
                <Form className="form">
                  <h1 className="title">
                    Setup a new {values.title != null ? `game of ${values.title}` : 'board game'}
                  </h1>
                  <p className="subtitle">
                    Select the players and the board
                  </p>

                  <div className="field">
                    <label className="label">Players</label>
                    {map((user, i) =>
                      renderUserOptions(user),
                      occupants)}
                  </div>

                  <div className="field">
                    <label className="label">Board</label>
                    <BoardSelector onChange={onChangeBoard} />
                    <ErrorMessage name="board" render={msg => <p className="help is-danger">{msg}</p>} />
                  </div>

                  <div className="field">
                    <label className="label">Counter Size</label>
                    <Field name="counterSize" className="slider is-fullwidth" step="0.001" min="0" max="0.5" type="range"/>
                    <ErrorMessage name="counterSize" render={msg => <p className="help is-danger">{msg}</p>} />
                  </div>

                  <div className="field">
                    <label className="label">Dice Colour</label>
                    <ColourPalettePicker id="board-dice-colour" name="diceColour" />
                    <ErrorMessage name="diceColour" render={msg => <p className="help is-danger">{msg}</p>} />
                  </div>

                  <div className="field">
                    {values.board != null ?
                      <BoardGameBoard
                        boardImage={values.board}
                        counterSize={values.counterSize}
                        counters={HashMap.ofObjectDictionary(values.counters)}
                        userInfo={id => classroom.userInfo(id)}
                        onMoveCounter={(id, position) => { setFieldValue(`counters[${id}].position`, position) }}
                        canMoveCounter={u => true}/> : <></>}
                  </div>

                  <div className="field">
                    <div className="control">
                      <button className="button is-link" type="submit" disabled={isSubmitting}>
                        Start Game
                      </button>
                    </div>
                  </div>
                </Form>
              );
            }
          }
        </Formik>
      </div>
    </>
  );
};

const mapState = ({ classroom }: AppState) => ({
  classroom: classroom.getOrThrow('Attempt to render activity when no classroom active')
});

export default connect(mapState, { sendChangeActivity, configure })(BoardGameSetup);
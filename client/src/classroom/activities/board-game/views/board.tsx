import React, { useRef, useState } from "react"
import { HashMap } from "prelude-ts"

import { Counter as CounterInfo, CounterIcon, Position } from "@cat-tuition/protocol/board-game"
import map from "iter-tools/methods/map";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { UserInfo } from "@cat-tuition/protocol/user";
import { Counter } from "./counter";
import useMeasure from "use-measure";

export interface BoardGameBoardProps {
  boardImage: string;
  counterSize: number;
  counters: HashMap<string, CounterInfo>;
  userInfo(userId: string): UserInfo;
  canMoveCounter(userId: string): boolean;
  onMoveCounter(userId: string, position: Position): void;
}

interface BoardGameState {
  isDragging: boolean;
}

export const BoardGameBoard = (props: BoardGameBoardProps) => {
  const { boardImage, counters, counterSize, userInfo, canMoveCounter, onMoveCounter } = props;

  const [state, setState] = useState<BoardGameState>({ isDragging: false });
  const boardRef = useRef<HTMLDivElement | null>(null);
  const measurement = useMeasure(boardRef);
  const denormalizedCounterSize = counterSize * measurement.width;

  const clampAndNormalizePosition = ({ x, y }: Position) =>
    ({ x: Math.max(0, x / measurement.width), y: Math.max(0, y / measurement.height) });

  const denormalizePosition = ({ x, y }: Position) =>
    ({ x: x * measurement.width, y: y * measurement.height });

  const onStart = (e: DraggableEvent, data: DraggableData, userId: string) => {
    setState({ ...state, isDragging: true });
    onMoveCounter(userId, clampAndNormalizePosition(data));
  };

  const onStop = (e: DraggableEvent, data: DraggableData, userId: string) => {
    setState({ ...state, isDragging: false });
    onMoveCounter(userId, clampAndNormalizePosition(data));
  };

  const onDrag = (e: DraggableEvent, data: DraggableData, userId: string) => {
    onMoveCounter(userId, clampAndNormalizePosition(data));
  };

  const { isDragging } = state;

  return (
    <section className="board-game">
      <img draggable="false" className="board-image" src={`/api/images/${boardImage}.png`} />
      <div ref={boardRef} className="board-counters">
        {map(([player, counter]) => {
          const { position, icon, colour } = counter;
          const canMove = canMoveCounter(player);
          return (
            <Draggable
              key={player}
              bounds="parent"
              axis="both"
              position={canMove && isDragging ? undefined : denormalizePosition(position) }
              onDrag={(e, d) => onDrag(e, d, player)}
              onStart={(e, d) => onStart(e, d, player)}
              onStop={(e, d) => onStop(e, d, player)}
              disabled={!canMove}>
              <div style={{ width: denormalizedCounterSize }} className="counter">
                <Counter
                  displayName={userInfo(player).displayName}
                  icon={icon}
                  colour={colour}
                  size={denormalizedCounterSize}/>
              </div>
            </Draggable>
          );
        }, counters)}
      </div>
    </section>
  );
}
import { BeginInfo as BoardGameBeginInfo, BOARD_GAME_ACTIVITY_ID, Player, Position } from "@cat-tuition/protocol/board-game";
import React, { useEffect, useRef } from "react";
import ReactDice from "react-dice-complete";
import './game.scss';
import { AppState } from "client/src/redux-store";
import { connect } from "react-redux";
import { State, StateTag } from "../state";
import { sendPieceMove, sendDiceRoll, configure, sendLockBoard, sendUnlockBoard } from "../actions";
import { sendChangeActivity } from "../../../actions";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import cx from "classnames";
import { BoardGameBoard } from "./board";
import "react-dice-complete/dist/react-dice-complete.css"
import useSound from "use-sound";
import { asDarkenedHexColour, asHexColour, asStrongInverseHexColour, SimpleColour } from "@cat-tuition/protocol/colour";

const rollSound = '/dicebgmusic.mp3';
const revealSound = '/dicereveal.mp3';

export interface BoardGameProps {
  user: User;
  userInfo(userId: string): UserInfo;
  isInOwnClass: boolean;
  configure(config: BoardGameBeginInfo): void;
  sendChangeActivity(info: ActivityBeginInfo): void

  sendLockBoard(): void;
  sendUnlockBoard(): void;
  sendPieceMove(position: Position, player?: Player): void;
  sendDiceRoll(): void;

  gameState: State;
}

export const BoardGame = (props: BoardGameProps) => {
  const { sendPieceMove, sendLockBoard, sendUnlockBoard, sendDiceRoll,
          isInOwnClass, userInfo, gameState, user } = props;

  const onStop = () => {
    const { sendChangeActivity } = props
    sendChangeActivity({ activityId: null });
  }

  const canRematch = () =>
    gameState.tag != StateTag.PreBegin;

  const onRematch = () => {
    const { configure, sendChangeActivity } = props;
    if (canRematch() && gameState.initialConfig.isSome()) {
      const info = gameState.initialConfig.get();
      configure(info);
      sendChangeActivity({ activityId: BOARD_GAME_ACTIVITY_ID, info });
    }
  }

  const cardFooter = (() => {
    if (isInOwnClass) {
      const isLocked = gameState.tag != StateTag.PreBegin && gameState.boardIsLocked;
      return (
        <div className="card-footer">
          <a onClick={() => onStop()} className="card-footer-item">Stop</a>
          <a onClick={() => onRematch()}
              className={cx("card-footer-item", {'is-disabled': !canRematch()})}>
            Rematch
          </a>
          {isLocked ?
            <a onClick={() => sendUnlockBoard()} className="card-footer-item">
              Unlock
            </a> :
            <a onClick={() => sendLockBoard()} className="card-footer-item">
              Lock
            </a>}
        </div>);
    } else {
      return (<></>);
    }
  })();

  const dice = useRef<any>(null);
  const hasRoll = gameState.tag != StateTag.PreBegin && gameState.lastRoll.isSome();
  const lastRolledWhen = gameState.tag != StateTag.PreBegin ? gameState.lastRoll.map(dr => dr.when).getOrElse(new Date()) : null;

  useEffect(() => {
    if (hasRoll) {
      gameState.lastRoll.ifSome(roll => {
        dice.current.rollAll([roll.value]);
      });
    }
  }, [hasRoll, lastRolledWhen]);

  const [playRoll, { stop: stopRoll }] = useSound(rollSound);
  const [playReveal, { stop: stopReveal }] = useSound(revealSound);

  if (gameState.tag != StateTag.PreBegin) {

    const onMoveCounter = (player: Player, position: Position) => {
      if (player == user.id) {
        sendPieceMove(position)
      } else {
        sendPieceMove(position, player);
      }
    }

    const onRoll = (num: number, y: number) => {
      stopRoll();
      playReveal()
    }

    const onRollRequested = () => {
      if (!gameState.boardIsLocked || isInOwnClass) {
        stopReveal();
        playRoll();
        sendDiceRoll();
      }
    }

    return (
      <>
        <div className="card-content">
          <p className="title">{gameState.title}</p>
          <BoardGameBoard
            counterSize={gameState.counterSize}
            boardImage={gameState.board}
            counters={gameState.counters}
            userInfo={userInfo}
            onMoveCounter={onMoveCounter}
            canMoveCounter={u => gameState.boardIsLocked ? isInOwnClass : u == user.id}/>
          <div onClick={onRollRequested} className="dice-panel">
            <ReactDice
              ref={dice}
              numDice={1}
              disableIndividual={true}
              rollDone={onRoll}
              faceColor={asHexColour(gameState.diceColour)}
              dotColor={asStrongInverseHexColour(gameState.diceColour)}
              outlineColor={asDarkenedHexColour(gameState.diceColour)}
              outline={true}/>
          </div>
        </div>
        {cardFooter}
      </>
    );
  } else {
    return (
      <>
        <div className="card-content">
          <p className="title">Board Game</p>
          <p className="subtitle">Waiting for the game to start...</p>
        </div>
        {cardFooter}
      </>
    );
  }
}

const mapState = ({ boardGame, profile, classroom: maybeClassroom }: AppState) => {
  const classroom = maybeClassroom.getOrThrow('Tried to render game when not in classroom');
  if (profile.user != null) {
    return ({
      gameState: boardGame,
      user: profile.user,
      userInfo: (u: string) => classroom.userInfo(u),
      isInOwnClass: profile.user != null ? classroom.isTutorOfClass(profile.user.id) : false });
  } else {
    throw new Error("User not set when attempting to mount game");
  }
}
export default connect(mapState, { sendPieceMove, sendDiceRoll, sendLockBoard, sendUnlockBoard, sendChangeActivity, configure })(BoardGame);
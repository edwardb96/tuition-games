import { CounterIcon } from "@cat-tuition/protocol/board-game";
import { SimpleColour } from "@cat-tuition/protocol/colour";
import objectKeys from "iter-tools/methods/object-keys";
import map from "iter-tools/methods/map";
import React from "react";
import { Field } from "formik";
import { Counter } from "./counter";
import './counter-picker.scss';

export interface CounterIconPicker {
  id: string;
  name: string;
  displayName: string;
  colour: SimpleColour;
}

export const CounterIconPicker = ({ id, name, colour, displayName } : CounterIconPicker) => {
  return (
    <div className="control counter-picker" role="group">
      {map((icon: keyof typeof CounterIcon) => {
        const counterIcon = CounterIcon[icon];
        return (
          <React.Fragment key={counterIcon}>
            <Field id={`${id}-${counterIcon}`} className="counter-radio-input" type="radio" name={name} value={counterIcon} />
            <label htmlFor={`${id}-${counterIcon}`} className="counter-radio-label">
              <Counter
                displayName={displayName}
                colour={colour}
                icon={counterIcon}
                size={32} />
            </label>
          </React.Fragment>
        );
      }, objectKeys(CounterIcon) as Iterable<keyof typeof CounterIcon>)}
    </div>
  );
};

// <label htmlFor={`${id}-${icon}`}>{icon.split('-').map(withUpperFirst).join(' ')}</label>
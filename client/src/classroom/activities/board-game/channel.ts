import { valdiateServerPayload } from "@cat-tuition/protocol/channels/board-game";
import { Action as ReduxAction, Dispatch } from "redux";
import { Channel } from "../../../redux-utils";
import { begin, beginSpectate, surrendered, diceRolled, pieceMoved, lockedBoard, unlockedBoard } from "./actions";

export class BoardGameChannel implements Channel {
  onReceive(payload: unknown, next: Dispatch<ReduxAction<any>>) {
    valdiateServerPayload(payload).match({
      Some: validPayload => {
        switch (validPayload.kind) {
          case 'begin':
            next(begin(validPayload));
            break;
          case 'begin-spectate':
            next(beginSpectate(validPayload));
            break;
          case 'dice-rolled':
            next(diceRolled(validPayload.player, validPayload.value));
            break;
          case 'piece-moved':
            next(pieceMoved(validPayload.player, validPayload.position));
            break;
          case 'surrendered':
            next(surrendered(validPayload.player));
            break;
          case 'locked-board':
            next(lockedBoard())
            break;
          case 'unlocked-board':
            next(unlockedBoard())
            break;
        }
      },
      None: () => {
        console.log('Invalid board-game payload from server', payload);
      }
    });
  }
}
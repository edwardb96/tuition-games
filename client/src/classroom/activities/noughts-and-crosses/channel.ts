import { valdiateServerPayload } from "@cat-tuition/protocol/channels/noughts-and-crosses";
import { Action as ReduxAction, Dispatch } from "redux";
import { Channel } from "../../../redux-utils";
import { begin, draw, win, drawMove, loseMove, move, beginSpectate, opponentSurrendered, surrendered } from "./actions";

export class NoughtsAndCrossesChannel implements Channel {
  onReceive(payload: any, next: Dispatch<ReduxAction<any>>) {
    valdiateServerPayload(payload).match({
      Some: validPayload => {
        switch (validPayload.kind) {
          case 'begin':
            next(begin(validPayload.player, validPayload.nextPlayer, validPayload.opponent, validPayload.boardColour));
            break;
          case 'begin-spectate':
            next(beginSpectate(validPayload));
            break;
          case 'draw':
            next(draw());
            break;
          case 'win':
            next(win());
            break;
          case 'draw-move':
            next(drawMove(validPayload.position));
            break;
          case 'move':
            next(move(validPayload.position));
            break;
          case 'lose-move':
            next(loseMove(validPayload.position));
            break;
          case 'opponent-surrendered':
            next(opponentSurrendered());
            break;
          case 'surrendered':
            next(surrendered(validPayload.player));
            break;
        }
      },
      None: () => {
        console.log('Invalid tic-tac toe payload from server', payload);
      }
    });
  }
}
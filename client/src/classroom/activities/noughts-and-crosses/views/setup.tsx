import React from "react";
import { connect } from "react-redux";
import { AppState } from "../../../../redux-store";
import { sendChangeActivity, activityChanged } from "../../../actions";
import { Classroom } from "../../../classroom";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import cx from "classnames";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import { UserInfo } from "@cat-tuition/protocol/user";
import { NOUGHTS_AND_CROSSES_ACTIVITY_ID, BeginInfo, noughtsAndCrossesBeginInfoSchema, Player } from "@cat-tuition/protocol/noughts-and-crosses";
import { form as joiToForms } from "joi-errors-for-forms";
import { configure } from "../actions";
import take from "iter-tools/methods/take";
import map from "iter-tools/methods/map";
import { ColourPalettePicker } from "../../../../colour-palette-picker/colour-palette-picker";
import { SimpleColour } from "@cat-tuition/protocol/colour";
const convertToForms = joiToForms();

export interface NoughtsAndCrossesSetupProps {
  classroom: Classroom;
  sendChangeActivity(info: ActivityBeginInfo): void;
  configure(config: BeginInfo): void;
}

const validateForm = (values: any) => {
  console.log("Validating...", values);
  const result = noughtsAndCrossesBeginInfoSchema.validate(values);
  if (result.error != null) {
    const errors = convertToForms(result.error);
    return errors;
  } else {
    return null;
  }
};

const NoughtsAndCrossesSetup = (props: NoughtsAndCrossesSetupProps) => {
  const { classroom, sendChangeActivity, configure } = props;
  const onSubmit = (info: BeginInfo, { setSubmitting }: FormikHelpers<BeginInfo>) => {
    sendChangeActivity({ activityId: NOUGHTS_AND_CROSSES_ACTIVITY_ID, info })
    configure(info);
    setSubmitting(false);
  };

  const defaultSetup: BeginInfo = (() => {
    const threeOrLessOccupants = [...map((u: UserInfo) => u.id, take(3, classroom.occupants()))];
    if (threeOrLessOccupants.length == 2) {
      const [xPlayer, oPlayer] = threeOrLessOccupants;
      return { xPlayer, oPlayer, firstPlayer: "X" as Player, boardColour: SimpleColour.White }
    } else if (threeOrLessOccupants.length == 1) {
      const [xPlayer] = threeOrLessOccupants;
      return { xPlayer, oPlayer: "", firstPlayer: "X" as Player, boardColour: SimpleColour.White }
    } else {
      return { xPlayer: "", oPlayer: "", firstPlayer: "X" as Player, boardColour: SimpleColour.White }
    }
  })();

  return (
    <>
      <div className="card-content">
        <Formik initialValues={defaultSetup}
          validate={validateForm}
          onSubmit={onSubmit}>
          {
            ({ isSubmitting, errors, touched }) => (
              <Form className="form">
                <h1 className="title">
                  Setup a new game of Noughts and Crosses
                </h1>
                <p className="subtitle">
                  Select the players
                </p>

                <div className="field">
                  <label className="label">Player for X</label>
                  <div className="control">
                    <div className={cx("select", { 'is-danger': errors.xPlayer && touched.xPlayer })}>
                      <Field type="xPlayer" name="xPlayer" as="select">
                        <option value="" disabled hidden>Choose Player...</option>
                        {Array.from(classroom.occupants()).map(user =>
                          <option key={user.id} value={user.id}>{user.displayName}</option>
                        )}
                      </Field>
                    </div>
                  </div>
                  <ErrorMessage name="xPlayer" render={msg => <p className="help is-danger">{msg}</p>} />
                </div>

                <div className="field">
                  <label className="label">Player for O</label>
                  <div className="control">
                    <div className={cx("select", { 'is-danger': errors.oPlayer && touched.oPlayer })}>
                      <Field type="oPlayer" name="oPlayer" as="select">
                        <option value="" disabled hidden>Choose Player...</option>
                        {Array.from(classroom.occupants()).map(user =>
                          <option key={user.id} value={user.id}>{user.displayName}</option>
                        )}
                      </Field>
                    </div>
                  </div>
                  <ErrorMessage name="oPlayer" render={msg => <p className="help is-danger">{msg}</p>} />
                </div>

                <div className="field">
                  <label className="label">Board Colour</label>
                  <ColourPalettePicker id="board-colour" name="boardColour" />
                </div>

                <div className="field">
                  <div className="control">
                    <button className="button is-link" type="submit" disabled={isSubmitting}>
                      Start Game
                    </button>
                  </div>
                </div>
              </Form>
            )
          }
        </Formik>
      </div>
    </>
  );
};

const mapState = ({ classroom }: AppState) => ({
  classroom: classroom.getOrThrow('Attempt to render activity when no classroom active')
});

export default connect(mapState, { sendChangeActivity, configure })(NoughtsAndCrossesSetup);
import { connect } from "react-redux";
import React from "react";
import { canTakeTurn, isGameFinished, State, StateTag } from "../state";
import { AppState } from "../../../../redux-store";
import "./game.scss";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import { nextPlayer as otherPlayer,
         Player,
         Position,
         BeginInfo,
         NOUGHTS_AND_CROSSES_ACTIVITY_ID } from "@cat-tuition/protocol/noughts-and-crosses";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import { TakeTurnAction, takeTurn, sendMove, configure } from "../actions";
import { sendChangeActivity } from "../../../actions";
import cx from "classnames";
import { SimpleColour } from "@cat-tuition/protocol/colour";

interface NoughtsAndCrossesProps {
  gameState: State;
  user: User;
  userInfo(userId: string): UserInfo;
  isInClass(userId: string): boolean;
  isInOwnClass: boolean;
  takeTurn(position: Position): TakeTurnAction;
  sendMove(player: Player, position: Position): void;
  sendChangeActivity(info: ActivityBeginInfo): void
  configure(config: BeginInfo): void;
}

interface BoardSquareProps {
  value: Player | null;
  colour: SimpleColour;
  canClickIfVacant: boolean
  onClick: () => void;
}

const BoardSquare = ({ value, onClick, canClickIfVacant, colour } : BoardSquareProps) => {
  return (
    <div className="tile is-parent">
      <div onClick={onClick}
           className={cx(`board-square tile is-child box is-${colour}-coloured`, {'is-clickable': value == null && canClickIfVacant })}>
        <svg viewBox="0 0 12 12">
          <text x="0.5" y="11.5">{value}</text>
        </svg>
      </div>
    </div>
  );
}

export class NoughtsAndCrossesGame extends React.Component<NoughtsAndCrossesProps> {
  render() {
    const { gameState, isInOwnClass } = this.props;
    const { board, boardColour } = gameState;
    const couldTakeTurn = canTakeTurn(gameState);
    const cardFooter = (() => {
      if (isInOwnClass) {
        return (
          <div className="card-footer">
            <a onClick={() => this.onStop()}
               className="card-footer-item">
              Stop</a>
            <a onClick={() => this.onReset()}
               className={cx("card-footer-item", {'is-disabled': !this.canReset()})}>
              Reset</a>
            <a onClick={() => this.onRematch()}
               className={cx("card-footer-item", {'is-disabled': !this.canRematch()})}>
              Rematch</a>
          </div>);
      } else {
        return (<></>);
      }
    })();

    const square = (i: number) =>
      (<BoardSquare onClick={() => this.handleClick(i)}
                    canClickIfVacant={couldTakeTurn}
                    colour={boardColour}
                    value={board[i]}/>);

    return (
      <>
        <div className="card-content">
          <p className="title">Noughts and Crosses</p>
          <p className="subtitle">{this.renderStatus()}</p>
          <div className="noughts-and-crosses">
            <div className="board-parent">
              <div className="board tile is-ancestor is-vertical">
                <div className="board-row tile">
                  {square(0)}
                  {square(1)}
                  {square(2)}
                </div>
                <div className="board-row tile">
                  {square(3)}
                  {square(4)}
                  {square(5)}
                </div>
                <div className="board-row tile">
                  {square(6)}
                  {square(7)}
                  {square(8)}
                </div>
              </div>
            </div>
          </div>
        </div>
        {cardFooter}
      </>
    );
  }

  renderStatus() {
    const { gameState, user, userInfo } = this.props;
    if (gameState.tag == StateTag.Player) {
      const { isDraw, isSurrender, winner, nextPlayer, userPlayer } = gameState;
      const opponentUser = userInfo(gameState.opponentPlayerId);
      if (isDraw) {
        return "It's a Draw!";
      } else if (winner != null) {
        if (winner == userPlayer) {
          if (isSurrender) {
            return `${opponentUser.displayName} surrendered, You won!`;
          } else {
            return `Well done ${user.displayName}, You won!`;
          }
        } else {
          if (isSurrender) {
            return `You surrendered, ${opponentUser.displayName} won!`
          } else {
            return `${opponentUser.displayName} won! Better luck next time.`;
          }
        }
      } else {
        if (nextPlayer == userPlayer) {
          return `It's your turn to place an ${userPlayer} piece`;
        } else {
          return `Waiting for ${opponentUser.displayName} to place an ${nextPlayer} piece...`;
        }
      }
    } else if (gameState.tag == StateTag.Spectator) {
      const { isDraw, isSurrender, winner, nextPlayer, players } = gameState;
      const userFromSymbol = (symbol : Player) => userInfo(players[symbol])
      if (isDraw) {
        return "It's a Draw";
      } else if (winner != null) {
        const winnerUser = userFromSymbol(winner);
        const loserUser = userFromSymbol(otherPlayer(winner));
        if (isSurrender) {
          return `${loserUser.displayName} surrendered to ${winnerUser.displayName}!`;
        } else {
          return `${winnerUser.displayName} won against ${loserUser.displayName}!`
        }
      } else {
        const nextUser = userFromSymbol(nextPlayer);
        return `Waiting for ${nextUser.displayName} to place an ${nextPlayer} piece...`;
      }
    } else {
      return "Waiting for the game to start...";
    }
  }

  onStop() {
    this.props.sendChangeActivity({ activityId: null });
  }

  canReset(): boolean {
    const { gameState } = this.props;
    return gameState.tag == StateTag.Player &&
             !isGameFinished(gameState);
  }

  onReset() {
    const { gameState, configure, sendChangeActivity } = this.props;
    if (this.canReset() && gameState.initialConfig.isSome()) {
      const info = gameState.initialConfig.get();
      configure(info);
      sendChangeActivity({ activityId: NOUGHTS_AND_CROSSES_ACTIVITY_ID, info });
    }
  }

  canRematch() {
    const { gameState } = this.props;
    return isGameFinished(gameState);
  }

  onRematch() {
    const { gameState, configure, sendChangeActivity } = this.props;
    if (this.canRematch() && gameState.initialConfig.isSome()) {
      const lastInitialConfig = gameState.initialConfig.get();
      const firstPlayer = lastInitialConfig.firstPlayer;
      const info = { ...lastInitialConfig, firstPlayer: otherPlayer(firstPlayer) }
      configure(info);
      sendChangeActivity({ activityId: NOUGHTS_AND_CROSSES_ACTIVITY_ID, info });
    }
  }

  handleClick(i: number) {
    const { gameState, takeTurn, sendMove } = this.props;
    if (gameState.tag == StateTag.Player) {
      const { userPlayer, nextPlayer } = gameState;
      if (userPlayer == nextPlayer && gameState.board[i] == null) {
        takeTurn(i);
        sendMove(userPlayer, i);
      }
    } else {
      console.log("User player not set");
    }
  }
}

const mapState = ({ noughtsAndCrosses, profile, classroom: maybeClassroom }: AppState) => {
  const classroom = maybeClassroom.getOrThrow('Tried to render game when not in classroom');
  if (profile.user != null) {
    return ({
      gameState: noughtsAndCrosses,
      user: profile.user,
      userInfo: (u: string) => classroom.userInfo(u),
      isInClass: (u: string) => classroom.isInClass(u),
      isInOwnClass: profile.user != null ? classroom.isTutorOfClass(profile.user.id) : false });
  } else {
    throw new Error("User not set when attempting to mount game");
  }
}
export default connect(mapState, { sendMove, sendChangeActivity, configure, takeTurn })(NoughtsAndCrossesGame);
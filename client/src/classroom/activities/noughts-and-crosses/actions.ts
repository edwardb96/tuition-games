import { Action, TagAction } from "../../../redux-utils";
import { Action as ReduxAction } from "redux";
import { send as sendWs } from "@giantmachines/redux-websocket";
import { NOUGHTS_AND_CROSSES_CHANNEL, PlayerBeginInfo, SpectatorBeginInfo, ClientPayload } from "@cat-tuition/protocol/channels/noughts-and-crosses";
import { Player, Position, MoveInfo, BeginInfo } from "@cat-tuition/protocol/noughts-and-crosses";
import { SimpleColour } from "@cat-tuition/protocol/colour";

export const CONFIGURE = 'GAMES::NOUGHTS-AND-CROSSES::CONFIGURE';
export const BEGIN = 'GAMES::NOUGHTS-AND-CROSSES::BEGIN';
export const BEGIN_SPECTATE = 'GAMES::NOUGHTS-AND-CROSSES::BEGIN_SPECTATE';
export const DRAW = 'GAMES::NOUGHTS-AND-CROSSES::DRAW';
export const DRAW_MOVE = 'GAMES::NOUGHTS-AND-CROSSES::DRAW_MOVE';
export const LOSE_MOVE = 'GAMES::NOUGHTS-AND-CROSSES::LOSE';
export const MOVE = 'GAMES::NOUGHTS-AND-CROSSES::MOVE';
export const TAKE_TURN = 'GAMES::NOUGHTS-AND-CROSSES::TAKE_TURN';
export const WIN = 'GAMES::NOUGHTS-AND-CROSSES::WIN';
export const OPPONENT_SURRENDERED = 'GAMES::NOUGHTS-AND-CROSSES::OPPONENT_SURRENDERED';
export const SURRENDERED = 'GAMES::NOUGHTS-AND-CROSSES::SURRENDERED';

export interface TakeTurnAction extends Action<typeof TAKE_TURN, ClientPayload> { };

export interface BeginAction extends Action<typeof BEGIN, PlayerBeginInfo> { };
export interface BeginSpectateAction extends Action<typeof BEGIN_SPECTATE, SpectatorBeginInfo> { };

export interface DrawAction extends TagAction<typeof DRAW> { };
export interface DrawMoveAction extends Action<typeof DRAW_MOVE, MoveInfo> { };
export interface LoseMoveAction extends Action<typeof LOSE_MOVE, MoveInfo> { };
export interface MoveAction extends Action<typeof MOVE, MoveInfo> { };
export interface WinAction extends TagAction<typeof WIN> { };
export interface OpponentSurrenderedAction extends TagAction<typeof OPPONENT_SURRENDERED> { };
export interface SurrenderedAction extends Action<typeof SURRENDERED, Player> { };
export interface ConfigureAction extends Action<typeof CONFIGURE, BeginInfo> { };

export type NoughtsAndCrossesAction =
  ConfigureAction |
  TakeTurnAction |
  BeginAction |
  BeginSpectateAction |
  SurrenderedAction |
  OpponentSurrenderedAction |
  DrawAction |
  DrawMoveAction |
  LoseMoveAction |
  MoveAction |
  WinAction;

export const configure = (info: BeginInfo): ConfigureAction => ({
  type: CONFIGURE,
  payload: info
})

export const begin = (player: Player, nextPlayer: Player, opponent: string, boardColour: SimpleColour): BeginAction => ({
  type: BEGIN,
  payload: { player, opponent, nextPlayer, boardColour }
});

export const draw = (): DrawAction => ({ type: DRAW })
export const win = (): WinAction => ({ type: WIN });

export const drawMove = (position: Position): DrawMoveAction => ({
  type: DRAW_MOVE,
  payload: { position }
})

export const loseMove = (position: Position): LoseMoveAction => ({
  type: LOSE_MOVE,
  payload: { position }
})

export const beginSpectate = (info: SpectatorBeginInfo): BeginSpectateAction =>
  ({ type: BEGIN_SPECTATE, payload: info })

export const move = (position: Position) : MoveAction => ({
  type: MOVE,
  payload: { position }
})

export const surrendered = (player: Player) => ({ type: SURRENDERED, payload: player })
export const opponentSurrendered = () => ({ type: OPPONENT_SURRENDERED })

export const takeTurn = (position: Position): TakeTurnAction => {
  return {
    type: TAKE_TURN,
    payload: { kind: 'move', position }
  };
}

export const sendMove = (player: Player, position: Position): ReduxAction => {
  return sendNoughtsAndCrosses({ kind: 'move', position });
}

export const sendNoughtsAndCrosses = (payload: ClientPayload): ReduxAction => {
  return sendWs({ channel: NOUGHTS_AND_CROSSES_CHANNEL, payload: payload });
}

import { Option } from "prelude-ts";
import { nextPlayer, withMove, withMoveUnchecked } from "@cat-tuition/protocol/noughts-and-crosses";
import { TUTEE_ENTERED_CLASS, WelcomeTuteeAction, TUTOR_ENTERED_CLASS, WelcomeTutorAction } from "../../actions";
import { State, initialState, StateTag } from "./state";
import {
  BEGIN,
  BEGIN_SPECTATE,
  CONFIGURE,
  DRAW,
  DRAW_MOVE,
  LOSE_MOVE,
  MOVE,
  TAKE_TURN,
  SURRENDERED,
  OPPONENT_SURRENDERED,
  WIN,
  NoughtsAndCrossesAction
} from "./actions";

export const reducer = (state: State = initialState, action: WelcomeTuteeAction | WelcomeTutorAction | NoughtsAndCrossesAction): State => {
  const currentPlayer = state.nextPlayer;
  switch (action.type) {
    case CONFIGURE:
      return { ...state, initialConfig: Option.of(action.payload) }
    case TUTEE_ENTERED_CLASS:
      return initialState;
    case TUTOR_ENTERED_CLASS:
      return initialState;
    case TAKE_TURN: {
      const newBoard = withMove(action.payload, currentPlayer, state.board);
      if (state.winner == null && newBoard != null) {
        return {
          ...state,
          board: newBoard,
          nextPlayer: nextPlayer(currentPlayer)
        };
      } else {
        return state;
      }
    }
    case MOVE: {
      return {
        ...state,
        isDraw: false,
        winner: null,
        nextPlayer: nextPlayer(currentPlayer),
        board: withMoveUnchecked(action.payload, currentPlayer, state.board),
      }
    }
    case DRAW_MOVE: {
      return {
        ...state,
        isDraw: true,
        winner: null,
        nextPlayer: nextPlayer(state.nextPlayer),
        board: withMoveUnchecked(action.payload, state.nextPlayer, state.board),
      }
    }
    case LOSE_MOVE: {
      let { position } = action.payload;
      return {
        ...state,
        isDraw: false,
        winner: state.nextPlayer,
        board: withMoveUnchecked(action.payload, state.nextPlayer, state.board),
      }
    }
    case WIN: {
      if (state.tag == StateTag.Player) {
        return {
          ...state,
          isDraw: false,
          winner: state.userPlayer,
        }
      } else {
        return state;
      }
    }
    case DRAW: {
      return {
        ...state,
        isDraw: true,
        winner: null
      }
    }
    case BEGIN: {
      const { player, opponent, nextPlayer, boardColour } = action.payload;
      return {
        ...state,
        tag: StateTag.Player,
        board: Array(9).fill(null),
        isDraw: false,
        isSurrender: false,
        winner: null,
        userPlayer: player,
        opponentPlayerId: opponent,
        nextPlayer,
        boardColour
      }
    }
    case BEGIN_SPECTATE: {
      const { board, winner, isDraw, nextPlayer, players } = action.payload;
      return {
        ...state,
        tag: StateTag.Spectator,
        board,
        winner,
        isDraw,
        nextPlayer,
        players
      }
    }
    case SURRENDERED: {
      if (state.tag == StateTag.Spectator) {
        const player = action.payload;
        return {
          ...state,
          winner: nextPlayer(player),
          isSurrender: true,
        }
      } else {
        return state;
      }
    }
    case OPPONENT_SURRENDERED: {
      if (state.tag == StateTag.Player) {
        return {
          ...state,
          winner: state.userPlayer,
          isSurrender: true
        }
      } else {
        return state;
      }
    }
    default:
      return state;
  }
}

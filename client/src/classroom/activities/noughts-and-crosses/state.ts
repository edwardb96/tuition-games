import { SimpleColour } from "@cat-tuition/protocol/colour";
import { Board, Player, BeginInfo } from "@cat-tuition/protocol/noughts-and-crosses";
import { Option } from "prelude-ts";

export interface StateBase {
  board: Board;
  winner: Player | null;
  isDraw: boolean;
  isSurrender: boolean;
  nextPlayer: Player;
  initialConfig: Option<BeginInfo>;
  boardColour: SimpleColour;
}

export const isGameFinished = (state: StateBase) => {
  return state.isDraw || state.isSurrender || state.winner != null;
}

export const canTakeTurn = (state: State) =>
  state.tag == StateTag.Player && state.nextPlayer == state.userPlayer;

export enum StateTag {
  PreBegin,
  Player,
  Spectator
}

export interface PlayerState extends StateBase {
  tag: StateTag.Player
  userPlayer: Player;
  opponentPlayerId: string;
}

export interface SpectatorState extends StateBase {
  tag: StateTag.Spectator
  players: { X: string, O: string }
}

export interface PreBeginState extends StateBase {
  tag: StateTag.PreBegin
}

export type State =
  PreBeginState |
  PlayerState |
  SpectatorState;

export const initialState: State = {
  tag: StateTag.PreBegin,
  isDraw: false,
  isSurrender: false,
  nextPlayer: 'X',
  board: Array(9).fill(null),
  winner: null,
  boardColour: SimpleColour.White,
  initialConfig: Option.none()
};
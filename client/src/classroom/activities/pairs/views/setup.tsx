import React from "react";
import { connect } from "react-redux";
import { AppState } from "../../../../redux-store";
import { sendChangeActivity } from "../../../actions";
import { Classroom } from "../../../classroom";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import { UserInfo } from "@cat-tuition/protocol/user";
import { BeginInfo, pairsBeginInfoSchema, PAIRS_ACTIVITY_ID } from "@cat-tuition/protocol/pairs";
import { SimpleColour } from "@cat-tuition/protocol/colour";
import { form as joiToForms } from "joi-errors-for-forms";
import { configure } from "../actions";
import CardSelector from "../../../../card-pairs/views/card-pair-selector";
import { CheckboxItem } from "../../../../checkbox-list/checkbox-item";
import { ColourPalettePicker } from "../../../../colour-palette-picker/colour-palette-picker";
import map from "iter-tools/methods/map";
const convertToForms = joiToForms();

export interface PairsSetupProps {
  classroom: Classroom;
  sendChangeActivity(info: ActivityBeginInfo): void;
  configure(config: BeginInfo): void;
}

const validateForm = (values: BeginInfo) => {
  console.log("Validating...", values);
  const result = pairsBeginInfoSchema.validate(values);
  if (result.error != null) {
    const errors = convertToForms(result.error);
    return errors;
  } else {
    return null;
  }
};

const PairsSetup = (props: PairsSetupProps) => {
  const { classroom, sendChangeActivity, configure } = props;
  const onSubmit = (info: BeginInfo, { setSubmitting }: FormikHelpers<BeginInfo>) => {
    sendChangeActivity({ activityId: PAIRS_ACTIVITY_ID, info })
    configure(info);
    setSubmitting(false);
  };

  const renderUserCheckbox = (user: UserInfo) => {
    return (
      <div key={user.id} className="field">
        <div className="control">
          <Field component={CheckboxItem} name="players" value={user.id} label={user.displayName} />
        </div>
      </div>
    );
  }

  const occupants = Array.from(classroom.occupants());

  return (
    <>
      <div className="card-content">
        <Formik
          initialValues={{
            players: [...map(u => u.id, classroom.occupants())],
            pairs: [],
            cardColour: SimpleColour.White }}
          validate={validateForm}
          onSubmit={onSubmit}>
          {
            ({ isSubmitting, errors, touched, values }) => (
              <Form className="form">
                <h1 className="title">
                  Setup a new game of Pairs
                </h1>
                <p className="subtitle">
                  Select the players and the cards
                </p>

                <div className="field">
                  <label className="label">Players</label>
                  {occupants.map(renderUserCheckbox)}
                  <ErrorMessage name="players" render={msg => <p className="help is-danger">{msg}</p>} />
                </div>

                <div className="field">
                  <label className="label">Card Colour</label>
                  <ColourPalettePicker id="card-colour" name="cardColour" />
                </div>

                <div className="field">
                  <label className="label">Cards</label>
                  <Field component={CardSelector} name="pairs" cardColour={values.cardColour} />
                  <ErrorMessage name="pairs" render={msg => <p className="help is-danger">{msg}</p>} />
                </div>

                <div className="field">
                  <div className="control">
                    <button className="button is-link" type="submit" disabled={isSubmitting}>
                      Start Game
                    </button>
                  </div>
                </div>
              </Form>
            )
          }
        </Formik>
      </div>
    </>
  );
};

const mapState = ({ classroom }: AppState) => ({
  classroom: classroom.getOrThrow('Attempt to render activity when no classroom active')
});

export default connect(mapState, { sendChangeActivity, configure })(PairsSetup);
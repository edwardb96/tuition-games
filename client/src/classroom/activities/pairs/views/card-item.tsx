import React from "react";
import { Card } from "@cat-tuition/protocol/pairs";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { UserInfo } from "@cat-tuition/protocol/user";
import cx from "classnames";
import { AppState } from "../../../../redux-store";
import { StateTag } from "../state";
import { connect } from "react-redux";
import { CardBackFace, CardFrontFace } from "../../../../card-pairs/views/game-card";
import { ProfileImage } from "../../../../profile-image/profile-image";

interface CardItemProps {
  position: number;
  colour: string;
  isTemporarilyFlipped: boolean;
  player: UserInfo | null;
  card: Card | null;
  canSelect: boolean;
  selected: boolean;
  onClick(): void;
}

const CardItem = (props: CardItemProps) => {
  const { onClick, player, isTemporarilyFlipped, card, selected, canSelect, colour } = props;

  if (card != null) {
    const playerImage =
      mapNullable(player,
        ({ displayName, favouriteColour }) =>
          <ProfileImage displayName={displayName} size={36} colour={favouriteColour}/>)
    return (
      <div className={cx(`game-card is-${colour}-coloured`, { 'is-flipped': isTemporarilyFlipped || player != null })}>
        <CardBackFace onClick={onClick} canClick={canSelect} isSelected={selected}/>
        <CardFrontFace card={card} iconImage={playerImage}/>
      </div>
    );
  } else {
    return (
      <div className={`game-card is-${colour}-coloured`}>
        <CardBackFace onClick={onClick} canClick={canSelect} isSelected={selected}/>
      </div>
    );
  }
}

const mapState = ({ pairs, classroom: maybeClassroom }: AppState, ownProps : { position: number }) => {
  const classroom = maybeClassroom.getOrThrow('Tried to render card-item when not in classroom');
  if (pairs.tag != StateTag.PreBegin) {
    const { board, deck } = pairs;
    const { position } = ownProps;
    const boardItem = board[position];
    if (boardItem != null) {
      const card = deck[boardItem.cardIndex];
      const { isTemporarilyFlipped, player } = boardItem;
      return { position, isTemporarilyFlipped, player: mapNullable(player, id => classroom.userInfo(id)), card }
    } else {
      return { position, isTemporarilyFlipped: false, player: null, card: null }
    }
  } else {
    throw new Error('Attempt to render card before the board is initialized')
  }
}
export default connect(mapState, {})(CardItem);
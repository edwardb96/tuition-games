import { connect } from "react-redux";
import React, { useEffect, useState } from "react";
import CardItem from "./card-item";
import { AppState } from "../../../../redux-store";
import "./game.scss";
import { ActivityBeginInfo } from "@cat-tuition/protocol/channels/classroom";
import { BeginInfo as PairsBeginInfo, Move, PAIRS_ACTIVITY_ID, Position, ScoreboardEntry } from "@cat-tuition/protocol/pairs";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import { sendFirstFlip, sendSecondFlip, unflip, configure } from "../actions";
import { sendChangeActivity } from "../../../actions";
import cx from "classnames";
import map from "iter-tools/methods/map";
import filter from "iter-tools/methods/filter";
import joinWith from "iter-tools/methods/join-with";
import stringFrom from "iter-tools/methods/string-from";
import { ClientBoard, isGameFinished, State, StateTag } from "../state";
import { HashMap } from "prelude-ts";
import { next } from "@cat-tuition/protocol/iter-tools-ext";
import ordinal from "ordinal";

interface PairsProps {
  gameState: State;
  user: User;
  userInfo(userId: string): UserInfo;
  isInClass(userId: string): boolean;
  isInOwnClass: boolean;
  sendChangeActivity(info: ActivityBeginInfo): void
  configure(config: PairsBeginInfo): void;
  sendFirstFlip(position: Position): void;
  sendSecondFlip(position: Position): void;
  unflip(move: Move): void;
}

interface PairsState {
  firstSelected?: number;
}

const PairsGame = (props: PairsProps) => {
  const { gameState, isInOwnClass, user, unflip } = props;
  const [state, setState] = useState<PairsState>({});

  const canSelect =
    gameState.tag == StateTag.Player &&
    gameState.players[gameState.currentPlayerIndex] == user.id;

  const previousMove =
    gameState.tag != StateTag.PreBegin ? gameState.lastMove : null;

  const cardColour =
    gameState.tag != StateTag.PreBegin ? gameState.cardColour : 'white';

  useEffect(() => {
    if (previousMove != null) {
      setTimeout(() => unflip(previousMove), 3000);
    }
  }, [previousMove])

  const onSelected = (position: number) => {
    if (canSelect) {
      const { sendFirstFlip, sendSecondFlip } = props;
      if (state.firstSelected == null) {
        setState({ firstSelected: position });
        sendFirstFlip(position);
      } else if (state.firstSelected != position) {
        setState({});
        sendSecondFlip(position);
      }
    }
  }

  const onStop = () => {
    props.sendChangeActivity({ activityId: null });
  }

  const canRematch = () => {
    const { gameState } = props;
    return isGameFinished(gameState);
  }

  const onRematch = () => {
    const { configure, sendChangeActivity } = props;
    if (canRematch() && gameState.initialConfig.isSome()) {
      const lastInitialConfig = gameState.initialConfig.get();
      const newPlayers = lastInitialConfig.players;
      newPlayers.push(newPlayers.shift() as string);
      const info = { ...lastInitialConfig, players: newPlayers }
      configure(info);
      sendChangeActivity({ activityId: PAIRS_ACTIVITY_ID, info });
    }
  }

  const canReset = (): boolean => {
    return gameState.tag == StateTag.Player &&
             !isGameFinished(gameState);
  }

  const onReset = () => {
    const { configure, sendChangeActivity } = props;
    if (canReset() && gameState.initialConfig.isSome()) {
      const info = gameState.initialConfig.get();
      configure(info);
      sendChangeActivity({ activityId: PAIRS_ACTIVITY_ID, info });
    }
  }

  const joinNames = (names: Iterable<string>, def: string = '') => {
    const iterator = names[Symbol.iterator]();
    let firstName : null | string = next(iterator, null);
    let secondName : null | string = next(iterator, null);
    if (firstName != null) {
      if (secondName != null) {
        return `${stringFrom(joinWith(', ', names))}, ${secondName} and ${firstName}`
      } else {
        return firstName;
      }
    } else {
      return def;
    }
  }

  const renderStatus = () => {
    const { gameState, user, userInfo } = props;
    const firstScore = (scoreboard: HashMap<string, ScoreboardEntry>) => scoreboard.findAny(_ => true).map(([_k, entry]) => entry.score).getOrElse(0);
    if (gameState.tag == StateTag.Player) {
      const { finalSurrender, scoreboard, currentPlayerIndex: nextPlayer, userPlayer, players } = gameState;
      if (scoreboard != null) {
        const scoreboardMap = HashMap.ofObjectDictionary<ScoreboardEntry>(scoreboard);
        const selfEntry = scoreboardMap.get(user.id).getOrThrow('User is not in the scoreboard.');
        if (finalSurrender == null) {
          const topRank = scoreboardMap.filter((id, entry) => entry.rank == 0);

          if (selfEntry.rank == 0) {
            if (players.length == topRank.length()) {
              return "It's a draw!";
            } else if (topRank.length() > 1) {
              const jointWinnerIdsExcludingSelf = filter(id => id != user.id, topRank.keySet());
              const jointWinnerDisplayNames = map(id => userInfo(id).displayName, jointWinnerIdsExcludingSelf);
              return `You are joint winners with ${joinNames(jointWinnerDisplayNames)} with ${selfEntry.score} pairs each.`;
            } else {
              if (scoreboardMap.allMatch((userId, entry) => userId == user.id || entry.score == 0)) {
                return `Wow ${user.displayName}, that's amazing you won them all!`
              } else {
                return `Well done ${user.displayName}, you won with ${selfEntry.score} pairs!`
              }
            }
          } else {
            const soleWinner = topRank.single();
            if (players.length > 2) {
              if (soleWinner.isSome()) {
                const [winnerId, winnerEntry] = soleWinner.get();
                return `You came in ${ordinal(selfEntry.rank + 1)} place with ${selfEntry.score} pairs. ${userInfo(winnerId).displayName} won with ${winnerEntry.score} pairs.`;
              } else {
                const jointWinnerDisplayNames = map(id => userInfo(id).displayName, topRank.keySet());
                return `You came in ${ordinal(selfEntry.rank + 1)} place with ${selfEntry.score} pairs. ${joinNames(jointWinnerDisplayNames)} are joint winners with ${firstScore(topRank)} pairs each.`;
              }
            } else {
              const [winnerId, winnerEntry] = soleWinner.getOrThrow('Sole winner expected');
              return `${userInfo(winnerId).displayName} won with ${winnerEntry.score} pairs. Better luck next time.`;
            }
          }
        } else {
          if (finalSurrender.player == user.id) {
            return `You surrendered, ${userInfo(players[0]).displayName} won!`;
          } else {
            return `${userInfo(finalSurrender.player).displayName} surrendered, You won with ${selfEntry.score} pairs!`;
          }
        }
      } else {
        if (nextPlayer == userPlayer) {
          if (state.firstSelected == null) {
            return `It's your turn, click to select the first card to turn over...`;
          } else {
            return `It's your turn, click to select the second card to turn over...`;
          }
        } else {
          const nextUser = userInfo(players[nextPlayer]);
          return `Waiting for ${nextUser.displayName} to take a turn...`;
        }
      }
    } else if (gameState.tag == StateTag.Spectator) {
      const { finalSurrender, scoreboard, currentPlayerIndex: nextPlayer, players } = gameState;
      if (scoreboard != null) {
        const scoreboardMap = HashMap.ofObjectDictionary<ScoreboardEntry>(scoreboard);
        const topRank = scoreboardMap.filter((id, entry) => entry.rank == 0);
        const soleWinner = topRank.single();

        if (finalSurrender == null) {
          if (players.length == topRank.length()) {
            return "It's a draw!";
          } else if (soleWinner.isSome()) {
            const [winnerId, winnerEntry] = soleWinner.get();
            return `${userInfo(winnerId).displayName} won with ${winnerEntry.score} pairs.`;
          } else {
            const jointWinnerDisplayNames = map(id => userInfo(id).displayName, topRank.keySet());
            return `${joinNames(jointWinnerDisplayNames)} are joint winners with ${firstScore(topRank)} pairs each.`;
          }
        } else {
          const winnerScore = scoreboardMap.get(finalSurrender.winner).getOrThrow('Expected winner would be in scoreboard').score;
          return `${userInfo(finalSurrender.player).displayName} surrendered to ${userInfo(finalSurrender.winner).displayName} who won with ${winnerScore} pairs!`;
        }
     } else {
        if (nextPlayer != null) {
          const nextUser = userInfo(players[nextPlayer]);
          return `Waiting for ${nextUser.displayName} to take a turn...`;
        }
      }
    } else {
      return "Waiting for the game to start...";
    }
  };


  const renderBoard = (gameStart: number, board: ClientBoard) =>
    <div className="columns is-multiline is-mobile board">
      {
        board.map((_, position) => {
          return (
            <div key={`${gameStart}-${position}`} className="column is-3 has-text-centered card-parent">
              <CardItem
                colour={cardColour}
                position={position}
                canSelect={canSelect}
                selected={state.firstSelected == position}
                onClick={() => onSelected(position)} />
            </div>);
        })
      }
    </div>;

  const cardFooter = (() => {
    if (isInOwnClass) {
      return (
        <div className="card-footer">
          <a onClick={() => onStop()}
              className="card-footer-item">
            Stop</a>
          <a onClick={() => onReset()}
              className={cx("card-footer-item", {'is-disabled': !canReset()})}>
            Reset</a>
          <a onClick={() => onRematch()}
              className={cx("card-footer-item", {'is-disabled': !canRematch()})}>
            Rematch</a>
        </div>);
    } else {
      return (<></>);
    }
  })();

  const renderedBoard = gameState.tag != StateTag.PreBegin ? renderBoard(gameState.beginTime, gameState.board) : <></>;

  return (
    <>
      <div className="card-content">
        <p className="title">Pairs</p>
        <p className="subtitle">{renderStatus()}</p>
        <div className="pairs">
          {renderedBoard}
        </div>
      </div>
      {cardFooter}
    </>
  );
};

const mapState = ({ pairs, profile, classroom: maybeClassroom }: AppState) => {
  const classroom = maybeClassroom.getOrThrow('Tried to render game when not in classroom');
  if (profile.user != null) {
    return ({
      gameState: pairs,
      user: profile.user,
      userInfo: (u: string) => classroom.userInfo(u),
      isInClass: (u: string) => classroom.isInClass(u),
      isInOwnClass: profile.user != null ? classroom.isTutorOfClass(profile.user.id) : false });
  } else {
    throw new Error("User not set when attempting to mount game");
  }
}
export default connect(mapState, { sendFirstFlip, sendSecondFlip, sendChangeActivity, unflip, configure })(PairsGame);
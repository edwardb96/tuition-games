import { Option } from "prelude-ts";
import { applyFirstFlip, applySecondFlip, CompletedMove, FlippedFirstCard, Move, Player } from "@cat-tuition/protocol/pairs";
import { TUTEE_ENTERED_CLASS, TUTOR_ENTERED_CLASS, WelcomeTuteeAction, WelcomeTutorAction } from "../../actions";
import { BEGIN, BEGIN_SPECTATE, CONFIGURE, FLIPPED_FINAL, FLIPPED_FIRST, FLIPPED_SECOND, PairsAction, SURRENDERED, UNFLIP } from "./actions";
import { ClientBoard, clientBoardFromBoard, ClientBoardItem, initialState, State, StateTag } from "./state";
import { purify } from "../../../array-functions";

const withFirstFlip = (player: Player, board: ClientBoard, move: FlippedFirstCard): ClientBoard => {
  const newBoard = board.slice();
  applyFirstFlip(player, board, move);
  const { firstCard } = move;
  newBoard[firstCard.position] = { isTemporarilyFlipped: true, cardIndex: firstCard.cardIndex, player: null };
  return newBoard;
}

const withSecondFlip = (player: Player, board: ClientBoard, move: CompletedMove): ClientBoard => {
  const newBoard = board.slice();
  applySecondFlip(player, board, move);
  const { firstCard, secondCard, isMatch } = move;
  const maybeNewPlayer = isMatch ? player : null
  newBoard[firstCard.position] = { isTemporarilyFlipped: true, cardIndex: firstCard.cardIndex, player: maybeNewPlayer };
  newBoard[secondCard.position] = { isTemporarilyFlipped: true, cardIndex: secondCard.cardIndex, player: maybeNewPlayer };
  return newBoard;
}

const withUnflipped =
  purify((newBoard : ClientBoard, move: Move) => {
    const { firstPosition, secondPosition } = move;
    (newBoard[firstPosition] as ClientBoardItem).isTemporarilyFlipped = false;
    (newBoard[secondPosition] as ClientBoardItem).isTemporarilyFlipped = false;
    return newBoard;
  });

export const reducer = (state: State = initialState, action: WelcomeTuteeAction | WelcomeTutorAction | PairsAction): State => {
  switch (action.type) {
    case CONFIGURE:
      return { ...state, initialConfig: Option.of(action.payload) }
    case TUTEE_ENTERED_CLASS:
      return initialState;
    case TUTOR_ENTERED_CLASS:
      return initialState;
    case BEGIN: {
      const { players, deck, userPlayer, cardColour } = action.payload;
      return {
        ...state,
        tag: StateTag.Player,
        board: Array(deck.length).fill(null),
        beginTime: Date.now(),
        finalSurrender: null,
        currentPlayerIndex: 0,
        scoreboard: null,
        cardColour,
        userPlayer,
        deck,
        players
      }
    }
    case BEGIN_SPECTATE: {
      const { board, deck, scoreboard, players,
              currentPlayerIndex, finalSurrender, cardColour } = action.payload;
      return {
        ...state,
        tag: StateTag.Spectator,
        board: clientBoardFromBoard(board),
        beginTime: Date.now(),
        deck,
        players,
        scoreboard,
        currentPlayerIndex,
        finalSurrender,
        cardColour
      }
    }
    default: {
      if (state.tag != StateTag.PreBegin) {
        const currentPlayerIndex = (state.currentPlayerIndex as number);
        switch (action.type) {
          case UNFLIP: {
            return { ...state, board: withUnflipped(state.board, action.payload), lastMove: null }
          }
          case FLIPPED_FIRST: {
            const firstFlip = action.payload;
            return {
              ...state,
              firstFlip: firstFlip,
              board: withFirstFlip(state.players[currentPlayerIndex], state.board, firstFlip),
            }
          }
          case FLIPPED_SECOND: {
            const secondFlip = action.payload;
            const { isMatch } = secondFlip;
            const { firstFlip } = state;
            if (firstFlip != null) {
              const incrementBy = isMatch ? 0 : 1;
              const lastMove = {
                firstPosition: firstFlip.firstCard.position,
                secondPosition: secondFlip.secondCard.position
              };

              return {
                ...state,
                lastMove,
                firstFlip: null,
                board: withSecondFlip(state.players[currentPlayerIndex], state.board, { ...firstFlip, ...secondFlip }),
                currentPlayerIndex: (currentPlayerIndex + incrementBy) % state.players.length
              }
            } else {
              throw new Error('Tried to flip second before flipping the first');
            }
          }
          case FLIPPED_FINAL: {
            const { firstFlip } = state;
            const { secondCard, scoreboard } = action.payload;
            if (firstFlip != null) {
              return {
                ...state,
                scoreboard,
                board: withSecondFlip(state.players[currentPlayerIndex], state.board, { isMatch: true, secondCard, ...firstFlip })
              }
            } else {
              throw new Error('Tried to flip second before flipping the first');
            }
          }
          case SURRENDERED: {
            const { player, scoreboard } = action.payload;
            const playerIndex = state.players.indexOf(player);
            if (playerIndex != -1) {
              const newPlayers = state.players.splice(playerIndex, 1);
              return {
                ...state,
                players: newPlayers,
                finalSurrender: { player, winner: state.players[0] },
                scoreboard: scoreboard
              }
            }
          }
          default:
            return state;
        }
      } else {
        return state;
      }
    }
  }
}

import { valdiateServerPayload } from "@cat-tuition/protocol/channels/pairs";
import { Channel } from "../../../redux-utils";
import { Action as ReduxAction, Dispatch } from "redux";
import { begin, beginSpectate, flippedFinal, flippedFirst, flippedSecond, surrendered } from "./actions";

export class PairsChannel implements Channel {
  onReceive(payload: unknown, next: Dispatch<ReduxAction<any>>) {
    valdiateServerPayload(payload).match({
      Some: validPayload => {
        switch (validPayload.kind) {
          case 'begin':
            next(begin(validPayload));
            break;
          case 'begin-spectate':
            next(beginSpectate(validPayload));
            break;
          case 'flipped-first':
            next(flippedFirst(validPayload));
            break;
          case 'flipped-second':
            next(flippedSecond(validPayload));
            break;
          case 'flipped-final':
            next(flippedFinal(validPayload));
            break;
          case 'surrendered':
            next(surrendered(validPayload.player, validPayload.scoreboard));
            break;
        }
      },
      None: () => {
        console.log('Invalid pairs payload from server', payload);
      }
    });
  }
}
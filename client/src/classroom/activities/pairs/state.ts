import { FinalSurrender } from "@cat-tuition/protocol/channels/pairs";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { Card, Board, Player, BeginInfo, BoardItem, Scoreboard, FlippedFirstCard, Move } from "@cat-tuition/protocol/pairs";
import { Option } from "prelude-ts";

export interface StateBase {
  scoreboard: Scoreboard | null;
  finalSurrender: FinalSurrender | null;
  initialConfig: Option<BeginInfo>;
  beginTime: number;
  firstFlip: FlippedFirstCard | null;
  lastMove: Move | null;
}

export interface ClientBoardItem extends BoardItem {
  isTemporarilyFlipped: boolean;
}

export type ClientBoard = (ClientBoardItem | null)[];

export const clientBoardFromBoard = (board: Board): ClientBoard =>
  board.map(item => mapNullable(item, i => ({...i, isTemporarilyFlipped: false })))

export const isGameFinished = (state: StateBase) =>
  state.scoreboard != null

export const canTakeTurn = (state: State) =>
  state.tag == StateTag.Player && state.currentPlayerIndex == state.userPlayer;

export enum StateTag {
  PreBegin,
  Player,
  Spectator
}

export interface PlayerState extends StateBase {
  tag: StateTag.Player;
  board: ClientBoard;
  deck: Card[];
  players: Player[];
  currentPlayerIndex: number;
  userPlayer: number;
  cardColour: string;
}

export interface SpectatorState extends StateBase {
  tag: StateTag.Spectator;
  board: ClientBoard;
  deck: Card[];
  players: Player[];
  currentPlayerIndex: number | null;
  cardColour: string;
}

export interface PreBeginState extends StateBase {
  tag: StateTag.PreBegin;
}

export type State =
  PreBeginState |
  PlayerState |
  SpectatorState;

export const initialState: State = {
  tag: StateTag.PreBegin,
  scoreboard: null,
  finalSurrender: null,
  firstFlip: null,
  lastMove: null,
  initialConfig: Option.none(),
  beginTime: 0
};
import { Action } from "../../../redux-utils";
import { Action as ReduxAction } from "redux";
import { send as sendWs } from "@giantmachines/redux-websocket";
import { PAIRS_CHANNEL, PlayerBeginInfo, SpectatorBeginInfo, ClientPayload, FlippedFinalCardInfo } from "@cat-tuition/protocol/channels/pairs";
import { Player, BeginInfo, Scoreboard, FlippedSecondCard, FlippedFirstCard, Move } from "@cat-tuition/protocol/pairs"
import { Position } from "@cat-tuition/protocol/noughts-and-crosses";

export const CONFIGURE = 'GAMES::PAIRS::CONFIGURE';
export const BEGIN = 'GAMES::PAIRS::BEGIN';
export const BEGIN_SPECTATE = 'GAMES::PAIRS::BEGIN_SPECTATE';

export const FLIPPED_FINAL = 'GAMES::PAIRS::FLIPPED_FINAL';
export const FLIPPED_FIRST = 'GAMES::PAIRS::FLIPPED_FIRST';
export const FLIPPED_SECOND = 'GAMES::PAIRS::FLIPPED_SECOND';

export const TAKE_TURN = 'GAMES::PAIRS::TAKE_TURN';
export const OPPONENT_SURRENDERED = 'GAMES::PAIRS::OPPONENT_SURRENDERED';
export const SURRENDERED = 'GAMES::PAIRS::SURRENDERED';
export const UNFLIP = 'GAMES::PAIRS::UNFLIP';

export interface BeginAction extends Action<typeof BEGIN, PlayerBeginInfo> { };
export interface BeginSpectateAction extends Action<typeof BEGIN_SPECTATE, SpectatorBeginInfo> { };

export interface FlippedFinalAction extends Action<typeof FLIPPED_FINAL, FlippedFinalCardInfo> { };
export interface FlippedFirstAction extends Action<typeof FLIPPED_FIRST, FlippedFirstCard> { };
export interface FlippedSecondAction extends Action<typeof FLIPPED_SECOND, FlippedSecondCard> { };
export interface SurrenderedAction extends Action<typeof SURRENDERED, {
  player: Player;
  scoreboard: Scoreboard | null;
}> {};
export interface ConfigureAction extends Action<typeof CONFIGURE, BeginInfo> { };
export interface UnflipAction extends Action<typeof UNFLIP, Move> { };

export type PairsAction =
  ConfigureAction |
  UnflipAction |
  BeginAction |
  BeginSpectateAction |
  SurrenderedAction |
  FlippedFirstAction |
  FlippedSecondAction |
  FlippedFinalAction;

export const configure = (info: BeginInfo): ConfigureAction => ({
  type: CONFIGURE,
  payload: info
})

export const begin = (info: PlayerBeginInfo): BeginAction => ({
  type: BEGIN,
  payload: info
});

export const flippedFinal = (finalCardFlipped: FlippedFinalCardInfo): FlippedFinalAction => ({
  type: FLIPPED_FINAL,
  payload: finalCardFlipped
});

export const flippedFirst = (firstCardFlipped: FlippedFirstCard): FlippedFirstAction => ({
  type: FLIPPED_FIRST,
  payload: firstCardFlipped
});

export const flippedSecond = (secondCardFlipped: FlippedSecondCard): FlippedSecondAction => ({
  type: FLIPPED_SECOND,
  payload: secondCardFlipped
});

export const beginSpectate = (info: SpectatorBeginInfo): BeginSpectateAction =>
  ({ type: BEGIN_SPECTATE, payload: info })

export const surrendered = (player: Player, scoreboard: Scoreboard | null = null): SurrenderedAction =>
  ({
    type: SURRENDERED,
    payload: { player, scoreboard }
  })

export const unflip = (move: Move): UnflipAction => {
  return {
    type: UNFLIP,
    payload: move
  };
};

export const sendFirstFlip = (position: Position): ReduxAction => {
  return sendPairs({ kind: 'flip-first', position });
};

export const sendSecondFlip = (position: Position): ReduxAction => {
  return sendPairs({ kind: 'flip-second', position });
};

export const sendPairs = (payload: ClientPayload): ReduxAction => {
  return sendWs({ channel: PAIRS_CHANNEL, payload: payload });
};

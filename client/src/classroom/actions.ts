import { Action, TagAction } from "../redux-utils";
import { UserInfo } from "@cat-tuition/protocol/user";
import {
  CLASSROOM_CHANNEL,
  ClassroomClientPayload as ClientPayload,
  TuteeWelcomeToClassroomInfo,
  TutorWelcomeToClassroomInfo,
  RecievedChatMessageInfo,
  ActivityBeginInfo,
  ActivityId
} from "@cat-tuition/protocol/channels/classroom";
import { Action as ReduxAction } from "redux";
import { send as sendWs } from "@giantmachines/redux-websocket";

export const CLASS_ENDED = 'CLASS::ENDED';
export const TUTEE_ADDED = 'CLASS::TUTEE_ADDED';
export const TUTEE_REMOVED = 'CLASS::TUTEE_REMOVED';
export const ACTIVITY_CHANGED = 'CLASS::ACTIVITY_CHANGED';
export const RECEIVE_CHAT_MESSAGE = 'CLASS::RECEIVE_CHAT_MESSAGE';
export const TUTEE_ENTERED_CLASS = 'CLASS::TUTEE_ENTERED';
export const TUTOR_ENTERED_CLASS = 'CLASS::TUTOR_ENTERED';
export const ENTERING_CLASS = 'CLASS::ENTERING';

export type ClassroomAction =
  ClassEndedAction
  | EnteringClassAction
  | TuteeAddedAction
  | TuteeRemovedAction
  | ActivityChangedAction
  | ReceiveChatMessageAction
  | WelcomeTuteeAction
  | WelcomeTutorAction;

export interface ClassEndedAction extends TagAction<typeof CLASS_ENDED> { };
export interface EnteringClassAction extends TagAction<typeof ENTERING_CLASS> { };
export interface TuteeAddedAction extends Action<typeof TUTEE_ADDED, UserInfo> { };
export interface TuteeRemovedAction extends Action<typeof TUTEE_REMOVED, string> { };
export interface ActivityChangedAction extends Action<typeof ACTIVITY_CHANGED, ActivityId | null> { };
export interface ReceiveChatMessageAction extends Action<typeof RECEIVE_CHAT_MESSAGE, RecievedChatMessageInfo> { };
export interface WelcomeTuteeAction extends Action<typeof TUTEE_ENTERED_CLASS, TuteeWelcomeToClassroomInfo> { };
export interface WelcomeTutorAction extends Action<typeof TUTOR_ENTERED_CLASS, TutorWelcomeToClassroomInfo> { };

export const classEnded = (): ClassEndedAction =>
  ({ type: CLASS_ENDED });

export const tuteeAdded = (tutee: UserInfo): TuteeAddedAction =>
  ({ type: TUTEE_ADDED, payload: tutee });

export const tuteeRemoved = (tuteeId: string): TuteeRemovedAction =>
  ({ type: TUTEE_REMOVED, payload: tuteeId });

export const activityChanged = (activityId: ActivityId | null): ActivityChangedAction =>
  ({ type: ACTIVITY_CHANGED, payload: activityId });

export const receiveChatMessage = (messageInfo: RecievedChatMessageInfo): ReceiveChatMessageAction =>
  ({ type: RECEIVE_CHAT_MESSAGE, payload: messageInfo });

export const tuteeEnteredClass = (info: TuteeWelcomeToClassroomInfo): WelcomeTuteeAction =>
  ({ type: TUTEE_ENTERED_CLASS, payload: info })

export const tutorEnteredClass = (info: TutorWelcomeToClassroomInfo): WelcomeTutorAction =>
  ({ type: TUTOR_ENTERED_CLASS, payload: info })

export const sendChatMessage = (chatMessage: string): ReduxAction =>
  sendClassroom({ kind: 'send-chat', chatMessage })

export const sendChangeActivity = (info: ActivityBeginInfo): ReduxAction =>
  sendClassroom({ kind: 'change-activity', ...info })

const sendClassroom = (payload?: ClientPayload): ReduxAction =>
  sendWs({ channel: CLASSROOM_CHANNEL, payload });
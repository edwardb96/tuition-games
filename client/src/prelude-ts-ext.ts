import { HashMap, WithEquality, Option, Vector } from "prelude-ts";

export function pop<K, V>(map: HashMap<K, V>, k: K & WithEquality): [Option<V>, HashMap<K, V>] {
  return [map.get(k), map.remove(k)];
}

export function adjust<K, V>(map: HashMap<K, V>,
  k: K & WithEquality,
  adjustedValue: (v: V) => V): HashMap<K, V> {
  let curV = map.get(k);
  return curV.map(v => map.put(k, adjustedValue(v))).getOrElse(map);
}

export function hashMapFromList<K, V>(list: V[], extractKey: (value: V) => K & WithEquality): HashMap<K,V> {
  return list.reduce((map, v) => map.put(extractKey(v), v), HashMap.empty<K,V>());
}

export function mapWithIndex<A, B>(vector: Vector<A>, f: (index: number, value: A) => B): Vector<B> {
  let s = { i: 0 };
  return vector.map(a => {
    let index = s.i++;
    return f(index, a);
  });
}

export function *optionIterable<T>(option: Option<T>): Iterable<T> {
  if (option.isSome()) {
    yield option.get();
  }
}

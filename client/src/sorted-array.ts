import { compareValues, ComparisonFunction } from "@cat-tuition/protocol/compare";
import { Option } from "prelude-ts";
import { insorted, remsorted, removedAt, replacedAt, updatedAt, purify, insort, removeAt } from "./array-functions";
import { eq, has, lt } from "sorted-array-functions";

export class SortedArray<T> implements Iterable<T> {
  private storage: T[];
  private compare: ComparisonFunction<T>;

  private constructor(sortedArray: T[], compare: ComparisonFunction<T>) {
    this.storage = sortedArray;
    this.compare = compare;
  }

  [Symbol.iterator](): Iterator<T, any, undefined> {
    return this.storage[Symbol.iterator]();
  }

  getUnchecked(index: number): T {
    return this.storage[index];
  }

  get(index: number): Option<T> {
    if (0 <= index && index < this.storage.length) {
      return Option.of(this.getUnchecked(index))
    } else {
      return Option.none();
    }
  }

  last(): Option<T> {
    if (this.storage.length > 0) {
      return Option.of(this.getUnchecked(0));
    } else {
      return Option.none();
    }
  }

  contains(value: T): boolean {
    return has(this.storage, value, this.compare)
  }

  add(value: T): SortedArray<T> {
    return new SortedArray<T>(insorted(this.storage, value, this.compare), this.compare);
  }

  indexOf(value: T): number {
    return eq(this.storage, value, this.compare);
  }

  removeAt(index: number): SortedArray<T> {
    return new SortedArray<T>(removedAt(this.storage, index), this.compare);
  }

  updateAt(index: number, update: (value: T) => T): SortedArray<T> {
    return new SortedArray<T>(updatedAt(this.storage, index, update), this.compare);
  }

  replaceReinsert(index: number, value: T): SortedArray<T> {
    return new SortedArray<T>(purify((array: T[]) => {
      removeAt(array, index);
      insort(array, value, this.compare);
    })(this.storage), this.compare);
  }

  replaceAt(index: number, value: T): SortedArray<T> {
    if ((index == 0 || this.compare(this.getUnchecked(index - 1), value) <= 0)
        && (index == this.storage.length - 1 || this.compare(value, this.getUnchecked(index + 1)) >= 0)) {
      return new SortedArray<T>(replacedAt(this.storage, index, value), this.compare);
    } else {
      throw new Error(`Tried to replace element ${this.storage[index]} at index ${index} element ${value} this would result in an out of order list.`);
    }
  }

  removeOnce(value: T): [boolean, SortedArray<T>] {
    const [contained, withoutValue] = remsorted(this.storage, value, this.compare);
    return [contained, new SortedArray<T>(withoutValue, this.compare)];
  }

  removeAll(value: T): [boolean, SortedArray<T>] {
    const beforeIndex = lt(this.storage, value, this.compare);
    if (beforeIndex >= 0) {
      let itemsToRemove = 1;
      while (itemsToRemove < this.storage.length &&
             this.compare(value, this.storage[beforeIndex + itemsToRemove]) == 0) {
        ++itemsToRemove;
      }
      return [true, new SortedArray<T>(removedAt(this.storage, beforeIndex + 1, itemsToRemove), this.compare)];
    } else {
      return [false, this];
    }
  }

  static empty<T>(compare: ComparisonFunction<T> = compareValues): SortedArray<T> {
    return new SortedArray<T>([], compare);
  }

  static fromSorted<T>(sortedIterable: Iterable<T>, compare: ComparisonFunction<T> = compareValues) {
    return new SortedArray<T>([...sortedIterable], compare);
  }

  static fromUnsorted<T>(iterable: Iterable<T>, compare: ComparisonFunction<T> = compareValues): SortedArray<T> {
    const items = [...iterable];
    items.sort(compare);
    return new SortedArray<T>(items, compare);
  }
}
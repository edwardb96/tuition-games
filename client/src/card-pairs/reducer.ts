import { CardPairsAction, RETRIEVED_CARD_PAIRS, CREATED_CARD_PAIR, REMOVED_CARD_PAIR, UPDATED_CARD_PAIR, ATTEMPTED_RETRIEVE_CARD_PAIRS, RETRIEVE_CARD_PAIRS_FAILURE, JUMPED_TO_CARD_PAIR } from "./actions";
import { attemptedRetrieval, failedRetrieval, isRetrieving, map as mapRetrieval, notYetRetrieved, successfulRetrieval } from "../retrieval";
import { CardPairsState, compareCardPairs, compareImageCards } from "./state";
import { SortedArray } from "../sorted-array";
import flatMap from "iter-tools/methods/flat-map";
import map from "iter-tools/methods/map";
import reduce from "iter-tools/methods/reduce";
import { HashSet } from "prelude-ts";
import { ImageCard, imageCardsFromPair, NewCardPair, WithCategories } from "@cat-tuition/protocol/pairs";

export const initialState: CardPairsState = notYetRetrieved;

export const reducer = (state: CardPairsState = initialState, action: CardPairsAction) => {
  switch (action.type) {
    case ATTEMPTED_RETRIEVE_CARD_PAIRS: {
      return attemptedRetrieval(action.payload);
    }
    case RETRIEVED_CARD_PAIRS: {
      if (isRetrieving(state)) {
        const cardPairs = SortedArray.fromUnsorted(action.payload, compareCardPairs);
        const withCategoryFrom =
          ({ categories }: NewCardPair) => (ic: ImageCard): ImageCard & WithCategories => ({ ...ic, categories })
        const imageCards = SortedArray.fromUnsorted(
          flatMap(pair =>
            map(withCategoryFrom(pair),
                imageCardsFromPair(pair)), cardPairs),
          compareImageCards);
        const categories = HashSet.ofIterable(flatMap(pair => pair.categories, cardPairs));
        return successfulRetrieval(state, { pairs: cardPairs, categories, imageCards, jumpToPairIndex: null })
      } else {
        throw new Error('Got success before starting the retrieval when retrieving card pairs');
      }
    }
    case RETRIEVE_CARD_PAIRS_FAILURE: {
      if (isRetrieving(state)) {
        return failedRetrieval(state);
      } else {
        throw new Error('Got failure before starting the retrieval when retrieving card pairs');
      }
    }
    case UPDATED_CARD_PAIR: {
      return mapRetrieval(state, ({ pairs, categories, imageCards }) => {
        const { index, update } = action.payload;
        const beforeUpdate = pairs.getUnchecked(index);
        const updated = { ...beforeUpdate, ...update };
        const { categories: beforeCategories } = beforeUpdate;
        const { categories: afterCategories } = update;
        const withoutPreUpdate = reduce(imageCards,
          (ics, card) => ics.removeOnce({ ...card, categories: beforeCategories })[1],
          imageCardsFromPair(beforeUpdate));
        const withPostUpdate = reduce(withoutPreUpdate,
          (ics, card) => ics.add({ ...card, categories: afterCategories }),
          imageCardsFromPair(updated));
        const newPairs = pairs.replaceReinsert(index, updated);
        const jumpToPairIndex = newPairs.indexOf(updated)
        return {
          pairs: newPairs,
          categories: categories.addAll(update.categories),
          imageCards: withPostUpdate,
          jumpToPairIndex
        };
      });
    }
    case CREATED_CARD_PAIR: {
      return mapRetrieval(state, ({ pairs, categories, imageCards }) => {
        const { categories: newCardCategories } = action.payload;
        const withNewCards = reduce(imageCards,
          (ics, card) => ics.add({ ...card, categories: newCardCategories }),
          imageCardsFromPair(action.payload));
        const newPairs = pairs.add(action.payload);
        const jumpToPairIndex = newPairs.indexOf(action.payload);
        return {
          pairs: newPairs,
          categories: categories.addAll(action.payload.categories),
          imageCards: withNewCards,
          jumpToPairIndex
        };
      });
    }
    case JUMPED_TO_CARD_PAIR: {
      return mapRetrieval(state, retrieved => {
        return { ...retrieved, jumpToPairIndex: null }
      })
    }
    case REMOVED_CARD_PAIR: {
      return mapRetrieval(state, retrieved => {
        const { pairs, categories, imageCards } = retrieved
        const { categories: beforeCardCategories } = action.payload;
        const withoutRemovedCards = reduce(imageCards,
          (ics, card) => ics.removeOnce({ ...card, categories: beforeCardCategories })[1],
          imageCardsFromPair(action.payload));
        return {
          ...retrieved,
          pairs: pairs.removeOnce(action.payload)[1],
          imageCards: withoutRemovedCards,
          categories
        };
      });
    }
    default: {
      return state;
    }
  }
};
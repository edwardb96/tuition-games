import { CardPair, CardPairId, cardPairIdSchema, NewCardPair, UpdatedCardPair } from "@cat-tuition/protocol/pairs";
import { validOrNone } from "@cat-tuition/protocol/joi-ext";
import { Option } from "prelude-ts";

import Joi from "joi";
import axios from "axios";
const { object } = Joi.types();

export const fetchCardPairs = async (): Promise<CardPair[]> => {
  return await (await axios.get('/api/card-pairs')).data;
}

export const putCardPair = async (id: CardPairId, pair: UpdatedCardPair): Promise<boolean> => {
  try {
    const response = await axios.put(`/api/card-pairs/${id}`, pair);
    return (response.status == 200);
  } catch(err) {
    return false;
  }
}

export interface PostCardPairResponse {
  id: CardPairId;
}

export const postCardPairResponseSchema =
  object.keys({
    id: cardPairIdSchema
  });

const validatePostCardPairResponse = (value: unknown): Option<PostCardPairResponse> => {
  return validOrNone(postCardPairResponseSchema, value);
};

export const postCardPair = async (newPair: NewCardPair): Promise<Option<PostCardPairResponse>> => {
  try {
    const response = await axios.post(`/api/card-pairs`, newPair);
    if (response.status == 200) {
      return validatePostCardPairResponse(response.data);
    } else {
      return Option.none();
    }
  } catch (err) {
    return Option.none();
  }
}

export const deleteCardPair = async (id: string): Promise<boolean> => {
  try {
    const response = await axios.delete(`/api/card-pairs/${id}`);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}
import React, { useState, useEffect, ChangeEvent, MouseEvent } from "react";
import { connect } from "react-redux";
import './card-pair-selector.scss';

import filter from "iter-tools/methods/filter";
import map from "iter-tools/methods/map";
import includesAny from "iter-tools/methods/includes-any";
import { FieldProps } from "formik";

import { BeginInfo, CardCategory, CardPair, cardPairToString, cardCategoriesSchema, cardCategorySchema, NewCardPair } from "@cat-tuition/protocol/pairs";
import { CardFrontFace } from "./game-card";
import { retrieveCardPairs, createCardPair } from "../actions";
import { AppState } from "../../redux-store";
import { asOption, get, hasRetrievalFailed, hasRetrievalFinished, hasRetrievalSucceeded, shouldReretrieve } from "../../retrieval";
import { CardPairsState, hasCardPair } from "../state";
import cx from "classnames";
import { TagsInput } from "../../tags-input/tags-input";
import { any } from "@cat-tuition/protocol/iter-tools-ext";

export interface CardSelectorProps extends FieldProps<CardPair[], BeginInfo> {
  retrieveCardPairs(): Promise<boolean>;
  createCardPair(pair: NewCardPair): Promise<string | null>;
  cardColour: string;
  cardPairs: CardPairsState
}

export interface CardSelectorState {
  query: string;
  categoriesFilter: string[];
}

export interface CardPairPreviewProps {
  pair: CardPair;
  canClick?: boolean;
  onClick?: () => void;
  onDelete?: () => void;
  isSelected?: boolean;
  cardColour: string;
}

export const CardPairPreview = (props: CardPairPreviewProps) => {
  const { pair, isSelected, onDelete, onClick, canClick, cardColour } = props;
  const { firstCard, secondCard } = pair;

  return (
    <div className="card-pair column is-6">
      <div className="columns">
        <div className="column is-6 has-text-centered card-parent">
          <div className={`game-card is-flipped is-${cardColour}-coloured`}>
            <CardFrontFace
              card={firstCard}
              isSelected={isSelected}
              canClick={canClick}
              onClick={onClick} />
          </div>
        </div>
        <div className="column is-6 has-text-centered card-parent">
          <div className={`game-card is-flipped is-${cardColour}-coloured`}>
            <CardFrontFace
              card={secondCard}
              isSelected={isSelected}
              canClick={canClick}
              onClick={onClick}
              onDelete={onDelete}  />
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardSelector = (props: CardSelectorProps) => {
  const [state, setState] = useState<CardSelectorState>({
    query: '',
    categoriesFilter: [],
  });

  const { field, form, cardColour } = props;
  const { name, value: selectedPairs } = field;
  const { setFieldValue, setFieldTouched } = form;
  const { query, categoriesFilter } = state;

  const loadCardPairs = async () => {
    const { retrieveCardPairs, cardPairs } = props;
    if (shouldReretrieve(cardPairs, 5)) {
      await retrieveCardPairs();
    }
  };

  useEffect(() => {
    loadCardPairs();
  });

  const queryCardPair: NewCardPair = {
    firstCard: { text: query },
    secondCard: { text: query },
    categories: categoriesFilter
  };

  const putPair = async () => {
    try {
      const { createCardPair } = props;
      const createdId = await createCardPair(queryCardPair);
      if (createdId != null) {
        setState({ ...state, query: '' });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onSelectedPair = (pair: CardPair) => {
    selectedPairs.push(pair);
    setState({ ...state, query: '' });
    setFieldTouched(name, true, false);
    setFieldValue(name, selectedPairs);
  }

  const onDeselectedPair = (index: number) => {
    selectedPairs.splice(index, 1);
    setFieldTouched(name, true, false);
    setFieldValue(name, selectedPairs);
  }

  const onChangeQuery = (e: ChangeEvent<HTMLInputElement>) =>
    setState({ ...state, query: e.target.value });

  const onChangeCategories = (categories: CardCategory[]) => {
    setState({ ...state, categoriesFilter: categories });
  }

  const onResetCards = (e: MouseEvent<HTMLButtonElement>) => {
    selectedPairs.splice(0, selectedPairs.length);
    setFieldTouched(name, true, false);
    setFieldValue(name, selectedPairs);
  }

  const { cardPairs } = props;
  const canPut = query.length > 0 &&
    hasRetrievalSucceeded(cardPairs) &&
    !hasCardPair({
      ...queryCardPair,
      /* id should be unused because the queryCardPair is always a text card */
      id: (null as unknown as string) }, get(cardPairs).pairs);

  const renderPair = (pair: CardPair) => {
    const isSelected = any(p => p.id == pair.id, selectedPairs);
    return (
      <a key={pair.id}
        className={cx("panel-block", { 'is-selected': isSelected })}
        onClick={() => onSelectedPair(pair)} >
        {cardPairToString(pair)}
      </a>
    );
  };

  const renderFilteredPairs = (cardPairs: Iterable<CardPair>) => {
    const items = filter(cp => {
      const cardPairName = cardPairToString(cp).toLowerCase();
      const queryLowerCase = query.toLowerCase();
      const queryLowerCaseSlashAsDevide = query.toLowerCase().replaceAll('/', '÷');
      const meetsCategoryFilter = (categoriesFilter.length == 0 || includesAny(categoriesFilter, cp.categories))
      return meetsCategoryFilter &&
        (cardPairName.includes(queryLowerCase) ||
         cardPairName.includes(queryLowerCaseSlashAsDevide))
    }, cardPairs);
    return (
      <div className="items">
        {[...map(renderPair, items)]}
      </div>
    );
  };

  const renderLoader = () => {
    return (
      <div className="loader-wrapper">
        <div className="loader is-loading" />
      </div>
    );
  };

  const renderPreview = () => {
    return (
      <div className="columns is-multiline board">
        {
          selectedPairs.map((pair, index) =>
            <CardPairPreview
              key={index}
              cardColour={cardColour}
              pair={pair}
              onDelete={() => onDeselectedPair(index)} />)
        }
      </div>
    );
  }

  return (
    <>
      <nav className="card-pair-selector panel">
        <div className="panel-block">
          <div className="field control has-addons">
            <div className="control is-expanded has-icons-left">
              <input onChange={e => onChangeQuery(e)} className="input" type="text" placeholder="Search" value={state.query} />
              <span className="icon is-left">
                <i className="fas fa-search" aria-hidden="true"></i>
              </span>
            </div>
            <div className="control">
              <button type="button" onClick={() => putPair()} className="button is-link" disabled={!canPut}>
                <i className="fas fa-plus" aria-hidden="true"></i>
              </button>
            </div>
          </div>
        </div>
        <div className="panel-block">
          <div className="field categories-filter control has-icons-left">
            <TagsInput
              onChange={onChangeCategories}
              placeholder="Filter by categories"
              suggestions={asOption(cardPairs).map(({ categories }) => categories.toArray())
                            .getOrElse([])}
              tagSchema={cardCategorySchema}
              value={categoriesFilter}/>
            <span className="icon is-left">
              <i className="fas fa-tags" aria-hidden="true"></i>
            </span>
          </div>
        </div>
        {hasRetrievalFinished(cardPairs)
          ? hasRetrievalFailed(cardPairs)
            /* TODO Write some ccs to display this error message more cleanly */
            ? <p className="is-danger">Failed to retrieve card pairs</p>
           : renderFilteredPairs(get(cardPairs).pairs)
          : renderLoader() }

        <div className="panel-block">
          <button type="button" onClick={onResetCards} className="button is-link is-outlined is-fullwidth">
            Reset Cards
          </button>
        </div>
      </nav>
      {renderPreview()}
    </>
  );
}
export default connect((s : AppState) => ({ cardPairs: s.cardPairs }), { retrieveCardPairs, createCardPair })(CardSelector);

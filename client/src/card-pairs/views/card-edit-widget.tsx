import React, { useState } from "react";
import { useEffect } from "react";
import { form as joiToForms } from "joi-errors-for-forms";
import { connect } from "react-redux";
import { ErrorMessage, Field, Form, Formik, FormikErrors, FormikHelpers, FormikProps } from "formik";

import { Card, cardCategoriesSchema, CardCategory, cardCategorySchema, CardPair, CardPairId, imageCardSchema, isImageCard, isTextCard, matchCard, NewCardPair, newCardPairSchema, textCardSchema, UpdatedCardPair } from "@cat-tuition/protocol/pairs";
import { createCardPair, updateCardPair, removeCardPair } from "../actions";

import { AppState } from "../../redux-store";

import cx from "classnames";
import { get, hasRetrievalSucceeded } from "../../retrieval";
import { CardFrontFace } from "./game-card";
import { ImagePicker, useImagePicker } from "../../image-picker/views/image-picker-field";
import { TagsField } from "./tags-field";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import ImagePickerModal, { ImagePickerTab } from "../../image-picker/views/image-picker-modal";
import { ImageId } from "@cat-tuition/protocol/image";
import "./card-edit-widget.scss";
import { FormikErrorsBuilder } from "../../formik-errors-builder";

const convertToForms = joiToForms();

export interface CardPairEditWidgetProps {
  initialTextOrDescription?: string | null;
  initialCategories?: CardCategory[] | null;
  initialIndex?: number;
  initialCardPair?: CardPair;
  cardCategories: CardCategory[];
  createCardPair(pair: NewCardPair): Promise<string | null>;
  updateCardPair(id: CardPairId, index: number, pair: UpdatedCardPair): Promise<boolean>;
  removeCardPair(id: CardPair): Promise<boolean>;
  onReset(): void;
}

interface CardEdits {
  textOrDescription: string;
  image: string | null;
}

interface CardPairEdits {
  firstCard: CardEdits;
  secondCard: CardEdits;
  categories: CardCategory[];
}

const CardPairEditWidget = (props: CardPairEditWidgetProps) => {
  const { createCardPair, updateCardPair, removeCardPair, onReset } = props;
  const { initialCardPair, initialIndex, initialTextOrDescription, initialCategories, cardCategories } = props;

  const cardFromEdits = ({ textOrDescription, image }: CardEdits): Card => {
    if (image != null) {
      return { image, description: textOrDescription };
    } else {
      return { text: textOrDescription };
    }
  }

  const updatedOrNewFromEdits = ({ firstCard, secondCard, categories }: CardPairEdits): NewCardPair => {
    return {
      firstCard: cardFromEdits(firstCard),
      secondCard: cardFromEdits(secondCard),
      categories: categories
    }
  }

  const validateForm = (values: CardPairEdits): FormikErrors<CardPairEdits> | undefined => {
    const { firstCard, secondCard, categories } = updatedOrNewFromEdits(values);

    const validateCard = (card: Card): FormikErrors<CardEdits> | null => {
      if (isTextCard(card)) {
        const result = textCardSchema.validate(card);
        return mapNullable(result.error, e => {
          const { text: textOrDescription } = convertToForms(e);
          return { textOrDescription }
        });
      } else {
        const result = imageCardSchema.validate(card);
        return mapNullable(result.error, e => {
          const { image, description: textOrDescription } = convertToForms(e);
          return { textOrDescription }
        });
      }
    };

    const validateCardCategories = (categories: CardCategory[]): CardCategory[] | null => {
      const result = cardCategoriesSchema.validate(categories);
      return mapNullable(result.error, e => {
        return convertToForms(e);
      });
    }

    const errors = FormikErrorsBuilder.build<CardPairEdits>()
                                      .add('firstCard', validateCard(firstCard))
                                      .add('secondCard', validateCard(secondCard))
                                      .add('categories', validateCardCategories(categories))
                                      .get();
    return errors;
  };

  const onSubmit = async (info: CardPairEdits, { setSubmitting, resetForm, setFieldError } : FormikHelpers<CardPairEdits>) => {
    if (initialIndex != null && initialCardPair != null) {
      const updatedPair = updatedOrNewFromEdits(info);
      const ok = await updateCardPair(initialCardPair.id, initialIndex, updatedPair);
      if (ok) {
        resetForm();
        onReset();
      } else {
        setFieldError('general', 'There was a problem updating the card pair, perhaps the server is down. Refresh and try again or contact an Admin')
      }
    } else {
      const newPair = updatedOrNewFromEdits(info);
      const newId = await createCardPair(newPair);
      if (newId != null) {
        resetForm();
        onReset();
      } else {
        setFieldError('general', 'There was a problem creating the card pair, perhaps the server is down. Refresh and try again or contact an Admin')
      }
    }
    setSubmitting(false);
  };

  const cardPairEdits: CardPairEdits = (() => {
    if (initialCardPair != null) {
      const { firstCard, secondCard, categories } = initialCardPair;
      const editsFromCard = (card: Card): CardEdits => {
        return matchCard<CardEdits>(card, {
          text: ({ text }) => ({ textOrDescription: text, image: null }),
          image: ({ image, description }) => ({ image, textOrDescription: description })
        })
      };
      return {
        firstCard: editsFromCard(firstCard),
        secondCard: editsFromCard(secondCard),
        categories
      };
    } else {
      return {
        firstCard: { textOrDescription: initialTextOrDescription || '', image: null },
        secondCard: { textOrDescription: initialTextOrDescription || '', image: null },
        categories: initialCategories || []
      };
    }
  })();

  const { title, isOpen, selectedTab, selectedImage,
    onPickImage, onPickedImage, onCancelPick } =
      useImagePicker({
        isOpen: false,
        selectedTab: ImagePickerTab.FromFile
      });

  return (
    <>
      <ImagePickerModal
        isOpen={isOpen}
        onSubmit={onPickedImage}
        onCancel={onCancelPick}
        title={title}
        selectedTab={selectedTab}
        selectedImage={selectedImage}/>

      <Formik initialValues={cardPairEdits}
              enableReinitialize
              validate={validateForm}
              onSubmit={onSubmit}>
        {
          ({ isSubmitting, errors, touched, values, setFieldValue, resetForm }: FormikProps<CardPairEdits>) => {

            useEffect(() => {
              if (initialCardPair == null && !touched.secondCard) {
                setFieldValue('secondCard.textOrDescription', values.firstCard.textOrDescription);
              }
            }, [values.firstCard.textOrDescription]);

            const onDelete = async () => {
              if (initialCardPair != null) {
                onReset();
                const ok = await removeCardPair(initialCardPair);
                if (!ok) {
                  console.log(`Failed to remove user with id ${initialCardPair.id}`);
                }
              }
            }

            const onCancel = () => {
              if (initialCardPair == null) {
                resetForm();
              }
              onReset();
            }

            const firstCardIsDanger =
              errors.firstCard && errors.firstCard.textOrDescription &&
              touched.firstCard && touched.firstCard.textOrDescription;

            const secondCardIsDanger =
              errors.secondCard && errors.secondCard.textOrDescription &&
              touched.secondCard && touched.secondCard.textOrDescription;


            return (
              <>
              <Form className="form box card-edit-widget">
                <p className="title">{initialCardPair == null ? 'Add' : 'Edit'} Card Pair</p>

                <div className="fields">
                  <div className="field">
                    <label className="label">Categories</label>
                    <div className="control">
                      <Field component={TagsField}
                        name="categories"
                        placeholder="Add Categories"
                        tagSchema={cardCategorySchema}
                        suggestions={cardCategories} />
                    </div>
                    <ErrorMessage name="categories" render={msg => <p className="help is-danger">{msg}</p>}/>
                  </div>

                  <div className="field">
                    <label className="label">Text or Description</label>
                    <div className="control">
                      <Field className={cx("input", { 'is-danger': firstCardIsDanger })}
                            type="text"
                            name="firstCard.textOrDescription"
                            placeholder="Text or Description" />
                    </div>
                    <ErrorMessage name="firstCard.textOrDescription" render={msg => <p className="help is-danger">{msg}</p>}/>
                  </div>

                  <div className="field">
                    <label className="label">Image</label>
                    <Field component={ImagePicker}
                      name="firstCard.image"
                      title="Pick an image for the first card"
                      initialTab={ImagePickerTab.FromFile}
                      onPickImage={onPickImage} />
                    <ErrorMessage name="firstCard.image" render={msg => <p className="help is-danger">{msg}</p>} />
                    <div className="card-parent">
                      <div className="game-card is-white-coloured is-flipped">
                        <CardFrontFace card={cardFromEdits(values.firstCard)}/>
                      </div>
                    </div>
                  </div>

                  <div className="field">
                    <label className="label">Text or Description</label>
                    <div className="control">
                      <Field
                        className={cx("input", { 'is-danger': secondCardIsDanger })}
                            type="text"
                            name="secondCard.textOrDescription"
                            placeholder="Text or Description" />
                    </div>
                    <ErrorMessage name="secondCard.textOrDescription" render={msg => <p className="help is-danger">{msg}</p>}/>
                  </div>


                  <div className="field">
                    <label className="label">Image</label>
                    <Field component={ImagePicker}
                      name="secondCard.image"
                      title="Pick an image for the second card"
                      initialTab={ImagePickerTab.FromFile}
                      onPickImage={onPickImage} />
                    <ErrorMessage name="secondCard.image" render={msg => <p className="help is-danger">{msg}</p>} />
                    <div className="card-parent">
                      <div className="game-card is-white-coloured is-flipped">
                        <CardFrontFace card={cardFromEdits(values.secondCard)}/>
                      </div>
                    </div>
                  </div>

                  <div className="field">
                    <p className="help is-danger">{(errors as any).general}</p>
                  </div>
                </div>

                <div className="fields">
                  <div className="field is-grouped">
                    <p className="control">
                      <button className="button is-link" type="submit" disabled={isSubmitting}>
                        Save Card Pair
                      </button>
                    </p>
                    <p className="control">
                      <button onClick={() => onCancel()} className="button is-link is-light" type="button" disabled={isSubmitting}>
                        Cancel
                      </button>
                    </p>
                    <p className="control">
                      <button onClick={() => onDelete()} className="button is-link is-light" type="button" disabled={isSubmitting}>
                        Delete
                      </button>
                    </p>
                  </div>
                </div>
              </Form>
              </>
            );
          }
        }
      </Formik>
    </>
  );
}
const mapState = (s: AppState, { initialIndex }: { initialIndex?: number }) => {
  const { cardPairs } = s;
  if (initialIndex != null && hasRetrievalSucceeded(cardPairs)) {
    const { pairs } = get(cardPairs)
    const initialCardPair = pairs.get(initialIndex).getOrThrow('Provided invalid pair index to edit');
    return { initialCardPair }
  } else {
    return { }
  }
};
export default connect(mapState, { createCardPair, updateCardPair, removeCardPair })(CardPairEditWidget);
import React, { MouseEvent, useEffect, useState } from "react";
import LoadingPage from "../../loading-page/views/loading-page";
import Navbar from "../../navbar/views/navbar";
import CardPairEditWidget from "./card-edit-widget";

import { CardCategory, CardPair, cardPairToString, cardCategorySchema, NewCardPair } from "@cat-tuition/protocol/pairs";
import { User } from "@cat-tuition/protocol/user";
import { SimpleColour } from "@cat-tuition/protocol/colour";
import { HashSet, Option } from "prelude-ts";
import { Redirect } from "react-router";
import { AppState } from "../../redux-store";
import { connect } from "react-redux";
import { CardPairPreview } from "./card-pair-selector";
import './card-page.scss';
import { asOption, shouldReretrieve } from "../../retrieval";
import { CardPairsState } from "../state";
import { retrieveCardPairs, jumpedToCardPair } from "../actions";
import map from "iter-tools/methods/map";
import filter from "iter-tools/methods/filter";
import drop from "iter-tools/methods/drop";
import take from "iter-tools/methods/take";
import enumerate from "iter-tools/methods/enumerate";
import includesAny from "iter-tools/methods/includes-any";
import cx from "classnames";
import { eq } from "sorted-array-functions";
import { compareValues } from "@cat-tuition/protocol/compare";
import { TagsInput } from "../../tags-input/tags-input";

export interface CardsPageProps {
  user?: User;
  cardPairs: CardPairsState;
  retrieveCardPairs(): Promise<boolean>;
  jumpedToCardPair(): void;
}

const pageSize =  4 * 2;

export interface CardsPageState {
  selectedIndex: number | null;
  newCardText: string | null;
  newCardCategories: CardCategory[] | null;
  query: string;
  categoriesFilter: CardCategory[];
  pageStartIndex: number;
}

export const CardsPage = (props: CardsPageProps) => {
  const [state, setState] = useState<CardsPageState>({
    query: '',
    newCardText: null,
    newCardCategories: null,
    selectedIndex: null,
    categoriesFilter: [],
    pageStartIndex: 0
  });

  const { user } = props;
  const { selectedIndex: selectedId, categoriesFilter, query, pageStartIndex } = state;

  const loadCardPairs = async () => {
    const { retrieveCardPairs, cardPairs } = props;
    if (shouldReretrieve(cardPairs, 5)) {
      await retrieveCardPairs();
    }
  };

  const { cardPairs: cardPairsRetrieval, jumpedToCardPair } = props;
  useEffect(() => {
    if (user != null && user.canManageCardPairs()) {
      loadCardPairs();
    }
  }, [user]);

  const maybeCardPairs = asOption(cardPairsRetrieval);

  type PageJump = { pageIndex: number, clearFilter: boolean }

  const retrievalInfo: Option<{
    filteredPairs: [number, CardPair][],
    jumpTo: Option<PageJump>
    categories: HashSet<string>
  }> = maybeCardPairs.map(({ pairs, categories, jumpToPairIndex: indexInUnfiltered }) => {
    const meetsCategoryFilter = (pair: NewCardPair): boolean =>
      categoriesFilter.length == 0 || includesAny(categoriesFilter, pair.categories)

    const meetsQueryFilter = (pair: NewCardPair): boolean =>
      cardPairToString(pair).includes(query);

    const meetsFilter = (pair: NewCardPair): boolean =>
      meetsCategoryFilter(pair) && meetsQueryFilter(pair)

    const filteredPairs: [number, CardPair][] = [...filter(([_, pair]) => meetsFilter(pair), enumerate(pairs))];
    const jumpTo: Option<PageJump> = (() => {
      if (indexInUnfiltered != null) {
        const compareFirst = ([a, _a]: [number, CardPair], [b, _b]: [number, CardPair]) => compareValues(a, b);
        if (filteredPairs.length > 0) {
          const indexInFiltered = eq(filteredPairs, [indexInUnfiltered, filteredPairs[0][1]], compareFirst);
          if (indexInFiltered != -1) {
            return Option.of<PageJump>({ pageIndex: Math.floor(indexInFiltered / pageSize), clearFilter: false });
          } else {
            return Option.of<PageJump>({ pageIndex: Math.floor(indexInUnfiltered / pageSize), clearFilter: true });
          }
        }
      }
      return Option.none<PageJump>()
    })();

    return { filteredPairs, jumpTo, categories };
  });

  const hasJump = retrievalInfo.map(({ jumpTo }) => jumpTo.isSome()).getOrElse(false);

  useEffect(() => {
    retrievalInfo.ifSome(({ jumpTo }) =>
      jumpTo.ifSome(({ pageIndex, clearFilter }: PageJump) => {
        const pageStartIndex = pageIndex * pageSize;
        if (clearFilter) {
          setState({ ...state, pageStartIndex, query: '', categoriesFilter: [] });
        } else {
          setState({ ...state, pageStartIndex });
        }
        jumpedToCardPair();
      }));
  }, [user, hasJump]);

  if (user != null) {
    if (user.canManageCardPairs()) {
      return retrievalInfo.match({
        Some: ({ filteredPairs, categories }) => {
          const pairsOnCurrentPageAndImediatelyAfter = [
            ...take(pageSize + 1, drop(pageStartIndex, filteredPairs))];

          const pairsOnCurrentPage = take(pageSize, pairsOnCurrentPageAndImediatelyAfter);
          const isFirstPage = pageStartIndex == 0;
          const isLastPage = pairsOnCurrentPageAndImediatelyAfter.length <= pageSize;

          const onEditReset = () =>
            setState((s: CardsPageState) => ({ ...s, selectedIndex: null, newCardText: null, newCardCategories: null }));

          const onQueryAdd = (e: MouseEvent<HTMLButtonElement>) =>
            setState({ ...state, newCardText: state.query, newCardCategories: categoriesFilter })

          const onSelectedPair = (index: number) =>
            setState({ ...state, selectedIndex: index });

          const onChangeCategories = (categoriesFilter: CardCategory[]) =>
            setState({ ...state, categoriesFilter, pageStartIndex: 0 });

          const onChangeQuery = (query: string) =>
            setState({ ...state, query, pageStartIndex: 0 });

          const onNextPage = () =>
            setState({ ...state, pageStartIndex: state.pageStartIndex + pageSize });

          const onPreviousPage = () =>
            setState({ ...state, pageStartIndex: state.pageStartIndex - pageSize });


          const editWidget = (() => {
            if (state.selectedIndex != null) {
              return (
                <CardPairEditWidget
                  initialIndex={state.selectedIndex}
                  cardCategories={categories.toArray()}
                  onReset={onEditReset} />
              );
            } else {
              return (
                <CardPairEditWidget
                  initialTextOrDescription={state.newCardText}
                  initialCategories={state.newCardCategories}
                  cardCategories={categories.toArray()}
                  onReset={onEditReset} />
              );
            }
          })();

          return (
            <>
              <Navbar shouldConnect={false}/>

              <div className="card-page management hero-body container">
                <p className="title is-1">Cards</p>

                <div className="columns">
                  <div className="column is-two-thirds card-view">
                    <div className="container">
                      <section className="card-search-options">
                        <div className="field has-addons">
                          <div className="control is-expanded has-icons-left">
                            <input onChange={e => onChangeQuery(e.target.value)}
                                  className="input"
                                  type="text"
                                  placeholder="Search"
                                  value={state.query}/>
                            <span className="icon is-left">
                              <i className="fas fa-search" aria-hidden="true"></i>
                            </span>
                          </div>
                          <div className="control">
                            <button
                              type="button"
                              onClick={onQueryAdd}
                              className="button is-link">
                              <i className="fas fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div className="field control has-icons-left">
                          <TagsInput
                            onChange={onChangeCategories}
                            placeholder="Filter by categories"
                            suggestions={categories.toArray()}
                            tagSchema={cardCategorySchema}
                            value={categoriesFilter}/>
                          <span className="icon is-left">
                            <i className="fas fa-tags" aria-hidden="true"></i>
                          </span>
                        </div>
                      </section>

                      <section className="card-items columns is-multiline">
                        {map(([index, pair]) =>
                          <CardPairPreview
                            key={pair.id}
                            pair={pair}
                            isSelected={index == selectedId}
                            cardColour={SimpleColour.White}
                            canClick
                            onClick={() => onSelectedPair(index)} />, pairsOnCurrentPage)}
                      </section>

                      <div className="spacer"/>

                      <footer className="pagination-controls">
                        <nav className="pagination is-centered" role="navigation" aria-label="pagination">
                          <button onClick={onPreviousPage} className="pagination-previous" disabled={isFirstPage}>
                            Previous Page
                          </button>
                          <button onClick={onNextPage} className="pagination-next" disabled={isLastPage}>
                            Next Page
                          </button>
                        </nav>
                      </footer>
                    </div>
                  </div>

                  <div className="column is-one-third">
                    {editWidget}
                  </div>
                </div>
              </div>
            </>
          );
        },
        None: () =>
          <LoadingPage shouldConnect={false} message="Loading the Card Management Dashboard..."/>
      })
    } else {
      return <Redirect push to='/404'/>
    }
  } else {
    return <LoadingPage shouldConnect={false} message="Loading the Card Management Dashboard..."/>
  }
};

const mapState = ({ profile, cardPairs }: AppState) => ({ user: profile.user, cardPairs });
export default connect(mapState, { retrieveCardPairs, jumpedToCardPair })(CardsPage);
import { CardCategory, CardPair, cardPairToString, ImageCard, NewCardPair, WithCategories } from "@cat-tuition/protocol/pairs";
import { compareWithKey } from "@cat-tuition/protocol/compare";
import { HashSet } from "prelude-ts";
import { Retrieve } from "../retrieval";
import { SortedArray } from "../sorted-array";

export type CardPairsState = Retrieve<{
  pairs: SortedArray<CardPair>,
  jumpToPairIndex: number | null;
  imageCards: SortedArray<ImageCard & WithCategories>,
  categories: HashSet<CardCategory>
}>;

export const compareCardPairs = (a: NewCardPair, b: NewCardPair): number =>
  compareWithKey(a, b, cardPairToString);

export const compareImageCards = (a: ImageCard, b: ImageCard): number =>
  compareWithKey(a, b, i => [i.description, i.image]);

export const hasCardPair = (cardPair: CardPair, cardPairs: SortedArray<CardPair>): boolean =>
  cardPairs.contains(cardPair);

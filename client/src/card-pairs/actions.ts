import { Card, CardPair, CardPairId, NewCardPair, UpdatedCardPair } from "@cat-tuition/protocol/pairs";
import { Action, TagAction, ThunkAction } from "../redux-utils";
import { deleteCardPair, fetchCardPairs, postCardPair, putCardPair } from "./api";
//import { fetchCardPairs, postCardPair, putCardPair, deleteCardPair } from "./api";

export const ATTEMPTED_RETRIEVE_CARD_PAIRS = 'CARD_PAIRS::RETRIEVE_CARD_PAIRS';
export const RETRIEVED_CARD_PAIRS = 'CARD_PAIRS::RETRIEVED_CARD_PAIRS';
export const RETRIEVE_CARD_PAIRS_FAILURE = 'CARD_PAIRS::RETRIEVE_CARD_PAIRS_FAILURE';
export const CREATED_CARD_PAIR = 'CARD_PAIRS::CREATED_CARD_PAIR';
export const REMOVED_CARD_PAIR = 'CARD_PAIRS::REMOVED_CARD_PAIR';
export const UPDATED_CARD_PAIR = 'CARD_PAIRS::UPDATED_CARD_PAIR';
export const JUMPED_TO_CARD_PAIR = 'CARD_PAIRS::JUMPED_TO';

export interface CardPairUpdateInfo {
  index: number;
  update: UpdatedCardPair;
}

export interface RetrievedCardPairsAction extends Action<typeof RETRIEVED_CARD_PAIRS, CardPair[]> { };
export interface AttemptedRetrieveCardPairsAction extends Action<typeof ATTEMPTED_RETRIEVE_CARD_PAIRS, Date> { };
export interface RetrieveCardPairsFailureAction extends TagAction<typeof RETRIEVE_CARD_PAIRS_FAILURE> { };
export interface CreatedCardPairAction extends Action<typeof CREATED_CARD_PAIR, CardPair> { };
export interface UpdatedCardPairAction extends Action<typeof UPDATED_CARD_PAIR, CardPairUpdateInfo> { };
export interface RemovedCardPairAction extends Action<typeof REMOVED_CARD_PAIR, CardPair> { };
export interface JumpedToCardPairAction extends TagAction<typeof JUMPED_TO_CARD_PAIR> { };

export type CardPairsAction =
  AttemptedRetrieveCardPairsAction |
  RetrievedCardPairsAction |
  RetrieveCardPairsFailureAction |
  CreatedCardPairAction |
  RemovedCardPairAction |
  UpdatedCardPairAction |
  JumpedToCardPairAction;

export const retrievedCardPairs = (cardPairs: CardPair[]): RetrievedCardPairsAction => ({
  type: RETRIEVED_CARD_PAIRS,
  payload: cardPairs
});

export const retrieveCardPairsFailure = (): RetrieveCardPairsFailureAction => ({
  type: RETRIEVE_CARD_PAIRS_FAILURE,
});

export const attemptedRetrieveCardPairs = (when: Date): AttemptedRetrieveCardPairsAction => ({
  type: ATTEMPTED_RETRIEVE_CARD_PAIRS,
  payload: when
});

export const jumpedToCardPair = (): JumpedToCardPairAction => ({
  type: JUMPED_TO_CARD_PAIR,
});


export const retrieveCardPairs = (): ThunkAction<Promise<boolean>> => async dispatch => {
  dispatch(attemptedRetrieveCardPairs(new Date()))
  try {
    const cardPairs = await fetchCardPairs();
    dispatch(retrievedCardPairs(cardPairs));
    return true;
  } catch(err) {
    dispatch(retrieveCardPairsFailure());
    return false;
  }
};

export const createdCardPair = (pair: CardPair): CreatedCardPairAction => ({
  type: CREATED_CARD_PAIR,
  payload: pair
})

export const createCardPair = (pair: NewCardPair): ThunkAction<Promise<string | null>> => async dispatch => {
  const response = await postCardPair(pair);
  return response.map(response => {
    const { id } = response;
    dispatch(createdCardPair({ id, ...pair }));
    return id;
  }).getOrNull();
};

export const updatedCardPair = (index: number, update: UpdatedCardPair): UpdatedCardPairAction => ({
  type: UPDATED_CARD_PAIR,
  payload: { index, update }
})

export const updateCardPair = (id: string, index: number, pair: UpdatedCardPair): ThunkAction<Promise<boolean>> =>
  async dispatch => {
    const ok = await putCardPair(id, pair);
    if (ok) {
      dispatch(updatedCardPair(index, pair));
    }
    return ok;
  };

export const removedCardPair = (cardPair: CardPair): RemovedCardPairAction => ({
  type: REMOVED_CARD_PAIR,
  payload: cardPair
});

export const removeCardPair = (cardPair: CardPair): ThunkAction<Promise<boolean>> => async dispatch => {
  const ok = await deleteCardPair(cardPair.id);
  if (ok) {
    dispatch(removedCardPair(cardPair));
  }
  return ok;
};
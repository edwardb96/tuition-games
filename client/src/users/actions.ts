import { NewUser, StoredUser, UpdatedUser } from "@cat-tuition/protocol/user";
import { AttemptedRetrieveCardPairsAction } from "../card-pairs/actions";
import { Action, TagAction, ThunkAction } from "../redux-utils";
import { fetchUsers, postUser, putUser, deleteUser } from "./api";

export const RETRIEVED_USERS = 'USERS::FETCHED_USERS';
export const RETRIEVE_USERS_FAILURE = 'USERS::FAILED_RETRIEVE';
export const ATTEMPTED_RETRIEVE_USERS = 'USERS::ATTEMPTED_RETRIEVE';
export const CREATED_USER = 'USERS::CREATED_USER';
export const REMOVED_USER = 'USERS::REMOVED_USER';
export const UPDATED_USER = 'USERS::UPDATED_USER';

export interface AttemptedRetrieveUsersAction extends Action<typeof ATTEMPTED_RETRIEVE_USERS, Date> { };
export interface RetrievedUsersAction extends Action<typeof RETRIEVED_USERS, StoredUser[]> { };
export interface RetrieveUsersFailureAction extends TagAction<typeof RETRIEVE_USERS_FAILURE> { };

export interface CreatedUserAction extends Action<typeof CREATED_USER, StoredUser> { };
export interface UpdatedUserAction extends Action<typeof UPDATED_USER, StoredUser> { };
export interface RemovedUserAction extends Action<typeof REMOVED_USER, string> { };

export type UsersAction =
  RetrievedUsersAction |
  AttemptedRetrieveUsersAction |
  RetrieveUsersFailureAction |
  CreatedUserAction |
  RemovedUserAction |
  UpdatedUserAction;

export const retrievedUsers = (users: StoredUser[]): RetrievedUsersAction => ({
  type: RETRIEVED_USERS,
  payload: users
});

export const retrieveUsers = (): ThunkAction<Promise<boolean>> => async dispatch => {
  dispatch(attemptedRetrieveUsers(new Date()))
  try {
    const cardPairs = await fetchUsers();
    dispatch(retrievedUsers(cardPairs));
    return true;
  } catch(err) {
    dispatch(retrieveUsersFailure());
    return false;
  }
};

export const attemptedRetrieveUsers = (when: Date): AttemptedRetrieveUsersAction => ({
  type: ATTEMPTED_RETRIEVE_USERS,
  payload: when
});

export const retrieveUsersFailure = (): RetrieveUsersFailureAction => ({
  type: RETRIEVE_USERS_FAILURE,
});

export const createdUser = (user: NewUser): CreatedUserAction => ({
  type: CREATED_USER,
  payload: user
})

export const createUser = (user: NewUser): ThunkAction<Promise<boolean>> => async dispatch => {
  const ok = await postUser(user);
  if (ok) {
    dispatch(createdUser(user));
  }
  return ok;
};

export const updatedUser = (id: string, user: UpdatedUser): UpdatedUserAction => ({
  type: UPDATED_USER,
  payload: { id, ...user }
})

export const updateUser = (id: string, user: UpdatedUser): ThunkAction<Promise<boolean>> => async dispatch => {
  const ok = await putUser(id, user);
  if (ok) {
    dispatch(updatedUser(id, user));
  }
  return ok;
};

export const removedUser = (id: string): RemovedUserAction => ({
  type: REMOVED_USER,
  payload: id
})

export const removeUser = (id: string): ThunkAction<Promise<boolean>> => async dispatch => {
  const ok = await deleteUser(id);
  if (ok) {
    dispatch(removedUser(id));
  }
  return ok;
};
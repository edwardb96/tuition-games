import { Role } from "@cat-tuition/protocol/user";
import { HashMap } from "prelude-ts";
import { UsersAction, RETRIEVED_USERS, CREATED_USER, REMOVED_USER, UPDATED_USER, RETRIEVE_USERS_FAILURE, ATTEMPTED_RETRIEVE_USERS } from "./actions";
import { UsersState } from "./state";
import map from "iter-tools/methods/map";
import { attemptedRetrieval, failedRetrieval, isRetrieving, map as mapRetrieval, notYetRetrieved, successfulRetrieval } from "../retrieval";

export const initialState: UsersState = notYetRetrieved;

export const reducer = (state: UsersState = initialState, action: UsersAction) => {
  switch (action.type) {
    case ATTEMPTED_RETRIEVE_USERS: {
      return attemptedRetrieval(action.payload);
    }
    case RETRIEVED_USERS: {
      if (isRetrieving(state)) {
        return successfulRetrieval(state, HashMap.ofIterable(map(u => [u.id, u], action.payload)));
      } else {
        throw new Error("Got success before retrieval when retrieving users");
      }
    }
    case RETRIEVE_USERS_FAILURE: {
      if (isRetrieving(state)) {
        return failedRetrieval(state);
      } else {
        throw new Error('Got failure before starting the retrieval when retrieving card pairs');
      }
    }
    case UPDATED_USER:
    case CREATED_USER:
      return mapRetrieval(state, s => s.put(action.payload.id, action.payload));
    case REMOVED_USER:
      return mapRetrieval(state, s => s.remove(action.payload));
    default:
      return state;
  }
};
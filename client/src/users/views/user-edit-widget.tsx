import React from "react";
import { useEffect } from "react";
import { form as joiToForms } from "joi-errors-for-forms";
import { connect } from "react-redux";

import { NewUser, newUserSchema, Role, rolesEqualToOrBelow, StoredUser, UpdatedUser, updatedUserSchema, User } from "@cat-tuition/protocol/user";
import { ErrorMessage, Field, Form, Formik, FormikHelpers, FormikProps } from "formik";
import { createUser, updateUser, removeUser } from "../actions";
import { AppState } from "../../redux-store";

import cx from "classnames";
import { SimpleColour } from "@cat-tuition/protocol/colour";
import { ColourPalettePicker } from "../../colour-palette-picker/colour-palette-picker";
import { ProfileImage } from "../../profile-image/profile-image";
import { get, hasRetrievalSucceeded } from "../../retrieval";
import "./user-edit-widget.scss"

const convertToForms = joiToForms();

export interface UserEditWidgetProps {
  initialId?: string;
  initialDisplayName?: string | null;
  initialUser?: StoredUser;
  self: User;
  isIdTaken(id: string): boolean;
  createUser(user: NewUser): Promise<boolean>;
  updateUser(id: string, user: UpdatedUser): Promise<boolean>;
  removeUser(id: string): Promise<boolean>;
  onReset(): void;
}

export interface UserEdits {
  id: string;
  displayName: string,
  favouriteColour: SimpleColour;
  email: string,
  role: keyof typeof Role;
}

const UserEditWidget = (props: UserEditWidgetProps) => {
  const { onReset, createUser, updateUser, initialUser, removeUser, initialDisplayName, self } = props;

  const nullifyEmpty = (s: string) => s == '' ? null : s;
  const updateFromEdits = ({ email, displayName, role, favouriteColour }: UserEdits): UpdatedUser =>
    ({ email: nullifyEmpty(email), favouriteColour, displayName, role: Role[role] });
  const newFromEdits = (edits: UserEdits): NewUser =>
    ({ ...edits, email: nullifyEmpty(edits.email), role: Role[edits.role] });
  const modifiableRoles = rolesEqualToOrBelow(self.role);

  const validateForm = (values: UserEdits) => {
    const { isIdTaken } = props;
    console.log("Validating...", values);
    const result =
      initialUser != null
        ? updatedUserSchema.validate(updateFromEdits(values))
        : newUserSchema.validate(newFromEdits(values));
    if (result.error != null) {
      console.log("Got this error", JSON.stringify(result.error.details[0].type));
      const errors = convertToForms(result.error);
      console.log("Got errors: ", errors);
      if (initialUser == null && errors.id == null && isIdTaken(values.id)) {
        errors.id = "This id is already in use";
      }
      return errors;
    } else if (initialUser == null && isIdTaken(values.id)) {
      return { id: "This id is already in use" };
    } else {
      console.log("Validated...", values);
      return null;
    }
  };

  const onSubmit = async (info: UserEdits, { setSubmitting, resetForm, setFieldError } : FormikHelpers<UserEdits>) => {
    if (initialUser != null) {
      const ok = await updateUser(initialUser.id, updateFromEdits(info));
      if (ok) {
        onReset();
      } else {
        console.log("Error when updating")
        setFieldError('general', 'There was a problem updating the user, perhaps the server is down. Refresh and try again or contact an Admin')
      }
    } else {
      const idOk = await createUser(newFromEdits(info));
      if (!idOk) {
        setFieldError('general', 'There was a problem creating the user, perhaps the server is down or else a user with this id was added since you loaded the page. Refresh and try again')
        console.log("Error when creating")
      } else {
        console.log("No id clash")
        resetForm();
        onReset();
      }
    }
    setSubmitting(false);
  };

  const guessIdFromDisplayName = (displayName: string) => displayName.toLowerCase().replaceAll(' ', '-');

  const userEdits: UserEdits = (() => {
    if (initialUser != null) {
      return { ...initialUser, email: initialUser.email || '', role: Role[initialUser.role] as keyof typeof Role };
    } else if (initialDisplayName != null) {
      return { id: guessIdFromDisplayName(initialDisplayName),
               displayName: initialDisplayName,
               role: Role[modifiableRoles[0]] as keyof typeof Role,
               favouriteColour: SimpleColour.Blue,
               email: "" }
    } else {
      throw new Error("Must provide either initialDisplayName or initialId");
    }
  })();

  return (
    <Formik initialValues={userEdits}
            enableReinitialize
            validate={validateForm}
            onSubmit={onSubmit}>
      {
        ({ isSubmitting, errors, touched, values, setFieldValue, resetForm }: FormikProps<UserEdits>) => {
          useEffect(() => {
            if (initialUser == null && !touched.id) {
              setFieldValue('id', guessIdFromDisplayName(values.displayName));
            }
          }, [values.displayName]);

          const onDelete = async () => {
            if (initialUser != null) {
              onReset();
              const ok = await removeUser(initialUser.id);
              if (!ok) {
                console.log(`Failed to remove user with id ${initialUser.id}`);
              }
            }
          }

          const onCancel = () => {
            if (initialUser == null) {
              resetForm();
            }
            onReset();
          }

          return (
            <>
            <Form className="form user-edit-widget">
              <p className="title">
                {values.displayName.length > 0 ? values.displayName : 'Add a new user'}
              </p>

              <ProfileImage displayName={values.displayName || 'New User'} size={64} colour={values.favouriteColour}/>

              <div className="field">
                <label className="label">Name</label>
                <div className="control">
                  <Field className={cx("input", { 'is-danger': errors.displayName && touched.displayName })}
                         type="text"
                         name="displayName"
                         placeholder="Name" />
                </div>
                <ErrorMessage name="displayName" render={msg => <p className="help is-danger">{msg}</p>}/>
              </div>

              <div className="field">
                <label className="label">Email</label>
                <div className="control">
                  <Field className={cx("input", { 'is-danger': errors.email && touched.email })}
                         type="email"
                         name="email"
                         placeholder="Email" />
                </div>
                <ErrorMessage name="email" render={msg => <p className="help is-danger">{msg}</p>}/>
              </div>

              <div className="field">
                <label className="label">Username</label>
                <div className="control is-expanded">
                  <Field className={cx("input", { 'is-danger': errors.id && touched.id })}
                         type="text"
                         name="id"
                         placeholder="Username"
                         disabled={initialUser != null}/>
                </div>
                <ErrorMessage name="id" render={msg => <p className="help is-danger">{msg}</p>}/>
              </div>

              <div className="field">
                <label className="label">Favourite Colour</label>
                <ColourPalettePicker id="user-favourite-colour" name="favouriteColour" />
              </div>

              <div className="field">
                <label className="label">Role</label>
                <div className="control is-expanded">
                  <div className={cx("select is-fullwidth", {'is-danger': errors.role && touched.role})}>
                    <Field type="select" name="role" as="select">
                      {modifiableRoles.map(role =>
                        <option key={Role[role]} value={Role[role]}>{Role[role]}</option>
                      )}
                    </Field>
                  </div>
                </div>
                <ErrorMessage name="role" render={msg => <p className="help is-danger">{msg}</p>}/>
              </div>

              <div className="field">
                <p className="help is-danger">{(errors as any).general}</p>
              </div>

              <div className="level">
                <div className="level-left">
                  <div className="level-item">
                  <div className="field is-grouped">
                    <p className="control">
                      <button className="button is-link" type="submit" disabled={isSubmitting}>
                        Save User
                      </button>
                    </p>
                    <p className="control">
                      <button onClick={() => onCancel()} className="button is-link is-light" type="button" disabled={isSubmitting}>
                        Cancel
                      </button>
                    </p>
                  </div></div>
                </div>
                <div className="level-right">
                  <div className="level-item">
                    <div className="field">
                      <p className="control">
                        <button onClick={onDelete} type="button" className="button is-link is-light" disabled={isSubmitting || initialUser == null || initialUser.id == self.id}>
                          Delete User
                        </button>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Form>
            </>
          );
        }
      }
    </Formik>
  );
}
const mapState = (s: AppState, { initialId }: { initialId?: string }) => {
  if (hasRetrievalSucceeded(s.users)) {
    const users = get(s.users);
    const isIdTaken = (id: string) => users.containsKey(id);
    if (initialId != null) {
      const initialUser = users.get(initialId).getOrThrow('Provided invalid id for user to edit')
      return { initialUser, isIdTaken }
    } else {
      return { isIdTaken };
    }
  } else {
    throw new Error("Tried to render edit widget while users are still loading");
  }
};
export default connect(mapState, { createUser, updateUser, removeUser })(UserEditWidget);
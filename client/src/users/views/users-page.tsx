import React, { ChangeEvent, MouseEvent, useEffect, useState } from "react";
import Navbar from "../../navbar/views/navbar";
import UserEditWidget from "./user-edit-widget";
import { AppState } from "../../redux-store";
import { connect } from "react-redux";
import cx from "classnames";
import filter from "iter-tools/methods/filter";
import take from "iter-tools/methods/take";
import map from "iter-tools/methods/map";
import "./users-page.scss";
import { Role, roleIsEqualToOrBelow, rolesEqualToOrBelow, StoredUser, UpdatedUser, User } from "@cat-tuition/protocol/user";
import { Option, HashMap } from "prelude-ts";
import { retrieveUsers } from "../actions";
import { Redirect } from "react-router";
import LoadingPage from "../../loading-page/views/loading-page";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { get, hasRetrievalSucceeded, isRetrieving, Retrieve, shouldReretrieve } from "../../retrieval";

export interface UsersPageProps {
  user?: User;
  users: Retrieve<HashMap<string, UpdatedUser>>;
  retrieveUsers(): Promise<boolean>;
}

const userItemLimit = 10;

export interface UsersPageState {
  selectedId: string | null;
  newUserDisplayName: string | null;
  query: string;
  tabFilter: Option<Role>;
}

export const UsersPage = (props: UsersPageProps) => {
  const [state, setState] = useState<UsersPageState>({
    query: '',
    newUserDisplayName: null,
    selectedId: null,
    tabFilter: Option.none()
  });

  const { user, users: usersRetrieval } = props;
  const { tabFilter } = state;

  const onEditReset = () =>
    setState({ ...state, newUserDisplayName: null, selectedId: null });

  const loadUsers = async () => {
    const { retrieveUsers, users } = props;
    if (shouldReretrieve(users, 5)) {
      await retrieveUsers();
    }
  };

  useEffect(() => {
    if (user != null && user.canManageUsers()) {
      loadUsers();
    }
  }, [user]);

  const onUserSelected = (id: string) =>
    setState({ ...state, selectedId: id });

  const onChangeQuery = (e: ChangeEvent<HTMLInputElement>) =>
    setState({ ...state, query: e.target.value });

  const onChangeTab = (tab: Option<Role>) =>
    setState({ ...state, tabFilter: tab });

  const onQueryAdd = (e: MouseEvent<HTMLButtonElement>) =>
    setState({ ...state, newUserDisplayName: state.query })

  const renderUser = ([id, user] : [string, UpdatedUser]) => {
    return (
      <a key={id}
        onClick={() => onUserSelected(id)}
        className="panel-block">
          {user.displayName}
      </a>
    );
  };

  const renderFilteredUsers = () => {
    if (hasRetrievalSucceeded(usersRetrieval)) {
      const { query, tabFilter } = state;
      const items = [...filter(([id, u]) =>
        (mapNullable(user, su => roleIsEqualToOrBelow(u.role, su.role)) || false) &&
        tabFilter.map(r => r == u.role).getOrElse(true) &&
        u.displayName.includes(query), get(usersRetrieval))];
      items.sort(([ka, ua], [kb, ub]) => {
        if (ua.displayName < ub.displayName) {
          return -1;
        } else if (ua.displayName > ub.displayName) {
          return 1;
        } else {
          return 0;
        }
      });
      return [...map(renderUser, take(userItemLimit, items))]
    } else {
      return <p className="panel-block">Oh no! Failed to load the users, try refreshing the page.</p>
    }
  };

  const renderLoader = () => {
    return (
      <div className="loader-wrapper">
        <div className="loader is-loading" />
      </div>
    );
  };

  if (user != null) {

    const editWidget = (() => {
      if (hasRetrievalSucceeded(usersRetrieval)) {
        if (state.selectedId != null) {
          return <UserEditWidget onReset={onEditReset} self={user} initialId={state.selectedId}/>
        } else {
          return <UserEditWidget onReset={onEditReset} self={user} initialDisplayName={state.newUserDisplayName || ''}/>
        }
      } else {
        return renderLoader();
      }
    })();

    const modifiableRoles = rolesEqualToOrBelow(user.role) || [];
    if (user.canManageUsers()) {
      return (
        <>
          <Navbar shouldConnect={false}/>
          <div className="users-page management hero-body container">
            <p className="title is-1">Users</p>

            <div className="tile is-ancestor">
              <div className="tile is-6 is-parent">
                <div className="tile is-child">
                  <nav className="user-list panel">
                    <div className="panel-block">
                      <div className="field control has-addons">
                        <div className="control is-expanded has-icons-left">
                          <input onChange={onChangeQuery} className="input" type="text" placeholder="Search" />
                          <span className="icon is-left">
                            <i className="fas fa-search" aria-hidden="true"/>
                          </span>
                        </div>
                        <div className="control">
                          <button onClick={onQueryAdd} className="button is-link">
                            <i className="fas fa-plus" aria-hidden="true"/>
                          </button>
                        </div>
                      </div>
                    </div>
                    <p className="panel-tabs">
                      <a onClick={() => onChangeTab(Option.none())}
                         className={cx({ 'is-active': tabFilter.isNone() })}>All</a>
                      {modifiableRoles.map((role: Role) =>
                        <a key={Role[role]} onClick={() => onChangeTab(Option.of(role))}
                          className={cx({ 'is-active': tabFilter.contains(role)})}>{Role[role]}s</a>
                      )}
                    </p>

                    {isRetrieving(usersRetrieval) ? renderLoader() : renderFilteredUsers()}
                  </nav>
                </div>
              </div>
              <div className="tile is-parent">
                <section className="tile is-child box">
                  {editWidget}
                </section>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return <Redirect push to='/404'/>
    }
  } else {
    return <LoadingPage shouldConnect={false} message="Loading the User Management Dashboard..."/>
  }
};


const mapState = ({ profile, users }: AppState) => ({ user: profile.user, users });
export default connect(mapState, { retrieveUsers })(UsersPage);
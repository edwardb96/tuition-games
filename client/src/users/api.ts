import { StoredUser, NewUser, UpdatedUser } from "@cat-tuition/protocol/user";
import axios from "axios";

export const fetchUsers = async (): Promise<StoredUser[]> => {
  return await (await axios.get('/api/users')).data;
}

export const postUser = async (newUser: NewUser): Promise<boolean> => {
  try {
    const response = await axios.post(`/api/users`, newUser);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}

export const putUser = async (id: string, updatedUser: UpdatedUser): Promise<boolean> => {
  try {
    const response = await axios.put(`/api/users/${id}`, updatedUser);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}

export const deleteUser = async (id: string): Promise<boolean> => {
  try {
    const response = await axios.delete(`/api/users/${id}`);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}
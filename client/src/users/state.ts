import { HashMap } from "prelude-ts";
import { StoredUser } from "@cat-tuition/protocol/user";
import { Retrieve } from "../retrieval";

export type UsersState = Retrieve<HashMap<string, StoredUser>>;
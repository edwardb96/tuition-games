import { Option } from "prelude-ts";

export enum RetrievalState {
  NotYet,
  InProgress,
  Success,
  Failure
}

export interface SucceededRetrieval<T> {
  tag: RetrievalState.Success;
  value: T;
  when: Date;
}

export interface FailedRetrieval {
  tag: RetrievalState.Failure;
  when: Date;
}

export interface Retrieving {
  tag: RetrievalState.InProgress;
  when: Date;
}

export interface NotYetRetrieved {
  tag: RetrievalState.NotYet;
}

export type Retrieve<T> =
  NotYetRetrieved |
  Retrieving |
  FailedRetrieval |
  SucceededRetrieval<T>;

export const notYetRetrieved: NotYetRetrieved = { tag: RetrievalState.NotYet };

export function attemptedRetrieval(when: Date): Retrieving {
  return { tag: RetrievalState.InProgress, when }
}

export function successfulRetrieval<T>(retrieval: Retrieving, value: T): SucceededRetrieval<T> {
  return { ...retrieval, tag: RetrievalState.Success, value }
}

export function failedRetrieval(retrieval: Retrieving): FailedRetrieval {
  return { ...retrieval, tag: RetrievalState.Failure }
}

export const addMinutes = (date: Date, minutes: number): Date =>
  new Date(date.getTime() + minutes * 60000);

export function shouldReretrieve<T>(retrieval: Retrieve<T>, ifOlderThanMins: number): boolean {
  return (retrieval.tag == RetrievalState.NotYet) || addMinutes(retrieval.when, ifOlderThanMins) < new Date();
}

export function isRetrieving<T>(retrieval: Retrieve<T>): retrieval is Retrieving {
  return (retrieval.tag == RetrievalState.InProgress)
}

export function hasRetrievalFailed<T>(retrieval: Retrieve<T>): retrieval is FailedRetrieval {
  return retrieval.tag == RetrievalState.Failure;
}

export function hasRetrievalSucceeded<T>(retrieval: Retrieve<T>): retrieval is SucceededRetrieval<T> {
  return retrieval.tag == RetrievalState.Success;
}

export function hasRetrievalFinished<T>(retrieval: Retrieve<T>): retrieval is SucceededRetrieval<T> | FailedRetrieval {
  return retrieval.tag == RetrievalState.Success || retrieval.tag == RetrievalState.Failure;
}

export function get<T>(retrieval: SucceededRetrieval<T>): T {
  return retrieval.value;
}

export function map<T>(retrieval: Retrieve<T>, mapper: (value: T) => T): Retrieve<T> {
  if (hasRetrievalSucceeded(retrieval)) {
    return { ... retrieval, value: mapper(get(retrieval)) }
  } else {
    return retrieval;
  }
}

export function asOption<T>(retrieval: Retrieve<T>): Option<T> {
  if (retrieval.tag == RetrievalState.Success) {
    return Option.of(retrieval.value);
  } else {
    return Option.none();
  }
}
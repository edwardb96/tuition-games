import React, { useState, MouseEvent, ChangeEvent, useRef, useEffect } from "react";

import { AppState } from "../../redux-store";
import { connect } from "react-redux";
import './image-picker.scss';
import cx from "classnames";
import { User } from "@cat-tuition/protocol/user";
import { ImagesState } from "../state";
import { ImageId } from "@cat-tuition/protocol/image";
import FromFileTab from "./from-file-tab";
import FromCardTab from "./from-card-tab";
import FromBoardTab from "./from-board-tab";

export interface ImagePickerModalProps {
  user?: User;
  images: ImagesState;
  title: string;
  isOpen: boolean;
  selectedTab: ImagePickerTab;
  selectedImage: ImageId | null;
  onSubmit(id: ImageId): void;
  onCancel(): void;
}

export enum ImagePickerTab {
  FromCard,
  FromBoard,
  FromFile
}

export interface ImagePickerModalState {
  selectedTab: ImagePickerTab;
  selectedImage: ImageId | null;
}

export const ImagePickerModal = (props: ImagePickerModalProps) => {
  const { title, isOpen, onSubmit, onCancel,
          selectedTab: initialSelectedTab,
          selectedImage: initialSelectedImage } = props;

  const [state, setState] = useState<ImagePickerModalState>({
    selectedTab: initialSelectedTab,
    selectedImage: null
  });

  const { selectedTab, selectedImage } = state;

  useEffect(() => {
    setState({ ...state, selectedTab: initialSelectedTab })
  }, [isOpen, initialSelectedTab])

  useEffect(() => {
    if (isOpen) {
      setState({ ...state, selectedImage: initialSelectedImage })
    } else {
      // Use this to prevent fade out of previous selection when opening
      // for a different card.
      setState({ ...state, selectedImage: null })
    }
  }, [isOpen, initialSelectedImage])


  const onSelectImage = (id: ImageId) => {
    console.log(`Selected image ${id}`);
    setState({ ...state, selectedImage: id })
  }

  const onPreSubmit = () => {
    if (selectedImage != null) {
      onSubmit(selectedImage)
    } else {
      throw new Error('Called submit without a selected image');
    }
  }

  const onPreCancel = () => {
    onCancel();
  }

  const renderTabItem = (tab: ImagePickerTab, name: string) => {
    const onSelectTab = (_: MouseEvent<HTMLAnchorElement>) =>
      setState({ ...state, selectedTab: tab });

    return (
      <li className={cx({ 'is-active' : selectedTab == tab  })}>
        <a onClick={onSelectTab}>{name}</a>
      </li>
    );
  };

  const renderTab = (selectedTab: ImagePickerTab) => {
    switch (selectedTab) {
      case ImagePickerTab.FromFile:
        return <FromFileTab selectedImage={selectedImage} onSelectImage={onSelectImage} />
      case ImagePickerTab.FromCard:
        return <FromCardTab selectedImage={selectedImage} onSelectImage={onSelectImage} />
      case ImagePickerTab.FromBoard:
        return <FromBoardTab selectedImage={selectedImage} onSelectImage={onSelectImage} />;
    }
  }

  return (
    <div className={cx("image-picker modal", { 'is-active': isOpen })}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <div className="top">
            <p className="modal-card-title">{title}</p>
            <button type="button" onClick={onPreCancel} className="delete" aria-label="close"></button>
          </div>
          <nav className="tab-items tabs">
            <ul>
              {renderTabItem(ImagePickerTab.FromBoard, 'From a Board')}
              {renderTabItem(ImagePickerTab.FromCard, 'From a Card')}
              {renderTabItem(ImagePickerTab.FromFile, 'From a File')}
            </ul>
          </nav>
        </header>
        <div className="modal-card-body">
          {renderTab(selectedTab)}
        </div>
        <footer className="modal-card-foot">
          <button type="button" onClick={onPreSubmit} className="button is-primary">Use Selected Image</button>
          <button type="button" onClick={onPreCancel} className="button">Cancel</button>
        </footer>
      </div>
    </div>
  );
};

const mapState = ({ images, profile }: AppState) => ({ user: profile.user, images });
export default connect(mapState, {})(ImagePickerModal);
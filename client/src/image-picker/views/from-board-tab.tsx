import React, { useEffect, useState } from "react";
import { ImageId } from "@cat-tuition/protocol/image";
import { AppState } from "../../redux-store";
import { useRef } from "react";
import { connect } from "react-redux";
import { BoardsState, compareBoards } from "../../boards/state";
import { TagsInput } from "../../tags-input/tags-input";
import { GalleryWidget } from "./gallery-widget";
import { asOption, get, hasRetrievalSucceeded, shouldReretrieve } from "../../retrieval";
import map from "iter-tools/methods/map";
import filter from "iter-tools/methods/filter";
import includesAny from "iter-tools/methods/includes-any";
import "./from-card-tab.scss"
import { retrieveBoards } from "../../boards/actions";
import { Board, BoardCategory, boardCategorySchema } from "@cat-tuition/protocol/board-game";
import { uniqueFromSorted } from "@cat-tuition/protocol/iter-tools-ext";
import { User } from "@cat-tuition/protocol/user";
import { BoardState } from "@cat-tuition/protocol/channels/noughts-and-crosses";

export interface FromBoardTabProps {
  user?: User;
  boards: BoardsState;
  selectedImage: ImageId | null;
  onSelectImage(imageId: ImageId | null): void;
  retrieveBoards(): Promise<boolean>;
}

interface FromBoardTabState {
  query: string;
  categoriesFilter: BoardCategory[];
}

export const FromBoardTab = (props: FromBoardTabProps) => {
  const { user, boards: boardsRetrieval,
          onSelectImage,
          selectedImage } = props;

  const galleryWidgetRef = useRef<GalleryWidget>(null);
  const [state, setState] = useState<FromBoardTabState>({
    query: '',
    categoriesFilter: []
  });

  const { query, categoriesFilter } = state;

  const loadBoards = async () => {
    const { retrieveBoards, boards } = props;
    if (shouldReretrieve(boards, 5)) {
      await retrieveBoards();
    }
  };

  useEffect(() => {
    if (user != null && user.canManageBoards()) {
      loadBoards();
    }
  }, [user]);

  const onChangeQuery = (query: string) => setState({ ...state, query });
  const onChangeCategories = (categoriesFilter: BoardCategory[]) =>
    setState({ ...state, categoriesFilter });


  const images = (() => {
    if (hasRetrievalSucceeded(boardsRetrieval)) {
      const { boards } = get(boardsRetrieval);
      return [
        ...
        map(({ image, title }: Board) => ({ image, description: title }),
          filter(board =>
            (categoriesFilter.length == 0 || includesAny(categoriesFilter, board.categories)) &&
              board.title.includes(query), boards))];
    } else {
      return [];
    }
  })();

  return (
    <div className="from-card-tab">
      <section className="card-search-options">
        <div className="field">
          <div className="control has-icons-left">
            <input onChange={e => onChangeQuery(e.target.value)}
              className="input search"
              type="text"
              placeholder="Search image cards"
              value={state.query}/>
            <span className="icon is-left">
              <i className="fas fa-search" aria-hidden="true"></i>
            </span>
          </div>
        </div>
        {
          asOption(boardsRetrieval).map(({ categories }) =>
            <div className="field control has-icons-left">
              <TagsInput
                onChange={onChangeCategories}
                placeholder="Filter by categories"
                suggestions={categories.toArray()}
                tagSchema={boardCategorySchema}
                value={categoriesFilter}/>
              <span className="icon is-left">
                <i className="fas fa-tags" aria-hidden="true"></i>
              </span>
            </div>).getOrElse(<></>)
        }
      </section>
      <GalleryWidget
        ref={galleryWidgetRef}
        itemSize={2}
        images={images}
        selectedImage={selectedImage}
        onSelectImage={onSelectImage} />
    </div>
  );
}

const mapState = ({ boards, profile }: AppState) => ({ user: profile.user, boards });
export default connect(mapState, { retrieveBoards })(FromBoardTab);
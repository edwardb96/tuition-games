import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import React, { ChangeEvent } from "react";

interface ImageUploadValues {
  file: File | null;
  isWidgit: boolean;
}

export interface ImageUploadProps {
  onUploadFile(file: File, isWidgit: boolean): Promise<string[] | null>;
}

export const ImageUpload = (props: ImageUploadProps) => {
  const validateForm = ({ file, isWidgit }: ImageUploadValues) => {
    if (file != null) {
      const twoMegabytes = 2000 * 1000;
      if (file.size < twoMegabytes) {
        if (file.type == 'image/png' || file.type == 'image/jpeg') {
          if (!isWidgit) {
            return {}
          } else {
            return { file: "If uploading a widgit card document then the selected file should be a .pdf document" };
          }
        } else if (file.type == 'application/pdf') {
          return {};
        } else {
          return { file: "The selected file should be a .png format image or a .pdf document" };
        }
      } else {
        return { file: "The selected file should be smaller than the 2MB limit" };
      }
    } else {
      return { file: "You must select a file to upload" }
    }
  }


  const onSubmit = async (values: ImageUploadValues, { setSubmitting, resetForm, setFieldError }: FormikHelpers<ImageUploadValues>) => {
    const { onUploadFile } = props;
    if (values.file != null) {
      const id = await onUploadFile(values.file, values.isWidgit);
      if (id != null) {
        resetForm();
      } else {
        setFieldError('general', "There was a problem uploading the file, perhaps the server is down. Refresh and try again or else contact an admin")
      }
    }
    setSubmitting(false);
  }

  const initialValues: ImageUploadValues = { file: null, isWidgit: false }

  return (
    <Formik initialValues={initialValues}
      validate={validateForm}
      onSubmit={onSubmit}>
      {
        ({ isSubmitting, errors, touched, values, setFieldValue, setFieldTouched }) => {

          const onFileChange = (e: ChangeEvent<HTMLInputElement>) => {
            const files = e.currentTarget.files;
            if (files != null && files.length > 0) {
              const file = files[0];
              if (file != null) {
                setFieldTouched('file', true, false);
                setFieldValue('file', file);
              } else {
                setFieldValue('file', null);
              }
            } else {
              setFieldValue('file', null);
            }
          };

          return (
            <Form className="form">
              <h1 className="title">
                Add Photos to the gallery
              </h1>
              <p className="subtitle">
                Pick a PNG, JPEG or PDF to upload
              </p>

              <div className="field">
                <div className="file has-name is-fullwidth">
                  <label className="file-label">
                    <input className="file-input" type="file" name="resume" accept=".png,.jpg,.JPG,.pdf" onChange={onFileChange}/>
                    <span className="file-cta">
                      <span className="file-icon">
                        <i className="fas fa-upload"></i>
                      </span>
                      <span className="file-label">
                        Choose a file…
                      </span>
                    </span>
                    <span className="file-name">
                      {mapNullable(values.file, f => f.name) || ''}
                    </span>
                  </label>
                </div>
                <ErrorMessage name="file" render={msg => <p className="help is-danger">{msg}</p>} />
              </div>

              <div className="field">
                <label className="checkbox">
                  <Field className="checkbox" type="checkbox" name="isWidgit" />
                  <span>This is a Widgit card PDF document</span>
                </label>
              </div>

              <div className="field">
                <p className="help is-danger">{(errors as any).general}</p>
              </div>

              <div className="field">
                <div className="control">
                  <button className="button is-link" type="submit" disabled={isSubmitting}>
                    Upload
                  </button>
                </div>
              </div>
            </Form>
          );
        }
      }
    </Formik>
  );
};
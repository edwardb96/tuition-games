import { FieldProps } from "formik";
import React, { useState } from "react";
import { ImagePickerTab } from "./image-picker-modal";
import { ImageId } from "@cat-tuition/protocol/image";

export interface ImagePickerProps {
  title: string;
  initialTab: ImagePickerTab;
  onPickImage(title: string,
              initialSelectedTab: ImagePickerTab,
              initialSelectedImage: ImageId | null,
              onSubmit: (id: ImageId) => void): void;
}

interface ImagePickerParentState {
  isOpen: boolean;
  title: string;
  selectedTab: ImagePickerTab;
  selectedImage: ImageId | null;
  onPickedImage: (id: ImageId) => void;
}

interface ImagePickerModalPropState {
  isOpen: boolean;
  title: string;
  selectedTab: ImagePickerTab;
  selectedImage: ImageId | null;
  onPickImage(
    title: string,
    selectedTab: ImagePickerTab,
    selectedImage: ImageId | null,
    notifyImagePicker: (id: ImageId) => void): void;
  onCancelPick(): void;
}

export const useImagePicker = (initialState: Partial<ImagePickerParentState>) => {
  const [state, setState] = useState<ImagePickerParentState>({
    isOpen: false,
    title: '',
    selectedTab: ImagePickerTab.FromFile,
    selectedImage: null,
    onPickedImage: (_id: ImageId) => { throw new Error('Unexpected pick image before submit handler set'); },
    ...initialState
  });

  const onCancelPick = () => setState({ ...state, isOpen: false });

  const onPickImage =
    (title: string,
     selectedTab: ImagePickerTab,
     selectedImage: ImageId | null,
     notifyImagePicker: (id: ImageId) => void) => {
      const onPickedImage = (id: ImageId) => {
        notifyImagePicker(id);
        // beware of the state capture here?
        setState({ ...state, isOpen: false });
      };
      setState({...state, title, isOpen: true, selectedImage, selectedTab, onPickedImage });
    };

  const { isOpen, title, selectedTab, selectedImage, onPickedImage } = state;
  return { isOpen, title, selectedTab, selectedImage, onPickImage, onPickedImage, onCancelPick };
};

export const ImagePicker = ({ field, form, title, initialTab, onPickImage } : FieldProps<ImageId | null> & ImagePickerProps) => {
  const { name, value } = field;
  const { setFieldValue, setFieldTouched } = form;

  const setValue = (id: ImageId | null) => {
    setFieldTouched(name, true, false);
    setFieldValue(name, id);
  }

  const onPick = () => {
    onPickImage(title, initialTab, value, (id) => setValue(id));
  }

  const onClear = () => setValue(null);

  return (
    <nav className="image-options level">
      <div className="level-item">
        <button className="button is-fullwidth" type="button" onClick={() => onPick()}>
          <span className="icon">
            <i className="fas fa-file-image"></i>
          </span>
          <span>Choose</span>
        </button>
      </div>
      <div className="level-item">
        <button className="button is-fullwidth" type="button" onClick={() => onClear()}>
          <span className="icon">
            <i className="fas fa-times"></i>
          </span>
          <span>Clear</span>
        </button>
      </div>
    </nav>
  );
};
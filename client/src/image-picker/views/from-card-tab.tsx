import React, { useEffect, useState } from "react";
import { ImageId } from "@cat-tuition/protocol/image";
import { AppState } from "../../redux-store";
import { useRef } from "react";
import { connect } from "react-redux";
import { CardPairsState, compareImageCards } from "../../card-pairs/state";
import { TagsInput } from "../../tags-input/tags-input";
import { GalleryWidget } from "./gallery-widget";
import { asOption, get, hasRetrievalSucceeded, shouldReretrieve } from "../../retrieval";
import filter from "iter-tools/methods/filter";
import includesAny from "iter-tools/methods/includes-any";
import "./from-card-tab.scss"
import { retrieveCardPairs } from "../../card-pairs/actions";
import { CardCategory, cardCategorySchema } from "@cat-tuition/protocol/pairs";
import { uniqueFromSorted } from "@cat-tuition/protocol/iter-tools-ext";
import { User } from "@cat-tuition/protocol/user";

export interface FromCardTabProps {
  user?: User;
  cardPairs: CardPairsState;
  selectedImage: ImageId | null;
  onSelectImage(imageId: ImageId | null): void;
  retrieveCardPairs(): Promise<boolean>;
}

interface FromCardTabState {
  query: string;
  categoriesFilter: CardCategory[];
}

export const FromCardTab = (props: FromCardTabProps) => {
  const { user, cardPairs,
          onSelectImage,
          selectedImage } = props;

  const galleryWidgetRef = useRef<GalleryWidget>(null);
  const [state, setState] = useState<FromCardTabState>({
    query: '',
    categoriesFilter: []
  });

  const { query, categoriesFilter } = state;

  const loadCardPairs = async () => {
    const { retrieveCardPairs, cardPairs } = props;
    if (shouldReretrieve(cardPairs, 5)) {
      await retrieveCardPairs();
    }
  };

  useEffect(() => {
    if (user != null && user.canManageCardPairs()) {
      loadCardPairs();
    }
  }, [user]);

  const onChangeQuery = (query: string) => setState({ ...state, query });
  const onChangeCategories = (categoriesFilter: CardCategory[]) =>
    setState({ ...state, categoriesFilter });


  const images = (() => {
    if (hasRetrievalSucceeded(cardPairs)) {
      const { imageCards } = get(cardPairs);
      return [...uniqueFromSorted(filter(
        i => (categoriesFilter.length == 0 ||
              includesAny(categoriesFilter, i.categories)) &&
             i.description.includes(query), imageCards), compareImageCards)];
    } else {
      return [];
    }
  })();

  return (
    <div className="from-card-tab">
      <section className="card-search-options">
        <div className="field">
          <div className="control has-icons-left">
            <input onChange={e => onChangeQuery(e.target.value)}
              className="input search"
              type="text"
              placeholder="Search image cards"
              value={state.query}/>
            <span className="icon is-left">
              <i className="fas fa-search" aria-hidden="true"></i>
            </span>
          </div>
        </div>
        {
          asOption(cardPairs).map(({ categories }) =>
            <div className="field control has-icons-left">
              <TagsInput
                onChange={onChangeCategories}
                placeholder="Filter by categories"
                suggestions={categories.toArray()}
                tagSchema={cardCategorySchema}
                value={categoriesFilter}/>
              <span className="icon is-left">
                <i className="fas fa-tags" aria-hidden="true"></i>
              </span>
            </div>).getOrElse(<></>)
        }
      </section>
      <GalleryWidget
        ref={galleryWidgetRef}
        itemSize={2}
        images={images}
        selectedImage={selectedImage}
        onSelectImage={onSelectImage} />
    </div>
  );
}

const mapState = ({ cardPairs, profile }: AppState) => ({ user: profile.user, cardPairs });
export default connect(mapState, { retrieveCardPairs })(FromCardTab);
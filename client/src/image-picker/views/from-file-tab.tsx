import React from "react";
import { ImageId } from "@cat-tuition/protocol/image";
import { AppState } from "../../redux-store";
import { useRef } from "react";
import { connect } from "react-redux";
import { ImagesState } from "../state";
import { GalleryWidget } from "./gallery-widget";
import { ImageUpload } from "./image-upload";
import map from "iter-tools/methods/map";
import {
  retrieveInitialUnassignedImages,
  retrieveUnassignedImagesAfter,
  removeUnassignedImage,
  uploadImages,
  RetrievalResultPair} from "../actions";

export interface FromFileTabProps {
  // cardPairs: HashMap<string, CardPair>;
  // retrieveCard(): Promise<boolean>;
  images: ImagesState;
  selectedImage: ImageId | null;
  onSelectImage(imageId: ImageId | null): void;

  uploadImages(image: File, isWidgit: boolean): Promise<ImageId[] | null>;

  removeUnassignedImage(id: ImageId): Promise<boolean>;
  retrieveInitialUnassignedImages(count: number): Promise<RetrievalResultPair<ImageId> | null>;
  retrieveUnassignedImagesAfter(id: ImageId, count: number): Promise<RetrievalResultPair<ImageId> | null>;
}

export const FromFileTab = (props: FromFileTabProps) => {
  const { images,
          retrieveInitialUnassignedImages,
          retrieveUnassignedImagesAfter,
          removeUnassignedImage,
          uploadImages,
          onSelectImage,
          selectedImage } = props;

  const galleryWidgetRef = useRef<GalleryWidget>(null);

  const onUploadFile = async (file: File, isWidgit: boolean): Promise<ImageId[] | null> => {
    const ids = await uploadImages(file, isWidgit);
    const galleryWidget = galleryWidgetRef.current;
    if (galleryWidget != null && ids != null) {
      galleryWidget.returnToFirstPage();
    }
    return ids;
  };

  return (
    <div className="columns">
      <div className="column is-one-third">
        <ImageUpload onUploadFile={onUploadFile}/>
      </div>
      <div className="column is-two-thirds">
        <GalleryWidget
          ref={galleryWidgetRef}
          images={map(image => ({ image }), images.likelyUnassignedImages)}
          itemSize={3}
          selectedImage={selectedImage}
          onSelectImage={onSelectImage}
          retrieveInitialImages={retrieveInitialUnassignedImages}
          retrieveImagesAfter={retrieveUnassignedImagesAfter}
          removeImage={removeUnassignedImage}/>
      </div>
    </div>
  );
}

const mapState = ({ images, profile }: AppState) => ({ user: profile.user, images });
export default connect(mapState, {
  uploadImages,
  retrieveInitialUnassignedImages,
  retrieveUnassignedImagesAfter,
  removeUnassignedImage
})(FromFileTab);
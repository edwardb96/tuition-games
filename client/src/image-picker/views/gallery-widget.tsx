import { ImageId, ImageMetadata } from "@cat-tuition/protocol/image";
import { mapNullable, takePenultimateOr } from "@cat-tuition/protocol/iter-tools-ext";
import { Vector, Option } from "prelude-ts";
import "./gallery-widget.scss"
import cx from "classnames";
import React, { MouseEvent, Component } from "react";
import drop from "iter-tools/methods/drop";
import take from "iter-tools/methods/take";
import takeLastOr from "iter-tools/methods/take-last-or";
import map from "iter-tools/methods/map";
import { RetrievalResultPair } from "../actions";

export interface MaybeCaptionedImage {
  description?: string;
  image: ImageId;
}

export interface GalleryProps {
  images: Iterable<MaybeCaptionedImage>;
  selectedImage: ImageId | null;
  itemSize: number;
  retrieveInitialImages?(count: number): Promise<RetrievalResultPair<ImageId> | null>;
  retrieveImagesAfter?(id: ImageId, count: number): Promise<RetrievalResultPair<ImageId> | null>;
  removeImage?(id: ImageId): Promise<boolean>;
  onSelectImage(id: ImageId): void;
}

export interface GalleryState {
  pageStartIndex: number;
  retrievedImagesLength: number;
  lastRetrievedId: ImageId | null;
  loadSuccess: boolean | null;
}

const pageSize = 6 * 2;
const lookaheadSize = 4;

export class GalleryWidget extends Component<GalleryProps, GalleryState> {
  constructor(props: GalleryProps) {
    super(props);
    this.state = {
      pageStartIndex: 0,
      loadSuccess: null,
      retrievedImagesLength: 0,
      lastRetrievedId: null
    };
  }

  componentDidMount() {
    this.tryRetrieveImages();
  }

  returnToFirstPage() {
    this.setState({ pageStartIndex: 0 })
  }

  private async tryRetrieveImages() {
    const { retrieveInitialImages } = this.props;
    if (retrieveInitialImages != null) {
      const retrievalResult = await retrieveInitialImages(pageSize + lookaheadSize);
      if (retrievalResult != null) {
        const { lastRetrievedItem, retrievalCount } = retrievalResult;
        this.setState({
          loadSuccess: true,
          retrievedImagesLength: retrievalCount,
          lastRetrievedId: lastRetrievedItem })
      } else {
        this.setState({ loadSuccess: false })
      }
    }
  };

  private async onNextPage() {
    const { retrieveImagesAfter } = this.props;
    const { pageStartIndex, retrievedImagesLength, lastRetrievedId } = this.state;
    const nextPageStartIndex = pageStartIndex + pageSize;

    if (retrieveImagesAfter != null) {
      const roomAfterNextPage = retrievedImagesLength - (nextPageStartIndex + pageSize);
      const itemsToRequest = lookaheadSize - roomAfterNextPage;
      if (itemsToRequest > 0) {
        console.log("Requesting %s items after %s", itemsToRequest, lastRetrievedId);
        const retrievalResult = await retrieveImagesAfter(lastRetrievedId as ImageId, itemsToRequest);
        if (retrievalResult != null) {
          const { lastRetrievedItem, retrievalCount } = retrievalResult;
          this.setState({
            pageStartIndex: nextPageStartIndex,
            retrievedImagesLength: retrievedImagesLength + retrievalCount,
            lastRetrievedId: lastRetrievedItem || this.state.lastRetrievedId
          });
        } else {
          console.log("Error getting next page");
        }
      } else {
        this.setState({ pageStartIndex: nextPageStartIndex })
      }
    } else {
      this.setState({ pageStartIndex: nextPageStartIndex })
    }
  };

  private onPreviousPage() {
    const { pageStartIndex } = this.state;
    this.setState({ pageStartIndex: pageStartIndex - pageSize });
  }

  private onSelectImage(image: ImageId) {
    this.props.onSelectImage(image);
  }

  render () {
    const { pageStartIndex } = this.state;
    const { images, selectedImage, itemSize, removeImage } = this.props;

    const imagesOnCurrentPageAndLookahead = [...take(pageSize + lookaheadSize, drop(pageStartIndex, images))];
    const imagesOnCurrentPage = take(pageSize, imagesOnCurrentPageAndLookahead);

    const isFirstPage = pageStartIndex == 0;
    const isLastPage = imagesOnCurrentPageAndLookahead.length <= pageSize;

    const onRemoveImage = (() => {
      if (removeImage != null) {
        return async (id: ImageId) => {
          const { retrieveImagesAfter } = this.props;
          if (retrieveImagesAfter != null) {
            const { pageStartIndex, retrievedImagesLength, lastRetrievedId } = this.state;
            const roomAfter = retrievedImagesLength - (pageStartIndex + pageSize);

            if (id == lastRetrievedId) {
              // We'll be on the last page if this happens
              if (imagesOnCurrentPageAndLookahead.length > 1) {
                const penultimateImageOnPage =
                  imagesOnCurrentPageAndLookahead[imagesOnCurrentPageAndLookahead.length - 2]
                this.setState({
                  retrievedImagesLength: retrievedImagesLength - 1,
                  lastRetrievedId: penultimateImageOnPage.image
                });
              } else {
                // Last resort - stream all the images just to get the penultimate id
                // Requires iterable repeatability but this is used elsewhere
                const penultimate = takePenultimateOr(null, drop(pageStartIndex, images));
                this.setState({
                  retrievedImagesLength: retrievedImagesLength - 1,
                  lastRetrievedId: mapNullable(penultimate, p => p.image)
                });
              }
            } else {
              this.setState({ retrievedImagesLength: retrievedImagesLength - 1 });
            }
            // roomAfter should be 2 since one of those two is about to disapear
            // as a result of the removal
            console.log(`Removing image ${id}`);
            if (roomAfter < 2) {
              await mapNullable(this.state.lastRetrievedId, async startAt => {
                await retrieveImagesAfter(startAt, pageSize + lookaheadSize);
              }) || Promise.resolve();
            }
          }

          await removeImage(id);
        }
      }
    })();

    return (
      <section className="gallery-widget">
        <section className="gallery-items columns is-multiline">
          {map(({ image, description }) =>
            <GalleryItem id={image} key={image}
              caption={description}
              itemSize={itemSize}
              isSelected={image == selectedImage}
              onSelect={() => this.onSelectImage(image)}
              onDelete={onRemoveImage != null ? () => onRemoveImage(image) : undefined}/>, imagesOnCurrentPage)}
        </section>
        <nav className="pagination is-centered" role="navigation" aria-label="pagination">
          <button
            className="pagination-previous"
            onClick={() => this.onPreviousPage()}
            disabled={isFirstPage}>
              Previous Page
          </button>
          <button
            className="pagination-next"
            onClick={() => this.onNextPage()}
            disabled={isLastPage}>
              Next Page
          </button>
        </nav>
      </section>
    );
  }
}

export interface GalleryItemProps {
  id: ImageId;
  caption?: string;
  isSelected: boolean;
  itemSize: number;
  onSelect(): void;
  onDelete?(): void;
}

export const GalleryItem = (props: GalleryItemProps) => {
  const { id, caption, isSelected, itemSize, onDelete, onSelect } = props;
  return (
    <div className={`column is-${itemSize}`}>
      <a onClick={() => onSelect()} className={cx("item", { 'is-selected': isSelected })}>
        <div className="image-aspect-box">
          <div className="image-container">
            <div className="image-contained">
              <img src={`/api/images/${id}.png`}/>
              {!isSelected && onDelete ? <button onClick={(e: MouseEvent<HTMLButtonElement>) => { e.stopPropagation(); onDelete() }} className="delete is-large"/> : <></>}
            </div>
          </div>
        </div>
        {mapNullable(caption, c => <p className="caption">{c}</p>) || <></>}
      </a>
    </div>
  );
};

import { imagesUsedByCardPair } from "@cat-tuition/protocol/pairs";
import { Vector } from "prelude-ts";
import { CardPairsAction, CREATED_CARD_PAIR, REMOVED_CARD_PAIR, UPDATED_CARD_PAIR } from "../card-pairs/actions";
import { ImagesAction, RETRIEVED_INITIAL_UNASSIGNED_IMAGES, RETRIEVED_UNASSIGNED_IMAGES_BEFORE, RETRIEVED_UNASSIGNED_IMAGES_AFTER, REMOVED_UNASSIGNED_IMAGE, ADDED_UNASSIGNED_IMAGES } from "./actions";
import { ImagesState } from "./state";

export const initialState: ImagesState = {
  likelyUnassignedImages: Vector.empty(),
};

export const reducer = (state: ImagesState = initialState, action: ImagesAction | CardPairsAction): ImagesState => {
  switch (action.type) {
    case RETRIEVED_INITIAL_UNASSIGNED_IMAGES: {
      const initialImages = action.payload;
      return { ...state, likelyUnassignedImages: Vector.ofIterable<string>(initialImages) };
    }
    case RETRIEVED_UNASSIGNED_IMAGES_BEFORE: {
      const { likelyUnassignedImages } = state;
      const imagesBefore = action.payload;
      return { ...state, likelyUnassignedImages: likelyUnassignedImages.prependAll(imagesBefore) };
    }
    case RETRIEVED_UNASSIGNED_IMAGES_AFTER: {
      const { likelyUnassignedImages } = state;
      const imagesAfter = action.payload;
      return { ...state, likelyUnassignedImages: likelyUnassignedImages.appendAll(imagesAfter) };
    }
    case ADDED_UNASSIGNED_IMAGES: {
      const ids = action.payload;
      const { likelyUnassignedImages } = state;
      return { ...state, likelyUnassignedImages: likelyUnassignedImages.prependAll(ids) }
    }
    case REMOVED_UNASSIGNED_IMAGE: {
      const id = action.payload;
      const { likelyUnassignedImages } = state;
      return { ...state, likelyUnassignedImages: likelyUnassignedImages.removeFirst(s => s == id) }
    }
    case CREATED_CARD_PAIR: {
      const { likelyUnassignedImages } = state;
      const newUnassignedImages = likelyUnassignedImages.removeAll(imagesUsedByCardPair(action.payload));
      return { ...state, likelyUnassignedImages: newUnassignedImages }
    }
    case UPDATED_CARD_PAIR: {
      const { likelyUnassignedImages } = state;
      const newUnassignedImages = likelyUnassignedImages.removeAll(imagesUsedByCardPair(action.payload.update));
      return { ...state, likelyUnassignedImages: newUnassignedImages }
    }
    case REMOVED_CARD_PAIR: {
      const { likelyUnassignedImages } = state;
      const newUnassignedImages = likelyUnassignedImages.prependAll(imagesUsedByCardPair(action.payload));
      return { ...state, likelyUnassignedImages: newUnassignedImages }
    }
    default: {
      return state;
    }
  }
};

import { ImageId } from "@cat-tuition/protocol/image";
import { Action, ThunkAction } from "../redux-utils";
import { deleteImage, fetchInitialUnassignedImages, fetchUnassignedImagesAfter, fetchUnassignedImagesBefore, postImages, PostImagesResponse } from "./api";

export const RETRIEVED_INITIAL_UNASSIGNED_IMAGES = 'IMAGES::RETRIEVED_INITIAL_UNASSIGNED_IMAGES';
export const RETRIEVED_UNASSIGNED_IMAGES_BEFORE = 'IMAGES::RETRIEVED_UNASSIGNED_IMAGES_BEFORE';
export const RETRIEVED_UNASSIGNED_IMAGES_AFTER = 'IMAGES::RETRIEVED_UNASSIGNED_IMAGES_AFTER';
export const REMOVED_UNASSIGNED_IMAGE = 'IMAGES::REMOVED_UNASSIGNED_IMAGE';
export const ADDED_UNASSIGNED_IMAGES = 'IMAGES::ADDED_UNASSIGNED_IMAGES';

export interface RetrievedInitialUnassignedImagesAction extends Action<typeof RETRIEVED_INITIAL_UNASSIGNED_IMAGES, ImageId[]> { }
export interface RetrievedUnassignedImagesBeforeAction extends Action<typeof RETRIEVED_UNASSIGNED_IMAGES_BEFORE, ImageId[]> { }
export interface RetrievedUnassignedImagesAfterAction extends Action<typeof RETRIEVED_UNASSIGNED_IMAGES_AFTER, ImageId[]> { }
export interface AddedUnassignedImagesAction extends Action<typeof ADDED_UNASSIGNED_IMAGES, ImageId[]> { }
export interface RemovedUnassignedImageAction extends Action<typeof REMOVED_UNASSIGNED_IMAGE, ImageId> { }

export type ImagesAction =
  RetrievedInitialUnassignedImagesAction |
  RetrievedUnassignedImagesBeforeAction |
  RetrievedUnassignedImagesAfterAction |
  AddedUnassignedImagesAction |
  RemovedUnassignedImageAction;


export const uploadImages = (file: File, isWidgit: boolean): ThunkAction<Promise<ImageId[] | null>> =>
  async dispatch => {
    const response = await postImages(file, isWidgit);
    return response.map(ids => {
      dispatch(addedUnassignedImages(ids));
      return ids;
    }).getOrNull();
  };

export const retrievedInitialUnassignedImages = (initialImages: ImageId[]): RetrievedInitialUnassignedImagesAction => ({
  type: RETRIEVED_INITIAL_UNASSIGNED_IMAGES,
  payload: initialImages
});

export const retrievedUnassignedImagesBefore = (imagesBefore: ImageId[]): RetrievedUnassignedImagesBeforeAction => ({
  type: RETRIEVED_UNASSIGNED_IMAGES_BEFORE,
  payload: imagesBefore
});

export const retrievedUnassignedImagesAfter = (imagesAfter: ImageId[]): RetrievedUnassignedImagesAfterAction => ({
  type: RETRIEVED_UNASSIGNED_IMAGES_AFTER,
  payload: imagesAfter
});

export const addedUnassignedImages = (ids: ImageId[]): AddedUnassignedImagesAction => ({
  type: ADDED_UNASSIGNED_IMAGES,
  payload: ids
});

export const removedUnassignedImage = (id: ImageId): RemovedUnassignedImageAction => ({
  type: REMOVED_UNASSIGNED_IMAGE,
  payload: id
});

export const removeUnassignedImage = (id: ImageId): ThunkAction<Promise<boolean>> =>
  async dispatch => {
    try {
      const result = await deleteImage(id);
      if (result) {
        dispatch(removedUnassignedImage(id));
      } else {
        console.log("Failed to remove image, is it still used by a card or board");
      }
      return result;
    } catch(err) {
      return false;
    }
  };

export type RetrievalResultPair<T> = {
  retrievalCount: number,
  lastRetrievedItem: T | null
};

export const sizeAndLast = <T>(array: T[]): RetrievalResultPair<T> => {
  const retrievalCount = array.length;
  if (retrievalCount > 0) {
    const lastRetrievedItem = array[array.length -1];
    return { retrievalCount, lastRetrievedItem };
  } else {
    return { retrievalCount, lastRetrievedItem: null };
  }
}

export const retrieveInitialUnassignedImages = (count: number): ThunkAction<Promise<RetrievalResultPair<ImageId> | null>> =>
  async dispatch => {
    try {
      const initialImages = await fetchInitialUnassignedImages(count);
      dispatch(retrievedInitialUnassignedImages(initialImages));
      return sizeAndLast(initialImages);
    } catch(err) {
      return null;
    }
  };

export const retrieveUnassignedImagesBefore = (id: string, count: number): ThunkAction<Promise<RetrievalResultPair<ImageId> | null>> =>
  async dispatch => {
    try {
      const imagesBefore = await fetchUnassignedImagesBefore(id, count);
      dispatch(retrievedUnassignedImagesBefore(imagesBefore));
      return sizeAndLast(imagesBefore);
    } catch(err) {
      return null;
    }
  };

export const retrieveUnassignedImagesAfter = (id: string, count: number): ThunkAction<Promise<RetrievalResultPair<ImageId> | null>> =>
  async dispatch => {
    try {
      const imagesAfter = await fetchUnassignedImagesAfter(id, count);
      dispatch(retrievedUnassignedImagesAfter(imagesAfter));
      return sizeAndLast(imagesAfter);
    } catch(err) {
      return null;
    }
  };

import { ImageId } from "@cat-tuition/protocol/image";
import { Vector } from "prelude-ts";

export type ImagesState = {
  likelyUnassignedImages: Vector<ImageId>;
};
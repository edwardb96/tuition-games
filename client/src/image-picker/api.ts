import axios from "axios";
import { ImageId, imageIdSchema } from "@cat-tuition/protocol/image";
import { Option } from "prelude-ts";
import Joi from "joi";
import { validOrNone } from "@cat-tuition/protocol/joi-ext";
const { array } = Joi.types();

export type PostImagesResponse = ImageId[];

export const postImagesResponseSchema =
  array.items(imageIdSchema)

const validatePostImageResponse = (value: unknown): Option<PostImagesResponse> =>
  validOrNone(postImagesResponseSchema, value);

export const fetchUnassignedImagesAfter = async (id: ImageId, count: number): Promise<ImageId[]> => {
  return await (await axios.get(`/api/images/unassigned?after=${id}&count=${count}`)).data;
}

export const fetchUnassignedImagesBefore = async (id: ImageId, count: number): Promise<ImageId[]> => {
  return await (await axios.get(`/api/images/unassigned?before=${id}&count=${count}`)).data;
}

export const fetchInitialUnassignedImages = async (count: number): Promise<ImageId[]> => {
  return await (await axios.get(`/api/images/unassigned?count=${count}`)).data;
}

export const postImages = async (image: File, isWidgit: boolean): Promise<Option<PostImagesResponse>> => {
  try {
    const formData = new FormData();
    formData.append('file', image);
    const url = `/api/images?crop_widgit_cards=${isWidgit}`;
    const response = await axios.post(url, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
    if (response.status == 200) {
      return validatePostImageResponse(response.data);
    } else {
      return Option.none();
    }
  } catch (err) {
    return Option.none();
  }
};

export const deleteImage = async (id: ImageId): Promise<boolean> => {
  try {
    const response = await axios.delete(`/api/images/${id}`);
    return (response.status == 200);
  } catch (err) {
    return false;
  }
}
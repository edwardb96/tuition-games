import React from "react";
import useFitText from "use-fit-text";
import { last } from "../array-functions";
import "./profile-image.scss";

export interface ProfileImageProps {
  displayName: string;
  size?: number;
  colour: string;
  className?: string;
}

const initials = (name: string) => {
  const nameParts = name.split(" ").filter(x => x.length > 0);
  if (nameParts.length < 1) {
    throw new Error("Cannot get initials form name with zero parts");
  } else if (nameParts.length == 1) {
    return nameParts[0].substr(0, 2);
  } else if (nameParts.length < 4) {
    return nameParts.map(s => s[0]).join('').toUpperCase();
  } else {
    return (nameParts[0][0] + (last(nameParts) as string)[0]).toUpperCase();
  }
}

export const ProfileImage = (props: ProfileImageProps) => {
  const { displayName, size, colour, className } = props;
  const { fontSize, ref } = useFitText({ maxFontSize: 1000, minFontSize: 5 });
  return (
    <figure style={{ height: size, width: size }} className={`profile-image image is-circular is-${colour}-coloured ${className || ''}`}>
      <div ref={ref} style={{ fontSize }} className="initials">{initials(displayName)}</div>
    </figure>
  );
}
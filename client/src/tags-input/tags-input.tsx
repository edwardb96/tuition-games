import React, { FocusEvent } from "react";
import ReactTagAutocomplete, { TagComponentProps } from "react-tag-autocomplete";
import { StringSchema } from "joi";
import { validOrNull } from "@cat-tuition/protocol/joi-ext";
import './tags-input.scss';

export interface TagsInputProps {
  placeholder: string;
  suggestions: string[];
  tagSchema: StringSchema;
  value: string[];
  onChange(value: string[]): void;
}

const classNames = {
  root: 'tags-input input',
  rootFocused: 'is-focused',
  selected: 'selected tags',
  selectedTag: 'selected-tag',
  selectedTagName: 'selected-tag-name',
  search: 'search',
  searchWrapper: 'search-wrapper',
  searchInput: 'search-input',
  suggestions: 'suggestions',
  suggestionActive: 'is-active',
  suggestionDisabled: 'is-disabled'
}

export interface NamedTagLike {
  name: string;
}

const TagComponent = ({ tag, removeButtonText, onDelete }: TagComponentProps) => {
  return (
    <span className="tag is-primary is-medium">
      {tag.name}
      <button type="button" onClick={onDelete} className="delete is-small"></button>
    </span>
  )
}

export const TagsInput = (props: TagsInputProps) => {
  const { onChange, value, suggestions, placeholder, tagSchema } = props;

  const stringToTag = (s: string) => ({ id: s, name: s })

  const setValue = (value: string[]) => {
    onChange(value);
  };

  const onDelete = (index: number) => {
    const newValue = value.slice();
    newValue.splice(index, 1);
    setValue(newValue);
  }

  const onAddition = (tag: NamedTagLike) => {
    const tagValue = validOrNull<string>(tagSchema, tag.name);
    if (tagValue != null && value.indexOf(tagValue) == -1) {
      setValue([...value, tag.name as string]);
    }
  }

  const tags = value.map(stringToTag);

  return (
    <ReactTagAutocomplete
      allowNew
      addOnBlur
      classNames={classNames}
      tags={tags}
      suggestions={suggestions.map(stringToTag)}
      onDelete={onDelete}
      minQueryLength={0}
      onAddition={onAddition}
      tagComponent={TagComponent}
      placeholderText={placeholder} />
  );
}

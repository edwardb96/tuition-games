import React from "react";
import { FieldProps } from "formik";
import { StringSchema } from "joi";
import { TagsInput } from "./tags-input";

export interface TagsFieldProps extends FieldProps<string[]> {
  placeholder: string;
  suggestions: string[];
  tagSchema: StringSchema;
}

export const TagsField = (props: TagsFieldProps) => {
  const { field, form, suggestions, placeholder, tagSchema } = props;
  const { setFieldValue, setFieldTouched } = form;
  const { name, value } = field;

  const onChange = (value: string[]) => {
    setFieldTouched(name, true, false);
    setFieldValue(name, value);
  };

  return (
    <TagsInput
      onChange={onChange}
      placeholder={placeholder}
      suggestions={suggestions}
      tagSchema={tagSchema}
      value={value} />
  );
}

import './lobby.scss';
import { connect } from "react-redux";
import React, { useEffect } from "react";
import { AppState } from "../../redux-store";
import { LobbyState } from "../lobby";
import { sendEnterLobby } from "../actions";
import { TutorLobby } from "./tutor-lobby";
import { TuteeLobby } from "./tutee-lobby";
import { ConnectionStateTag, ProfileState } from "../../navbar/reducer";
import { Redirect } from "react-router";
import Navbar from "../../navbar/views/navbar";
import LoadingPage from "../../loading-page/views/loading-page";

export interface LobbyProps {
  lobby: LobbyState;
  profile: ProfileState;
  sendEnterLobby: () => void;
}

const Lobby = (props: LobbyProps) => {
  const { lobby, profile, sendEnterLobby } = props;
  const { connection, user } = profile;

  useEffect(() => {
    if (user != null && connection.tag == ConnectionStateTag.Connected) {
      sendEnterLobby();
    }
  }, [user, connection.tag]);

  if (user != null && connection.tag == ConnectionStateTag.Connected && lobby.isLoaded) {
    if (lobby.invitedToClassroom == null) {
      const renderedLobby = (() => {
        switch (lobby.kind) {
          case 'tutee':
            return <TuteeLobby user={user}/>;
          case 'tutor':
            return <TutorLobby lobby={lobby} user={user} />
        }
      })();
      return (
        <>
          <Navbar shouldConnect/>
          {renderedLobby}
        </>
      );
    } else {
      return <Redirect push to={`/classroom/${lobby.invitedToClassroom}`}/>
    }
  } else {
    return <LoadingPage shouldConnect message="Connecting to lobby..."/>;
  }
}

const mapState = (s: AppState) => ({ lobby: s.lobby, profile: s.profile });
const mapDispatch = { sendEnterLobby };
export default connect(mapState, mapDispatch)(Lobby);
import React from "react";
import { User } from "@cat-tuition/protocol/user";

interface TuteeLobbyProps {
  user: User;
}

export const TuteeLobby = (props: TuteeLobbyProps) => {
  return (
    <div className="hero-body container">
      <h1 className="title is-1">Hi {props.user.displayName}! Welcome to the Lobby</h1>
      <h3 className="subtitle is-3">Waiting for your tutor to connect...</h3>
    </div>
  );
}
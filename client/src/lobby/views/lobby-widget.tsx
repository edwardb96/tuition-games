import { Role, StoredUser, User, UserInfo } from "@cat-tuition/protocol/user";
import { ProfileImage } from "../../profile-image/profile-image";
import React, { ChangeEvent, useEffect, useState } from "react";
import { mapNullable } from "@cat-tuition/protocol/iter-tools-ext";
import { HashMap } from "prelude-ts";
import map from "iter-tools/methods/map";
import filter from "iter-tools/methods/filter";
import { get, hasRetrievalSucceeded, shouldReretrieve } from "../../retrieval";
import { inviteTutee } from "../api";
import { UsersState } from "../../users/state";
import { AppState } from "../../redux-store";
import { connect } from "react-redux";
import { retrieveUsers } from "../../users/actions";
import cx from "classnames"

interface TuteeRowProps {
  tutee: UserInfo;
  onAdmit?: () => void;
  onInvite?: () => void;
}


const TuteeRow = (props: TuteeRowProps) => {
  const { tutee, onAdmit, onInvite } = props;
  const { displayName, favouriteColour } = tutee;
  return (
    <tr className="tutee-row">
      <td>
        <ProfileImage size={36} displayName={displayName} colour={favouriteColour} />
      </td>
      <td>
        {tutee.displayName}
      </td>
      <td className="level-right">
        {mapNullable(onAdmit, onClick =>
          <button onClick={onClick} className="button small is-primary">Admit</button>)}
        {mapNullable(onInvite, onClick =>
          <button onClick={onClick} className="button small is-primary">Invite</button>)}
      </td>
    </tr>
  );
};

export interface LobbyWidgetProps {
  user?: User;
  users: UsersState;
  retrieveUsers(): Promise<boolean>;

  usersWaiting: HashMap<string, UserInfo>;
  onSendAdmittance(userId: string): void;
}

interface LobbyWidgetState {
  query: string;
  invitationLink: string | null;
  showLink: boolean;
}

export const LobbyWidget = (props: LobbyWidgetProps) => {
  const { usersWaiting, onSendAdmittance, user, users: usersRetrieval } = props;

  const loadUsers = async () => {
    const { retrieveUsers, users } = props;
    if (shouldReretrieve(users, 5)) {
      await retrieveUsers();
    }
  };

  useEffect(() => {
    if (user != null && user.canManageUsers()) {
      loadUsers();
    }
  }, [user]);

  const loadedUsersNotSelfOrOnline = (() => {
    if (user != null && hasRetrievalSucceeded(usersRetrieval)) {
      return get(usersRetrieval).filter(userId =>
        userId != user.id && !usersWaiting.containsKey(userId));
    } else {
      return HashMap.empty<string, StoredUser>();
    }
  })();

  const [state, setState] = useState<LobbyWidgetState>({
    query: '',
    invitationLink: null,
    showLink: false
  });

  const { invitationLink, showLink } = state;

  const onChangeQuery = (e: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, query: e.target.value });
  };

  const onInvite = async (id: string) => {
    const token = await inviteTutee(id)
    if (token != null) {
      const invitationLink = `${process.env.BASE_URL}/invitation?token=${token}`;
      setState({ ...state, invitationLink, showLink: true });
    } else {
      console.log(`Failed to get invite link for ${id}`);
    }
  };

  const onCloseInvitationLink = () => {
    setState({ ...state, showLink: false });
  }

  const filteredAdmittableTutees =
    filter(u => u.displayName.includes(state.query), usersWaiting.valueIterable());

  const filteredInvitableTutees =
    filter(u => u.displayName.includes(state.query) && user != null && u.role < user.role,
      loadedUsersNotSelfOrOnline.valueIterable());

  const onCopy = async () => {
    if (state.invitationLink != null) {
      await navigator.clipboard.writeText(state.invitationLink);
      onCloseInvitationLink();
    }
  }

  return (
    <>
      <section className="lobby-widget">
        <div className="search-control control has-icons-left">
          <input onChange={onChangeQuery}
                  className="input"
                  type="text"
                  placeholder="Search users"
                  value={state.query}/>
          <span className="icon is-left">
            <i className="fas fa-search" aria-hidden="true"></i>
          </span>
        </div>
        <table className="users-table table is-fullwidth is-striped">
          <colgroup>
            <col className="profile-image-col" />
          </colgroup>
          <tbody>
            {map(tutee =>
              <TuteeRow
                key={tutee.id}
                onAdmit={() => onSendAdmittance(tutee.id)}
                tutee={tutee} />, filteredAdmittableTutees)}
            {map(tutee =>
              <TuteeRow
                key={tutee.id}
                onInvite={() => onInvite(tutee.id)}
                tutee={tutee} />, filteredInvitableTutees)}
          </tbody>
        </table>
        <aside className={cx("notification is-primary", { 'is-active': showLink })}>
          <button onClick={onCloseInvitationLink} className="delete"/>
          {invitationLink}
          <div className="cover"/>
          <button onClick={onCopy} className="copy button is-primary is-inverted">
            <span className="icon">
              <i className="far fa-clipboard"/>
            </span>
            <span>
              Copy To Clipboard
            </span>
          </button>
        </aside>
      </section>
    </>
  );
}

const mapState = ({ profile, users }: AppState) => ({ user: profile.user, users });
export default connect(mapState, { retrieveUsers })(LobbyWidget);
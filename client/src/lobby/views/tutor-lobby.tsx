import React from "react";
import { ClassroomWidget } from "../../classroom/views/classroom-widget";
import { TutorLobbyState, usersWaiting } from "../lobby";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import flatMap from "iter-tools/methods/flat-map";
import map from "iter-tools/methods/map";
import { optionIterable } from "../../prelude-ts-ext";
import { ProfileImage } from "../../profile-image/profile-image";

interface TutorLobbyProps {
  lobby: TutorLobbyState
  user: User;
}

const TuteeIcon = (props: { tutee: UserInfo }) => {
  const { displayName, favouriteColour } = props.tutee;
  return (
    <div className="tutee-icon column has-text-centered">
      <div className="image-wrapper">
        <ProfileImage size={48} colour={favouriteColour} displayName={displayName}/>
      </div>
      <p className="title is-6">{props.tutee.displayName}</p>
      <p className="subtitle is-7">{props.tutee.id}</p>
    </div>
  );
}

export const TutorLobby = (props: TutorLobbyProps) => {
  const { user, lobby } = props;
  const { classrooms, usersOnline } = lobby;
  return (
    <div className="hero-body container">
      <h1 className="title is-1">Lobby</h1>
      <section className="columns is-multiline">
        {classrooms.toArray().map(
          ([classroomId, classroom]) => {
            const tutor = classroom.tutor;
            const tutees =
              flatMap(s => optionIterable(usersOnline.get(s)), classroom.tutees)
            return (
              <ClassroomWidget
                id={classroomId}
                key={classroomId}
                isActive
                isOwn={false}
                tutor={tutor}
                tutees={[...tutees]} />
            )
          })
        }
        <ClassroomWidget
          key={user.id}
          id={user.id}
          isActive={false}
          isOwn
          tutor={user}
          tutees={[]} />
      </section>
      <section className="users-waiting card">
        <div className="card-content">
          <header className="header is-fullwidth">
            <p className="title is-4">Tutees Waiting</p>
          </header>
          <section className="waiting-users columns is-multiline is-centered">
            {map(([tuteeId, tutee]) => <TuteeIcon key={tuteeId} tutee={tutee}/>, usersWaiting(lobby))}
          </section>
        </div>
      </section>
    </div>
  );
}
// <header className="header"><h3 className="title is-3">Tutees Waiting</h3></header>
import { Action, TagAction, ThunkAction } from "../redux-utils";
import { UserIdInfo, UserInfo } from "@cat-tuition/protocol/user";
import { ClassroomIdInfo, ClassroomInfo } from "@cat-tuition/protocol/classroom";
import {
  LOBBY_CHANNEL,
  LobbyClientPayload as ClientPayload,
  TutorWelcomeToLobbyInfo,
  TuteeWelcomeToLobbyInfo,
  UserInfoAttachment,
  InvitationInfo } from "@cat-tuition/protocol/channels/lobby";
import { FetchedUserAction } from "../navbar/actions";
import { Action as ReduxAction } from "redux";
import { send as sendWs } from "@giantmachines/redux-websocket";
import { TuteeEnteredClassroomPayload } from "@cat-tuition/protocol/channels/classroom";

export const WELCOME_TUTOR = 'LOBBY::WELCOME_TUTOR';
export const WELCOME_TUTEE = 'LOBBY::WELCOME_TUTEE';
export const CLASSROOM_ACTIVE = 'LOBBY::CLASSROOM_ACTIVE';
export const CLASSROOM_INACTIVE = 'LOBBY::CLASSROOM_INACTIVE';
export const USER_JOINED_LOBBY = 'LOBBY::USER_JOINED_LOBBY';
export const USER_LEFT_LOBBY = 'LOBBY::USER_LEFT_LOBBY';
export const USER_JOINED_CLASS = 'LOBBY::USER_JOINED_CLASS';
export const USER_LEFT_CLASS = 'LOBBY::USER_LEFT_CLASS';
export const START_CLASSROOM = 'LOBBY::START_CLASSROOM';
export const JOIN_CLASSROOM = 'LOBBY::JOIN_CLASSROOM';
export const ADMITTED_TO_CLASS = 'LOBBY::INVITED';
export const ENTERED_LOBBY = 'LOBBY::ENTERED_LOBBY';
export const NO_SUCH_CLASS = 'LOBBY::NO_SUCH_CLASS';

export type LobbyAction =
  FetchedUserAction |
  WelcomeTuteeAction |
  WelcomeTutorAction |
  EnteredLobbyAction |
  UserJoinedLobbyAction |
  UserLeftLobbyAction |
  UserJoinedClassAction |
  UserLeftClassAction |
  ClassroomActiveAction |
  ClassroomInactiveAction |
  StartClassroomAction |
  JoinClassroomAction |
  NoSuchClassroomAction |
  AdmittedToClassAction;

export interface WelcomeTuteeAction extends Action<typeof WELCOME_TUTEE, TuteeWelcomeToLobbyInfo> { };
export interface WelcomeTutorAction extends Action<typeof WELCOME_TUTOR, TutorWelcomeToLobbyInfo> { };
export interface UserJoinedClassAction extends Action<typeof USER_JOINED_CLASS, UserIdInfo & ClassroomIdInfo> { };
export interface UserLeftClassAction extends Action<typeof USER_LEFT_CLASS, UserIdInfo & ClassroomIdInfo> { };
export interface UserJoinedLobbyAction extends Action<typeof USER_JOINED_LOBBY, UserInfoAttachment> { };
export interface UserLeftLobbyAction extends Action<typeof USER_LEFT_LOBBY, string> { };
export interface ClassroomActiveAction extends Action<typeof CLASSROOM_ACTIVE, ClassroomInfo> { };
export interface ClassroomInactiveAction extends Action<typeof CLASSROOM_INACTIVE, string> { };
export interface EnteredLobbyAction extends TagAction<typeof ENTERED_LOBBY> { };
export interface StartClassroomAction extends TagAction<typeof START_CLASSROOM> { };
export interface JoinClassroomAction extends Action<typeof JOIN_CLASSROOM, ClassroomIdInfo> { };
export interface AdmittedToClassAction extends Action<typeof ADMITTED_TO_CLASS, InvitationInfo> { };
export interface NoSuchClassroomAction extends TagAction<typeof NO_SUCH_CLASS> { };

export const welcomeTutee = (welcome: TuteeWelcomeToLobbyInfo): WelcomeTuteeAction =>
  ({ type: WELCOME_TUTEE, payload: welcome });

export const welcomeTutor = (welcome: TutorWelcomeToLobbyInfo): WelcomeTutorAction =>
  ({ type: WELCOME_TUTOR, payload: welcome });

export const enteredLobby = (): EnteredLobbyAction =>
  ({ type: ENTERED_LOBBY })

export const userJoinedLobby = (user: UserInfo): UserJoinedLobbyAction =>
  ({ type: USER_JOINED_LOBBY, payload: { user } });

export const userLeftLobby = (tuteeId: string): UserLeftLobbyAction =>
  ({ type: USER_LEFT_LOBBY, payload: tuteeId });

export const userJoinedClass = (userId: string, classroomId: string): UserJoinedClassAction =>
  ({ type: USER_JOINED_CLASS, payload: { userId, classroomId } });

export const userLeftClass = (userId: string, classroomId: string): UserLeftClassAction =>
  ({ type: USER_LEFT_CLASS, payload: { userId, classroomId } });

export const classroomActive = (classroom: ClassroomInfo): ClassroomActiveAction =>
  ({ type: CLASSROOM_ACTIVE, payload: classroom });

export const classroomInactive = (classroomId: string): ClassroomInactiveAction =>
  ({ type: CLASSROOM_INACTIVE, payload: classroomId });

export const joinClassroom = (classroomId: string): JoinClassroomAction =>
  ({ type: JOIN_CLASSROOM, payload: { classroomId } })

export const noSuchClassroom = (): NoSuchClassroomAction =>
  ({ type: NO_SUCH_CLASS });

export const invitedToClass = (info: InvitationInfo): AdmittedToClassAction =>
  ({ type: ADMITTED_TO_CLASS, payload: info });

const sendLobby = (payload?: ClientPayload): ReduxAction =>
  sendWs({ channel: LOBBY_CHANNEL, payload });

export const sendEnterClassroom = (classroomId: string): ReduxAction =>
  sendLobby({ kind: 'enter-classroom', classroomId })

export const sendEnterLobby = (): ReduxAction =>
  sendLobby({ kind: 'enter-lobby' })

export const sendAdmittance = (userId: string): ReduxAction =>
  sendLobby({ kind: 'admit-tutee', userId })
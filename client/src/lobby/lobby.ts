import { UserInfo } from "@cat-tuition/protocol/user";
import { ClassroomInfo } from "@cat-tuition/protocol/classroom";
import { HashMap, HashSet } from "prelude-ts";
import { Classroom } from "./classroom";
import map from "iter-tools/methods/map";

export type LobbyState =
  LoadingLobbyState
  | TutorLobbyState
  | TuteeLobbyState;

export interface LobbyOccupants {
  classrooms: HashMap<string, Classroom>;
  usersWaiting: HashSet<string>;
  usersOnline: HashMap<string, UserInfo>;
}

export interface TuteeLobbyState {
  isLoaded: true;
  kind: 'tutee';
  invitedToClassroom: string | null;
}

export interface TutorLobbyState extends LobbyOccupants {
  isLoaded: true;
  kind: 'tutor';
  invitedToClassroom: string | null;
}

export interface LoadingLobbyState {
  isLoaded: false;
  kind: null | 'tutee' | 'tutor';
}

export const usersWaiting = (lobby: LobbyState): HashMap<string, UserInfo> => {
  if (lobby.isLoaded && lobby.kind == 'tutor') {
    const { usersWaiting, usersOnline } = lobby;
    return HashMap.ofIterable(map(userId =>
      [userId, usersOnline.get(userId).getOrThrow('Got user who is waiting but not online?')],
      usersWaiting));
  } else {
    return HashMap.empty<string, UserInfo>();
  }
}

export const loadLobbyOccupantsLists = (classroomList: ClassroomInfo[], waitingList: UserInfo[]): LobbyOccupants => {
  const [waitingMap, usersWaiting] =
    waitingList.reduce(([map, set], user) => [map.put(user.id, user), set.add(user.id)],
      [HashMap.empty<string, UserInfo>(), HashSet.empty<string>()]);

  const extractedClassrooms = classroomList.map(info => Classroom.fromClassroomInfo(info));
  const usersOnline = extractedClassrooms.reduce(
    (map, [_, users]) => map.mergeWith(users, (a, _) => a),
    waitingMap)
  const classrooms: HashMap<string, Classroom> =
    HashMap.ofIterable<string, Classroom>(
      extractedClassrooms.map(([classroom, _]) => [classroom.id, classroom]));

  return { classrooms, usersWaiting, usersOnline };
};
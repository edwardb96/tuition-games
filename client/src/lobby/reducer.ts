import { UserInfo } from "@cat-tuition/protocol/user";
import { TUTEE_ENTERED_CLASS, WelcomeTuteeAction } from "../classroom/actions";
import { adjust } from "../prelude-ts-ext";
import { LobbyAction,
         ADMITTED_TO_CLASS,
         CLASSROOM_ACTIVE, CLASSROOM_INACTIVE,
         USER_JOINED_CLASS, USER_JOINED_LOBBY,
         USER_LEFT_CLASS, USER_LEFT_LOBBY,
         WELCOME_TUTOR, WELCOME_TUTEE } from "./actions";
import { Classroom } from "./classroom";
import { loadLobbyOccupantsLists, LobbyState } from "./lobby";

export const initialState: LobbyState = { kind: null, isLoaded: false };

export const reducer = (state: LobbyState = initialState, action: LobbyAction | WelcomeTuteeAction): LobbyState => {
  switch (action.type) {
    case WELCOME_TUTOR: {
      if (!state.isLoaded && state.kind == null) {
        const { classrooms, usersWaiting } = action.payload;
        return {
          ...state,
          kind: 'tutor',
          isLoaded: true,
          invitedToClassroom: null,
          ...loadLobbyOccupantsLists(classrooms, usersWaiting)
        };
      } else {
        return state;
      }
    }
    case WELCOME_TUTEE: {
      if (!state.isLoaded && state.kind == null) {
        return {
          ...state,
          kind: 'tutee',
          isLoaded: true,
          invitedToClassroom: null,
        };
      } else {
        return state;
      }
    }
    case ADMITTED_TO_CLASS: {
      if (state.isLoaded) {
        return { ...state, invitedToClassroom: action.payload.tutor.id };
      } else {
        return state
      }
    }
    case CLASSROOM_ACTIVE: {
      if (state.isLoaded && state.kind == 'tutor') {
        const [classroom, _] = Classroom.fromClassroomInfo(action.payload);
        const usersWaiting = state.usersWaiting.remove(classroom.tutor.id);
        const classrooms = state.classrooms.put(classroom.id, classroom);
        return { ...state, usersWaiting, classrooms };
      } else {
        return state;
      }
    }
    case CLASSROOM_INACTIVE: {
      if (state.isLoaded && state.kind == 'tutor') {
        const classrooms = state.classrooms.remove(action.payload);
        return { ...state, classrooms };
      } else {
        return state;
      }
    }
    case USER_JOINED_LOBBY: {
      const tutee : UserInfo = action.payload.user;
      if (state.isLoaded && state.kind == 'tutor') {
        const usersOnline = state.usersOnline.put(tutee.id, tutee);
        const usersWaiting = state.usersWaiting.add(tutee.id);
        return { ...state, usersOnline, usersWaiting }
      } else {
        return state;
      }
    }
    case USER_LEFT_LOBBY: {
      if (state.isLoaded && state.kind == 'tutor') {
        const tuteeId = action.payload;
        const usersOnline = state.usersOnline.remove(tuteeId);
        const usersWaiting = state.usersWaiting.remove(tuteeId);
        return { ...state, usersOnline, usersWaiting };
      } else {
        return state;
      }
    }
    case USER_JOINED_CLASS: {
      if (state.isLoaded && state.kind == 'tutor') {
        const { userId, classroomId } = action.payload;
        const classrooms =
          adjust(state.classrooms, classroomId,
            classroom => classroom.withTutee(userId))
        console.log('Removing from class %s', userId);
        const usersWaiting = state.usersWaiting.remove(userId);
        return { ...state, classrooms, usersWaiting }
      } else {
        return state;
      }
    }
    case TUTEE_ENTERED_CLASS: {
      if (state.isLoaded) {
        return { ...state, invitedToClassroom: null }
      } else {
        return state;
      }
    }
    case USER_LEFT_CLASS: {
      if (state.isLoaded && state.kind == 'tutor') {
        const { userId, classroomId } = action.payload;
        const classrooms =
          adjust(state.classrooms, classroomId,
            classroom => classroom.withoutTutee(userId));
        //const usersWaiting = state.usersWaiting.add(tuteeId);
        return { ...state, classrooms };
      } else {
        return state;
      }
    }
    default:
      return state;
  }
};
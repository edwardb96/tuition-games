import axios from "axios";

export const inviteTutee = async (tutee: string): Promise<string | null> => {
  try {
    const response = await axios.get(`/api/tutee-token?tutee=${tutee}`);
    if (response.status == 200) {
      return response.data;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
}
import { UserInfo } from "@cat-tuition/protocol/user";
import { ClassroomInfo } from "@cat-tuition/protocol/classroom";
import { HashMap, HashSet } from "prelude-ts";

export class Classroom {
  tutor: UserInfo;
  tutees: HashSet<string>;

  constructor(tutor: UserInfo, tutees: HashSet<string>) {
    this.tutor = tutor;
    this.tutees = tutees;
  }

  get id(): string {
    return this.tutor.id;
  }

  withTutor(tutor: UserInfo): Classroom {
    return new Classroom(tutor, this.tutees);
  }

  withTutee(tuteeId: string): Classroom {
    return new Classroom(this.tutor, this.tutees.add(tuteeId));
  }

  withoutTutee(tuteeId: string): Classroom {
    return new Classroom(this.tutor, this.tutees.remove(tuteeId));
  }

  static fromClassroomInfo({ tutor, tutees }: ClassroomInfo): [Classroom, HashMap<string, UserInfo>] {
    const tuteeSet = HashSet.ofIterable(tutees.map(u => u.id));
    let userMap: HashMap<string, UserInfo> = HashMap.of([tutor.id, tutor])
    userMap = tutees.reduce((map, user) => map.put(user.id, user), userMap)
    return [new Classroom(tutor, tuteeSet), userMap];
  }
}
import { valdiateLobbyServerPayload } from "@cat-tuition/protocol/channels/lobby";
import { ThunkDispatch, Channel } from "../redux-utils";
import { LobbyAction,
         welcomeTutee,
         welcomeTutor,
         userJoinedLobby,
         userLeftLobby,
         userJoinedClass,
         userLeftClass,
         classroomActive,
         classroomInactive,
         invitedToClass,
         enteredLobby,
         noSuchClassroom
       } from "./actions";

export class LobbyChannel implements Channel {
  onReceive(payload: any, next: ThunkDispatch<LobbyAction>) {
    valdiateLobbyServerPayload(payload).match({
      Some: validPayload => {
        switch (validPayload.kind) {
          case 'welcome-tutee':
            next(welcomeTutee(validPayload))
            break;
          case 'welcome-tutor':
            next(welcomeTutor(validPayload))
            break;
          case 'entered-lobby':
            next(enteredLobby())
            break;
          case 'user-joined-lobby':
            next(userJoinedLobby(validPayload.user));
            break;
          case 'user-left-lobby':
            next(userLeftLobby(validPayload.userId));
            break;
          case 'user-joined-class':
            next(userJoinedClass(validPayload.userId, validPayload.classroomId));
            break;
          case 'no-such-classroom':
            next(noSuchClassroom());
            break;
          case 'user-left-class':
            next(userLeftClass(validPayload.userId, validPayload.classroomId));
            break;
          case 'classroom-active':
            next(classroomActive(validPayload));
            break;
          case 'classroom-inactive':
            next(classroomInactive(validPayload.classroomId));
            break;
          case 'invited-to-class':
            next(invitedToClass(validPayload));
            break;
        }
      },
      None: () => {
        console.log('Invalid payload from server', payload);
      }
    });
  }
}
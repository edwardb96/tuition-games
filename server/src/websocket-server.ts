import { Option } from "prelude-ts";

import { ClientMessage, validateClientMessageShell } from "@cat-tuition/protocol/message";
import { User, UserInfo, Role } from "@cat-tuition/protocol/user";
import { LOBBY_CHANNEL, TuteeWelcomeToLobbyInfo, TutorWelcomeToLobbyInfo } from "@cat-tuition/protocol/channels/lobby";
import { PING_CHANNEL, ServerPayload as PingPayload, validatePingClientPayload } from "@cat-tuition/protocol/channels/ping";
import { LobbyChannel, ServerState } from "./channels/lobby";
import { PerUserSession, UserSession, SessionStore, pingFrequencyInMiliseconds } from "./session";
import { Classroom } from "./classroom"
import { WebSocketAuthenticator } from "./authentication/websocket-authenticator";

import WebSocket from "ws";
import { Socket } from "net";
import { IncomingMessage } from "http";

function pop<K, V>(map: Map<K, V>, k: K): Option<V> {
  const item = Option.ofNullable(map.get(k));
  item.ifSome(v => {
    map.delete(k);
  });
  return item
};

const subtractMinutes = (date: Date, minutes: number): Date =>
  new Date(date.getTime() - minutes * 60000);

const MAX_MINUTES_IN_LOBBY = 8;

export class CatTuitionWebSocketServer implements ServerState, SessionStore {
  sessions: Map<string, PerUserSession>;
  lobby: Set<string>;
  classrooms: Map<string, Classroom>;

  lobbyChannel: LobbyChannel;
  wss: WebSocket.Server;
  wsAuth: WebSocketAuthenticator;

  constructor(path: string, wsAuth: WebSocketAuthenticator) {
    this.sessions = new Map<string, PerUserSession>();
    this.lobby = new Set<string>();
    this.classrooms = new Map<string, Classroom>();
    this.lobbyChannel = new LobbyChannel(this);
    this.wss = new WebSocket.Server({ clientTracking: false, noServer: true, path });
    this.wsAuth = wsAuth;
  }

  start(): WebSocket.Server {
    this.wss.on('connection', (ws: WebSocket, req: IncomingMessage, user: User) => {
      console.log(`Cat Server Connected websocket user ${user.displayName} with role ${Role[user.role]}`);
      this.onConnect(ws, user);
    });
    this.sendPings();
    return this.wss;
  }

  private sendPings() {
    for (let session of this.sessions.values()) {
      if (session.isNonResponsive()) {
        session.kickNonResponsive();
      } else if (session.classId.isNone() && session.lastEnteredLobby < subtractMinutes(new Date(), MAX_MINUTES_IN_LOBBY)) {
        session.kickTooLongInLobby();
      } else {
        session.sendPing();
      }
    }
    setTimeout(() => this.sendPings(), pingFrequencyInMiliseconds)
  }

  async handleUpgrade(req: IncomingMessage, socket: Socket, head: Buffer) {
    await this.wsAuth.authenticate(req, socket, user => {
      this.wss.handleUpgrade(req, socket, head, ws => {
        this.wss.emit('connection', ws, req, user);
      });
    });
  }

  sessionForUser(userId: string): Option<UserSession> {
    return Option.ofNullable(this.sessions.get(userId));
  }

  private classroom(classroomId: string) {
    return Option.ofNullable(this.classrooms.get(classroomId));
  }

  private isInOwnClass(session: UserSession) {
    return session.classId.map(id => id == session.user.id).getOrElse(false);
  }

  inviteTutee(session: UserSession, userId: string) {
    if (this.isInOwnClass(session)) {
      const classroom = Option.of(this.classrooms.get(session.user.id))
        .getOrThrow('In class when it no longer exists');

      if (this.lobby.has(userId)) {
        this.sessionForUser(userId).ifSome(newTutee => {
          // Tell user
          this.lobbyChannel.sendAdmittance(newTutee, {
            classroomId: classroom.id,
            tutor: this.userInfo(classroom.tutor)
          });
          classroom.inviteTutee(newTutee);
        })
      }
    } else {
      session.kickBadData('Can only add tutees to your own class which you must be in.');
    }
  }

  joinClassAsTutee(session: UserSession, classroomId: string) {
    const classroomToJoin = this.classroom(classroomId);
    if (classroomToJoin.isSome()) {
      if (classroomToJoin.get().addTutee(session)) {
        for (let observer of this.connectedLobbyObserversExcept(session.user.id)) {
          this.lobbyChannel.sendUserJoinedClass(observer, session.user.id, classroomId);
        }
        this.lobby.delete(session.user.id);
      }
    } else {
      this.lobbyChannel.sendNoSuchClass(session);
    }
  }

  leaveClassAsTutee(session: UserSession, classroom: Classroom) {
    const { user } = session;
    classroom.removeTutee(session);

    for (let observer of this.connectedLobbyObserversExcept(user.id)) {
      this.lobbyChannel.sendUserLeftClass(observer, user.id, classroom.id);
    }
  }

  enterClass(session: UserSession, classroomId: string) {
    if (session.classId.isSome()) {
      const currentClassroomId = session.classId.get();
      if (currentClassroomId != classroomId) {
        if (currentClassroomId == session.user.id) {
          this.endClassroomIfExists(currentClassroomId)
        } else {
          this.leaveClassAsTutee(session, this.classroom(currentClassroomId)
            .getOrThrow('session classId points to non-existant class'));
          this.joinClassAsTutee(session, classroomId);
        }
      }
    } else {
      if (session.user.id == classroomId) {
        this.startClass(session);
      } else {
        this.joinClassAsTutee(session, classroomId);
      }
    }
  }

  enterLobby(session: UserSession) {
    if (!this.lobby.has(session.user.id)) {
      session.classId.ifSome(classId => {
        // End class if tutor for one.
        const wasTutoring = this.endClassroomIfExists(session.user.id);
        if (!wasTutoring) {
          // Leave class if a tutee in one.
          this.leaveClassAsTutee(session, this.classroom(classId)
            .getOrThrow('Session classId points to non-existing class'));
        }
      });

      this.lobby.add(session.user.id);
      session.onEnteredLobby();
      console.log(`Added user ${session.user.id} to lobby it is now %s`, this.lobby);

      this.lobbyChannel.sendEntered(session);

      // Tell all lobby watchers that user is in lobby
      for (let observer of this.connectedLobbyObserversExcept(session.user.id)) {
        this.lobbyChannel.sendUserJoinedLobby(observer, session.user.toInfo());
      }
    } else {
      this.lobbyChannel.sendEntered(session);
    }
  }

  private endClassroomIfExists(classId: string): boolean {
    const maybeClassroom = pop(this.classrooms, classId);
    return maybeClassroom.map(classroom => {
      for (let observer of this.connectedLobbyObservers()) {
        this.lobbyChannel.sendClassroomInactive(observer, classroom.id)
      }
      classroom.notifyEnded();
      return true;
    }).getOrElse(false);
  }

  private startClass(session: UserSession) {
    if (session.user.canStartClass()) {
      session.classId = Option.of(session.user.id);
      this.classrooms.set(session.user.id, new Classroom(session, this));
      this.lobby.delete(session.user.id);
      for (let observer of this.connectedLobbyObservers()) {
        console.log(`Sending to connected observer ${observer.id}`);
        this.lobbyChannel.sendClassroomActive(observer, { tutor: session.user.toInfo(), tutees: [] })
      }
      console.log("Started classroom");
    } else {
      session.kickBadPermission();
    }
  }

  onConnect(socket: WebSocket, user: User) {
    const maybeSession = this.sessions.get(user.id);
    if (maybeSession != null) {
      console.log("Duplicate Connection Will Be Closed");
      maybeSession.closeDuplicate();
    }

    const session = new PerUserSession(user, socket);
    this.sessions.set(user.id, session);

    socket.on('message', rawMessage => {
      this.onMessage(session, rawMessage);
    });

    socket.on('close', (code: number, reason: string) => {
      console.log(`Closing session because ${reason}`)
      this.onClosedSession(session);
    });

    if (session.user.canShowClasses()) {
      this.lobbyChannel.sendWelcomeTutor(session, this.tutorWelcomeInfo(session.user.id));
    } else {
      this.lobbyChannel.sendWelcomeTutee(session, this.tuteeWelcomeInfo(session.user.id));
    }
  }

  private debugPrintSessions() {
    console.log("Existing Sessions: ")
    for (let k of this.sessions.keys()) {
      console.log("- " + k)
    }
  }

  *connectedLobbyObservers(): Iterable<UserSession> {
    for (let session of this.sessions.values()) {
      if (session.user.canShowClasses()) {
        console.log(`Got connected observer ${session.id}`);
        yield session;
      }
    }
  }

  *connectedLobbyObserversExcept(userId: string): Iterable<UserSession> {
    console.log(`Excluding ${userId}`);
    for (let session of this.sessions.values()) {
      if (session.user.canShowClasses() && session.user.id != userId) {
        console.log(`Got connected observer ${session.id}`);
        yield session;
      }
    }
  }

  onClosedSession(session: UserSession) {
    const { user, classId: maybeClassId } = session;
    console.log(`Cat Server Disconnected websocket user ${user.displayName} with role ${Role[user.role]} wasDuplicate?`, session.wasDuplicate);
    if (!session.wasDuplicate) {
      // If the user has a classroom open we need to notify the tutees and
      // anyone who can watch classes in the lobby
      const wasTutoring = this.endClassroomIfExists(user.id);
      if (!wasTutoring) {
        maybeClassId.ifSome(classId => {
          // If the user is in a class as a tutee, remove them from it.
          const classroom = this.classroom(classId)
            .getOrThrow('ClassId pointed to non-existing class');
          this.leaveClassAsTutee(session, classroom);
        });
      }

      // Notify users if leaving the lobby
      if (this.lobby.has(user.id)) {
        for (let observer of this.connectedLobbyObservers()) {
          this.lobbyChannel.sendUserLeftLobby(observer, user.id)
        }
        this.lobby.delete(user.id)
      }

      // Should probably tell the user themselves
      // if they didn't close the session and they should know?
      // not sure that can this happen so not doing anything yet.
      this.sessions.delete(user.id);

      this.debugPrintSessions();
    }
  }

  onMessage(session: PerUserSession, rawMessage: WebSocket.Data) {
    try {
      const rawParsedJSON = JSON.parse(rawMessage.toString());
      validateClientMessageShell(rawParsedJSON).match({
        Some: clientMessage => {
          if (clientMessage.channel == PING_CHANNEL) {
            const validPong = validatePingClientPayload(clientMessage.payload);
            if (validPong) {
              session.gotPong();
            } else {
              session.kickBadData('Malformed ping message');
            }
          } else if (clientMessage.channel == LOBBY_CHANNEL) {
            this.lobbyChannel.onReceive(clientMessage as ClientMessage<typeof LOBBY_CHANNEL, any>, session);
          } else if (clientMessage.channel.startsWith('classroom')) {
            const maybeClassroom = session.classId.mapNullable(id => this.classrooms.get(id));
            maybeClassroom.ifSome(classroom => {
              classroom.onMessage(clientMessage, session);
            });
          } else {
            session.kickBadData(`Invalid message format. Unexpected channel '${clientMessage.channel}'.`);
          }
        },
        None: () => {
          session.kickBadData('Invalid message format. Message is missing a channel.');
        }
      });
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.log(e);
        console.log("Message was: %s", rawMessage);
        session.kickBadData('Expected JSON, failed to parse');
      } else {
        throw e;
      }
    }
  }

  userInfo(id: string): UserInfo {
    const connection = this.sessions.get(id);
    if (connection != null) {
      return connection.user.toInfo();
    } else {
      throw Error('Tried to get info for disconnected user');
    }
  }

  private currentUsersWaiting(userId: string): UserInfo[] {
    let usersWaiting : UserInfo[] = [];
    for (let id of this.lobby) {
      if (id != userId) {
        usersWaiting.push(this.userInfo(id));
      }
    }
    return usersWaiting
  }

  private currentActiveClassrooms() {
    return Array.from(this.classrooms.values()).map(c => c.toInfo(u => this.userInfo(u)));
  }

  private tutorWelcomeInfo(userId: string): TutorWelcomeToLobbyInfo {
    const classrooms = this.currentActiveClassrooms();
    const usersWaiting = this.currentUsersWaiting(userId);
    return { usersWaiting, classrooms };
  }

  private tuteeWelcomeInfo(userId: string): TuteeWelcomeToLobbyInfo {
    return {};
  }
}

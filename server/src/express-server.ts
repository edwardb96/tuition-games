import express, { Response, Request, urlencoded } from "express";
import path from "path";
import { makeRestRoutes } from "./routes/rest";
import { makeAuthRoutes } from "./routes/auth";
import { ExpressAuthenticator, isOidcResponse } from "./authentication/express-authenticator";
import { WebsocketTokenManager } from "./authentication/websocket-token-manager";
import { CardPairsStore } from "./stores/card-pairs";
import { UserEditStore } from "./stores/user-edit";
import { ImageStore } from "./stores/images";
import cookieParser from "cookie-parser";
import { TuteeTokenManager } from "./authentication/tutee-token-manager";
import { UserStore } from "./stores/user";
import { BoardsStore } from "./stores/boards";

export const makeExpressApp = (distDir: string,
                               auth: ExpressAuthenticator,
                               wsTokenManager: WebsocketTokenManager,
                               tuteeTokenManager: TuteeTokenManager,
                               userStore: UserStore,
                               userEditsStore: UserEditStore,
                               imageStore: ImageStore,
                               cardPairStore: CardPairsStore,
                               boardsStore: BoardsStore) => {

  const authRouter = makeAuthRoutes().getOrThrow('Auth0 environment variables are not set properly!');

  const sendDist = (relativePath: string) => (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, distDir, relativePath));
  }

  const sendSpa = (req: Request, res: Response) => {
    console.log(`Got request for ${req.originalUrl}`);
    sendDist('index.html')(req, res);
  };

  const app = express();
  app.set('trust proxy', 1)


  app.use(authRouter);

  app.get('/splash', sendSpa);

  app.get('/', [cookieParser(), auth.hasAuthOrToken()], sendSpa);
  app.use('/api',
    makeRestRoutes(
      wsTokenManager,
      tuteeTokenManager,
      auth,
      userStore,
      userEditsStore,
      imageStore,
      cardPairStore,
      boardsStore));

  app.get('/unverified-email', [auth.hasAuth()], sendSpa);

  app.get('/login', urlencoded({ extended: false }),
    (req: Request, res: Response) => {
      if (isOidcResponse(res)) {
        res.clearCookie('tuteeSession');
        res.oidc.login({ returnTo: process.env['BASE_URL'] });
      }
    });

  app.get('/logout', (req: Request, res: Response) => {
    res.clearCookie('tuteeSession');
    if (isOidcResponse(res)) {
      res.oidc.logout();
    }
  });

  app.get('/invitation', async (req: Request, res: Response) => {
    console.log("Got request for /invitation")
    const tuteeToken = req.query['token'];
    if (tuteeToken != null && typeof tuteeToken === 'string') {
      await tuteeTokenManager.authenticateTuteeToken(
        tuteeToken,
        (token, user) => {
          const expiryTime = new Date(token.exp * 1000);
          //console.log(`expires at ${expiryTime.toLocaleTimeString()}`)
          res.cookie('tuteeSession', tuteeToken, { expires: expiryTime });
          res.redirect(`/classroom/${token.classroom}`)
        },
        error => {
          res.sendStatus(401);
        });
    } else {
      res.sendStatus(401)
    }
  });

  app.use(express.static(path.join(__dirname, distDir),
    { index: false }));

  app.get('*', [cookieParser(), auth.hasAuthOrToken(), auth.withHtmlUser()], sendSpa);
  return app;
}
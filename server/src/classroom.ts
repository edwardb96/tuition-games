import { Option } from "prelude-ts";

import { User, UserInfo } from "@cat-tuition/protocol/user";
import { ClassroomInfo } from "@cat-tuition/protocol/classroom";
import { ClientMessage } from "@cat-tuition/protocol/message";
import { CLASSROOM_CHANNEL, RecievedChatMessageInfo } from "@cat-tuition/protocol/channels/classroom";
import { NOUGHTS_AND_CROSSES_CHANNEL } from "@cat-tuition/protocol/channels/noughts-and-crosses";
import { PAIRS_CHANNEL } from "@cat-tuition/protocol/channels/pairs";
import { PAIRS_ACTIVITY_ID, validatePairsBeginInfo } from "@cat-tuition/protocol/pairs";
import { NOUGHTS_AND_CROSSES_ACTIVITY_ID, validateNoughtsAndCrossesBeginInfo } from "@cat-tuition/protocol/noughts-and-crosses";
import { ActivityId,
         ActivityBeginInfo,
         TuteeWelcomeToClassroomInfo,
         TutorWelcomeToClassroomInfo } from "@cat-tuition/protocol/channels/classroom"

import { ClassroomChannel, ClassroomState } from "./channels/classroom";
import { SessionStore, UserSession } from "./session";
import { Activity, ActivityServer } from "./channels/activity";
import { NoughtsAndCrossesActivity } from "./channels/activities/noughts-and-crosses";
import { PairsActivity } from "./channels/activities/pairs";
import { BOARD_GAME_CHANNEL } from "@cat-tuition/protocol/channels/board-game";
import { BoardGameActivity } from "./channels/activities/board-game";
import { BOARD_GAME_ACTIVITY_ID, validateBoardGameBeginInfo } from "@cat-tuition/protocol/board-game";


export class Classroom implements ClassroomState {
  tutor: string;
  invitedTutees: Set<string>;
  tutees: Set<string>;
  classroomChannel: ClassroomChannel;
  sessions: SessionStore;
  currentActivity: Option<ClassroomActivity>;

  constructor(tutor: UserSession, sessions: SessionStore) {
    this.tutor = tutor.user.id;
    this.invitedTutees = new Set<string>();
    this.tutees = new Set<string>();
    this.sessions = sessions;
    this.classroomChannel = new ClassroomChannel(this);
    this.currentActivity = Option.none();
    this.classroomChannel.sendTutorEntered(tutor, this.tutorWelcomeInfo())
  }

  get id() {
    return this.tutor;
  }

  toInfo(userInfo: (id: string) => UserInfo): ClassroomInfo {
    const tutor = userInfo(this.tutor);
    const tutees = Array.from(this.tutees).map(userInfo);
    return { tutor, tutees }
  }

  tutorWelcomeInfo() : TutorWelcomeToClassroomInfo {
    return {
      ...this.toInfo(u => this.sessions.userInfo(u)),
      activityId: this.currentActivity.map(a => a.id).getOrNull()
    };
  }

  tuteeWelcomeInfo() : TuteeWelcomeToClassroomInfo {
    return {
      ...this.toInfo(u => this.sessions.userInfo(u)),
      activityId: this.currentActivity.map(a => a.id).getOrNull()
    };
  }

  *tuteesSessionsExcept(userId: string): Iterable<UserSession> {
    for (let tutee of this.tutees) {
      if (tutee != userId) {
        const tuteeSession = this.sessions.sessionForUser(tutee);
        if (tuteeSession.isSome()) {
          yield tuteeSession.get();
        }
      }
    }
  }

  *tuteeSessions() : Iterable<UserSession> {
    for (let tutee of this.tutees) {
      const tuteeSession = this.sessions.sessionForUser(tutee);
      if (tuteeSession.isSome()) {
        yield tuteeSession.get();
      }
    }
  }

  *classroomOccupantSessions(): Iterable<UserSession> {
    yield this.tutorSession();
    yield* this.tuteeSessions();
  }

  private tutorSession() : UserSession {
    return this.sessions.sessionForUser(this.tutor)
      .getOrThrow('Tutor disconnected without ending class?');
  }

  inviteTutee(newTutee: UserSession) {
    this.invitedTutees.add(newTutee.id);
  }

  addTutee(newTutee: UserSession): boolean {
    if (!this.tutees.has(newTutee.user.id)) {
      if (this.invitedTutees.has(newTutee.user.id) || newTutee.user.canJoinClassroomUninvited(this.id)) {
        this.tutees.add(newTutee.user.id);
        newTutee.classId = Option.of(this.id);
        // Welcome the tutee
        this.classroomChannel.sendTuteeEntered(newTutee, this.tuteeWelcomeInfo())

        // Tell the tutor
        this.classroomChannel.sendTuteeAdded(this.tutorSession(), newTutee.user.toInfo());

        // Tell other tutees
        for (let tutee of this.tuteesSessionsExcept(newTutee.id)) {
          this.classroomChannel.sendTuteeAdded(tutee, newTutee.user.toInfo());
        }

        // Add to current activity
        this.currentActivity.ifSome(
          activity => activity.userJoined(newTutee.user.id))

        return true;
      } else {
        newTutee.kickBadPermission();
        return false;
      }
    } else {
      return false;
    }
  }

  removeTutee(exTutee: UserSession) {
    const exTuteeId = exTutee.user.id
    if (this.tutees.has(exTuteeId)) {
      this.tutees.delete(exTuteeId);
      exTutee.classId = Option.none();

      console.log("%s is leaving, current activity is %s", exTuteeId, this.currentActivity);
      this.currentActivity.ifSome(
        activity => activity.userLeft(exTuteeId))

      // Tell the tutor
      this.classroomChannel.sendTuteeRemoved(this.tutorSession(), exTuteeId);

      // Tell other tutees
      for (let tutee of this.tuteeSessions()) {
        this.classroomChannel.sendTuteeRemoved(tutee, exTuteeId);
      }
      return true;
    } else {
      return false;
    }
  }

  receivedChatMessage(session: UserSession, chatMessage: string): void {
    const senderId = session.user.id;
    const recievedAt = new Date();
    const message = { chatMessage, senderId, recievedAt };

    for (let tutee of this.tuteesSessionsExcept(senderId)) {
      this.classroomChannel.sendChatMessage(tutee, message);
    }

    if (session.user.id != this.tutor) {
      const tutorSession = this.sessions.sessionForUser(this.tutor)
                                        .getOrThrow('Tutor not present in own class');
      this.classroomChannel.sendChatMessage(tutorSession, message);
    }
  }

  notifyEnded() {
    const tutorSession = this.tutorSession();
    tutorSession.classId = Option.none();

    // Tell the tutees
    for (let tutee of this.tuteeSessions()) {
      this.classroomChannel.sendClassEnded(tutee);
      tutee.classId = Option.none();
    }
  }

  notifyActivityChanged(newActivity: Option<ClassroomActivity>) {
    for (let occupant of this.classroomOccupantSessions()) {
      this.classroomChannel.sendActivityChanged(occupant, newActivity.map(a => a.id).getOrNull());
    }
  }

  changeActivity(session: UserSession, info: ActivityBeginInfo): void {
    if (session.user.id == this.tutor) {
      if (info.activityId != null) {
        const newActivity = ClassroomActivity.tryMake(info, {
          classroomOccupants: () => this.classroomOccupantSessions(),
          send: (userId, payload) =>
            this.sessions.sessionForUser(userId)
              .getOrThrow(`Activity ${info.activityId} tried to contact user ${userId} when they weren't in the classroom`)
              .send(payload)
        });
        if (newActivity.isSome()) {
          this.currentActivity = newActivity;
          this.notifyActivityChanged(newActivity);
          this.currentActivity.get().sendBegin();
        } else {
          session.kickBadData('Activity does not exist or else the wrong begin info was provided');
        }
      } else {
        this.currentActivity = Option.none();
        this.notifyActivityChanged(Option.none())
      }
    } else {
      session.kickBadPermission();
    }
  }

  onMessage(clientMessage: ClientMessage<string, any>, session: UserSession): void {
    if (clientMessage.channel == CLASSROOM_CHANNEL) {
      const classroomMessage = clientMessage as ClientMessage<typeof CLASSROOM_CHANNEL, any>;
      this.classroomChannel.onReceive(classroomMessage, session);
    } else {
      this.currentActivity.map(ca => {
        if (ca.channel === clientMessage.channel) {
          ca.onReceive(clientMessage as ClientMessage<typeof ca.channel, any>, session);
        }
      });
    }
  }
}

type ClassroomActivityChannel = typeof NOUGHTS_AND_CROSSES_CHANNEL | typeof PAIRS_CHANNEL | typeof BOARD_GAME_CHANNEL;
export type ClassroomActivitiesUnion = NoughtsAndCrossesActivity | PairsActivity | BoardGameActivity;

class ClassroomActivity implements Activity<ActivityId, ClassroomActivityChannel> {
  private activity: ClassroomActivitiesUnion;

  private constructor(activity: ClassroomActivitiesUnion) {
    this.activity = activity;
  }

  get id() { return this.activity.id };

  get channel() { return this.activity.channel };

  userLeft(userId: string): void {
    this.activity.userLeft(userId);
  }

  userJoined(userId: string): void {
    this.activity.userJoined(userId);
  }

  sendBegin(): void {
    this.activity.sendBegin();
  }

  onReceive(clientMessage: ClientMessage<ClassroomActivityChannel, any>, session: UserSession): void {
    if (clientMessage.channel == this.channel) {
      (this.activity as unknown as Activity<string, string>).onReceive(clientMessage, session);
    }
  }

  static tryMake(beginInfo: ActivityBeginInfo,
                 activityServer: ActivityServer<string, any>): Option<ClassroomActivity> {
    if (beginInfo.activityId != null) {
      switch (beginInfo.activityId) {
        case NOUGHTS_AND_CROSSES_ACTIVITY_ID:
          return validateNoughtsAndCrossesBeginInfo(beginInfo.info)
            .map(info => new ClassroomActivity(new NoughtsAndCrossesActivity(activityServer, info)));
        case PAIRS_ACTIVITY_ID:
          return validatePairsBeginInfo(beginInfo.info)
            .map(info => new ClassroomActivity(new PairsActivity(activityServer, info)));
        case BOARD_GAME_ACTIVITY_ID:
          return validateBoardGameBeginInfo(beginInfo.info)
            .map(info => new ClassroomActivity(new BoardGameActivity(activityServer, info)));
      }
    } else {
      return Option.none();
    }
  }
}
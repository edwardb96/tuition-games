import { ClientMessage } from "@cat-tuition/protocol/message";
import { UserSession } from "./session";

export interface Channel<Tag = string> {
  onReceive: (clientMessage: ClientMessage<Tag, any>,
              session: UserSession) => void
}
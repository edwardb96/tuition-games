import { Card, CardPair, CardPairId, isImageCard, matchCard, NewCardPair, UpdatedCardPair } from "@cat-tuition/protocol/pairs";
import { CardPairsStore } from "../card-pairs";
import { Firestore, FirestoreDataConverter, QueryDocumentSnapshot, FieldValue, Transaction, WriteBatch } from "@google-cloud/firestore";

interface FirestoreCardPairInfo {
  firstCard: FirestoreCardInfo;
  secondCard: FirestoreCardInfo;
  categories: string[];
}

interface FirestoreCardInfo {
  text: string | null;
  image: string | null;
  description: string | null;
}

//const makeKeywords = (text: string): string[] => {
//  const lowercaseText = text.toLowerCase();
//  let keywords: string[] = new Array(lowercaseText.length - 1);
//  for (let i = 1; i < lowercaseText.length; i++) {
//    keywords.push(lowercaseText.substring(0, i))
//  }
//  return keywords;
//};

const cardToFirestore = (c: Card): FirestoreCardInfo =>
  matchCard(c, {
    text: ({ text }): FirestoreCardInfo => ({ text, image: null, description: null }),
    image: ({ image, description }): FirestoreCardInfo => ({ text: null, image, description })
  });

const firestoreToCard = (c: FirestoreCardInfo): Card => {
  const { image, text, description } = c;
  if (image != null && description != null) {
    return { image, description };
  } else if (text != null) {
    return { text }
  } else {
    throw new Error("Invalid card in database");
  }
};

const newCardPairToFirestore = ({ firstCard, secondCard, categories }: NewCardPair) => ({
  firstCard: cardToFirestore(firstCard),
  secondCard: cardToFirestore(secondCard),
  categories
});

const newCardPairConverter: FirestoreDataConverter<NewCardPair> = {
  toFirestore: (pair: CardPair) => newCardPairToFirestore(pair),
  fromFirestore: (snapshot: QueryDocumentSnapshot<FirestoreCardPairInfo>) => {
    const { firstCard, secondCard, categories } = snapshot.data()!;
    return {
      id: snapshot.id,
      firstCard: firestoreToCard(firstCard),
      secondCard: firestoreToCard(secondCard),
      categories
    }
  }
};

const cardPairConverter: FirestoreDataConverter<CardPair> = {
  toFirestore: (pair: CardPair) => newCardPairToFirestore(pair),
  fromFirestore: (snapshot: QueryDocumentSnapshot<FirestoreCardPairInfo>) => {
    const { firstCard, secondCard, categories } = snapshot.data()!;
    return {
      id: snapshot.id,
      firstCard: firestoreToCard(firstCard),
      secondCard: firestoreToCard(secondCard),
      categories
    }
  }
};

export class FirestoreCardPairsStore implements CardPairsStore {
  private connection: Firestore;
  private cardPairsCollectionName: string;
  private imageCollectionName: string;

  constructor(connection: Firestore, cardPairsCollectionName: string, imageCollectionName: string) {
    this.connection = connection;
    this.cardPairsCollectionName = cardPairsCollectionName;
    this.imageCollectionName = imageCollectionName;
  }

  private cardPairsCollection() {
    return this.connection.collection(this.cardPairsCollectionName);
  }

  private imageCollection() {
    return this.connection.collection(this.imageCollectionName);
  }

  async createCardPair(pair: NewCardPair): Promise<CardPairId> {
    const createdDoc = this.cardPairsCollection()
                           .withConverter(newCardPairConverter)
                           .doc();

    const batch = this.connection.batch();
    batch.create(createdDoc, pair);
    this.useImagesInCardPair(batch, createdDoc.id, pair);
    await batch.commit();
    return createdDoc.id;
  }

  async getCardPair(id: CardPairId): Promise<CardPair | null> {
    const result = await this.cardPairsCollection()
      .doc(id)
      .withConverter(cardPairConverter)
      .get();
    if (result.exists) {
      return result.data()!;
    } else {
      return null;
    }
  }

  async getAllCardPairs(): Promise<CardPair[]> {
    const results = await this.cardPairsCollection()
                              .withConverter(cardPairConverter)
                              .get();
    return results.docs.map(x => x.data()!);
  }

  async createOrUpdateCardPair(id: CardPairId, newCardPair: UpdatedCardPair): Promise<void> {
    await this.connection.runTransaction(async transaction => {
      const cardPairRef = this.cardPairsCollection()
                              .withConverter(newCardPairConverter)
                              .doc(id);
      const oldCardPair = await transaction.get(cardPairRef)
      if (oldCardPair.exists) {
        this.unuseImagesInCardPair(transaction, id, oldCardPair.data()!);
      }
      transaction.set(cardPairRef, newCardPair);
      this.useImagesInCardPair(transaction, id, newCardPair);
    })
  }

  async deleteCardPair(id: CardPairId): Promise<void> {
    await this.connection.runTransaction(async transaction => {
      const cardPairRef = this.cardPairsCollection()
                              .withConverter(cardPairConverter)
                              .doc(id);
      const cardPair = await transaction.get(cardPairRef);
      if (cardPair.exists) {
        this.unuseImagesInCardPair(transaction, id, cardPair.data()!);
      }
      transaction.delete(cardPairRef);
    });
  }

  adjustImageUsedInCards(transaction: Transaction | WriteBatch, card: Card, value: FieldValue) {
    if (isImageCard(card)) {
      const cardImageRef = this.imageCollection().doc(card.image);
      transaction.update(cardImageRef, { usedInCards: value });
    }
  }

  useImageInCard(transaction: Transaction | WriteBatch, card: Card, id: CardPairId) {
    this.adjustImageUsedInCards(transaction, card, FieldValue.arrayUnion(id))
  }

  unuseImageInCard(transaction: Transaction | WriteBatch, card: Card, id: CardPairId) {
    this.adjustImageUsedInCards(transaction, card, FieldValue.arrayRemove(id))
  }

  unuseImagesInCardPair(transaction: Transaction | WriteBatch, id: CardPairId, cardPair: UpdatedCardPair) {
    const { firstCard, secondCard } = cardPair;
    this.unuseImageInCard(transaction, firstCard, id);
    this.unuseImageInCard(transaction, secondCard, id);
  }

  useImagesInCardPair(transaction: Transaction | WriteBatch, id: CardPairId, cardPair: UpdatedCardPair) {
    const { firstCard, secondCard } = cardPair;
    this.useImageInCard(transaction, firstCard, id);
    this.useImageInCard(transaction, secondCard, id);
  }
}
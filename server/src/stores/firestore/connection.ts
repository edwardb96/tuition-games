import { Firestore } from "@google-cloud/firestore";
import { pemFromEnv } from "../../keys";
import { Option } from "prelude-ts";

export const makeFirestoreConnection = (): Option<Firestore> => {
  if (process.env.FIRESTORE_PROJECT_ID != null &&
    process.env.FIRESTORE_CLIENT_EMAIL != null &&
    process.env.FIRESTORE_PRIVATE_KEY != null) {
    return Option.of(new Firestore({
      //kind: 'express-sessions',
      projectId: process.env.FIRESTORE_PROJECT_ID,
      ssl: true,
      credentials: {
        client_email: process.env.FIRESTORE_CLIENT_EMAIL,
        private_key: pemFromEnv(process.env.FIRESTORE_PRIVATE_KEY),
      },
    }));
  } else {
    return Option.none();
  }
}
import { Board, BoardId, NewBoard, UpdatedBoard } from "@cat-tuition/protocol/board-game";
import { BoardsStore } from "../boards";
import { Firestore, FirestoreDataConverter, QueryDocumentSnapshot, FieldValue, Transaction, WriteBatch } from "@google-cloud/firestore";

interface FirestoreBoardInfo {
  title: string;
  image: string;
  counterSize: number;
  categories: string[];
}

const newBoardToFirestore = ({ title, image, counterSize, categories }: NewBoard) =>
  ({ title, image, counterSize, categories });

const boardConverter: FirestoreDataConverter<Board> = {
  toFirestore: (board: Board) => newBoardToFirestore(board),
  fromFirestore: (snapshot: QueryDocumentSnapshot<FirestoreBoardInfo>) => {
    const { title, image, counterSize, categories } = snapshot.data()!;
    return { id: snapshot.id, title, image, counterSize, categories }
  }
};

export class FirestoreBoardsStore implements BoardsStore {
  private connection: Firestore;
  private boardsCollectionName: string;
  private imageCollectionName: string;

  constructor(connection: Firestore, boardsCollectionName: string, imageCollectionName: string) {
    this.connection = connection;
    this.boardsCollectionName = boardsCollectionName;
    this.imageCollectionName = imageCollectionName;
  }

  private boardsCollection() {
    return this.connection.collection(this.boardsCollectionName);
  }

  private imageCollection() {
    return this.connection.collection(this.imageCollectionName);
  }

  async createBoard(board: NewBoard): Promise<BoardId> {
    const createdDoc = this.boardsCollection()
                           .withConverter(boardConverter)
                           .doc();

    const batch = this.connection.batch();
    batch.create(createdDoc, board);
    this.useImageInBoard(batch, createdDoc.id, board);
    await batch.commit();
    return createdDoc.id;
  }

  async getBoard(id: BoardId): Promise<Board | null> {
    const result = await this.boardsCollection()
      .doc(id)
      .withConverter(boardConverter)
      .get();
    if (result.exists) {
      return result.data()!;
    } else {
      return null;
    }
  }

  async getAllBoards(): Promise<Board[]> {
    const results = await this.boardsCollection()
                              .withConverter(boardConverter)
                              .get();
    return results.docs.map(x => x.data()!);
  }

  async createOrUpdateBoard(id: BoardId, newBoard: UpdatedBoard): Promise<void> {
    await this.connection.runTransaction(async transaction => {
      const boardRef = this.boardsCollection()
                           .withConverter(boardConverter)
                           .doc(id);
      const oldBoard = await transaction.get(boardRef)
      if (oldBoard.exists) {
        this.unuseImageInBoard(transaction, id, oldBoard.data()!);
      }
      transaction.set(boardRef, newBoard);
      this.useImageInBoard(transaction, id, newBoard);
    })
  }

  async deleteBoard(id: BoardId): Promise<void> {
    await this.connection.runTransaction(async transaction => {
      const boardRef = this.boardsCollection()
                           .withConverter(boardConverter)
                           .doc(id);
      const board = await transaction.get(boardRef);
      if (board.exists) {
        this.unuseImageInBoard(transaction, id, board.data()!);
      }
      transaction.delete(boardRef);
    });
  }

  adjustImageUsedInBoard(transaction: Transaction | WriteBatch, board: UpdatedBoard, value: FieldValue) {
    const boardRef = this.imageCollection().doc(board.image);
    transaction.update(boardRef, { usedInCards: value });
  }

  useImageInBoard(transaction: Transaction | WriteBatch, id: BoardId, board: UpdatedBoard) {
    this.adjustImageUsedInBoard(transaction, board, FieldValue.arrayUnion(id))
  }

  unuseImageInBoard(transaction: Transaction | WriteBatch, id: BoardId, board: UpdatedBoard) {
    this.adjustImageUsedInBoard(transaction, board, FieldValue.arrayRemove(id))
  }
}
import { ImageId, ImageMetadata } from "@cat-tuition/protocol/image";
import { ImageStore } from "../images";
import { Firestore, QueryDocumentSnapshot, DocumentData, FieldValue, Timestamp } from "@google-cloud/firestore";

export class FirestoreImageStore implements ImageStore {
  private connection: Firestore;
  private imageCollectionName: string;

  constructor(connection: Firestore, imageCollectionName: string) {
    this.connection = connection;
    this.imageCollectionName = imageCollectionName;
  }

  private imageCollection() {
    return this.connection.collection(this.imageCollectionName);
  }

  async getImageData(id: ImageId): Promise<Buffer | null> {
    const imageDocument = await this.imageCollection()
                                    .doc(id)
                                    .get();
    if (imageDocument.exists) {
      const imageData = imageDocument.get('imageData');
      if (imageData != null) {
        return (imageData as Buffer);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  async getAllUnassignedImages(): Promise<string[]> {
    const queryResults = await this.imageCollection()
                                   .where('usedInCards', '==', [])
                                   .where('usedInBoards', '==', [])
                                   .orderBy('createdAt', 'desc')
                                   .get();
    return queryResults.docs.map((snapshot: QueryDocumentSnapshot<DocumentData>) => snapshot.id)
  }

  async getAllImageMetadatas(): Promise<ImageMetadata[]> {
    const queryResults = await this.imageCollection()
                                   .orderBy('createdAt', 'desc')
                                   .get();
    return queryResults.docs
      .map((snapshot: QueryDocumentSnapshot<DocumentData>) => ({
        id: snapshot.id,
        usedInBoards: snapshot.get('usedInBoards'),
        usedInCards: snapshot.get('usedInCards'),
        createdAt: (snapshot.get('createdAt') as Timestamp).toDate()
      }));
  }

  async getUnassignedImagesAfter(id: ImageId, count: number): Promise<ImageId[] | null> {
    const afterSnapshot = await this.imageCollection().doc(id).get();
    if (afterSnapshot.exists) {
      const queryResults = await this.imageCollection()
                                     .where('usedInCards', '==', [])
                                     .where('usedInBoards', '==', [])
                                     .orderBy('createdAt', 'desc')
                                     .startAfter(afterSnapshot)
                                     .limit(count)
                                     .get();
      return queryResults.docs.map((snapshot: QueryDocumentSnapshot<DocumentData>) => snapshot.id)
    } else {
      return null;
    }
  }

  async getUnassignedImagesBefore(id: ImageId, count: number): Promise<ImageId[] | null> {
    const beforeSnapshot = await this.imageCollection().doc(id).get();
    if (beforeSnapshot.exists) {
      const queryResults = await this.imageCollection()
                                     .where('usedInCards', '==', [])
                                     .where('usedInBoards', '==', [])
                                     .orderBy('createdAt', 'desc')
                                     .endBefore(beforeSnapshot)
                                     .limitToLast(count)
                                     .get();
      return queryResults.docs.map((snapshot: QueryDocumentSnapshot<DocumentData>) => snapshot.id)
    } else {
      return null;
    }
  }

  async getInitialUnassignedImages(count: number): Promise<string[]> {
    const queryResults = await this.imageCollection()
                                   .where('usedInCards', '==', [])
                                   .where('usedInBoards', '==', [])
                                   .orderBy('createdAt', 'desc')
                                   .limit(count)
                                   .get();
    return queryResults.docs.map((snapshot: QueryDocumentSnapshot<DocumentData>) => snapshot.id)
  }

  async createImage(buffer: Buffer): Promise<string> {
    const documentRef = await this.imageCollection()
                                  .add({ createdAt: FieldValue.serverTimestamp(),
                                         usedInBoards: [],
                                         usedInCards: [],
                                         imageData: buffer });
    return documentRef.id;
  }

  async deleteImage(id: ImageId): Promise<boolean> {
    const imageRef = this.imageCollection().doc(id);

    return await this.connection.runTransaction(async transaction => {
      const snapshot = await transaction.get(imageRef);
      const usedInBoards = snapshot.get('usedInBoards');
      const usedInCards = snapshot.get('usedInCards');
      if (usedInBoards.length == 0 && usedInCards.length == 0) {
        transaction.delete(imageRef);
        return true;
      } else {
        return false;
      }
    });
  }

  async getImageMetadata(id: ImageId): Promise<ImageMetadata | null> {
    const snapshot = await this.imageCollection()
                               .doc(id)
                               .get();
    if (snapshot.exists) {
      return {
        id: snapshot.id,
        usedInBoards: snapshot.get('usedInBoards'),
        usedInCards: snapshot.get('usedInCards'),
        createdAt: (snapshot.get('createdAt') as Timestamp).toDate()
      };
    } else {
      return null;
    }
  }
}
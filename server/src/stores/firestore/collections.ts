export const makeCollectionName = (isProduction: boolean, basename: string) => {
  if (!isProduction && basename != 'Users') {
    return `Experimental ${basename}`;
  } else {
    return basename;
  }
}
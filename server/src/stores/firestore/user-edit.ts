import { Role, StoredUser, UpdatedUser, NewUser } from "@cat-tuition/protocol/user";
import { UserEditStore } from "../user-edit";
import { Firestore, FirestoreDataConverter, QueryDocumentSnapshot, Transaction } from "@google-cloud/firestore";
import { SimpleColour } from "@cat-tuition/protocol/colour";

interface FirestoreUserInfo {
  id: string;
  displayName: string;
  email: string;
  favouriteColour: string;
  role: keyof typeof Role;
}

const userEditsConverter: FirestoreDataConverter<StoredUser | UpdatedUser> = ({
  toFirestore: (user: UpdatedUser | StoredUser) => ({
    displayName: user.displayName,
    email: user.email,
    favouriteColour: user.favouriteColour,
    role: Role[user.role]
  }),
  fromFirestore: (snapshot: QueryDocumentSnapshot<FirestoreUserInfo>) => {
    const data = snapshot.data()!;
    return {
      id: snapshot.id,
      displayName: data.displayName,
      email: data.email,
      favouriteColour: data.favouriteColour as SimpleColour,
      role: Role[data.role],
    };
  }
});


export class FirestoreUserEditStore implements UserEditStore {
  private connection: Firestore;
  private usersCollectionName: string;

  constructor(connection: Firestore, usersCollectionName: string) {
    this.connection = connection;
    this.usersCollectionName = usersCollectionName;
  }

  private usersCollection() {
    return this.connection.collection(this.usersCollectionName);
  }

  async getAllUserEdits(): Promise<StoredUser[]> {
    const results = await this.usersCollection()
                              .withConverter(userEditsConverter)
                              .get();
    return results.docs.map(x => (x.data()! as StoredUser));
  }

  async deleteUser(id: string): Promise<void> {
    await this.usersCollection()
              .doc(id)
              .delete();
  }

  async createUser(newEdit: NewUser): Promise<boolean> {
    try {
      await this.usersCollection()
                .doc(newEdit.id)
                .withConverter(userEditsConverter)
                .create(newEdit);
      return true;
    } catch (err: any) {
      if (err != null && err.hasOwnProperty('code') && err.code == 6) {
        return false;
      } else {
        throw err;
      }
    }
  }

  // PUT /users { id: 'edward-brown', ... } does creation or update, idempotent but can't change id.
  // just restrict id changes to new items

  async createOrUpdateUser(id: string, newEdit: UpdatedUser): Promise<void> {
    await this.usersCollection()
              .doc(id)
              .withConverter(userEditsConverter)
              .set(newEdit)
  }
}
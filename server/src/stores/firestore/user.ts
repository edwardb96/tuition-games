import { UserInfo, Role, PrivateUserInfo } from "@cat-tuition/protocol/user";
import { UserStore } from "../user";
import { Firestore, FirestoreDataConverter, QueryDocumentSnapshot } from "@google-cloud/firestore";
import { SimpleColour } from "@cat-tuition/protocol/colour";

interface FirestoreUserInfo {
  id: string;
  email: string | null;
  displayName: string;
  role: keyof typeof Role;
  favouriteColour: string;
}

const userConverter: FirestoreDataConverter<UserInfo & PrivateUserInfo> = {
  toFirestore: (user: UserInfo & PrivateUserInfo) => ({
    id: user.id,
    email: user.email,
    displayName: user.displayName,
    role: Role[user.role],
    favouriteColour: user.favouriteColour
  }),
  fromFirestore: (snapshot: QueryDocumentSnapshot<FirestoreUserInfo>) => {
    const data = snapshot.data()!;
    return {
      id: snapshot.id,
      displayName: data.displayName,
      favouriteColour: data.favouriteColour as SimpleColour,
      role: Role[data.role],
      email: data.email
    };
  }
};

export class FirestoreUserStore implements UserStore {
  private connection: Firestore;
  private usersCollectionName: string;

  constructor(connection: Firestore, usersCollectionName: string) {
    this.connection = connection;
    this.usersCollectionName = usersCollectionName;
  }

  private usersCollection() {
    return this.connection.collection(this.usersCollectionName);
  }

  async findUserByEmail(email: string): Promise<UserInfo & PrivateUserInfo | null> {
    const results = await this.usersCollection().where('email', '==', email)
      .withConverter(userConverter)
      .get();
    if (results.size > 0) {
      return results.docs[0].data()!;
    } else {
      return null;
    }
  }

  async findUserById(id: string): Promise<UserInfo & PrivateUserInfo | null> {
    const result = await this.usersCollection().doc(id)
      .withConverter(userConverter)
      .get();
    if (result.exists) {
      return result.data()!;
    } else {
      return null;
    }
  }
}
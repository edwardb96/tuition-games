import { Board, BoardId, NewBoard, UpdatedBoard } from "@cat-tuition/protocol/board-game";

export interface BoardsStore {
  getBoard(id: BoardId): Promise<Board | null>;
  getAllBoards(): Promise<Board[]>;
  createBoard(board: NewBoard): Promise<BoardId>;
  createOrUpdateBoard(id: BoardId, pair: UpdatedBoard): Promise<void>;
  deleteBoard(id: BoardId): Promise<void>;
}
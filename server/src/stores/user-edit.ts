import { NewUser, StoredUser, UpdatedUser } from "@cat-tuition/protocol/user";

export interface UserEditStore {
  createUser(user: NewUser): Promise<boolean>;
  createOrUpdateUser(id: string, user: UpdatedUser): Promise<void>;
  deleteUser(id: string): Promise<void>;
  getAllUserEdits(): Promise<StoredUser[]>;
}
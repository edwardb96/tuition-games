import { PrivateUserInfo, UserInfo } from "@cat-tuition/protocol/user";

export interface UserStore {
  findUserByEmail(email: string): Promise<UserInfo & PrivateUserInfo | null>;
  findUserById(id: string): Promise<UserInfo & PrivateUserInfo | null>;
}
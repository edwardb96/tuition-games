import { ImageId, ImageMetadata } from "@cat-tuition/protocol/image";

export interface ImageStore {
  createImage(image: Buffer): Promise<ImageId>;
  deleteImage(id: ImageId): Promise<boolean>;
  getImageMetadata(id: ImageId): Promise<ImageMetadata | null>;
  getImageData(id: ImageId): Promise<Buffer | null>;
  getAllUnassignedImages(): Promise<ImageId[]>;
  getAllImageMetadatas(): Promise<ImageMetadata[]>;
  getUnassignedImagesAfter(id: ImageId, count: number): Promise<ImageId[] | null>;
  getUnassignedImagesBefore(id: ImageId, count: number): Promise<ImageId[] | null>;
  getInitialUnassignedImages(count: number): Promise<ImageId[]>;
}
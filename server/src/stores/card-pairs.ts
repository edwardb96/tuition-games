import { CardPair, CardPairId, NewCardPair, UpdatedCardPair } from "@cat-tuition/protocol/pairs";

export interface CardPairsStore {
  getCardPair(id: CardPairId): Promise<CardPair | null>;
  getAllCardPairs(): Promise<CardPair[]>;
  createCardPair(pair: NewCardPair): Promise<CardPairId>;
  createOrUpdateCardPair(id: CardPairId, pair: UpdatedCardPair): Promise<void>;
  deleteCardPair(id: CardPairId): Promise<void>;
}
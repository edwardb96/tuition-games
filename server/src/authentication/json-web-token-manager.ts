import { PrivateUserInfo, User, UserInfo } from "@cat-tuition/protocol/user";
import { WebsocketAuthToken, validateWebsocketAuthToken } from "@cat-tuition/protocol/websocket-token";
import { sign, verify } from "jsonwebtoken";
import { Option, Either } from "prelude-ts";
import { UserStore } from "../stores/user";
import { WebsocketTokenManager, TokenCredentials } from "./websocket-token-manager";
import { TuteeTokenManager } from "./tutee-token-manager";
import { TuteeAuthToken, validateTuteeAuthToken, WithExpiry } from "@cat-tuition/protocol/tutee-token";

const WEBSOCKET_SESSION_SUBJECT = 'websocket-session';
const TUTEE_SESSION_SUBJECT = 'tutee-session';

export class JsonWebTokenManager implements WebsocketTokenManager, TuteeTokenManager {
  private credentials: TokenCredentials;
  private userStore: UserStore;

  constructor(credentials: TokenCredentials, userStore: UserStore) {
    this.credentials = credentials;
    this.userStore = userStore;
  }

  sign(payload: object, subject: string, expiresIn: string) {
    return sign(payload, this.credentials.privateKey, { expiresIn, subject, algorithm: 'ES256' });
  }

  verifyToken<T>(token: string, subject: string, validate: (payload: unknown) => Option<T>): Either<unknown, T> {
    try {
      const rawTokenPayload = verify(token, this.credentials.publicKey, { algorithms: ['ES256'], subject });
      return validate(rawTokenPayload).match({
        Some: tokenPayload => Either.right(tokenPayload),
        None: () => Either.left(new Error('Authentication token has invalid payload'))
      });
    } catch (err) {
      console.log("Caught from verify");
      return Either.left(err);
    }
  }

  makeWebsocketToken(tokenPayload: WebsocketAuthToken): string {
    return this.sign(tokenPayload, WEBSOCKET_SESSION_SUBJECT, '10s');
  }

  async authenticateWebsocketToken(
    token: string, onSuccess: (user: User) => void, onFailure: (error: unknown) => void) {
    this.verifyToken(token, WEBSOCKET_SESSION_SUBJECT, validateWebsocketAuthToken).match({
      Left: onFailure,
      Right: async token => {
        const user = await this.userStore.findUserById(token.id);
        if (user != null) {
          onSuccess(new User(user, null, token.classroom));
        } else {
          onFailure(new Error('User does not exist with that id.'));
        }
      }
    });
  }

  makeTuteeToken(tokenPayload: TuteeAuthToken): string {
    return this.sign(tokenPayload, TUTEE_SESSION_SUBJECT, '1.5 hrs');
  }

  async authenticateTuteeToken(token: string, onSuccess: (token: TuteeAuthToken & WithExpiry, user: UserInfo & PrivateUserInfo) => void, onFailure: (error: unknown) => void) {
    this.verifyToken(token, TUTEE_SESSION_SUBJECT, validateTuteeAuthToken).match({
      Left: onFailure,
      Right: async token => {
        const user = await this.userStore.findUserById(token.id);
        if (user != null) {
          onSuccess(token, user);
        } else {
          onFailure(new Error('User does not exist with that id.'));
        }
      }
    });
  }
}
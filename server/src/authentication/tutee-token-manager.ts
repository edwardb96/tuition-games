import { PrivateUserInfo, UserInfo } from "@cat-tuition/protocol/user";
import { TuteeAuthToken, WithExpiry } from "@cat-tuition/protocol/tutee-token";

export interface TuteeTokenManager {
  makeTuteeToken(tokenPayload: TuteeAuthToken): string;
  authenticateTuteeToken(
    token: string,
    onSucess: (token: TuteeAuthToken & WithExpiry, user: UserInfo & PrivateUserInfo) => void,
    onFailure: (error: unknown) => void): Promise<void>;
}
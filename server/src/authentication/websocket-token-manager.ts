import { User } from "@cat-tuition/protocol/user";
import { WebsocketAuthToken } from "@cat-tuition/protocol/websocket-token";

export interface TokenCredentials {
  privateKey: string;
  publicKey: string;
}

export interface WebsocketTokenManager {
  makeWebsocketToken(tokenPayload: WebsocketAuthToken): string;
  authenticateWebsocketToken(
    token: string,
    onSucess: (user: User) => void,
    onFailure: (error: unknown) => void): Promise<void>;
}
import { UserStore } from "../stores/user";
import { requiresAuth } from "express-openid-connect";
import { NextFunction, Response, Request } from "express";
import { User } from "@cat-tuition/protocol/user";
import { TuteeTokenManager } from "./tutee-token-manager";
import { Either } from "prelude-ts";

export interface WithTokenUser {
  tokenUser: User;
}

export type RequestWithUser = Request & WithTokenUser;

enum NoUserReason {
  UnverifiedEmail,
  NoCredentials,
  NoSuchUser
}

export const isOidcRequest = (req: Request) =>
  req.oidc != null;
export const isOidcResponse = (res: Response): res is Response =>
  res.oidc != null;

export const isTokenRequest = (req: Request): req is Request & WithTokenUser =>
  req.hasOwnProperty('tokenUser') && (req as Request & WithTokenUser).tokenUser != null

const userFromRequest = async (req: RequestWithUser, userStore: UserStore): Promise<Either<[NoUserReason, string | null], User>> => {
  if (isOidcRequest(req)) {
    const auth0User = req.oidc.user as any;
    if (auth0User != null) {
      if (auth0User.email != null && auth0User.email_verified) {
        const user = await userStore.findUserByEmail(auth0User.email);
        if (user != null) {
          return Either.right(new User(user, user.email));
        } else {
          return Either.left([NoUserReason.NoSuchUser, null]);
        }
      } else {
        return Either.left([NoUserReason.UnverifiedEmail, auth0User.email]);
      }
    } else {
      if (isTokenRequest(req)) {
        return Either.right(req.tokenUser);
      } else {
        return Either.left([NoUserReason.NoCredentials, null]);
      }
    }
  } else if (isTokenRequest(req)) {
    return Either.right(req.tokenUser);
  } else {
    return Either.left([NoUserReason.NoCredentials, null]);
  }
}

export class ExpressAuthenticator {
  private userStore: UserStore;
  private tuteeTokenManager: TuteeTokenManager;

  constructor(userStore: UserStore, tuteeTokenManager: TuteeTokenManager) {
    this.userStore = userStore;
    this.tuteeTokenManager = tuteeTokenManager;
  }

  async authenticate(req: RequestWithUser, res: Response, onSuccess: (user: User) => void, forHtml: boolean = false) {
    const unauthorized = (res: Response) => res.sendStatus(401);
    const userOrError = await userFromRequest(req, this.userStore);
    userOrError.match({
      Left: ([error, email]) => {
        switch (error) {
          case NoUserReason.NoSuchUser:
          case NoUserReason.NoCredentials:
            unauthorized(res);
            break;
          case NoUserReason.UnverifiedEmail:
            if (forHtml) {
              res.redirect(`/unverified-email?email='${email}'`);
            } else {
              unauthorized(res);
            }
            break;
        }
      },
      Right: onSuccess
    });
  }

  withHtmlUser(pred?: (user: User) => boolean) {
    return async (req: Request, res: Response, next: NextFunction) => {
      await this.authenticate(req as RequestWithUser, res, user => {
        if (!pred || pred(user)) {
          next();
        } else {
          res.sendStatus(403);
        }
      }, true);
    }
  }

  hasAuthOrToken() {
    return async (req: Request, res: Response, next: NextFunction) => {
      const tuteeToken = req.cookies['tuteeSession'];
      if (tuteeToken != null) {
        await this.tuteeTokenManager.authenticateTuteeToken(tuteeToken,
          (token, user) => {
            (req as Request & WithTokenUser).tokenUser = new User(user, user.email, token.classroom);
            next();
          },
          _ => {
            res.sendStatus(401);
          });
      } else {
        return this.hasAuth()(req, res, next);
      }
    }
  }

  hasAuth() {
    return requiresAuth();
  }

  withUser(pred?: (user: User) => boolean) {
    return async (req: Request, res: Response, next: NextFunction) => {
      await this.authenticate(req as RequestWithUser, res, user => {
        if (!pred || pred(user)) {
          next();
        } else {
          res.sendStatus(403);
        }
      });
    }
  }
}
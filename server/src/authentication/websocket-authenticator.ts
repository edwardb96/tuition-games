import { User } from "@cat-tuition/protocol/user";
import { IncomingMessage } from "http";
import { Socket } from "net";
import url from "url";
import querystring from "querystring";
import { WebsocketTokenManager } from "./websocket-token-manager";

export class WebSocketAuthenticator {
  private tokenManager: WebsocketTokenManager;

  constructor(tokenManager: WebsocketTokenManager) {
    this.tokenManager = tokenManager;
  }

  async authenticate(req: IncomingMessage, socket: Socket, onSuccess: (user: User) => void) {
    const token = tokenFromRequest(req);
    if (token != null) {
      await this.tokenManager.authenticateWebsocketToken(token, onSuccess, err => {
        socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
        socket.destroy();
      });
    } else {
      socket.write('HTTP/1.1 403 Forbidden\r\n\r\n');
      socket.destroy();
    }
  }
}

const tokenFromRequest = (req: IncomingMessage): string | null => {
  const queryParams = queryParamsFromRequest(req);
  if (queryParams != null && typeof queryParams.token == 'string') {
    return queryParams.token;
  } else {
    return null;
  }
}

const queryParamsFromRequest = (req: IncomingMessage) => {
  if (req.url != null) {
    const query = url.parse(req.url).query
    if (query != null) {
      return querystring.parse(query);
    }
  }
  return null;
}
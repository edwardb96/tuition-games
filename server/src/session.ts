import { Option } from "prelude-ts";
import WebSocket from "ws";
import { User, UserInfo } from "@cat-tuition/protocol/user";
import { ServerMessage } from "@cat-tuition/protocol/message";
import { PingPayload, PING_CHANNEL } from "@cat-tuition/protocol/channels/ping";

export interface UserSession {
  readonly id: string;
  readonly user: User;
  readonly wasDuplicate: boolean;
  classId: Option<string>;
  lastEnteredLobby: Date;
  isInOwnClass(): boolean;
  onEnteredLobby(): void;
  kickBadData(message: string): void;
  kickBadPermission(): void;
  closeDuplicate(): void;
  send(message: ServerMessage<string, any>): void
  close(reason: string): void;
}

export interface SessionStore {
  userInfo(userId: string): UserInfo;
  sessionForUser(userId: string): Option<UserSession>;
}

const pingFrequencyInSeconds = 45;
export const pingFrequencyInMiliseconds = 1000 * pingFrequencyInSeconds;
const nonResponsiveThresholdInMiliseconds = pingFrequencyInMiliseconds;
const pingMessage: ServerMessage<typeof PING_CHANNEL, PingPayload> = { channel: 'ping', payload: { kind: 'ping'} };
const pingMessageString = JSON.stringify(pingMessage)

export class PerUserSession implements UserSession {
  user: User;
  socket: WebSocket;
  classId: Option<string>;
  wasDuplicate: boolean;
  lastEnteredLobby: Date;
  private lastPing: number | null;
  private lastPong: number | null;

  public get id() : string {
    return this.user.id;
  }

  constructor(user: User, socket: WebSocket) {
    this.user = user;
    this.socket = socket;
    this.classId = Option.none();
    this.wasDuplicate = false;
    this.lastPing = null;
    this.lastPong = null;
    this.lastEnteredLobby = new Date();
  }

  onEnteredLobby() {
    this.lastEnteredLobby = new Date();
  }

  sendPing() {
    console.log("%s session.sendPing()", this.user.id, new Date().toUTCString());
    this.socket.send(pingMessageString);
    this.lastPing = Date.now();
  }

  gotPong() {
    console.log("%s session.gotPing()", this.user.id, new Date().toUTCString());
    this.lastPong = Date.now();
  }

  isInOwnClass() {
    return this.classId.map(c => c == this.user.id).getOrElse(false);
  }

  isNonResponsive() {
    if (this.lastPing != null) {
      if (this.lastPong != null) {
        const pingTime = (this.lastPong - this.lastPing);
        console.log("For user %s, lastPing=%s, lastPong=%s",
          this.user.id,
          new Date(this.lastPing).toUTCString(),
          new Date(this.lastPong).toUTCString())
        return nonResponsiveThresholdInMiliseconds < pingTime;
      } else {
        console.log("For user %s, lastPing=%s, lastPong=null",
          this.user.id,
          new Date(this.lastPing).toUTCString())
        return true;
      }
    } else {
      return false;
    }
  }

  kick(code: number, reason: string) {
    this.socket.close(code, reason);
  }

  kickBadData(message: string) {
    this.kick(1003, `Bad Data: ${message}`);
  }

  kickBadPermission() {
    this.kick(1008, 'You attempted to perform an operation you don\'t have permission for');
  }

  kickNonResponsive() {
    this.kick(4001, "The client is not responding to ping messages.");
  }

  kickTooLongInLobby() {
    this.kick(4002, "You waited in the lobby for too long");
  }

  closeDuplicate() {
    this.wasDuplicate = true;
    this.socket.close(1000, 'Connected Twice');
  }

  close(reason: string) {
    this.socket.close(1000, reason);
  }

  send(message: ServerMessage<string, any>) {
    this.socket.send(JSON.stringify(message));
  }
}
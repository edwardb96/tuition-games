import { auth } from "express-openid-connect";
import { Option } from "prelude-ts";

export const makeAuthRoutes = () =>
  Option.ofNullable(auth({
    routes: {
      postLogoutRedirect: '/splash',
      logout: false,
      login: false
    },
    idpLogout: true,
    authRequired: false,
    authorizationParams: {
      response_type: 'code id_token',
    }
  }));
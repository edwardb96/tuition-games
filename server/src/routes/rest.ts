import { validateUserId } from "@cat-tuition/protocol/user";
import cookieParser from "cookie-parser";
import { Router, Request, Response } from "express";
import { ExpressAuthenticator, RequestWithUser } from "../authentication/express-authenticator";
import { TuteeTokenManager } from "../authentication/tutee-token-manager";
import { WebsocketTokenManager } from "../authentication/websocket-token-manager";
import { BoardsStore } from "../stores/boards";
import { CardPairsStore } from "../stores/card-pairs";
import { ImageStore } from "../stores/images";
import { UserStore } from "../stores/user";
import { UserEditStore } from "../stores/user-edit";
import { makeBoardsRestRoutes } from "./rest/boards";
import { makeCardPairsRestRoutes } from "./rest/card-pairs";
import { makeImageRestRoutes } from "./rest/images";
import { makeUserEditsRestRoutes } from "./rest/user-edits";

export const makeRestRoutes = (wsTokenManager: WebsocketTokenManager,
                               tuteeTokenManager: TuteeTokenManager,
                               auth: ExpressAuthenticator,
                               userStore: UserStore,
                               userEditStore: UserEditStore,
                               imageStore: ImageStore,
                               cardPairStore: CardPairsStore,
                               boardsStore: BoardsStore): Router => {
  const api = Router();

  api.get('/user', [cookieParser(), auth.hasAuthOrToken()],
    async (req: Request, res: Response) => {
      console.log("Got request for /api/user");
      await auth.authenticate(req as RequestWithUser, res, user => {
        res.json(user.toFullInfoWithToken());
      });
    });

  api.get('/ws-token', [cookieParser(), auth.hasAuthOrToken()],
    async (req: Request, res: Response) => {
      console.log("Got request for /api/ws-token");
      await auth.authenticate(req as RequestWithUser, res, user => {
        const token = wsTokenManager.makeWebsocketToken({ id: user.id, classroom: user.tokenClassroom });
        res.set('Content-Type', 'text/plain');
        res.send(token);
      });
    });

  api.get('/tutee-token', [auth.hasAuth()],
    async (req: Request, res: Response) => {
      console.log("Got request for /api/tutee-token");
      await auth.authenticate(req as RequestWithUser, res, user => {
        if (user.canCreateTuteeTokens()) {
          validateUserId(req.query['tutee']).match({
            Some: async tuteeId => {
              const tuteeUser = await userStore.findUserById(tuteeId)
              if (tuteeUser != null) {
                if (tuteeUser.role < user.role) {
                  const token = tuteeTokenManager.makeTuteeToken({ id: tuteeId, classroom: user.id });
                  res.set('Content-Type', 'text/plain');
                  res.send(token);
                } else {
                  res.sendStatus(403);
                }
              } else {
                res.sendStatus(400);
              }
            },
            None: async () => {
              res.sendStatus(400);
            }
          });
        } else {
          res.sendStatus(403);
        }
      });
    });

  api.use('/images', makeImageRestRoutes(auth, imageStore))
  api.use('/users', makeUserEditsRestRoutes(auth, userEditStore));
  api.use('/card-pairs', makeCardPairsRestRoutes(auth, cardPairStore));
  api.use('/boards', makeBoardsRestRoutes(auth, boardsStore));

  return api;
};
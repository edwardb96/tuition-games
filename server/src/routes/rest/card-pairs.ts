import { validateCardPairId, validateNewCardPair } from "@cat-tuition/protocol/pairs";
import { Router, Request, Response, json } from "express";
import { ExpressAuthenticator } from "../../authentication/express-authenticator";
import { CardPairsStore } from "../../stores/card-pairs";

export const makeCardPairsRestRoutes = (auth: ExpressAuthenticator, cardPairStore: CardPairsStore): Router => {
  const api = Router();

  api.get('/', [auth.hasAuth(), auth.withUser(user => user.canGetCardPairs())],
    async (req: Request, res: Response) => {
      console.log("Got request for /api/card-pairs");
      try {
        const results = await cardPairStore.getAllCardPairs();
        res.json(results);
      } catch(e) {
        console.log(e);
      }
    });

  api.get('/:id', [auth.hasAuth(), auth.withUser(user => user.canGetCardPairs())],
    async (req: Request, res: Response) => {
      console.log(`Got get request for /api/card-pairs/${req.params.id}`);
      try {
        const cardPairId = validateCardPairId(req.params.id);
        if (cardPairId.isSome()) {
          const result = await cardPairStore.getCardPair(cardPairId.get());
          if (result != null) {
            res.json(result);
          } else {
            res.sendStatus(404);
          }
        } else {
          res.sendStatus(400);
        }
      } catch (err) {
        res.sendStatus(500);
      }
    });

  api.put('/:id', [auth.hasAuth(), auth.withUser(user => user.canAddCardPairs()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got put request for /api/card-pairs/${req.params.id}`);
      try {
        const updatedCardPair = validateNewCardPair(req.body);
        const cardPairId = validateCardPairId(req.params.id);
        console.log(req.body, cardPairId);
        if (updatedCardPair.isSome() && cardPairId.isSome()) {
          await cardPairStore.createOrUpdateCardPair(cardPairId.get(), updatedCardPair.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.post('/', [auth.hasAuth(), auth.withUser(user => user.canAddCardPairs()), json()],
    async (req: Request, res: Response) => {
      console.log("Got post request for /api/card-pairs");
      try {
        const newCardPair = validateNewCardPair(req.body);
        if (newCardPair.isSome()) {
          const id = await cardPairStore.createCardPair(newCardPair.get());
          res.json({ id });
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.delete('/:id', [auth.hasAuth(), auth.withUser(user => user.canRemoveCardPairs()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got delete request for /api/card-pairs/${req.params.id}`);
      try {
        const userId = validateCardPairId(req.params.id);
        if (userId.isSome()) {
          await cardPairStore.deleteCardPair(userId.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  return api;
}
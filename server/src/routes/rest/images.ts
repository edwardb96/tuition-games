import { Router, Request, Response, json } from "express";
import expressFileUpload, { UploadedFile } from "express-fileupload";
import { Option } from "prelude-ts";

import Joi from "joi";
import isPng from "is-png";
import { promisify } from "util";
import { readFile } from "fs";
import asyncMap from "iter-tools/methods/async-map";
import arrayFromAsync from "iter-tools/methods/array-from-async";

import { ImageId, imageIdSchema, validateImageId } from "@cat-tuition/protocol/image";
import { validOrNone } from "@cat-tuition/protocol/joi-ext";
import { ImageStore } from "../../stores/images";
import { ExpressAuthenticator } from "../../authentication/express-authenticator";
import { extractWidgitCardsFromPdf } from "../../image-processing/widgit-card-pdf";
import { extractImagePerPageFromPdf } from "../../image-processing/png-from-pdf";
import { convertJpegToPng } from "../../image-processing/png-from-jpeg";
import { asyncWrap, takeWhile } from "iter-tools";
import cookieParser from "cookie-parser";

const { object, number, boolean } = Joi.types();

const readFileAsync = promisify(readFile);

interface PageForwardParams {
  after: ImageId;
  count: number;
}

interface PageBackParams {
  before: ImageId;
  count: number;
}

interface FirstPageParams {
  count: number;
}

type PaginationParams = PageForwardParams | PageBackParams | FirstPageParams;

const isPageForward = (pagination: PaginationParams): pagination is PageForwardParams => {
  return pagination.hasOwnProperty('after');
}

const isPageBack = (pagination: PaginationParams): pagination is PageBackParams => {
  return pagination.hasOwnProperty('before');
}

const pageForwardParamsSchema = object.keys({
  after: imageIdSchema.required(),
  count: number.integer().min(0).max(50).required()
});

const pageBackParamsSchema = object.keys({
  before: imageIdSchema.required(),
  count: number.integer().min(0).max(50).required()
});

const firstPageParamsSchema = object.keys({
  count: number.integer().min(0).max(50).required()
});

const paginationParamsSchema =
  Joi.alternatives(firstPageParamsSchema,
                   pageBackParamsSchema,
                   pageForwardParamsSchema);

const validatePaginationParams = (v: unknown): Option<PaginationParams> =>
  validOrNone(paginationParamsSchema, v)

interface PostImageParams {
  crop_widgit_cards: boolean;
}

const postImageParamsSchema =
  object.keys({
    crop_widgit_cards: boolean
  });

const validatePostImageParams = (v: unknown): Option<PostImageParams> =>
  validOrNone(postImageParamsSchema, v)

export const makeImageRestRoutes = (auth: ExpressAuthenticator, imageStore: ImageStore): Router => {
  const api = Router();

  const fileUpload = (sizeLimitInMegabytes: number) => {
    const oneMegabyte = 1000000 /* bytes */;
    return expressFileUpload({
      limits: { fileSize: sizeLimitInMegabytes * oneMegabyte },
      useTempFiles : true,
      tempFileDir : '/tmp/'
    });
  }

  api.get('/', [auth.hasAuth(), auth.withUser(user => user.canGetImageMetadata())],
    async (req: Request, res: Response) => {
      console.log("Got get request for /api/images");
      try {
        const results = await imageStore.getAllImageMetadatas();
        res.json(results);
      } catch (e) {
        res.sendStatus(500);
        console.log(e);
      }
    });

  api.get('/unassigned', [auth.hasAuth(), auth.withUser(user => user.canGetImageMetadata())],
    async (req: Request, res: Response) => {
      console.log("Got get request for /api/images/unassigned");
      try {
        if (Object.keys(req.query).length == 0) {
          const results = await imageStore.getAllUnassignedImages();
          res.json(results);
        } else {
          const paginationParams = validatePaginationParams(req.query);
          await paginationParams.match({
            Some: async pagination => {
              if (isPageForward(pagination)) {
                const { after, count } = pagination;
                const results = await imageStore.getUnassignedImagesAfter(after, count);
                if (results != null) {
                  res.json(results);
                } else {
                  res.sendStatus(400);
                }
              } else if (isPageBack(pagination)) {
                const { before, count } = pagination;
                const results = await imageStore.getUnassignedImagesBefore(before, count);
                if (results != null) {
                  res.json(results);
                } else {
                  res.sendStatus(400);
                }
              } else {
                const { count } = pagination;
                const results = await imageStore.getInitialUnassignedImages(count);
                res.json(results);
              }
            },
            None: async () => {
              res.sendStatus(400);
            }
          });
        }
      } catch(e) {
        res.sendStatus(500);
        console.log(e);
      }
    });


  const fileFromRequest = (req: Request): UploadedFile | null => {
    if (req.files != null && req.files.file != null && !Array.isArray(req.files.file)) {
      return req.files.file;
    } else {
      return null;
    }
  }

  const isPdf = (buffer: Buffer) =>
    buffer.byteLength > 4 &&
      buffer[0] == 0x25 &&
      buffer[1] == 0x50 &&
      buffer[2] == 0x44 &&
      buffer[3] == 0x46;

  const isJpeg = (buffer: Buffer) =>
    buffer.byteLength > 4 &&
      buffer[0] == 0xFF &&
      buffer[1] == 0xD8 &&
      buffer[2] == 0xFF &&
      buffer[3] == 0xE0;

  const widgitCardImageBuffersFromRequest = async (req: Request): Promise<AsyncIterable<Buffer> | null> => {
    const file = fileFromRequest(req);
    if (file != null && file.mimetype == 'application/pdf') {
      const pdfBuffer = await readFileAsync(file.tempFilePath);
      if (isPdf(pdfBuffer)) {
        return extractWidgitCardsFromPdf(pdfBuffer);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  const imageBuffersFromRequest = async (req: Request): Promise<AsyncIterable<Buffer> | null> => {
    const file = fileFromRequest(req);
    if (file != null) {
      switch (file.mimetype) {
        case 'image/png': {
          const pngBuffer = await readFileAsync(file.tempFilePath);
          if (isPng(pngBuffer)) {
            return asyncWrap([pngBuffer]);
          } else {
            return null;
          }
        }
        case 'image/jpeg': {
          const jpegBuffer = await readFileAsync(file.tempFilePath);
          if (isJpeg(jpegBuffer)) {
            const pngBuffer = await convertJpegToPng(jpegBuffer);
            return pngBuffer != null ? asyncWrap([pngBuffer]) : asyncWrap([]);
          } else {
            console.log("Not a jpeg?");
            return null;
          }
        }
        case 'application/pdf': {
          const pdfBuffer = await readFileAsync(file.tempFilePath);
          if (isPdf(pdfBuffer)) {
            return extractImagePerPageFromPdf(pdfBuffer);
          } else {
            return null;
          }
        }
        default: {
          return null;
        }
      }
    } else {
      return null;
    }
  }

  api.post('/', [auth.hasAuth(), auth.withUser(user => user.canCreateImages()), fileUpload(2/*mb*/)],
    async (req: Request, res: Response) => {
      console.log("Got post request for /api/images");
      try {
        await validatePostImageParams(req.query).match({
          Some: async postImageParams => {
            const imageBuffers = postImageParams.crop_widgit_cards ?
              await widgitCardImageBuffersFromRequest(req)
              : await imageBuffersFromRequest(req);
            if (imageBuffers != null) {
              const ids = await arrayFromAsync(asyncMap(b => imageStore.createImage(b), imageBuffers));
              res.json(ids);
            } else {
              res.sendStatus(400);
            }
          },
          None: async () => {
            res.sendStatus(400);
          }
        });
      } catch(e) {
        console.log(e);
        res.sendStatus(500);
      }
    });

  api.get('/:id.png', [cookieParser(), auth.hasAuthOrToken(), auth.withUser(user => user.canGetImages()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got get request for /api/images/${req.params.id}.png`);
      try {
        const imageId = validateImageId(req.params.id);
        if (imageId.isSome()) {
          const imageData = await imageStore.getImageData(imageId.get());
          if (imageData != null) {
            res.set('Cache-Control', 'public, max-age=604800')
            res.status(200).contentType('image/png').send(imageData);
          } else {
            res.sendStatus(404);
          }
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.delete('/:id', [auth.hasAuth(), auth.withUser(user => user.canDeleteUsers()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got delete request for /api/images/${req.params.id}`);
      try {
        const imageId = validateImageId(req.params.id);
        if (imageId.isSome()) {
          const success = await imageStore.deleteImage(imageId.get());
          if (success) {
            res.sendStatus(200);
          } else {
            res.sendStatus(400);
          }
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    })

  api.get('/:id', [auth.hasAuth(), auth.withUser(user => user.canDeleteUsers()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got delete request for /api/images/${req.params.id}`);
      try {
        const imageId = validateImageId(req.params.id);
        if (imageId.isSome()) {
          const imageMetadata = await imageStore.getImageMetadata(imageId.get());
          if (imageMetadata != null) {
            res.json(imageMetadata);
          } else {
            res.sendStatus(404);
          }
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    })

  return api;
}
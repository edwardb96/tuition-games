import { validateNewUser, validateUpdatedUser, validateUserId } from "@cat-tuition/protocol/user";
import { Router, Request, Response, json } from "express";
import { requiresAuth } from "express-openid-connect";
import { UserEditStore } from "server/src/stores/user-edit";
import { ExpressAuthenticator } from "../../authentication/express-authenticator";

export const makeUserEditsRestRoutes = (auth: ExpressAuthenticator, userEditStore: UserEditStore): Router => {
  const api = Router();

  api.get('/', [auth.hasAuth(), auth.withUser(user => user.canGetUsers())],
    async (req: Request, res: Response) => {
      console.log("Got get request for /api/users");
      try {
        const results = await userEditStore.getAllUserEdits();
        res.json(results);
      } catch(e) {
        console.log(e);
        res.sendStatus(500);
      }
    });

  api.post('/', [auth.hasAuth(), auth.withUser(user => user.canPostUsers()), json()],
    async (req: Request, res: Response) => {
      console.log("Got post request for /api/users");
      try {
        const newUser = validateNewUser(req.body);
        if (newUser.isSome()) {
          const result = await userEditStore.createUser(newUser.get());
          if (result) {
            res.sendStatus(200);
          } else {
            res.sendStatus(400);
          }
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.put('/:id', [auth.hasAuth(), auth.withUser(user => user.canPutUsers()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got put request for /api/users/${req.params.id}`);
      try {
        const updatedUser = validateUpdatedUser(req.body);
        const userId = validateUserId(req.params.id);
        if (updatedUser.isSome() && userId.isSome()) {
          await userEditStore.createOrUpdateUser(userId.get(), updatedUser.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.delete('/:id', [auth.hasAuth(), auth.withUser(user => user.canDeleteUsers()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got delete request for /api/users/${req.params.id}`);
      try {
        const userId = validateUserId(req.params.id);
        if (userId.isSome()) {
          await userEditStore.deleteUser(userId.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  return api;
}
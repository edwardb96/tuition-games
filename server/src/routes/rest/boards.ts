import { validateBoardId, validateEditedBoard } from "@cat-tuition/protocol/board-game";
import { Router, Request, Response, json } from "express";
import { ExpressAuthenticator } from "../../authentication/express-authenticator";
import { BoardsStore } from "../../stores/boards";

export const makeBoardsRestRoutes = (auth: ExpressAuthenticator, boardStore: BoardsStore): Router => {
  const api = Router();

  api.get('/', [auth.hasAuth(), auth.withUser(user => user.canGetBoards())],
    async (req: Request, res: Response) => {
      console.log("Got request for /api/boards");
      try {
        const results = await boardStore.getAllBoards();
        res.json(results);
      } catch(e) {
        console.log(e);
      }
    });

  api.get('/:id', [auth.hasAuth(), auth.withUser(user => user.canGetBoards())],
    async (req: Request, res: Response) => {
      console.log(`Got get request for /api/boards/${req.params.id}`);
      try {
        const boardId = validateBoardId(req.params.id);
        if (boardId.isSome()) {
          const result = await boardStore.getBoard(boardId.get());
          if (result != null) {
            res.json(result);
          } else {
            res.sendStatus(404);
          }
        } else {
          res.sendStatus(400);
        }
      } catch (err) {
        res.sendStatus(500);
      }
    });

  api.put('/:id', [auth.hasAuth(), auth.withUser(user => user.canAddBoards()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got put request for /api/boards/${req.params.id}`);
      try {
        const updatedBoard = validateEditedBoard(req.body);
        const boardId = validateBoardId(req.params.id);
        console.log(req.body, boardId);
        if (updatedBoard.isSome() && boardId.isSome()) {
          await boardStore.createOrUpdateBoard(boardId.get(), updatedBoard.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.post('/', [auth.hasAuth(), auth.withUser(user => user.canAddBoards()), json()],
    async (req: Request, res: Response) => {
      console.log("Got post request for /api/boards");
      try {
        const newBoard = validateEditedBoard(req.body);
        if (newBoard.isSome()) {
          const id = await boardStore.createBoard(newBoard.get());
          res.json({ id });
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  api.delete('/:id', [auth.hasAuth(), auth.withUser(user => user.canRemoveBoards()), json()],
    async (req: Request, res: Response) => {
      console.log(`Got delete request for /api/boards/${req.params.id}`);
      try {
        const userId = validateBoardId(req.params.id);
        if (userId.isSome()) {
          await boardStore.deleteBoard(userId.get());
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      } catch(e) {
        res.sendStatus(500);
      }
    });

  return api;
}
import { makeFirestoreConnection } from "./stores/firestore/connection";
import { makeExpressApp } from "./express-server";
import { IncomingMessage } from "http";
import { CatTuitionWebSocketServer } from "./websocket-server";
import { Socket } from "net";
import { FirestoreUserStore } from "./stores/firestore/user";
import { WebSocketAuthenticator } from "./authentication/websocket-authenticator";
import { ExpressAuthenticator } from "./authentication/express-authenticator";
import { JsonWebTokenManager } from "./authentication/json-web-token-manager";
import { makeTokenCredentials } from "./keys";
import { FirestoreCardPairsStore } from "./stores/firestore/card-pairs";
import { FirestoreUserEditStore } from "./stores/firestore/user-edit";
import { FirestoreImageStore } from "./stores/firestore/images";
import { makeCollectionName } from "./stores/firestore/collections";
import makeAllTimesTables, { clearTimesTables } from "./migration/times-table";
import { FirestoreBoardsStore } from "./stores/firestore/boards";

const port = Number(process.env.PORT) || 5000;
const isProduction = process.env.NODE_ENV == 'production';
const distDir = '../../client/dist';

const firestore = makeFirestoreConnection().getOrThrow('Firestore environment variables are not set!');
const websocketsKeys = makeTokenCredentials().getOrThrow('Websocket key environment variables are not set!');

const cardPairsCollectionName = makeCollectionName(isProduction, 'Card Pairs');
const boardsCollectionName = makeCollectionName(isProduction, 'Boards');
const imagesCollectionName = makeCollectionName(isProduction, 'Images');
const usersCollectionName = makeCollectionName(isProduction, 'Users');

const userStore = new FirestoreUserStore(firestore, usersCollectionName);
const userEditsStore = new FirestoreUserEditStore(firestore, usersCollectionName);
const cardPairStore = new FirestoreCardPairsStore(firestore, cardPairsCollectionName, imagesCollectionName);
const boardsStore = new FirestoreBoardsStore(firestore, boardsCollectionName, imagesCollectionName);
const imageStore = new FirestoreImageStore(firestore, imagesCollectionName);
const tokenManager = new JsonWebTokenManager(websocketsKeys, userStore);
const wsAuth = new WebSocketAuthenticator(tokenManager);
const auth = new ExpressAuthenticator(userStore, tokenManager);

const expressApp =
  makeExpressApp(
    distDir,
    auth,
    tokenManager,
    tokenManager,
    userStore,
    userEditsStore,
    imageStore,
    cardPairStore,
    boardsStore);

const server = expressApp.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

const wsServer = new CatTuitionWebSocketServer('/api', wsAuth);

server.on('upgrade',
  async (req: IncomingMessage, socket: Socket, head) =>
    await wsServer.handleUpgrade(req, socket, head));

//const prodCardPairStore = new FirestoreCardPairsStore(firestore, 'Card Pairs', 'Images');
//clearTimesTables(prodCardPairStore);
//makeAllTimesTables(prodCardPairStore);

wsServer.start();
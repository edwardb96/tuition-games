import { subClass, State } from "gm";
import isPng from "is-png";
import { Readable } from "stream";
import { a4PageSize, Vector2 } from "./vector2";

export const convertJpegToPng = (buffer: Buffer, maxPageSize: Vector2 = a4PageSize): Promise<Buffer | null> => {
  return new Promise<Buffer | null>(( resolve, reject ) => {
    const command = subClass({ imageMagick: false });
    const stream = Readable.from(buffer);

    const resizeIfLargerCommand: State =
      command(stream, `in.jpg`)
        .resize(maxPageSize.x, maxPageSize.y, '>')

    resizeIfLargerCommand.toBuffer('png',
      (err, buffer) => {
        if (err == null) {
          if (isPng(buffer)) {
            resolve(buffer)
          } else {
            resolve(null);
          }
        } else {
          reject(err)
        }
      });
  });
};
export interface Vector2 {
  x: number;
  y: number
};

export interface CropParameters {
  topLeft: Vector2;
  areaSize: Vector2;
}

export const a4PageSize: Vector2 = { x: 1050, y: 1485 };
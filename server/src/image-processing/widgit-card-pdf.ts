import { readFile, writeFile } from "fs-extra";
import arrayFromAsync from "iter-tools/methods/array-from-async";
import asyncMap from "iter-tools/methods/async-map";
import range from "iter-tools/methods/range";
import { a4PageSize } from "./vector2";
import { extractImageFromPdf } from "./png-from-pdf";

export async function *extractWidgitCardsFromPdf(buffer: Buffer): AsyncGenerator<Buffer> {
  let image = null;
  let pageIndex = 0;
  do {
    image = await extractWidgitCardFromPdf(buffer, pageIndex, 0);
    if (image != null) {
      yield image;
      yield* asyncMap(cardIndex =>
        extractWidgitCardFromPdf(buffer, pageIndex, cardIndex) as Promise<Buffer>, range(1, 5));
    }
    ++pageIndex;
  } while (image != null);
}

const topCardOffset = 180;
const marginHeight = 36;

export const extractWidgitCardFromPdf =
  (buffer: Buffer, pageIndex: number, cardIndex: number): Promise<Buffer | null> => {
    const cardSize = { x: 280, y: 218 };
    const cardOffset = { x: 383, y: topCardOffset + ((marginHeight + cardSize.y) * cardIndex) };
    return extractImageFromPdf(buffer, pageIndex, a4PageSize, {
      topLeft: cardOffset,
      areaSize: cardSize
    });
  };

export const main = async () => {
  const buffer = await readFile("cards.pdf");
  const s: string[] =
    await arrayFromAsync(asyncMap(
      async (buffer: Buffer, index: number) => {
        const fileName = `extracted.${index}.png`
        await writeFile(fileName, buffer)
        return fileName
      },
      extractWidgitCardsFromPdf(buffer)));
  console.log(s)
};

//main();
import { subClass, State } from "gm";
import isPng from "is-png";
import { Readable } from "stream";
import asyncTakeWhile from "iter-tools/methods/async-take-while";
import asyncMap from "iter-tools/methods/async-map";
import range from "iter-tools/methods/range";
import { Vector2, CropParameters, a4PageSize } from "./vector2";

const pageIndexToBigError =
  '\nRequested FirstPage is greater than the number of pages in the file: ';

export const extractImagePerPageFromPdf = (buffer: Buffer): AsyncIterableIterator<Buffer> =>
  asyncTakeWhile((b: Buffer | null) => b != null,
    asyncMap(async pageIndex => await extractImageFromPdf(buffer, pageIndex, a4PageSize), range())) as AsyncIterableIterator<Buffer>

export const extractImageFromPdf = (
  buffer: Buffer, pageIndex: number, maxPageSize: Vector2,
  crop?: CropParameters): Promise<Buffer | null> => {
    return new Promise<Buffer | null>(( resolve, reject ) => {
      const command = subClass({ imageMagick: false });
      const stream = Readable.from(buffer);

      const baseCommand: State =
        command(stream, `x.pdf[${pageIndex}]`)
          .density(200, 200)
          .resize(maxPageSize.x, maxPageSize.y)

      const maybeWithCrop = (command: State): State => {
        if (crop != null) {
          const { topLeft, areaSize } = crop;
          return command.crop(areaSize.x, areaSize.y, topLeft.x, topLeft.y);
        } else {
          return command;
        }
      }

      const extractCommand = maybeWithCrop(baseCommand);

      extractCommand.toBuffer('png',
        (err, buffer) => {
          if (err == null) {
            if (isPng(buffer)) {
              resolve(buffer)
            } else {
              const errorMessageStart = buffer.slice(0, 70).toString("utf-8");
              if (errorMessageStart == pageIndexToBigError) {
                resolve(null);
              } else {
                reject(new Error(`Image extraction did not produce a valid PNG file.\nThe begining of the failure message was: ${errorMessageStart}`));
              }
            }
          } else {
            reject(err)
          }
        });
    });
  };
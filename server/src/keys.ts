import { Option } from "prelude-ts";
import { TokenCredentials } from "./authentication/websocket-token-manager";

export const pemFromEnv = (envvar: string): string => envvar.replace(/\\n/g, '\n');

export const makeTokenCredentials = (): Option<TokenCredentials> => {
  const privateKey = process.env.WEBSOCKETS_TOKEN_PRIVATE_KEY;
  const publicKey = process.env.WEBSOCKETS_TOKEN_PUBLIC_KEY;
  if (privateKey != null && publicKey != null) {
    return Option.of({
      privateKey: pemFromEnv(privateKey),
      publicKey: pemFromEnv(publicKey)
    });
  } else {
    return Option.none();
  }
}

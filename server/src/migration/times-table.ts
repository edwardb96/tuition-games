import { CardPairsStore } from "../stores/card-pairs";
import { Card, CardPair, NewCardPair } from "@cat-tuition/protocol/pairs";
import { filter, map, range } from "iter-tools";

const makeCardPair = (firstText: string, secondText: string, categories: string[]): NewCardPair => ({
  firstCard: { text: firstText },
  secondCard: { text: secondText },
  categories
});

export const clearTimesTables = async (cardStore: CardPairsStore) => {
  const cardPairs = await cardStore.getAllCardPairs();
  const isTimesTableCard = (c: CardPair): boolean => c.categories.filter(x => x.endsWith('Times Table')).length > 1;
  for (let pair of filter(isTimesTableCard, cardPairs)) {
    cardStore.deleteCardPair(pair.id);
  }
}

export const makeAllTimesTables = async (cardStore : CardPairsStore) => {
  const makeTimesTable = async (n: number) => {
    const makeMultiple = (m: number) => {
      const question = `${m}x${n}`;
      const answer = `${m * n}`;
      const categories = [`${n} Times Table`, `${m} Times Table`];
      return makeCardPair(question, answer, categories);
    }

    for (let card of map(makeMultiple, range(1, 13))) {
      await cardStore.createCardPair(card);
    }

    const makeDivision = (m: number) => {
      const question = `${n * m}÷${n}`;
      const answer = `${m}`;
      const categories = [`${n} Times Table`, `${m} Times Table`];
      return makeCardPair(question, answer, categories);
    }

    if (n != 0) {
      for (let card of map(makeDivision, range(0, 13))) {
        await cardStore.createCardPair(card);
      }
    }
  }

  for (let m of range(1, 13)) {
    await makeTimesTable(m);
  }
}

export default makeAllTimesTables;
import { Channel } from "../channel";
import { UserInfo } from "@cat-tuition/protocol/user";
import { ClientMessage } from "@cat-tuition/protocol/message";
import { ClassroomInfo } from "@cat-tuition/protocol/classroom";
import { LOBBY_CHANNEL,
         LobbyServerPayload,
         validateLobbyClientPayload,
         InvitationInfo,
         TuteeWelcomeToLobbyInfo,
         TutorWelcomeToLobbyInfo } from "@cat-tuition/protocol/channels/lobby";
import { UserSession } from "../session";

export interface ServerState {
  inviteTutee(session: UserSession, userId: string): void;

  enterLobby(session: UserSession): void;
  enterClass(session: UserSession, classroomId: string): void;
}

export class LobbyChannel implements Channel<typeof LOBBY_CHANNEL> {
  private server: ServerState;

  constructor(server: ServerState) {
    this.server = server;
  }

  private send(recipient: UserSession, payload: LobbyServerPayload) {
    recipient.send({ channel: LOBBY_CHANNEL, payload });
  }

  sendClassroomInactive(recipient: UserSession, classroomId: string) {
    this.send(recipient, { kind: 'classroom-inactive', classroomId })
  }

  sendClassroomActive(recipient: UserSession, classroomInfo: ClassroomInfo) {
    this.send(recipient, { kind: 'classroom-active', ...classroomInfo })
  }

  sendWelcomeTutee(recipient: UserSession, _info: TuteeWelcomeToLobbyInfo) {
    this.send(recipient, { kind: 'welcome-tutee' });
  }

  sendWelcomeTutor(recipient: UserSession, info: TutorWelcomeToLobbyInfo) {
    this.send(recipient, { kind: 'welcome-tutor', ...info });
  }

  sendEntered(recipient: UserSession) {
    this.send(recipient, { kind: 'entered-lobby'})
  }

  sendUserLeftLobby(recipient: UserSession, userId: string) {
    this.send(recipient, { kind: 'user-left-lobby', userId })
  }

  sendUserJoinedLobby(recipient: UserSession, userInfo: UserInfo) {
    this.send(recipient, { kind: 'user-joined-lobby', user: userInfo });
  }

  sendUserJoinedClass(recipient: UserSession, userId: string, classroomId: string) {
    this.send(recipient, { kind: 'user-joined-class', userId, classroomId });
  }

  sendNoSuchClass(recipient: UserSession) {
    this.send(recipient, { kind: 'no-such-classroom' })
  }

  sendUserLeftClass(recipient: UserSession, userId: string, classroomId: string) {
    this.send(recipient, { kind: 'user-left-class', userId, classroomId });
  }

  sendAdmittance(recipient: UserSession, info: InvitationInfo) {
    this.send(recipient, { kind: 'invited-to-class', ...info });
  }

  onReceive(clientMessage: ClientMessage<typeof LOBBY_CHANNEL, any>, session: UserSession): void {
    // Handle start class
    validateLobbyClientPayload(clientMessage.payload).match({
      Some: payload => {
        switch(payload.kind) {
          case 'enter-lobby': {
            this.server.enterLobby(session);
            break;
          }
          case 'enter-classroom': {
            const { classroomId } = payload;
            this.server.enterClass(session, classroomId)
            break;
          }
          case 'admit-tutee': {
            const { userId } = payload;
            this.server.inviteTutee(session, userId);
            break;
          }
        }
      },
      None: () => {
        session.kickBadData('Malformed lobby message');
      }
    })
  }
}
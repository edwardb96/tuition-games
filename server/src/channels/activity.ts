import { ClientMessage } from "@cat-tuition/protocol/message";
import { Channel } from "../channel";
import { UserSession } from "../session";

export interface Activity<Id, ChannelTag> extends Channel<ChannelTag> {
  readonly id: Id;
  readonly channel: ChannelTag;
  userLeft(userId: string): void;
  userJoined(userId: string): void;
  sendBegin(): void;
}

export interface ActivityServer<ChannelTag, Payload> {
  classroomOccupants(): Iterable<UserSession>;
  send(userId: string, payload: ClientMessage<ChannelTag, Payload>): void;
}

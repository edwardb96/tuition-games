import { Channel } from "../channel";
import { ClientMessage } from "@cat-tuition/protocol/message";
import { UserInfo } from "@cat-tuition/protocol/user";
import { ClassroomServerPayload,
         CLASSROOM_CHANNEL,
         ChatMessageInfo,
         validateClassroomClientPayload,
         TuteeWelcomeToClassroomInfo,
         TutorWelcomeToClassroomInfo,
         ActivityBeginInfo,
         ActivityId,
         RecievedChatMessageInfo} from "@cat-tuition/protocol/channels/classroom";
import { UserSession } from "../session";

export interface ClassroomState {
  changeActivity(session: UserSession, changeActivityInfo: ActivityBeginInfo): void;
  receivedChatMessage(session: UserSession, chatMessage: string): void;
}

export class ClassroomChannel implements Channel<typeof CLASSROOM_CHANNEL> {
  private classroom: ClassroomState;

  constructor(classroom: ClassroomState) {
    this.classroom = classroom;
  }

  private send(recipient: UserSession, payload: ClassroomServerPayload) {
    recipient.send({ channel: CLASSROOM_CHANNEL, payload });
  }

  sendTuteeEntered(recipient: UserSession, info: TuteeWelcomeToClassroomInfo) {
    this.send(recipient, { kind: 'tutee-entered-classroom', ...info })
  }

  sendTutorEntered(recipient: UserSession, info: TutorWelcomeToClassroomInfo) {
    this.send(recipient, { kind: 'tutor-entered-classroom', ...info })
  }

  sendClassEnded(recipient: UserSession) {
    this.send(recipient, { kind: 'class-ended' });
  }

  sendTuteeAdded(recipient: UserSession, info: UserInfo) {
    this.send(recipient, { kind: 'tutee-added', ...info });
  }

  sendTuteeRemoved(recipient: UserSession, userId: string) {
    this.send(recipient, { kind: 'tutee-removed', userId });
  }

  sendActivityChanged(recipient: UserSession, activityId: ActivityId | null) {
    this.send(recipient, { kind: 'activity-changed', activityId });
  }

  sendChatMessage(recipient: UserSession, info: RecievedChatMessageInfo) {
    this.send(recipient, { kind: 'receive-chat', ...info });
  }

  onReceive(clientMessage: ClientMessage<typeof CLASSROOM_CHANNEL, any>, session: UserSession): void {
    // Handle start class
    validateClassroomClientPayload(clientMessage.payload).match({
      Some: payload => {
        switch (payload.kind) {
          case 'change-activity': {
            this.classroom.changeActivity(session, payload);
            break;
          }
          case 'send-chat': {
            this.classroom.receivedChatMessage(session, payload.chatMessage);
            break;
          }
        }
      },
      None: () => {
        session.kickBadData('Malformed classroom message');
      }
    });
  }
}
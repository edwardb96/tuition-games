import { ClientMessage } from "@cat-tuition/protocol/message";
import {
  BeginInfo,
  BOARD_GAME_ACTIVITY_ID,
  Counter,
  Counters,
  Player,
  Position } from "@cat-tuition/protocol/board-game";
import {
  BOARD_GAME_CHANNEL,
  ServerPayload,
  ClientPayload,
  validateClientPayload } from "@cat-tuition/protocol/channels/board-game";
import { Activity, ActivityServer } from "../activity";
import { UserSession } from "../../session";
import { ImageId } from "@cat-tuition/protocol/image";
import { SimpleColour } from "@cat-tuition/protocol/colour";

export type BoardGameServer = ActivityServer<typeof BOARD_GAME_CHANNEL, ServerPayload>;

const randomInt = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min +1)) + min;

export class BoardGameActivity implements Activity<typeof BOARD_GAME_ACTIVITY_ID, typeof BOARD_GAME_CHANNEL> {
  readonly id = BOARD_GAME_ACTIVITY_ID;
  readonly channel = BOARD_GAME_CHANNEL;
  readonly server: BoardGameServer;
  readonly title: string;
  readonly board: ImageId;
  readonly counterSize: number;
  readonly diceColour: SimpleColour;
  counters: Map<string, Counter>;
  boardIsLocked: boolean;

  constructor(server: BoardGameServer, beginInfo: BeginInfo) {
    this.server = server;
    this.board = beginInfo.board;
    this.title = beginInfo.title;
    this.counters = new Map<string, Counter>(Object.entries(beginInfo.counters));
    this.counterSize = beginInfo.counterSize;
    this.diceColour = beginInfo.diceColour;
    this.boardIsLocked = false;
  }

  countersRecord() {
    let counters: Counters = {};
    for (let [id, counter] of this.counters) {
      counters[id] = counter;
    }
    return counters;
  }

  sendBegin() {
    for (let session of this.server.classroomOccupants()) {
      this.send(session.user.id, {
        kind: 'begin',
        title: this.title,
        board: this.board,
        counters: this.countersRecord(),
        counterSize: this.counterSize,
        diceColour: this.diceColour,
        userPlayer: session.user.id
      });
    }
  }

  private get isRunning(): boolean {
    return true;
  }

  onReceive(clientMessage: ClientMessage<typeof BOARD_GAME_CHANNEL, any>, session: UserSession) {
    validateClientPayload(clientMessage.payload).match({
      Some: payload => {
        if (this.isRunning) {
          this.onMove(session, payload);
        } else {
          session.kickBadData('Tried to move when the game was not running');
        }
      },
      None: () => {
        console.log(clientMessage.payload)
        session.kickBadData('Invalid board-game message');
      }
    })
  }

  send(userId: string, payload: ServerPayload) {
    this.server.send(userId, { channel: BOARD_GAME_CHANNEL, payload })
  }

  sendToSpectator(userId: string, payload: ServerPayload) {
    this.send(userId, payload);
  }

  userJoined(userId: string) {
    this.sendToSpectator(userId, {
      kind: 'begin-spectate',
      title: this.title,
      board: this.board,
      counters: this.countersRecord(),
      counterSize: this.counterSize,
      diceColour: this.diceColour
    });
  }

  userLeft(userId: string) {
    this.counters.delete(userId);
    this.sendToCurrentPlayers({ kind: 'surrendered', player: userId })
  }

  sendToCurrentPlayers(payload: ServerPayload) {
    for (let session of this.server.classroomOccupants()) {
      this.send(session.user.id, payload);
    }
  }

  movePiece(session: UserSession, player: Player, position: Position) {
    console.log(`Got a move request from ${session.id} for player ${player}`);
    const counter = this.counters.get(player)
    if (counter != null) {
      if (!this.boardIsLocked || session.isInOwnClass()) {
        counter.position = position
        this.sendToCurrentPlayers({ kind: 'piece-moved', player, position });
      }
    } else {
      session.kickBadData('Tried to move when you are not participating in the game');
    }
  }

  lockBoard(session: UserSession) {
    if (session.isInOwnClass()) {
      if (!this.boardIsLocked) {
        this.boardIsLocked = true;
        this.sendToCurrentPlayers({ kind: 'locked-board' });
      }
    } else {
      session.kickBadPermission();
    }
  }

  unlockBoard(session: UserSession) {
    if (session.isInOwnClass()) {
      if (this.boardIsLocked) {
        this.boardIsLocked = false;
        this.sendToCurrentPlayers({ kind: 'unlocked-board' });
      }
    } else {
      session.kickBadPermission();
    }
  }

  rollDice(session: UserSession) {
    if (this.counters.has(session.user.id)) {
      if (!this.boardIsLocked) {
        this.sendToCurrentPlayers({ kind: 'dice-rolled', player: session.user.id, value: randomInt(1, 6) });
      }
    } else {
      session.kickBadData('Tried to roll the dice when you are not participating in the game');
    }
  }

  onMove(session: UserSession, payload: ClientPayload) {
    switch (payload.kind) {
      case 'dice-roll':
        this.rollDice(session);
        break;
      case 'piece-move':
        const { position, player } = payload;
        this.movePiece(session, player != null ? player : session.user.id, position);
        break;
      case 'lock-board':
        this.lockBoard(session)
        break;
      case 'unlock-board':
        this.unlockBoard(session)
        break;
    }
  }
}
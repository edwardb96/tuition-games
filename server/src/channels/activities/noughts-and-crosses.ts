import { ClientMessage } from "@cat-tuition/protocol/message";
import {
  NOUGHTS_AND_CROSSES_ACTIVITY_ID,
  Board,
  Player,
  Position,
  BeginInfo,
  calculateWinner,
  isDraw } from "@cat-tuition/protocol/noughts-and-crosses";
import {
  NOUGHTS_AND_CROSSES_CHANNEL,
  ServerPayload,
  playersInOrder,
  validateClientPayload } from "@cat-tuition/protocol/channels/noughts-and-crosses";
import { Activity, ActivityServer } from "../activity";
import { UserSession } from "../../session";
import { SimpleColour } from "@cat-tuition/protocol/colour";

export type NoughtsAndCrossesServer = ActivityServer<typeof NOUGHTS_AND_CROSSES_CHANNEL, ServerPayload>;

export class NoughtsAndCrossesActivity implements Activity<typeof NOUGHTS_AND_CROSSES_ACTIVITY_ID, typeof NOUGHTS_AND_CROSSES_CHANNEL> {
  readonly id = NOUGHTS_AND_CROSSES_ACTIVITY_ID;
  readonly channel = NOUGHTS_AND_CROSSES_CHANNEL;
  readonly server: NoughtsAndCrossesServer;
  readonly xPlayer: string;
  readonly oPlayer: string;
  readonly boardColour: SimpleColour;
  board: Board;
  winner: null | Player;
  isDraw: boolean;

  nextPlayerSymbol: Player;
  nextPlayerId: string;
  currentPlayerSymbol: Player;
  currentPlayerId: string;

  constructor(server: NoughtsAndCrossesServer, beginInfo: BeginInfo) {
    this.server = server;
    this.board = Array(9).fill(null);
    this.xPlayer = beginInfo.xPlayer;
    this.oPlayer = beginInfo.oPlayer;
    [this.currentPlayerId, this.currentPlayerSymbol,
      this.nextPlayerId, this.nextPlayerSymbol] = playersInOrder(beginInfo);
    this.winner = null;
    this.isDraw = false;
    this.boardColour = beginInfo.boardColour;

    console.log("Started game with %s", this.players);
  }

  sendBegin() {
    this.sendToSpectators({
      kind: 'begin-spectate',
      board: this.board,
      nextPlayer: this.currentPlayerSymbol,
      players: this.players,
      isDraw: this.isDraw,
      winner: this.winner,
      boardColour: this.boardColour
    });

    this.sendToCurrentPlayer({
      kind: 'begin',
      player: this.currentPlayerSymbol,
      opponent: this.nextPlayerId,
      nextPlayer: this.currentPlayerSymbol,
      boardColour: this.boardColour
    });

    this.sendToCurrentOpponnent({
      kind: 'begin',
      player: this.nextPlayerSymbol,
      opponent: this.currentPlayerId,
      nextPlayer: this.currentPlayerSymbol,
      boardColour: this.boardColour
    });
  }

  private get isRunning(): boolean {
    return this.winner == null && !this.isDraw;
  }

  canContinueWithoutUser(userId: string): boolean {
    return userId != this.currentPlayerId && userId != this.nextPlayerId;
  }

  onReceive(clientMessage: ClientMessage<typeof NOUGHTS_AND_CROSSES_CHANNEL, any>, session: UserSession) {
    validateClientPayload(clientMessage.payload).match({
      Some: payload => {
        if (this.isRunning) {
          if (session.user.id == this.currentPlayerId) {
            this.onMove(this.currentPlayerSymbol, payload.position, session);
          } else {
            session.kickBadData('Tried to move when it was not your turn');
          }
        } else {
          session.kickBadData('Tried to move when the game was not running');
        }
      },
      None: () => {
        session.kickBadData('Invalid noughts-and-crosses message');
      }
    })
  }

  send(userId: string, payload: ServerPayload) {
    this.server.send(userId, { channel: NOUGHTS_AND_CROSSES_CHANNEL, payload })
  }

  sendToSpectator(userId: string, payload: ServerPayload) {
    this.send(userId, payload);
  }

  sendToCurrentOpponnent(payload: ServerPayload) {
    this.send(this.nextPlayerId, payload);
  }

  sendToCurrentPlayer(payload: ServerPayload) {
    this.send(this.currentPlayerId, payload);
  }

  get players(): { X: string, O: string } {
    return { X: this.xPlayer, O: this.oPlayer };
  }

  userJoined(userId: string) {
    this.sendToSpectator(userId, {
      kind: 'begin-spectate',
      board: this.board,
      nextPlayer: this.nextPlayerSymbol,
      players: this.players,
      isDraw: this.isDraw,
      winner: this.winner,
      boardColour: this.boardColour
    });
  }

  userLeft(userId: string) {
    if (userId == this.nextPlayerId) {
      this.sendToCurrentPlayer({ kind: 'opponent-surrendered' })
      this.sendToSpectators({ kind: 'surrendered', player: this.nextPlayerSymbol })
      this.winner = this.currentPlayerSymbol;
    } else if (userId == this.currentPlayerId) {
      this.sendToCurrentOpponnent({ kind: 'opponent-surrendered' })
      this.sendToSpectators({ kind: 'surrendered', player: this.currentPlayerSymbol })
      this.winner = this.nextPlayerSymbol;
    }
  }

  sendToSpectators(payload: ServerPayload) {
    for (let session of this.server.classroomOccupants()) {
      if (session.user.id != this.currentPlayerId &&
        session.user.id != this.nextPlayerId) {
        this.send(session.user.id, payload);
      }
    }
  }

  onMove(player: Player, position: Position, session: UserSession) {
    if (this.board[position] == null) {
      this.board[position] = player;
      const winner = calculateWinner(this.board);
      if (winner != null) {
        this.sendToCurrentPlayer({ kind: 'win' });
        this.sendToCurrentOpponnent({ kind: 'lose-move', position });
        this.sendToSpectators({ kind: 'lose-move', position });
        this.winner = winner;
      } else if (isDraw(this.board)) {
        this.sendToCurrentPlayer({ kind: 'draw' });
        this.sendToCurrentOpponnent({ kind: 'draw-move', position });
        this.sendToSpectators({ kind: 'draw-move', position });
        this.isDraw = true;
      } else {
        this.sendToCurrentOpponnent({ kind: 'move', position });
        this.sendToSpectators({ kind: 'move', position });
        [this.currentPlayerId, this.nextPlayerId] = [this.nextPlayerId, this.currentPlayerId];
        [this.currentPlayerSymbol, this.nextPlayerSymbol] = [this.nextPlayerSymbol, this.currentPlayerSymbol];
      }
    } else {
      session.kickBadData('Tried to make illegal noughts-and-crosses move');
    }
  }
}
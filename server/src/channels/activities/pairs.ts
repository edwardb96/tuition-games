import { PAIRS_ACTIVITY_ID, BeginInfo, Card, Player, Board, Scoreboard, CardPair, hasMatchingContent, FlippedFirstCard, performSecondFlip, performFirstFlip } from "@cat-tuition/protocol/pairs"
import { ServerPayload, PAIRS_CHANNEL, validateClientPayload, FinalSurrender, ClientPayload } from "@cat-tuition/protocol/channels/pairs";
import { ClientMessage } from "@cat-tuition/protocol/message"
import { Activity, ActivityServer } from "../activity";
import { UserSession } from "../../session";
import flatMap from "iter-tools/methods/flat-map";
import enumerate from "iter-tools/methods/enumerate";
import map from "iter-tools/methods/map";
import range from "iter-tools/methods/range";
import reduce from "iter-tools/methods/reduce";
import { sum } from "@cat-tuition/protocol/iter-tools-ext";
import { Position } from "@cat-tuition/protocol/noughts-and-crosses";
import { SimpleColour } from "@cat-tuition/protocol/colour";

export type PairsServer = ActivityServer<typeof PAIRS_CHANNEL, ServerPayload>;

export const matchIndex = (index: number) =>
  (index % 2)
    ? index - 1
    : index + 1;

export const isPair =
  (deck: Deck) =>
    (firstCardIndex: number, secondCardIndex: number) => {
      const firstCard = deck[firstCardIndex];
      const secondCard = deck[secondCardIndex];
      const matchForFirstCard = deck[matchIndex(firstCardIndex)];
      const matchForSecondCard = deck[matchIndex(secondCardIndex)];
      return hasMatchingContent(firstCard, matchForSecondCard) ||
        hasMatchingContent(secondCard, matchForFirstCard);
    };

export const randomInt = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

export const shuffle = (items: any[]) => {
  for (let i of range(items.length - 1, 0, -1)) {
    const j = randomInt(0, i);
    [items[j], items[i]] = [items[i], items[j]]
  }
}

export const randomBoardUsingDeck = (deck: Card[]): Board => {
  const board: Board = [...map(cardIndex => ({ player: null, cardIndex }), range(deck.length))];
  shuffle(board);
  return board;
}

function cardsFromPair(cardPair: CardPair): [Card, Card] {
  const { firstCard, secondCard } = cardPair;
  return [firstCard, secondCard]
}

type Deck = Card[];

export class PairsActivity implements Activity<typeof PAIRS_ACTIVITY_ID, typeof PAIRS_CHANNEL> {
  readonly channel = PAIRS_CHANNEL;
  readonly id = PAIRS_ACTIVITY_ID;

  private readonly server: PairsServer;
  private readonly deck: Deck;
  private readonly cardColour: SimpleColour;
  private readonly board: Board;
  private readonly scoreTally: Map<string, number>;
  private readonly players: string[];

  private currentPlayerIndex: number | null;
  private scoreboard: Scoreboard | null;
  private finalSurrender: FinalSurrender | null;
  private firstFlip: FlippedFirstCard | null;

  private get isRunning() {
    return this.scoreboard == null;
  }

  constructor(server: PairsServer, beginInfo: BeginInfo) {
    this.server = server;
    this.players = beginInfo.players;
    this.deck = [...flatMap(cardsFromPair, beginInfo.pairs)];
    this.board = randomBoardUsingDeck(this.deck);
    this.scoreboard = null;
    this.finalSurrender = null;
    this.firstFlip = null;
    this.scoreTally = new Map<string, number>(map(p => [p, 0], this.players));
    this.currentPlayerIndex = 0;
    this.cardColour = beginInfo.cardColour;
  }

  sendBegin(): void {
    this.sendToSpectators({
      kind: 'begin-spectate',
      players: this.players,
      deck: this.deck,
      board: this.board,
      currentPlayerIndex: this.currentPlayerIndex,
      scoreboard: null,
      finalSurrender: this.finalSurrender,
      cardColour: this.cardColour
    });

    for (let [i, player] of enumerate(this.players)) {
      this.send(player, {
        kind: 'begin',
        players: this.players,
        deck: this.deck,
        userPlayer: i,
        cardColour: this.cardColour
      });
    }
  }

  onReceive(clientMessage: ClientMessage<typeof PAIRS_CHANNEL, any>, session: UserSession) {
    validateClientPayload(clientMessage.payload).match({
      Some: payload => {
        if (this.isRunning && this.currentPlayerIndex != null) {
          if (session.user.id == this.players[this.currentPlayerIndex]) {
            this.onFlip(session.user.id, payload, session);
          } else {
            session.kickBadData(`Tried to move (${session.user.id}) when it was not your turn, it was ${this.players[this.currentPlayerIndex]}'s turn`);
          }
        } else {
          session.kickBadData('Tried to move when the activity was not running');
        }
      },
      None: () => {
        session.kickBadData('Invalid pairs message');
      }
    });
  }

  onFlip(player: Player, payload: ClientPayload, session: UserSession) {
    switch (payload.kind) {
      case 'flip-first': {
        this.onFirstFlip(player, payload.position, session)
        return;
      }
      case 'flip-second': {
        this.onSecondFlip(player, payload.position, session)
        return;
      }
    }
  }

  private makeScoreboard(): Scoreboard {
    const scores = [...this.scoreTally.entries()];
    scores.sort(([idA, scoreA], [idB, scoreB]) => scoreB - scoreA);
    const { scoreboard } = reduce({ scoreboard: {}, lastRank: -1, lastScore: -1 },
      ({ scoreboard, lastRank, lastScore }, [id, score]) => {
        const rank = score == lastScore ? lastRank : lastRank + 1;
        return {
          scoreboard: { ...scoreboard, [id]: { rank, score } },
          lastRank: rank,
          lastScore: score
        };
      }, scores);
    return scoreboard;
  }

  onFirstFlip(player: Player, position: Position, session: UserSession) {
    if (this.firstFlip == null) {
      const firstFlip = performFirstFlip(player, this.board, position);
      if (firstFlip != null) {
        this.sendToPlayers({ kind: 'flipped-first', ...firstFlip });
        this.sendToSpectators({ kind: 'flipped-first', ...firstFlip });
        this.firstFlip = firstFlip;
      } else {
        session.kickBadData('Tried to make illegal pairs move');
      }
    } else {
      session.kickBadData('Tried to flip the first card twice in a row');
    }
  }

  onSecondFlip(player: Player, position: Position, session: UserSession) {
    if (this.firstFlip != null) {
      const secondFlip = performSecondFlip(player, this.board, this.firstFlip, position, isPair(this.deck));
      if (secondFlip != null && this.currentPlayerIndex != null) {
        if (secondFlip.isMatch) {
          this.scoreTally.set(player, (this.scoreTally.get(player) as number) + 1)
          if (sum(this.scoreTally.values()) == this.deck.length / 2) {
            const scoreboard = this.makeScoreboard();
            this.scoreboard = scoreboard;
            this.currentPlayerIndex = null;
            this.sendToPlayers({
              kind: 'flipped-final',
              ...secondFlip,
              scoreboard
            });
            this.sendToSpectators({
              kind: 'flipped-final',
              ...secondFlip,
              scoreboard
            });
            this.firstFlip = null;
          } else {
            this.sendToPlayers({ kind: 'flipped-second', ...secondFlip });
            this.sendToSpectators({ kind: 'flipped-second', ...secondFlip });
            this.firstFlip = null;
          }
        } else {
          this.sendToPlayers({ kind: 'flipped-second', ...secondFlip });
          this.sendToSpectators({ kind: 'flipped-second', ...secondFlip });
          this.firstFlip = null;
          this.currentPlayerIndex = (this.currentPlayerIndex + 1) % this.players.length;
        }
      } else {
        session.kickBadData('Tried to make illegal pairs move');
      }
    } else {
      session.kickBadData('Tried to flip the second card before flipping the first');
    }
  }

  private send(userId: string, payload: ServerPayload) {
    this.server.send(userId, { channel: PAIRS_CHANNEL, payload })
  }

  private sendToPlayers(payload: ServerPayload) {
    for (let player of this.players) {
      this.send(player, payload);
    }
  }

  private sendToSpectators(payload: ServerPayload) {
    for (let session of this.server.classroomOccupants()) {
      if (!this.scoreTally.has(session.user.id)) {
        this.send(session.user.id, payload);
      }
    }
  }

  userJoined(userId: string) {
    this.sendToSpectators({
      kind: 'begin-spectate',
      players: this.players,
      deck: this.deck,
      board: this.board,
      currentPlayerIndex: this.isRunning ? this.currentPlayerIndex : null,
      scoreboard: this.scoreboard,
      finalSurrender: this.finalSurrender,
      cardColour: this.cardColour
    });
  }

  userLeft(userId: string) {
    if (this.scoreTally.delete(userId)) {
      // userId surrendered
      const userIndex = this.players.findIndex(id => id == userId);
      if (userIndex != -1) {
        this.players.splice(userIndex, 1);
        if (this.players.length == 1) {
          const scoreboard = this.makeScoreboard();
          this.scoreboard = scoreboard;
          this.finalSurrender = { player: userId, winner: this.players[0] };
          this.sendToPlayers({ kind: 'surrendered', player: userId, scoreboard })
        }
      } else {
        throw new Error("Failed player in scoreboard but not in players");
      }
    }
  }
}
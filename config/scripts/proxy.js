const Proxy = require('redbird');
const proxy = new Proxy({
//  port: 443,

  // Specify filenames to default SSL certificates (in case SNI is not supported by the
  // user's browser)
  ssl: {
    port: 443,
    key: "localhost+2-key.pem",
    cert: "localhost+2.pem",
  }
});

// Since we will only have one https host, we dont need to specify additional certificates.
proxy.register('localhost', 'http://localhost:1234', { ssl: true });


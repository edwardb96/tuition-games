import Joi from "joi";
import { Option } from "prelude-ts";
import { validOrNone } from "./joi-ext";
const { string } = Joi.types();

export type ImageId = string;

export interface ImageMetadata {
  id: ImageId;
  usedInCards: string[];
  usedInBoards: string[];
  createdAt: Date
}

export const imageIdSchema = string.pattern(/^([-_a-zA-Z0-9]{1,1000})$/);

export const validateImageId = (v: unknown): Option<ImageId> =>
  validOrNone(imageIdSchema, v);
import Joi from "joi";
import { Option } from "prelude-ts";
import { ImageId, imageIdSchema } from "./image";
import { validOrNone } from "./joi-ext";
import { userIdSchema } from "./user";
import map from "iter-tools/methods/map";
import { colourSchema, SimpleColour } from "./colour";
const { object, string, array, boolean } = Joi.types();

export const PAIRS_ACTIVITY_ID = 'pairs';

export type Player = string;
export type Position = number;

export type Board = (BoardItem | null)[]

export interface ScoreboardEntry {
  rank: number;
  score: number;
}

export type Scoreboard = Record<string, ScoreboardEntry>;

export interface BoardItem {
  player: Player | null;
  cardIndex: number;
}

export type CardCategory = string;
export const cardCategorySchema = string.trim().min(2).max(100);
export const cardCategoriesSchema = array.items(cardCategorySchema);

export type CardPairId = string;
export const cardPairIdSchema = string.pattern(/^([-_a-zA-Z0-9]{1,1000})$/);

export interface TextCard {
  text: string;
}

const cardTextMessages = {
  'string.empty': 'You must specify the text or image for this card.',
  'string.min': 'The text for this card is too short. It must be at least 1 character long excluding spaces at the begining and end.',
  'string.max': 'The text for this card is too long. It must be at most 100 characters long excluding spaces at the begining and end.',
};

export const textOrDescriptionSchema = string.trim().min(1).max(100);

export const textCardSchema =
  object.keys({ text: textOrDescriptionSchema.required().messages(cardTextMessages) });

export const isTextCard = (card: Card): card is TextCard => {
  return card.hasOwnProperty('text');
};

export interface ImageCard {
  image: string;
  description: string;
}

const cardDescriptionMessages = {
  'string.empty': 'You must specify the description for this image card.',
  'string.min': 'The description for this image card is too short. It must be at least 1 character long excluding spaces at the begining and end.',
  'string.max': 'The description for this image card is too long. It must be at most 100 characters long excluding spaces at the begining and end.',
};

export const imageCardSchema =
  object.keys({
    image: imageIdSchema.required(),
    description: textOrDescriptionSchema.required().messages(cardDescriptionMessages)
  });

export const isImageCard = (card: Card): card is ImageCard => {
  return card.hasOwnProperty('image');
};

export type Card = TextCard | ImageCard;
export const cardSchema = Joi.alternatives(textCardSchema, imageCardSchema);

export type CardMatchCases<T> = { text: (card: TextCard) => T, image: (imageCard: ImageCard) => T }

export function matchCard<T>(card: Card, { text, image }: CardMatchCases<T>): T {
  if (isTextCard(card)) {
    return text(card);
  } else {
    return image(card);
  }
}

export const hasMatchingContent = (cardA: Card, cardB: Card): boolean =>
  matchCard(cardA, {
    text: tca =>
      matchCard(cardB, {
        text: tcb => tca.text == tcb.text,
        image: _ => false
      }),
    image: ica =>
      matchCard(cardB, {
        text: _ => false,
        image: icb => ica.image == icb.image
      })
  });

export interface WithCategories {
  categories: CardCategory[];
}

export interface NewCardPair extends WithCategories {
  firstCard: Card;
  secondCard: Card;
}

export interface CardPair extends NewCardPair {
  id: CardPairId;
}

export type UpdatedCardPair = NewCardPair;

export const cardToString = (card: Card): string =>
  matchCard(card, {
    image: ic => ic.description,
    text: tc => tc.text
  });

export const cardPairToString = (cardPair: NewCardPair): string => {
  const { firstCard, secondCard } = cardPair;
  const firstCardAsString = cardToString(firstCard);
  const secondCardAsString = cardToString(secondCard);
  if (firstCardAsString == secondCardAsString) {
    return firstCardAsString;
  } else {
    return `${firstCardAsString} and ${secondCardAsString}`;
  }
}

export function imagesUsedByCardPair(cardPair: UpdatedCardPair): Iterable<ImageId> {
  return map(({ image }) => image, imageCardsFromPair(cardPair));
}

export function* imageCardsFromPair(cardPair: UpdatedCardPair): Iterable<ImageCard> {
  const { firstCard, secondCard } = cardPair;
  if (isImageCard(firstCard)) {
    yield firstCard;
  }
  if (isImageCard(secondCard)) {
    yield secondCard;
  }
}

export const validateCardPairId = (v: unknown): Option<CardPairId> =>
  validOrNone(cardPairIdSchema, v);

export const cardPairSchema =
  object.keys({
    id: cardPairIdSchema.required(),
    firstCard: cardSchema.required(),
    secondCard: cardSchema.required(),
    categories: cardCategoriesSchema.required()
  });

export const validateCardPair = (v: unknown): Option<CardPair> =>
  validOrNone(cardPairSchema, v);

export const newCardPairSchema =
  object.keys({
    firstCard: cardSchema.required(),
    secondCard: cardSchema.required(),
    categories: cardCategoriesSchema.required()
  });

export const validateNewCardPair = (v: unknown): Option<NewCardPair> =>
  validOrNone(newCardPairSchema, v);

export interface BeginInfo {
  players: string[];
  pairs: CardPair[];
  cardColour: SimpleColour;
}

export interface Move {
  firstPosition: Position;
  secondPosition: Position;
}

export interface FlippedCard {
  position: Position;
  cardIndex: number;
}

export interface FlippedFirstCard {
  firstCard: FlippedCard
}

export interface FlippedSecondCard {
  secondCard: FlippedCard
  isMatch: boolean;
}

export interface CompletedMove extends FlippedFirstCard, FlippedSecondCard { }

const playerMessages = {
  'array.min': 'Must select at least two players.'
};

const cardPairMessages = {
  'array.min': 'Must select at least two pairs of cards.'
};

export const pairsBeginInfoSchema =
  object.keys({
    players: array.items(userIdSchema).min(2).required().messages(playerMessages),
    pairs: array.items(cardPairSchema).min(2).required().messages(cardPairMessages),
    cardColour: colourSchema.required()
  });

export const validatePairsBeginInfo = (v: unknown): Option<BeginInfo> =>
  validOrNone(pairsBeginInfoSchema, v);

export const applyFirstFlip = (player: Player,
                               board: Board,
                               move: FlippedFirstCard): void => {
  const { firstCard } = move;
  board[firstCard.position] = { cardIndex: firstCard.cardIndex, player: null }
}

export const applySecondFlip = (player: Player,
                                board: Board,
                                move: CompletedMove): void => {
  const { firstCard, secondCard, isMatch } = move;
  const newPlayer = isMatch ? player : null;
  board[firstCard.position] = { cardIndex: firstCard.cardIndex, player: newPlayer }
  board[secondCard.position] = { cardIndex: secondCard.cardIndex, player: newPlayer }
}

const isFlipped = (item: BoardItem) => item.player != null;

export const canFlipFirst = (board: Board, position: Position): FlippedFirstCard | null => {
  const firstItem = board[position];
  if (firstItem != null && !isFlipped(firstItem)) {
    return { firstCard: { cardIndex: firstItem.cardIndex, position } }
  } else {
    return null;
  }
}

export const canFlipSecond = (board: Board,
                              firstFlip: FlippedFirstCard,
                              secondPosition: Position,
                              isMatch: (firstCardIndex: number, secondCardIndex: number) => boolean): FlippedSecondCard | null => {
  const { cardIndex: firstCardIndex, position: firstPosition } = firstFlip.firstCard;
  const secondItem = board[secondPosition];
  if (secondItem != null &&
      firstPosition != secondPosition && !isFlipped(secondItem)) {
    const secondCardIndex = secondItem.cardIndex;
    return {
      secondCard: { cardIndex: secondCardIndex, position: secondPosition },
      isMatch: isMatch(firstCardIndex, secondCardIndex)
    };
  } else {
    return null;
  }
};

export const performFirstFlip =
  (player: Player, board: Board, position: Position): FlippedFirstCard | null => {
    const firstFlip = canFlipFirst(board, position);
    if (firstFlip != null) {
      applyFirstFlip(player, board, firstFlip);
      return firstFlip;
    } else {
      return null;
    }
  };

export const performSecondFlip =
  (player: Player,
   board: Board,
   firstFlip: FlippedFirstCard,
   secondPosition: Position,
   isMatch: (firstCardIndex: number, secondCardIndex: number) => boolean): FlippedSecondCard | null => {
    const secondFlip = canFlipSecond(board, firstFlip, secondPosition, isMatch);
    if (secondFlip != null) {
      const completedMove = { ...firstFlip, ...secondFlip };
      applySecondFlip(player, board, completedMove);
      return secondFlip;
    } else {
      return null;
    }
  };
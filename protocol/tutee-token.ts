import Joi from "joi";
import { Option } from "prelude-ts";
import { validOrNone } from "./joi-ext";
import { userIdSchema } from "./user";
const { object, number } = Joi.types();

export interface WithExpiry {
  exp: number
}

export interface TuteeAuthToken {
  id: string;
  classroom: string;
}

export const tuteeAuthTokenSchema =
  object.keys({
    id: userIdSchema.required(),
    classroom: userIdSchema.required(),
    exp: number.integer().required()
  }).unknown();

export const validateTuteeAuthToken = (rawTokenPayload: unknown): Option<TuteeAuthToken & WithExpiry> =>
  validOrNone(tuteeAuthTokenSchema, rawTokenPayload);
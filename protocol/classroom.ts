import { UserInfo, userInfoSchema } from "./user";
import Joi from "joi";

const { object, array, string } = Joi.types();

export interface ClassroomInfo {
  tutor: UserInfo;
  tutees: UserInfo[];
}

export interface ClassroomIdInfo {
  classroomId: string;
}

export const classroomSchema =
  object.keys({
    tutor: userInfoSchema.required(),
    tutees: array.items(userInfoSchema).required()
  });
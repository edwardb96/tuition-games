import Joi from "joi";
import { Option } from "prelude-ts";
import { validOrNone } from "./joi-ext";
import { userIdSchema } from "./user";
const { object } = Joi.types();

export interface WebsocketAuthToken {
  id: string;
  classroom: string | null;
}

export const websocketAuthTokenSchema =
  object.keys({
    id: userIdSchema.required(),
    classroom: userIdSchema.allow(null).required()
  }).unknown();

export const validateWebsocketAuthToken = (rawTokenPayload: unknown): Option<WebsocketAuthToken> =>
  validOrNone(websocketAuthTokenSchema, rawTokenPayload);
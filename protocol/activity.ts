export interface ActivityIdInfo<IdType = string> {
  activityId: IdType;
}

export interface ActivityBeginInfoBase<IdType, InfoType> extends ActivityIdInfo<IdType> {
  info: InfoType
}
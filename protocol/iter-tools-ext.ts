import reduce from "iter-tools/methods/reduce";
import filter from "iter-tools/methods/filter";
import { compareValues, ComparisonFunction } from "./compare";

export const sum = (iterable: Iterable<number>): number =>
  reduce(0, (x, y) => x + y, iterable);

export const mapNullable = <A, B>(nullable: A | undefined | null, func: (nonNull: A) => B): B | null => {
  return nullable != null ? func(nullable) : null;
}

export const takePenultimateOr = <T, U>(whenLessThanTwo: U, iterable: Iterable<T>): T | U => {
  let penultimate: T | U = whenLessThanTwo, last: T | U = whenLessThanTwo;
  for (let item of iterable) {
    penultimate = last;
    last = item;
  }
  return penultimate;
}

export const mapMaybeDefined = <A, B>(maybeDefined: A | undefined, func: (nonNull: A) => B): B | undefined => {
  if (maybeDefined != null) {
    return func(maybeDefined);
  }
}

export const next = <T, U>(iterable: Iterator<T>, def: U): T | U => {
  const { value, done } = iterable.next();
  if (done) {
    return def;
  } else {
    return value;
  }
}

export const any = <T>(predicate: (item: T) => boolean, iterable: Iterable<T>): boolean => {
  const ifTrue: Iterator<T> = filter(predicate, iterable);
  const { done } = ifTrue.next();
  return !done;
}

function *_uniqueFromSorted<T>(sorted: Iterable<T>, compare: ComparisonFunction<T> = compareValues): Iterable<T> {
  const iterator = sorted[Symbol.iterator]();
  let maybeValue = iterator.next();
  if (!maybeValue.done) {
    let previousValue = maybeValue.value;
    yield previousValue;

    maybeValue = iterator.next();
    while (!maybeValue.done) {
      if (compare(previousValue, maybeValue.value) == -1) {
        yield maybeValue.value;
      }
      previousValue = maybeValue.value;
      maybeValue = iterator.next();
    }
  }
}

// This makes uniqueFromSorted repeatable
export const uniqueFromSorted = <T>(sorted: Iterable<T>, compare: ComparisonFunction<T>): Iterable<T> =>
  ({
    [Symbol.iterator]: () => _uniqueFromSorted(sorted, compare)[Symbol.iterator]()
  });

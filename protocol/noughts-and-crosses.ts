import { Option } from "prelude-ts";
import { userIdSchema } from "./user";
import Joi, { ref } from "joi";
import { validOrNone } from "./joi-ext";
import { colourSchema, SimpleColour } from "./colour";
const { object, string, number } = Joi.types();

export const NOUGHTS_AND_CROSSES_ACTIVITY_ID = 'noughts-and-crosses'

export type Player = 'X' | 'O';
export type Position = number;
export type Board = (Player | null)[];

export const player = string.valid('X', 'O');
export const position = number.integer().required().min(0).max(8);

export const isDraw = (board: Board) =>
  board.filter(x => x == null).length == 0;

export const nextPlayer = (currentPlayer: Player): Player => {
  switch (currentPlayer) {
    case 'X': return 'O';
    case 'O': return 'X';
  }
}

export function calculateWinner(squares: Board): Player | null {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export interface MoveInfo {
  position: Position;
}

export const withMove = (move: MoveInfo, player: Player, board: Board) => {
  if (board[move.position] == null) {
    return withMoveUnchecked(move, player, board);
  } else {
    return null;
  }
}

export const withMoveUnchecked = (move: MoveInfo, player: Player, board: Board) => {
  const newBoard = board.slice();
  newBoard[move.position] = player;
  return newBoard;
}

export interface BeginInfo {
  xPlayer: string;
  oPlayer: string;
  firstPlayer: Player;
  boardColour: SimpleColour;
}

const playerMessages = (symbol: string) =>
  ({
    'string.email': `Must specify a valid email for ${symbol} player`,
    'string.empty': `Must choose a value for ${symbol} player`,
    'any.invalid': `O player cannot be the same as X player`
  });

export const noughtsAndCrossesBeginInfoSchema =
  object.keys({
    xPlayer: userIdSchema.required().messages(playerMessages('X')),
    oPlayer: userIdSchema.disallow(ref('xPlayer'))
      .required().messages(playerMessages('O')),
    firstPlayer: player.required(),
    boardColour: colourSchema.required()
  });

export const validateNoughtsAndCrossesBeginInfo = (v: unknown): Option<BeginInfo> =>
  validOrNone(noughtsAndCrossesBeginInfoSchema, v);

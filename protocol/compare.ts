export type ComparisonFunction<T> = (a: T, b: T) => number;

export function compareWithKey<T, K>(a: T, b: T, key: (item: T) => K): number {
  return compareValues(key(a), key(b));
}

export function compareValues<T>(a: T, b: T): number {
  if (a > b) {
    return 1;
  } else if (a < b) {
    return -1;
  } else {
    return 0;
  }
}
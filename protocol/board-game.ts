import { ImageId, imageIdSchema } from "./image";
import Joi from "joi";
import { userIdSchema } from "./user";
import { Option } from "prelude-ts";
import { validOrNone } from "./joi-ext";
import { colourSchema, SimpleColour } from "./colour";
const { string, number, object, array } = Joi.types();

export const BOARD_GAME_ACTIVITY_ID = 'board-game';

export type Player = string;

export interface Position {
  x: number;
  y: number;
}

export interface PositionInfo {
  position: Position;
}

export interface PlayerInfo {
  player: Player;
}

export interface PieceMovedInfo extends PositionInfo, PlayerInfo { }

export interface DiceRollMetadata {
  when: Date;
}

export interface DiceRoll extends PlayerInfo {
  value: number;
}


export type Counters = Record<Player, Counter>;

export interface BeginInfo {
  counters: Counters;
  board: ImageId;
  counterSize: number;
  diceColour: SimpleColour;
  title: string;
}

export enum CounterIcon {
  Puck = 'puck',
  Snail = "snail",
  Tortoise = "tortoise",
  Boot = 'boot',
  Cat = 'cat',
  Tank = 'tank',
  Racecar = 'racecar',
  Jet = 'jet',
}

const counterIcons : CounterIcon[] =
  Object.keys(CounterIcon).map(k =>
    CounterIcon[k as keyof typeof CounterIcon]);

export const counterIconSchema = string.valid(...counterIcons);

export const coordinateSchema = number.min(0);

export const positionSchema = object.keys({
  x: coordinateSchema.required(),
  y: coordinateSchema.required()
});

export interface Counter {
  icon: CounterIcon;
  colour: SimpleColour;
  position: Position;
};

export const counterSchema = object.keys({
  position: positionSchema.required(),
  colour: colourSchema.required(),
  icon: counterIconSchema.required(),
})

export const countersSchema =
  object.pattern(userIdSchema, counterSchema);

export type BoardId = string;
export const boardIdSchema = string.pattern(/^([-_a-zA-Z0-9]{1,1000})$/);
export const validateBoardId = (v: unknown): Option<BoardId> =>
  validOrNone(boardIdSchema, v);

export type BoardCategory = string;

export const boardCategorySchema = string.trim().min(1).max(100);
export const boardCategoriesSchema = array.items(boardCategorySchema);

export interface NewBoard {
  title: string;
  image: string;
  counterSize: number;
  categories: BoardCategory[];
}

export type UpdatedBoard = NewBoard;

export const titleSchema = string.trim().min(1).max(100);

const titleMessages = {
  'string.empty': 'You must specify a title at least 1 character long',
  'string.min': 'You must specify a title at least 1 character long',
  'string.max': 'You must specify a title at less than 101 characters long'
};

const boardMessages = {
  'string.base': 'You must select a board to use'
};

const boardImageMessages = {
  'string.base': 'You must select a board image to use',
  'string.empty': 'You must select a board image to use'
};

export const counterSizeSchema = number.min(0).max(0.5).required();

export const editedBoardSchema =
  object.keys({
    title: titleSchema.required().messages(titleMessages),
    image: imageIdSchema.required().messages(boardImageMessages),
    counterSize: counterSizeSchema.required(),
    categories: boardCategoriesSchema.required()
  });

export const validateEditedBoard = (v: unknown): Option<UpdatedBoard> =>
  validOrNone(editedBoardSchema, v);

export interface Board extends NewBoard {
  id: BoardId;
}

export const boardSchema =
  object.keys({
    id: boardIdSchema.required(),
    title: titleSchema.required().messages(titleMessages),
    image: imageIdSchema.required().messages(boardMessages),
    counterSize: number.min(0).max(1).required(),
    categories: boardCategoriesSchema.required()
  });

export const boardGameBeginInfoSchema =
  object.keys({
    counters: countersSchema.required(),
    counterSize: counterSizeSchema.required(),
    diceColour: colourSchema.required(),
    board: imageIdSchema.required().messages(boardMessages),
    title: titleSchema.required().messages(titleMessages)
  });

export const validateBoardGameBeginInfo = (v: unknown): Option<BeginInfo> =>
  validOrNone(boardGameBeginInfoSchema, v);
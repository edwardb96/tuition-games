import Joi from "joi";
import { validOrNone } from "./joi-ext";
import { Option } from "prelude-ts";
const { any, object, string } = Joi.types();

export interface ServerMessage<ChannelTag, Payload> {
  channel: ChannelTag,
  payload: Payload
}

export interface ClientMessage<ChannelTag, Payload> {
  channel: ChannelTag,
  payload: Payload
}

export interface Kinded<Kind> {
  kind: Kind
}

export const kind = (x: string) => string.valid(x).required();

export const validateServerMessageShell = (v: unknown) : Option<ServerMessage<string, any>>  => {
  const schema = object.keys({
    channel: string.min(1).trim().pattern(/^[-a-z0-9\/]+$/).required(),
    payload: any.allow(null).required()
  })

  return validOrNone(schema, v);
}

export const validateClientMessageShell = (v : any) : Option<ClientMessage<string, any>> => {
  const schema = object.keys({
    channel: string.min(1).trim().pattern(/^[-a-z0-9\/]+$/).required(),
    payload: any.allow(null).required()
  });

  return validOrNone(schema, v);
}
export interface TokenCredentials {
  privateKey: string;
  publicKey: string;
}

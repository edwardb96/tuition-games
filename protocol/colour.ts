import Joi from "joi";
const { string } = Joi.types();
//export enum CardColour {
//  White = '#FFFFFF',
//  Red = '#DB2828',
//  Orange = '#F2711C',
//  Yellow = '#FBBD08',
//  Green = '#21BA45',
//  Blue = '#2185D0',
//  Violet = '#6435C9',
//  Pink = '#E03997'
//}

const simpleColours: Record<string, string> = {
  white: '#FFFFFF',
  red: '#DB2828',
  orange: '#F2711C',
  yellow: '#FBBD08',
  green: '#21BA45',
  blue: '#2185D0',
  violet: '#6435C9',
  pink: '#E03997'
};

const simpleDarkenedColours: Record<string, string> = {
  white: '#000000',
  red: '#711313',
  orange: '#873A08',
  yellow: '#816102',
  green: '#0E4E1D',
  blue: '#103F62',
  violet: '#321A64',
  pink: '#851554'
}

export const asHexColour = (colour: SimpleColour): string =>
  simpleColours[colour];

export const asDarkenedHexColour = (colour: SimpleColour): string =>
  simpleDarkenedColours[colour];

export const asStrongInverseHexColour = (colour: SimpleColour): string =>
  colour == SimpleColour.White ? '#000000' : '#FFFFFF'

export const simpleColoursInverted: Record<string, string> = {
  white: '#FFFFFF',
  red: '#FFFFFF',
  orange: '#FFFFFF',
  yellow: '#FBBD08',
  green: '#21BA45',
  blue: '#2185D0',
  violet: '#6435C9',
  pink: '#E03997'
};

export enum SimpleColour {
  White = 'white',
  Red = 'red',
  Orange = 'orange',
  Yellow = 'yellow',
  Green = 'green',
  Blue = 'blue',
  Violet = 'violet',
  Pink = 'pink'
}

const colours : SimpleColour[] = Object.keys(SimpleColour).map(k => SimpleColour[k as keyof typeof SimpleColour]);
export const colourSchema = string.valid(...colours);
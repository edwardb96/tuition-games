import Joi from "joi";
const { object, any, string, number } = Joi.types();
import { Option } from "prelude-ts";
import range from "iter-tools/methods/range";
import { validOrNone } from "./joi-ext";
import { colourSchema, SimpleColour } from "./colour";

export { User, Role }

enum Role {
  Tutee,
  Tutor,
  Admin
}

export interface UserInfo {
  id: string;
  displayName: string;
  favouriteColour: SimpleColour;
  role: Role;
}

export interface PrivateUserInfo {
  email: string | null;
}

export interface UserIdInfo {
  userId: string;
}

export const emailSchema = string.email({ tlds: { allow: false } });
export const userIdSchema = string.pattern(/^([-a-z0-9]+)$/);
export const roleSchema = number.integer().min(Role.Tutee).max(Role.Admin)
export const displayNameSchema = string.trim().min(2).max(80);

export const userInfoSchema =
  object.keys({
    id: userIdSchema.required(),
    displayName: displayNameSchema.required(),
    favouriteColour: colourSchema.required(),
    role: roleSchema.required()
  });

export const fullUserInfoSchema =
  object.keys({
    id: userIdSchema.required(),
    email: emailSchema.allow(null).required(),
    displayName: displayNameSchema.required(),
    favouriteColour: colourSchema.required(),
    role: roleSchema.required()
  });

export interface WithTuteeTokenInfo {
  readonly tokenClassroom: string;
}

export interface WithOptionalTokenInfo {
  readonly tokenClassroom: string | null;
}

class User {
  readonly id: string;
  readonly displayName: string;
  readonly favouriteColour: SimpleColour;
  readonly role: Role;
  readonly email: string | null;
  readonly tokenClassroom: string | null;

  constructor(info: UserInfo, email: string | null = null, tokenClassroom: string | null = null) {
    const { id, displayName, favouriteColour, role } = info;
    this.id = id;
    this.displayName = displayName;
    this.favouriteColour = favouriteColour;
    this.role = role;
    this.email = email;
    this.tokenClassroom = tokenClassroom;
  }

  static fromUserInfo(info: UserInfo) {
    return new User(info);
  }

  static fromFullUserInfo(info: UserInfo & PrivateUserInfo & WithOptionalTokenInfo) {
    return new User(info, info.email, info.tokenClassroom);
  }

  isTokenUser(): this is User & WithTuteeTokenInfo {
    return this.tokenClassroom != null;
  }

  toInfo(): UserInfo {
    return {
      id: this.id,
      displayName: this.displayName,
      favouriteColour: this.favouriteColour,
      role: this.role
    };
  }

  toFullInfo(): UserInfo & PrivateUserInfo {
    return { ...this.toInfo(), email: this.email }
  }

  toFullInfoWithToken(): UserInfo & PrivateUserInfo & WithOptionalTokenInfo {
    return { ...this.toInfo(), email: this.email, tokenClassroom: this.tokenClassroom }
  }

  canCreateNewUsers(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canStartClass(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canShowClasses(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canCreateTuteeTokens(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canJoinClassroomUninvited(classroomId: string): boolean {
    return this.isAdminOrTutorAndNotToken() || this.tokenClassroom == classroomId;
  }

  canManageCardPairs(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canGetCardPairs(): boolean {
    return this.canManageCardPairs();
  }

  canAddCardPairs(): boolean {
    return this.canManageCardPairs();
  }

  canRemoveCardPairs(): boolean {
    return this.canManageCardPairs();
  }

  canManageUsers() : boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canGetUsers(): boolean {
    return this.canManageUsers();
  }

  canPostUsers(): boolean {
    return this.canManageUsers();
  }

  canPutUsers(): boolean {
    return this.canManageUsers();
  }

  canDeleteUsers(): boolean {
    return this.canManageUsers();
  }

  canGetImages(): boolean {
    return true;
  }

  canManageImages(): boolean {
    return this.isAdminOrTutorAndNotToken();
  }

  canDeleteImages(): boolean {
    return this.canManageImages();
  }

  canCreateImages(): boolean {
    return this.canManageImages();
  }

  canGetImageMetadata(): boolean {
    return this.canManageImages();
  }

  canManageBoards() {
    return this.isAdminOrTutorAndNotToken();
  }

  canAddBoards(): boolean {
    return this.canManageBoards();
  }

  canGetBoards(): boolean {
    return this.canManageBoards();
  }

  canRemoveBoards(): boolean {
    return this.canManageBoards();
  }

  private isAdminOrTutorAndNotToken() {
    if (!this.isTokenUser()) {
      switch (this.role) {
        case Role.Admin: return true;
        case Role.Tutor: return true;
        default: return false;
      }
    } else {
      return false;
    }
  }
}

export const rolesEqualToOrBelow = (role: Role) => [...range(Role.Tutee, role), role];
export const roleIsEqualToOrBelow = (lhs: Role, rhs: Role) => lhs <= rhs;

const displayNameMessages = {
  'string.empty': "You must provide a name which can be used to display messages about this user",
  'string.min': "The name must be as least 2 characters long",
  'string.max': "The name length should not exceed 80 characters"
};

const emailMessages = {
  'string.base': "You must provide an email address which can be used to identify the user unless they are a tutee.",
  'string.empty': "You must provide an email address which can be used to identify the user unless they are a tutee.",
  'string.email': "This is not a valid email address"
};

export interface UpdatedUser {
  displayName: string,
  email: string | null,
  favouriteColour: SimpleColour;
  role: Role
}

export const updatedUserSchema =
  object.keys({
    displayName: displayNameSchema.required().messages(displayNameMessages),
    favouriteColour: colourSchema.required(),
    role: roleSchema.required(),
    email: emailSchema.required().messages(emailMessages)
      .when('role', { is: Joi.valid(Role.Tutee), then: Joi.allow(null) }),
  });

export interface StoredUser {
  id: string;
  displayName: string,
  email: string | null,
  favouriteColour: SimpleColour;
  role: Role
}

export interface NewUser extends StoredUser { }

const idMessages = {
  'string.empty': "You must provide a username, it should contain one or more lower-case alphanumeric or hyphen characters",
  'string.pattern.base': "The username must only contain one or more lower-case alphanumeric or hyphen characters"
}

export const newUserSchema =
  object.keys({
    id: userIdSchema.required().messages(idMessages),
    displayName: displayNameSchema.required().messages(displayNameMessages),
    email: emailSchema.required().messages(emailMessages)
      .when('role', { is: Joi.valid(Role.Tutee), then: Joi.allow(null) }),
    favouriteColour: colourSchema.required(),
    role: roleSchema.required()
  });

export const validateUserId = (v: unknown): Option<string> =>
  validOrNone(userIdSchema, v);

export const validateNewUser = (v: unknown): Option<NewUser> =>
  validOrNone(newUserSchema, v);

export const validateUpdatedUser = (v: unknown): Option<UpdatedUser> =>
  validOrNone(updatedUserSchema, v);

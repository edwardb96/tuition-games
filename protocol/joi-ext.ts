import { attempt, Schema } from "joi";
import { Option } from "prelude-ts";

export function validOrElse<V, E>(orElse: E, schema: Schema, value: unknown): V | E {
  try {
    return attempt(value, schema);
  } catch (e) {
    console.log(e);
    return orElse;
  }
}

export function validOrNull<V>(schema: Schema, value: unknown): V | null {
  return validOrElse(null, schema, value)
}

export function validOrNone<V>(schema: Schema, value: unknown): Option<V> {
  return Option.ofNullable(validOrNull(schema, value));
}

export function isValid(schema: Schema, value: unknown): boolean {
  try {
    return attempt(value, schema) != null;
  } catch (e) {
    return false;
  }
}
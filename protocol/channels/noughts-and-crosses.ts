import Joi from "joi";
import { Option } from "prelude-ts";
import { kind, Kinded } from "../message";
import { userIdSchema } from "../user";
import { Board, Player, player, Position, position, BeginInfo, MoveInfo } from "../noughts-and-crosses";
import { validOrNone } from "../joi-ext";
import { colourSchema, SimpleColour } from "../colour";
const { boolean, array, number, object, string } = Joi.types();

export const NOUGHTS_AND_CROSSES_CHANNEL = 'classroom/noughts-and-crosses';

export const playersInOrder = (info: BeginInfo): [string, Player, string, Player] => {
  const { firstPlayer, xPlayer, oPlayer } = info;
  if (firstPlayer == 'X') {
    return [xPlayer, 'X', oPlayer, 'O'];
  } else {
    return [oPlayer, 'O', xPlayer, 'X'];
  }
}

export interface PlayerBeginInfo {
  player: Player;
  nextPlayer: Player;
  opponent: string;
  boardColour: SimpleColour;
}


export interface SpectatorBeginInfo {
  board: Board;
  winner: Player | null;
  isDraw: boolean;
  nextPlayer: Player;
  players: { X: string, O: string };
  boardColour: SimpleColour;
}

export interface PlayerInfo {
  player: Player;
}

// Initialization
export interface BeginPayload extends Kinded<'begin'>, PlayerBeginInfo { }
export interface BeginSpectatePayload extends Kinded<'begin-spectate'>, SpectatorBeginInfo { }

// Player notifications
export interface MovePayload extends Kinded<'move'>, MoveInfo { }
export interface DrawMovePayload extends Kinded<'draw-move'>, MoveInfo { }
export interface LoseMovePayload extends Kinded<'lose-move'>, MoveInfo { }
export interface DrawPayload extends Kinded<'draw'> { }
export interface OpponentSurrenderedPayload extends Kinded<'opponent-surrendered'> { }
export interface WinPayload extends Kinded<'win'> { }

// Spectator notifications
export interface SurrenderedPayload extends Kinded<'surrendered'>, PlayerInfo { }

export type ServerPayload =
  BeginPayload |
  BeginSpectatePayload |
  MovePayload |
  DrawMovePayload |
  LoseMovePayload |
  DrawPayload |
  OpponentSurrenderedPayload |
  WinPayload |
  SurrenderedPayload;

export const validateClientPayload = (v: unknown): Option<ClientPayload> => {
  const schema = object.keys({
    kind: kind('move'),
    position: position.required()
  });

  return validOrNone(schema, v);
}

export const valdiateServerPayload = (v: unknown): Option<ServerPayload> => {
  const beginPayload = object.keys({
    kind: kind('begin'),
    player: player.required(),
    nextPlayer: player.required(),
    opponent: userIdSchema.required(),
    boardColour: colourSchema.required()
  });

  const beginSpectatePayload = object.keys({
    kind: kind('begin-spectate'),
    board: array.items(player.allow(null)).length(9).required(),
    nextPlayer: player.required(),
    players: object.keys({ X: userIdSchema.required(), O: userIdSchema.required() }).required(),
    isDraw: boolean.strict().required(),
    winner: player.allow(null).required(),
    boardColour: colourSchema.required()
  });

  const movePayload = object.keys({
    kind: string.pattern(/^(draw-|lose-)?move$/).required(),
    position: position.required()
  });

  const drawPayload = object.keys({
    kind: kind('draw')
  })

  const winPayload = object.keys({
    kind: kind('win')
  })

  const opponentSurrenderPayload = object.keys({
    kind: kind('opponent-surrendered')
  });

  const schema = Joi.alternatives(
    beginPayload,
    beginSpectatePayload,
    movePayload,
    drawPayload,
    winPayload,
    opponentSurrenderPayload)

  return validOrNone(schema, v);
}

export interface ClientPayload extends Kinded<'move'> {
  position: Position;
}

export interface BoardState {
  board: Board;
  userPlayer: (string | null);
}


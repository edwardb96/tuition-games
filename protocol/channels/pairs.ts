import Joi, { ref } from "joi";
import { Option } from "prelude-ts";
import { validOrNone } from "../joi-ext";
import { kind, Kinded } from "../message";
import { Card, Scoreboard, FlippedSecondCard, Player, Board, Position, FlippedFirstCard, cardSchema } from "../pairs";
import { SimpleColour, colourSchema } from "../colour";
import { userIdSchema } from "../user";
const { object, array, number, string, boolean } = Joi.types();

export const PAIRS_CHANNEL = 'classroom/pairs';

export type ServerPayload =
  BeginPayload
  | BeginSpectatePayload
  | FlippedFirstCardPayload
  | FlippedSecondCardPayload
  | FlippedFinalCardPayload
  | SurrenderedPayload;

export type ClientPayload =
  FlipFirstCardPayload |
  FlipSecondCardPayload;

export interface PlayerInfo {
  player: Player;
}

// Initialization
export interface BeginPayload extends Kinded<'begin'>, PlayerBeginInfo { }
export interface BeginSpectatePayload extends Kinded<'begin-spectate'>, SpectatorBeginInfo { }

export interface ScoreboardInfo {
  scoreboard: Scoreboard;
}

export interface FlippedFinalCardInfo extends ScoreboardInfo, FlippedSecondCard { }

// Move notifications
export interface FlippedFirstCardPayload extends Kinded<'flipped-first'>, FlippedFirstCard { }
export interface FlippedSecondCardPayload extends Kinded<'flipped-second'>, FlippedSecondCard { }
export interface FlippedFinalCardPayload extends Kinded<'flipped-final'>, FlippedFinalCardInfo {}

export interface SurrenderedPayload extends Kinded<'surrendered'>, PlayerInfo {
  scoreboard: Scoreboard | null;
}

export interface PositionInfo {
  position: Position;
}

export interface FlipFirstCardPayload extends Kinded<'flip-first'>, PositionInfo { }
export interface FlipSecondCardPayload extends Kinded<'flip-second'>, PositionInfo { }

export const validateClientPayload = (v: unknown): Option<ClientPayload> => {
  const schema = object.keys({
    kind: string.pattern(/^flip-(first|second)$/).required(),
    position: position.required(),
  });

  return validOrNone(schema, v);
}

export interface PlayerBeginInfo {
  players: string[];
  deck: Card[];
  userPlayer: number;
  cardColour: SimpleColour;
}

export interface FinalSurrender {
  player: string;
  winner: string;
}

export interface SpectatorBeginInfo {
  deck: Card[];
  board: Board;
  players: string[];
  currentPlayerIndex: number | null;
  scoreboard: Scoreboard | null;
  finalSurrender: FinalSurrender | null;
  cardColour: string;
}

const cardItemSchema = (deckSizeRef: Joi.Reference) => object.keys({
  player: userIdSchema.allow(null).required(),
  cardIndex: number.integer().min(0).less(deckSizeRef).required()
})

const position = number.integer().min(0);

export const valdiateServerPayload = (v: unknown): Option<ServerPayload> => {
  const beginPayload = object.keys({
    kind: kind('begin'),
    players: array.items(userIdSchema).min(2).required(),
    deck: array.items(cardSchema).min(2).required(),
    userPlayer: number.integer().min(0).less(ref('players.length')),
    cardColour: colourSchema.required()
  });

  const scoreboardSchema = object.pattern(userIdSchema, object.keys({
    rank: number.integer().min(0).required(),
    score: number.integer().min(0).required()
  }));

  const beginSpectatePayload = object.keys({
    kind: kind('begin-spectate'),
    deck: array.items(cardSchema).min(2).required(),
    board: array.items(cardItemSchema(ref('/deck.length')).allow(null))
                .length(ref('deck.length'))
                .required(),
    currentPlayerIndex: number.integer().min(0).less(ref('players.length')).allow(null).required(),
    players: array.items(userIdSchema).required(),
    scoreboard: scoreboardSchema.allow(null).required(),
    cardColour: colourSchema.required(),
    finalSurrender: object.keys({
      player: userIdSchema,
      winner: userIdSchema
    }).allow(null).required()
  });

  const flippedCard = object.keys({
    cardIndex: number.integer().min(0).required(),
    position: position.required()
  });

  const firstCardFlipped = object.keys({
    kind: kind('flipped-first'),
    firstCard: flippedCard.required(),
  });

  const secondCardFlipped = object.keys({
    kind: kind('flipped-second'),
    secondCard: flippedCard.required(),
    isMatch: boolean.required()
  });

  const finalCardFlipped = object.keys({
    kind: kind('flipped-final'),
    secondCard: flippedCard.required(),
    isMatch: boolean.required(),
    scoreboard: scoreboardSchema.required()
  });

  const surrenderedPayload = object.keys({
    kind: kind('surrendered'),
    player: userIdSchema.required(),
    scoreboard: scoreboardSchema.allow(null).required()
  });

  const schema = Joi.alternatives(
    beginPayload,
    beginSpectatePayload,
    firstCardFlipped,
    secondCardFlipped,
    finalCardFlipped,
    surrenderedPayload);

  return validOrNone(schema, v);
}

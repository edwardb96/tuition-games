import Joi from "joi";
import { isValid, validOrNone } from "../joi-ext";
import { kind, Kinded } from "../message";
const { object } = Joi.types();

export const PING_CHANNEL = 'ping';

export type ServerPayload = PingPayload;
export type ClientPayload = PongPayload;

export interface PingPayload extends Kinded<'ping'> { }
export interface PongPayload extends Kinded<'pong'> { }

export const validatePingClientPayload = (v: unknown): boolean => {
  const schema = object.keys({
    kind: kind('pong'),
  });
  return isValid(schema, v);
}

export const validatePingServerPayload = (v: unknown): boolean => {
  const schema = object.keys({
    kind: kind('ping'),
  });
  return isValid(schema, v);
}
import Joi from "joi";
import { Option } from "prelude-ts";
import { validOrNone } from "../joi-ext";
import { kind, Kinded } from "../message";
import { userIdSchema } from "../user";
import { countersSchema, counterSizeSchema, Counters, DiceRoll, PieceMovedInfo, PlayerInfo, PositionInfo, positionSchema, titleSchema } from "../board-game";
import { ImageId, imageIdSchema } from "../image";
import { colourSchema, SimpleColour } from "../colour";
const { object, array, number, string, boolean } = Joi.types();

export const BOARD_GAME_CHANNEL = 'classroom/board-game';

export type ServerPayload =
  BeginPayload
  | BeginSpectatePayload
  | PieceMovedPayload
  | DiceRolledPayload
  | SurrenderedPayload
  | UnlockedBoardPayload
  | LockedBoardPayload;

export type ClientPayload =
  PieceMovePayload
  | DiceRollPayload
  | LockBoardPayload
  | UnlockBoardPayload;


// initialization
export interface BeginPayload extends Kinded<'begin'>, PlayerBeginInfo { }
export interface BeginSpectatePayload extends Kinded<'begin-spectate'>, SpectatorBeginInfo { }

// requests
export interface PieceMovePayload extends Kinded<'piece-move'>, PositionInfo, Partial<PlayerInfo> { }
export interface DiceRollPayload extends Kinded<'dice-roll'> { }
export interface LockBoardPayload extends Kinded<'lock-board'> { }
export interface UnlockBoardPayload extends Kinded<'unlock-board'> { }

// notifications
export interface DiceRolledPayload extends Kinded<'dice-rolled'>, DiceRoll { }
export interface PieceMovedPayload extends Kinded<'piece-moved'>, PieceMovedInfo { }
export interface SurrenderedPayload extends Kinded<'surrendered'>, PlayerInfo {}
export interface LockedBoardPayload extends Kinded<'locked-board'> { }
export interface UnlockedBoardPayload extends Kinded<'unlocked-board'> { }

export interface PlayerBeginInfo {
  counters: Counters;
  title: string;
  counterSize: number;
  diceColour: SimpleColour;
  board: ImageId;
  userPlayer: string;
}

export interface SpectatorBeginInfo {
  counters: Counters;
  title: string;
  counterSize: number;
  diceColour: SimpleColour;
  board: ImageId;
}

export const valdiateServerPayload = (v: unknown): Option<ServerPayload> => {
  const beginPayload = object.keys({
    kind: kind('begin'),
    title: titleSchema.required(),
    board: imageIdSchema.required(),
    counters: countersSchema.required(),
    counterSize: counterSizeSchema.required(),
    diceColour: colourSchema.required(),
    userPlayer: userIdSchema.required(),
  });

  const beginSpectatePayload = object.keys({
    kind: kind('begin-spectate'),
    title: titleSchema.required(),
    board: imageIdSchema.required(),
    counters: countersSchema.required(),
    counterSize: counterSizeSchema.required(),
    diceColour: colourSchema.required(),
  });

  const pieceMovedPayload = object.keys({
    kind: kind('piece-moved'),
    player: userIdSchema.required(),
    position: positionSchema.required()
  });

  const diceRolledPayload = object.keys({
    kind: kind('dice-rolled'),
    player: userIdSchema.required(),
    value: number.integer().min(1).max(6).required()
  });

  const surrenderedPayload = object.keys({
    kind: kind('surrendered'),
    player: userIdSchema.required(),
  });

  const lockedBoardPayload = object.keys({
    kind: kind('locked-board'),
  });

  const unlockedBoardPayload = object.keys({
    kind: kind('unlocked-board'),
  });

  const schema = Joi.alternatives(
    beginPayload,
    beginSpectatePayload,
    pieceMovedPayload,
    diceRolledPayload,
    surrenderedPayload,
    lockedBoardPayload,
    unlockedBoardPayload);

  return validOrNone(schema, v);
}

export const validateClientPayload = (v: unknown): Option<ClientPayload> => {
  const diceRollPayload = object.keys({
    kind: kind('dice-roll')
  });

  const pieceMovePayload = object.keys({
    kind: kind('piece-move'),
    position: positionSchema.required(),
    player: userIdSchema
  });

  const lockBoardPayload = object.keys({
    kind: kind('lock-board'),
  });

  const unlockBoardPayload = object.keys({
    kind: kind('unlock-board'),
  });

  const schema = Joi.alternatives(
    diceRollPayload,
    pieceMovePayload,
    lockBoardPayload,
    unlockBoardPayload);

  return validOrNone(schema, v);
}


import Joi from "joi";
import { Option } from "prelude-ts";
import { ClassroomIdInfo, ClassroomInfo, classroomSchema } from "../classroom";
import { validOrNone } from "../joi-ext";
import { kind, Kinded } from "../message";
import { UserIdInfo, userIdSchema, UserInfo, userInfoSchema } from "../user";

const { array, object, string } = Joi.types();

export const LOBBY_CHANNEL = 'lobby';

export interface TutorWelcomeToLobbyInfo {
  usersWaiting: UserInfo[];
  classrooms: ClassroomInfo[];
}

export interface TuteeWelcomeToLobbyInfo { }

export interface UserInfoAttachment {
  user: UserInfo
}

export interface InvitationInfo extends ClassroomIdInfo {
  tutor: UserInfo;
}

export interface AdmitTuteePayload extends Kinded<'admit-tutee'>, UserIdInfo { }
export interface EnterClassroomPayload extends Kinded<'enter-classroom'>, ClassroomIdInfo { }
export interface NoSuchClassroomPayload extends Kinded<'no-such-classroom'> { }
export interface EnterLobbyPayload extends Kinded<'enter-lobby'> { }

export type LobbyClientPayload =
  AdmitTuteePayload | EnterLobbyPayload | EnterClassroomPayload;

export interface WelcomeTuteeToLobbyPayload extends Kinded<'welcome-tutee'>, TuteeWelcomeToLobbyInfo { }
export interface WelcomeTutorToLobbyPayload extends Kinded<'welcome-tutor'>, TutorWelcomeToLobbyInfo { }
export interface EnteredLobbyPayload extends Kinded<'entered-lobby'> { }

export interface UserJoinedLobbyPayload extends Kinded<'user-joined-lobby'>, UserInfoAttachment { }
export interface UserLeftLobbyPayload extends Kinded<'user-left-lobby'>, UserIdInfo { }

export interface UserJoinedClassPayload extends Kinded<'user-joined-class'>, ClassroomIdInfo, UserIdInfo { }
export interface UserLeftClassPayload extends Kinded<'user-left-class'>, ClassroomIdInfo, UserIdInfo { }

export interface ClassroomActivePayload extends Kinded<'classroom-active'>, ClassroomInfo { }
export interface ClassroomInactivePayload extends Kinded<'classroom-inactive'>, ClassroomIdInfo { }
export interface AdmittedToClassPayload extends Kinded<'invited-to-class'>, InvitationInfo { }

export type LobbyServerPayload =
  WelcomeTutorToLobbyPayload
  | WelcomeTuteeToLobbyPayload
  | EnteredLobbyPayload
  | UserJoinedLobbyPayload
  | UserLeftLobbyPayload
  | UserJoinedClassPayload
  | UserLeftClassPayload
  | ClassroomActivePayload
  | ClassroomInactivePayload
  | AdmittedToClassPayload
  | NoSuchClassroomPayload;

export const validateLobbyClientPayload = (v: unknown): Option<LobbyClientPayload> => {
  const joinClassroomPayload = object.keys({
    kind: kind('enter-classroom'),
    classroomId: userIdSchema.required()
  });

  const joinLobbyPayload = object.keys({
    kind: kind('enter-lobby'),
  });

  const admitTuteePayload = object.keys({
    kind: kind('admit-tutee'),
    userId: userIdSchema.required()
  });

  const schema = Joi.alternatives(joinClassroomPayload, joinLobbyPayload, admitTuteePayload);
  return validOrNone(schema, v);
}

export const valdiateLobbyServerPayload = (v: unknown): Option<LobbyServerPayload> => {
  const welcomeTutorPayload = object.keys({
    kind: kind('welcome-tutor'),
    usersWaiting: array.items(userInfoSchema).required(),
    classrooms: array.items(classroomSchema).required()
  });

  const welcomeTuteePayload = object.keys({
    kind: kind('welcome-tutee'),
  });

  const enteredLobby = object.keys({
    kind: kind('entered-lobby'),
  });

  const userJoinedClassPayload = object.keys({
    kind: kind('user-joined-class'),
    userId: userIdSchema.required(),
    classroomId: userIdSchema.required()
  });

  const userLeftClassPayload = object.keys({
    kind: kind('user-left-class'),
    userId: userIdSchema.required(),
    classroomId: userIdSchema.required()
  })

  const userJoinedLobbyPayload = object.keys({
    kind: kind('user-joined-lobby'),
    user: userInfoSchema.required()
  });

  const userLeftLobbyPayload = object.keys({
    kind: kind('user-left-lobby'),
    userId: userIdSchema.required()
  })

  const classroomActivePayload = object.keys({
    kind: kind('classroom-active'),
    tutor: userInfoSchema.required(),
    tutees: array.items(userInfoSchema).required()
  });

  const classroomInactivePayload = object.keys({
    kind: kind('classroom-inactive'),
    classroomId: userIdSchema.required(),
  });

  const invitedToClassPayload = object.keys({
    kind: kind('invited-to-class'),
    tutor: userInfoSchema.required(),
    classroomId: userIdSchema.required()
  });

  const noSuchClassroomPayload = object.keys({
    kind: kind('no-such-classroom')
  });

  const schema = Joi.alternatives(
    welcomeTuteePayload,
    welcomeTutorPayload,
    enteredLobby,
    invitedToClassPayload,
    userJoinedClassPayload,
    noSuchClassroomPayload,
    userLeftClassPayload,
    userJoinedLobbyPayload,
    userLeftLobbyPayload,
    classroomActivePayload,
    classroomInactivePayload
  );

  return validOrNone(schema, v);
}


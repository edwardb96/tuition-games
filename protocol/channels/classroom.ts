import Joi, { date } from "joi";
import { Option } from "prelude-ts";
import { kind, Kinded } from "../message";
import { roleSchema, UserIdInfo, userIdSchema, UserInfo, userInfoSchema } from "../user";
import { ActivityBeginInfoBase, ActivityIdInfo } from "../activity";
import { NOUGHTS_AND_CROSSES_ACTIVITY_ID, BeginInfo as NoughtsAndCrossesBeginInfo } from "../noughts-and-crosses";
import { PAIRS_ACTIVITY_ID, BeginInfo as PairsBeginInfo, cardCategorySchema } from "../pairs";
import { BOARD_GAME_ACTIVITY_ID, BeginInfo as BoardGameBeginInfo } from "../board-game";
import { validOrNone } from "../joi-ext";
const { any, string, object, array } = Joi.types();

export const CLASSROOM_CHANNEL = 'classroom';

export type ActivityId =
  typeof NOUGHTS_AND_CROSSES_ACTIVITY_ID |
  typeof PAIRS_ACTIVITY_ID |
  typeof BOARD_GAME_ACTIVITY_ID;

export type ActivityBeginInfo =
  | ActivityBeginInfoBase<typeof NOUGHTS_AND_CROSSES_ACTIVITY_ID, NoughtsAndCrossesBeginInfo>
  | ActivityBeginInfoBase<typeof PAIRS_ACTIVITY_ID, PairsBeginInfo>
  | ActivityBeginInfoBase<typeof BOARD_GAME_ACTIVITY_ID, BoardGameBeginInfo>
  | NullActivityBeginInfo;

export interface NullActivityBeginInfo extends ActivityIdInfo<null> { };

export interface ChatMessageInfo {
  chatMessage: string;
}

export interface RecievedChatMessageInfo extends ChatMessageInfo {
  senderId: string;
  recievedAt: Date;
}

export interface TutorWelcomeToClassroomInfo extends ActivityIdInfo<null | ActivityId> {
  tutor: UserInfo;
  tutees: UserInfo[];
}

export interface TuteeWelcomeToClassroomInfo extends ActivityIdInfo<null | ActivityId> {
  tutor: UserInfo;
  tutees: UserInfo[];
}

export interface ClassEndedPayload extends Kinded<'class-ended'> { }
export interface TuteeAddedPayload extends Kinded<'tutee-added'>, UserInfo { }
export interface TuteeRemovedPayload extends Kinded<'tutee-removed'>, UserIdInfo { }
export interface ActivityChangedPayload extends Kinded<'activity-changed'>, ActivityIdInfo<ActivityId | null> { }
export interface ReceiveChatMessagePayload extends Kinded<'receive-chat'>, RecievedChatMessageInfo { }

export interface SendChatMessagePayload extends Kinded<'send-chat'>, ChatMessageInfo { }
type ChangeActivityPayload = Kinded<'change-activity'> & ActivityBeginInfo;

export interface TuteeEnteredClassroomPayload extends Kinded<'tutee-entered-classroom'>, TuteeWelcomeToClassroomInfo { }
export interface TutorEnteredClassroomPayload extends Kinded<'tutor-entered-classroom'>, TutorWelcomeToClassroomInfo { }

export type ClassroomClientPayload =
  SendChatMessagePayload
  | ChangeActivityPayload;

export type ClassroomServerPayload =
  ClassEndedPayload
  | TuteeAddedPayload
  | TuteeRemovedPayload
  | ActivityChangedPayload
  | ReceiveChatMessagePayload
  | TuteeEnteredClassroomPayload
  | TutorEnteredClassroomPayload

const activityIdSchema =
  string.valid(null,
    NOUGHTS_AND_CROSSES_ACTIVITY_ID,
    PAIRS_ACTIVITY_ID,
    BOARD_GAME_ACTIVITY_ID);

const chatMessageSchema = string.trim().min(1).max(10000);

export const validateClassroomClientPayload = (v: unknown): Option<ClassroomClientPayload> => {
  const sendChatPayload = object.keys({
    kind: kind('send-chat'),
    chatMessage: chatMessageSchema.required()
  });

  const changeActivityPayload = object.keys({
    kind: kind('change-activity'),
    activityId: activityIdSchema.required(),
    info: any.default(null)
  })

  const schema =
    Joi.alternatives(
      sendChatPayload,
      changeActivityPayload);

  return validOrNone(schema, v);
}

export const valdiateClassroomServerPayload = (v: unknown): Option<ClassroomServerPayload> => {
  const tutorEnteredClassroom = object.keys({
    kind: kind('tutor-entered-classroom'),
    tutor: userInfoSchema.required(),
    tutees: array.items(userInfoSchema).required(),
    activityId: activityIdSchema.required()
  });

  const tuteeEnteredClassroom = object.keys({
    kind: kind('tutee-entered-classroom'),
    tutor: userInfoSchema.required(),
    tutees: array.items(userInfoSchema).required(),
    activityId: activityIdSchema.required()
  });

  const classEndedPayload = object.keys({
    kind: kind('class-ended'),
  });

  const tuteeAddedPayload = object.keys({
    kind: kind('tutee-added'),
    id: userIdSchema.required(),
    displayName: string.min(2).max(80).required(),
    favouriteColour: cardCategorySchema.required(),
    role: roleSchema.required()
  });

  const tuteeRemovedPayload = object.keys({
    kind: kind('tutee-removed'),
    userId: userIdSchema.required(),
  });

  const activityChangedPayload = object.keys({
    kind: kind('activity-changed'),
    activityId: activityIdSchema.required()
  });

  const receiveChatMessagePayload = object.keys({
    kind: kind('receive-chat'),
    senderId: userIdSchema.required(),
    recievedAt: date(),
    chatMessage: chatMessageSchema.required()
  });


  const schema =
    Joi.alternatives(
      tuteeEnteredClassroom,
      tutorEnteredClassroom,
      classEndedPayload,
      tuteeAddedPayload,
      tuteeRemovedPayload,
      activityChangedPayload,
      receiveChatMessagePayload
    );

  return validOrNone(schema, v) ;
}

